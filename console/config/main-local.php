<?php
return [
    'bootstrap' => [
        'gii',
        'fineuploader',
    ],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'fineuploader' => 'awesome\fineuploader\Module',
    ],
];
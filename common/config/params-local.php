<?php

return [
    'media-info-command' => [
        'duration' => '/u01/khonoidungos/env/ffmpeg/ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "%file_path%"',
        'width' => '/u01/khonoidungos/env/ffmpeg/ffprobe -v error -show_entries stream=width ' .
            '-of default=noprint_wrappers=1 "%file_path%" | cut -d "=" -f 2',
        'height' => '/u01/khonoidungos/env/ffmpeg/ffprobe -v error -show_entries stream=height ' .
            '-of default=noprint_wrappers=1 "%file_path%" | cut -d "=" -f 2',
    ],
];

return [
    'testing_ffmpeg_windows_mode' => false,
    'media-info-command' => [
        'duration' => 'D:/ffmpeg-4.2.2-win64-shared/bin/ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "%file_path%"',
        'width' => 'D:/ffmpeg-4.2.2-win64-shared/bin/ffprobe -v error -show_entries stream=width ' .
            '-of default=noprint_wrappers=1 "%file_path%"',
        'height' => 'D:/ffmpeg-4.2.2-win64-shared/bin/ffprobe -v error -show_entries stream=height ' .
            '-of default=noprint_wrappers=1 "%file_path%"',
    ],
];


<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=192.168.146.252:3307;dbname=khonoidung_mozambique',
            'username' => 'khonoidung',
            'password' => 'khonoidung',
//            'dsn' => 'mysql:host=10.240.158.84:8033;dbname=csm',
//            'username' => 'csm-proxysql',
//            'password' => 'DL3#kwCD',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'cache' => [
//            'class' => 'yii\caching\FileCache',
            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => '10.30.141.120',
//                'port' => 8378,
//                'password' => 'Redis%@4Csm',
//                'database' => 12,
//            ]

            'redis' => [
                'hostname' => '192.168.146.252',
            	'port' => 9311,
            	'password' => 'Sxpm@2021',                                                                                                                                                                                                                                      
            	'database' => 0,
	    ]
        ],
        'session' => [
            'class' => 'yii\redis\Session',
			'timeout' => 7200, // 2 * 60 * 60 = 7200 // 2 hours
//            'redis' => [
//                'hostname' => '10.30.141.120',
//                'port' => 8378,
//                'password' => 'Redis%@4Csm',
//                'database' => 11,
//            ],

            'redis' => [
                'hostname' => '192.168.146.252',
            	'port' => 9311,
            	'password' => 'Sxpm@2021',                                                                                                                                                                                                                                      
            	'database' => 0,  
            ],
            'cookieParams' => [
//                'expire' => 0
                'lifetime' => 0
            ],
        ],
//        'uploadChunks' => [
//            'class' => 'yii\redis\Connection',
//            'hostname' => '10.30.141.120',
//            'password' => 'Redis%@4Csm',
//            'port' => 8378,
//            'database' => 15,
//        ],

        'uploadChunks' => [
	    'class' => 'yii\redis\Connection',
            'hostname' => '192.168.146.252',
            'port' => 9311,
            'password' => 'Sxpm@2021',                                                                                                                                                                                                                                      
            'database' => 0,  
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
        ],
    ],
];

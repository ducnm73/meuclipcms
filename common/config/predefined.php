<?php

/**
 *  define const default
 */
define('ACTIVE', 1);
define('INACTIVE', 0);
// STATUS MEDIA
define('STATUS_MEDIA_DRAFT', 0);
define('STATUS_MEDIA_APPROVED_PROPOSAL', 1);
define('STATUS_MEDIA_REJECTED', 2);
define('STATUS_MEDIA_APPROVED', 3);
define('STATUS_MEDIA_TRANSFER_PENDING', 4);
define('STATUS_MEDIA_TRANSFER_SUCCESS', 5);
define('STATUS_MEDIA_PUBLISHED', 6);
define('STATUS_MEDIA_LOCKED', 7);
define('STATUS_MEDIA_DELETED', 10);
define('STATUS_MEDIA_WAIT_CONTRACT_APPROVE', 11);
define('STATUS_MEDIA_CONTRACT_REJECT', 12);
// TYPE MEDIA
define('TYPE_MEDIA_CLIP', 1);
define('TYPE_MEDIA_FILM', 2);
define('TYPE_MEDIA_FILM_SERIES', 3);
define('TYPE_MEDIA_MUSIC', 4);
define('TYPE_MEDIA_TV_SERIES', 5);
define('TYPE_MEDIA_TV', 6);
define('TYPE_MEDIA_TRAILER', 7);
// FILE TYPE MEDIA
define('FILE_TYPE_MEDIA_LOCAL', 1);
define('FILE_TYPE_MEDIA_STORAGE', 2);
define('FILE_TYPE_MEDIA_LINK', 3);
// CONVERT STATUS MEDIA
define('CONVERT_STATUS_MEDIA_NOT_YET', 0);
define('CONVERT_STATUS_MEDIA_CONVERT_SUCCESS', 1);
define('CONVERT_STATUS_MEDIA_CONVERTING', 2);
define('CONVERT_STATUS_MEDIA_FAILED_DOWNLOAD', 3);
define('CONVERT_STATUS_MEDIA_FAILED_UPLOAD', 4);
define('CONVERT_STATUS_MEDIA_FAILED_CONVERT', 5);
// ITEM DELETED
define('TYPE_ITEM_DELETE_MEDIA', 1);
define('TYPE_ITEM_DELETE_ATTRIBUTE', 2);
define('TYPE_ITEM_DELETE_RELATIONSHIP', 3);
// ATTRIBUTE TYPE
define('TYPE_ATTRIBUTE_CATEGORY', 1);
define('TYPE_ATTRIBUTE_FILM_SERIES', 2);
define('TYPE_ATTRIBUTE_TV_SHOW', 3);
define('TYPE_ATTRIBUTE_TV_SHOW_SERIES', 4);
define('TYPE_ATTRIBUTE_FILM', 5);
define('TYPE_ATTRIBUTE_SITCOM', 6);
define('TYPE_ATTRIBUTE_COMPOSER', 7);
define('TYPE_ATTRIBUTE_SINGER', 8);
define('TYPE_ATTRIBUTE_ACTOR', 9);
define('TYPE_ATTRIBUTE_DIRECTOR', 10);
define('TYPE_ATTRIBUTE_CATEGORY_FILM', 11);
define('TYPE_ATTRIBUTE_CATEGORY_TV_SHOW', 12);
define('TYPE_ATTRIBUTE_CATEGORY_MUSIC', 13);
// MEDIA METADATA
define('META_TITLE', 'title');
define('META_AUTHOR', 'author');
define('META_COMPOSER', 'composer');
define('META_SINGER', 'singer');
define('META_ALBUM', 'album');
define('META_YEAR', 'year');
define('META_TRACK', 'track');
define('META_COMMENT', 'comment');
define('META_GENRE', 'genre');
define('META_COPYRIGHT', 'copyright');
define('META_DESCRIPTION', 'description');
define('META_ACTOR', 'actor');
define('META_DIRECTOR', 'director');
define('META_COUNTRY', 'country');
define('META_LANGUAGE', 'language');
define('META_SUBTITLE_LANGUAGE', 'subtitle-language');
define('META_CONTENT_FILTER', 'content-filter');
define('META_IMDB_RATING', 'imdb-rating');
// CRAWLER TYPE
define('TYPE_CRAWLER_EXCEL', 1);
define('TYPE_CRAWLER_WEBSITE', 2);
define('TYPE_CRAWLER_WEB_SERVICE', 3);
// CRAWLER STATE
define('STATE_CRAWLER_NOT_RUNNING', 0);
define('STATE_CRAWLER_RUNNING', 1);
define('STATE_CRAWLER_STOPPED', 2);
// CRAWLER SCHEDULE TYPE
define('SCHEDULE_TYPE_ONE_TIME', 0);
define('SCHEDULE_TYPE_CRON_TAB', 1);
// ITEM DELETE TYPE
define('TYPE_DELETE_MEDIA', 1);
define('TYPE_DELETE_ATTRIBUTE', 2);
define('TYPE_DELETE_RELATION', 3);
// ITEM DELETE STATUS
define('STATUS_DELETE_NOT_RUNNING', 0);
define('STATUS_DELETE_RUNNING', 1);
define('STATUS_DELETE_STOPPED', 2);
// MEDIA ACTIONS
define('MEDIA_ACTION_CREATE', 10);
define('MEDIA_ACTION_UPDATE', 11);
define('MEDIA_ACTION_REQUIRE_APPROVE', 20);
define('MEDIA_ACTION_APPROVE', 21);
define('MEDIA_ACTION_DECLINE', 22);
define('MEDIA_ACTION_DELETE', 23);
define('MEDIA_ACTION_APPROVE_AUTO', 30);
define('MEDIA_ACTION_DECLINE_AUTO', 31);
define('MEDIA_ACTION_NEED_TO_REVIEW', 32);
define('MEDIA_ACTION_PUSH_STORAGE_SUCCESS', 40);
define('MEDIA_ACTION_PUSH_STORAGE_FAIL', 41);
define('MEDIA_ACTION_ENCODE_SUCCESS', 50);
define('MEDIA_ACTION_ENCODE_FAIL', 51);
define('MEDIA_ACTION_PUBLISH_SUCCESS', 60);
define('MEDIA_ACTION_PUBLISH_FAIL', 61);
define('MEDIA_ACTION_LOCK', 70);
// end
define('UNIDENTIFIED', 'N/A');

define('CACHE_TIMEOUT', 1);
define('CACHE_PAGE_TIMEOUT', 1);

define('TYPE_MULTI_LANGUAGE', 1);

define('STATUS_NOT_YET_CONVERTED',0);
define('STATUS_CONVERTED_SUCCESS',1);
define('STATUS_IS_CONVERTING',2);
define('STATUS_GET_FILE_FAIL',3);
define('STATUS_PUT_FILE_FAIL',4);
define('STATUS_CONVERTED_FAIL',5);
define('STATUS_FILE_INFO_FAIL',6);
define('STATUS_SPLIT_FAIL',7);
define('STATUS_MERGE_FAIL',8);
define('STATUS_CHECK_FOR_MERGE',9);
define('STATUS_WAIT_FOR_MERGING',10);
define('STATUS_IS_MERGING',11);
define('STATUS_DUR_DEVIATION',12);



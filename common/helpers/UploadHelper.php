<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 12/13/2016
 * Time: 4:40 PM
 */

namespace common\helpers;


use Yii;

class UploadHelper
{
    public static function getFilePathToSave($filePath, $type)
    {
        if (is_file($filePath)) {
            $config = Yii::$app->params['upload'][$type]['basePath'];
            $file = explode($config, $filePath);
            return $config . $file[1];
        }
        return '';
    }

    public static function getBasePathUpload($type)
    {
        return Yii::$app->params['upload']['basePath'] . Yii::$app->params['upload'][$type]['basePath'];
    }

//    public static function getListFtpFileInPath($relativePath)
//    {
//        try {
//            $ftp = new \FtpClient\FtpClient();
//            $host = Yii::$app->params['ftp']['host'];
//            $ftp->connect($host);
//            $ftp->login(Yii::$app->params['ftp']['user_name'], Yii::$app->params['ftp']['password']);
//            $fileList = $ftp->scanDir($relativePath, false);
//            $ftp->close();
//            return $fileList;
//
//        } catch (\Exception $exception) {
//            Yii::error($exception->getMessage(), 'upload');
//            return false;
//        }
//    }
//
//
//    function php_file_tree($userName, $directory, $first_call = true, $allowExtention)
//    {
//        if (substr($directory, -1) == "/") $directory = substr($directory, 0, strlen($directory) - 1);
//
//        // Recursive function called by php_file_tree() to list directories/files
//
//        // Get and sort directories/files
//        $file = self::getListFtpFileInPath('./' . $userName . $directory);
//
//        // Filter unwanted extensions
//        if (!empty($allowExtention)) {
//            foreach ($file as $key => $this_file) {
//                if ($this_file['type'] == 'file') {
//                    $ext = substr($this_file['name'], strrpos($this_file['name'], ".") + 1);
//                    if (!in_array($ext, $allowExtention)) unset($file[$key]);
//                }
//            }
//        }
//
//        if (count($file) > 0) {
//            $php_file_tree = "<ul";
//            if ($first_call) {
//                $php_file_tree .= " class=\"php-file-tree\"";
//                $first_call = false;
//            }
//            $php_file_tree .= ">";
//            foreach ($file as $path => $this_file) {
//                if ($this_file['type'] == 'directory') {
//                    // Directory
//                    if (substr($path, 0, 9) == 'directory') {
//                        $link = substr($path, 10 + strlen($userName));
//                    }
//                    $folderName = $this_file['name'];
//                    $php_file_tree .= "<li class=\"pft-directory\"><a data-is-loaded=0 ; data-path = \"$link\">$folderName</a>";
//                    $php_file_tree .= "</li>";
//                } elseif ($this_file['type'] == 'file') {
//                    // File
//                    // Get extension (prepend 'ext-' to prevent invalid classes from extensions that begin with numbers)
//                    $ext = "ext-" . substr($this_file['name'], strrpos($this_file['name'], ".") + 1);
//                    if (substr($path, 0, 4) == 'file') {
//                        $link = substr($path, 5 + strlen($userName));
//                    }
//                    $fileName = $this_file['name'];
//                    $php_file_tree .= "<li class=\"pft-file " . strtolower($ext) . "\"><a data-path = \"$link\">$fileName</a></li>";
//                }
//
//            }
//            $php_file_tree .= "</ul>";
//        }
//        return $php_file_tree;
//    }

    public static function getFtpBaseUrl()
    {
        $user_name = Yii::$app->user->identity->username;
        if ($user_name) {
            return Yii::$app->params['upload']['baseUrl'] . Yii::$app->params['ftp']['upload_path'] . '/' . $user_name;
        }
        return '';
    }

    public static function getFtpBasePath($path)
    {
        $user_name = Yii::$app->user->identity->username;
        if ($user_name) {
            return Yii::$app->params['ftp']['upload_path'] . '/' . $user_name . $path;
        }
        return '';
    }


    // new version not using ftp................
    function php_file_tree($userName, $directory, $first_call = true, $extensions)
    {
        if (substr($directory, -1) == "/") $directory = substr($directory, 0, strlen($directory) - 1);
        $relativePath = $directory;
        if ($directory == '') {
            $directory = Yii::$app->params['upload']['basePath'] . Yii::$app->params['ftp']['upload_path'] . '/' . $userName;
        } else {
            $directory = Yii::$app->params['upload']['basePath'] . Yii::$app->params['ftp']['upload_path'] . '/' . $userName . '/' . $directory;
        }
        // Get and sort directories/files
        if (function_exists("scandir")) $file = scandir($directory); else $file = self::php4_scandir($directory);
        natcasesort($file);
        // Make directories first
        $files = $dirs = array();
        foreach ($file as $this_file) {
            if (is_dir("$directory/$this_file")) $dirs[] = $this_file; else $files[] = $this_file;
        }
        $file = array_merge($dirs, $files);

        // Filter unwanted extensions
        if (!empty($extensions)) {
            foreach (array_keys($file) as $key) {
                if (!is_dir("$directory/$file[$key]")) {
                    $ext = substr($file[$key], strrpos($file[$key], ".") + 1);
                    if (!in_array($ext, $extensions)) unset($file[$key]);
                }
            }
        }

        if (count($file) > 2) { // Use 2 instead of 0 to account for . and .. "directories"
            $php_file_tree = "<ul";
            if ($first_call) {
                $php_file_tree .= " class=\"php-file-tree\"";
            }
            $php_file_tree .= ">";
            foreach ($file as $this_file) {
                if ($this_file != "." && $this_file != "..") {
                    if (is_dir("$directory/$this_file")) {
                        // Directory
                        $link = "$relativePath/$this_file" ;
                        $folderName = htmlspecialchars($this_file);
                        $php_file_tree .= "<li class=\"pft-directory\"><a data-is-loaded=0 ; data-path = \"$link\">$folderName</a>";
                        $php_file_tree .= "</li>";
                    } else {
                        // File
                        // Get extension (prepend 'ext-' to prevent invalid classes from extensions that begin with numbers)
                        $ext = "ext-" . substr($this_file, strrpos($this_file, ".") + 1);
                        $link = "$relativePath/$this_file" ;
                        $fileName = htmlspecialchars($this_file);
                        $php_file_tree .= "<li class=\"pft-file " . strtolower($ext) . "\"><a data-path = \"$link\">$fileName</a></li>";
                    }
                }
            }
            $php_file_tree .= "</ul>";
        }
        return $php_file_tree;
    }


    // For PHP4 compatibility
    function php4_scandir($dir)
    {
        $dh = opendir($dir);
        while (false !== ($filename = readdir($dh))) {
            $files[] = $filename;
        }
        sort($files);
        return ($files);
    }


}
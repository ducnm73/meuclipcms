<?php
/**
 * Created by PhpStorm.
 * User: cuongtt11
 * Date: 7/31/2019
 * Time: 10:29 AM
 */

namespace common\helpers;

use yii\i18n\MissingTranslationEvent;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        \Yii::error("@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @",'i18n');
    }
}
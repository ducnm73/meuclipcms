<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 12/28/2016
 * Time: 11:54 PM
 */

namespace common\helpers;

use awesome\fineuploader\UploadAction;
use Yii;

class LargeUploadAction extends UploadAction
{
    public function run()
    {
        $time = 2 * 60 * 60; // 2 hours
        Yii::$app->session->timeout = $time;
        set_time_limit($time);
        ini_set('max_execution_time', $time);
        ini_set('max_input_time', $time);

        return parent::run();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 10/2/2017
 * Time: 2:07 PM
 */

namespace common\helpers;


class MediaHelper
{
    const STR_REPLACE = '%file_path%';

    static function getDuration($filePath)
    {
        // testing mode
        if(\Yii::$app->params['testing_ffmpeg_windows_mode']){
            $result = shell_exec(str_replace(self::STR_REPLACE, $filePath,
                \Yii::$app->params['media-info-command']['duration'])) ;
            preg_match('/[+-]?([0-9]*[.])?[0-9]+/', $result, $matches);
            return intval($matches[0]) ;
        }
        //todo cuongtt why floatval
        return round(floatval(shell_exec(str_replace(self::STR_REPLACE, $filePath,
            \Yii::$app->params['media-info-command']['duration']))));
    }

    static function getWidth($filePath)
    {
        // testing mode
        if(\Yii::$app->params['testing_ffmpeg_windows_mode']){
            $result = shell_exec(str_replace(self::STR_REPLACE, $filePath,
                \Yii::$app->params['media-info-command']['width'])) ;
            preg_match('/[+-]?([0-9]*[.])?[0-9]+/', $result, $matches);
            return intval($matches[0]) ;
        }
        return intval(shell_exec(str_replace(self::STR_REPLACE, $filePath,
            \Yii::$app->params['media-info-command']['width'])));
    }

    static function getHeight($filePath)
    {
        // testing mode
        if(\Yii::$app->params['testing_ffmpeg_windows_mode']){
            $result = shell_exec(str_replace(self::STR_REPLACE, $filePath,
                \Yii::$app->params['media-info-command']['height'])) ;
            preg_match('/[+-]?([0-9]*[.])?[0-9]+/', $result, $matches);
            return intval($matches[0]) ;
        }
        return intval(shell_exec(str_replace(self::STR_REPLACE, $filePath,
            \Yii::$app->params['media-info-command']['height'])));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: HoangL
 * Date: 7/30/2019
 * Time: 3:57 PM
 */

namespace common\helpers;

use Yii;

class I18nHelper
{
    public static function getI18nBackendParams($params)
    {
        if (is_array($params)) {
            array_walk_recursive($params, function (&$value) {
                $value = Yii::t('backend', $value);
            });
            return $params;
        }
        return Yii::t('backend', $params);
    }
}
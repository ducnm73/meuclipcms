<?php

namespace common\models;

use backend\models\CsmAttribute;
use Yii;

class CsmMediaBase extends \common\models\db\CsmMediaDB {

    public static function processAttr($attributes, $ids, $info, $position)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attributes[] = array_merge($csmAttribute->generateJsonObj(), [
                'info' => trim($info),
                'position' => is_numeric($position) ? (int)$position : ''
            ]);
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }

    public static function processUpdateAttr($attributes, $ids, $mediaId)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attrMedia = $csmAttribute->getAttrMedia($mediaId);
            /* @var CsmMediaAttribute $attrMedia */
            $attributes[] = array_merge($csmAttribute->generateJsonObj(), [
                'info' => $attrMedia->info,
                'position' => is_numeric($attrMedia->position) ? (int)$attrMedia->position : ''
            ]);
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }

    public static function processUpdateAttrFull($attributes, $ids, $mediaId, $info, $position)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attributes[] = array_merge($csmAttribute->generateJsonObj(), [
                'info' => trim($info),
                'position' => is_numeric($position) ? (int)$position : ''
            ]);
            $attrMedia = $csmAttribute->getAttrMedia($mediaId);
            /* @var CsmMediaAttribute $attrMedia */
            if ($attrMedia && ($attrMedia->info != $info || $attrMedia->position != $position)) {
                $attrMedia->info = trim($info);
                $attrMedia->position = is_numeric($position) ? (int)$position : '';
                $attrMedia->save();
            }
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }

    public static function processAttrParent($attributes, $ids)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attributes[] = array_merge($csmAttribute->generateJsonObj());
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }

    //file url của audio, subtitle
    public function getFileUrl($type, $originalPath)
    {
        if($originalPath){
            return Yii::$app->params['upload']['baseUrl'] . $originalPath;
        }
        return "" ;
    }


    // trả về path local để chạy ffmpeg
    public function getFullOriginalPath()
    {
        if ($this->original_path) {
            return Yii::$app->params['upload']['basePath'] . $this->original_path;
        }

        return "";
    }

    //custom peru
    public function getLocalFilePath()
    {
        if ($this->original_path) {
            return Yii::$app->params['upload']['baseUrl'] . $this->original_path;
        }

        return "";
    }


    //todo cuongtt type =2
    public function getLocalImagePath()
    {
        if ($this->image_path) {
            return Yii::$app->params['upload']['baseUrl'] . $this->image_path;
        }
        return "";
    }

    public function getLocalPosterPath()
    {
        if ($this->poster_path) {
            return Yii::$app->params['upload']['baseUrl'] . $this->poster_path;
        }
        return "";
    }

}
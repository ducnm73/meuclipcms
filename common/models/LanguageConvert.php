<?php
/**
 * Created by PhpStorm.
 * User: anhntm23
 * Date: 11/1/2018
 * Time: 10:15 AM
 */

namespace common\models;
use Yii;


class LanguageConvert
{
    public static function getLang( $field,$multilan,$lang) {
        $key = $field . '_' . $lang;
        if(isset($multilan->$key))
        {
            return $multilan->$key;
        }
        else{
            return '';
        }
    }

    public static function arrayExclude($array, Array $excludeKeys){
        foreach($excludeKeys as $key){
            $key=trim($key);
            unset($array[$key]);
        }
        return $array;
    }



    public static function convertFieldsToArray($multilangString,$arrayFieldNames=[],$lang, $content = array()){
        $arrayResponse=[];
        $multilang = json_decode($multilangString);
        foreach ($arrayFieldNames as $fieldName){
            $fieldName=trim($fieldName);
            $arrayResponse[$fieldName]=self::getLang($fieldName,$multilang,$lang);
            if(!isset($arrayResponse[$fieldName]) || $arrayResponse[$fieldName] == '') {
                $arrayResponse[$fieldName] = $content[$fieldName];
            }
        }
        return $arrayResponse;
    }

    public static function convertFieldsToObject($multilangString,$arrayFieldNames=[],$lang,$mObject){
        $multilang = json_decode($multilangString);
        foreach ($arrayFieldNames as $fieldName){
            $fieldName=trim($fieldName);

            $mObject[$fieldName]=self::getLang($fieldName,$multilang,$lang);
            //Cach 2
            //$mObject->$fieldName=self::getLang($fieldName,$multilang,$lang);
        }
        return $mObject;
    }


}
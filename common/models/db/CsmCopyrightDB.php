<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_copyright".
 *
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $start_time
 * @property string $end_time
 * @property integer $status
 * @property string $file_path
 * @property string $cp_id
 *
 * @property CsmMediaDB $id0
 * @property CsmCpDB $cp
 */
class CsmCopyrightDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_copyright';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_time', 'end_time'], 'safe'],
            [['status', 'cp_id'], 'integer'],
            [['name', 'code', 'file_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'code' => Yii::t('backend', 'Code'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'status' => Yii::t('backend', 'Status'),
            'file_path' => Yii::t('backend', 'File Path'),
            'cp_id' => Yii::t('backend', 'Cp ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(CsmMediaDB::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCp()
    {
        return $this->hasOne(CsmCpDB::className(), ['id' => 'cp_id']);
    }
}

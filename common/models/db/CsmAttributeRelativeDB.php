<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_attribute_relative".
 *
 * @property string $id
 * @property string $attribute_id
 * @property string $relative_id
 *
 * @property CsmAttributeDB $attribute
 * @property CsmAttributeDB $relative
 */
class CsmAttributeRelativeDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_attribute_relative';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'relative_id'], 'integer'],
            [['attribute_id', 'relative_id'], 'unique', 'targetAttribute' => ['attribute_id', 'relative_id'], 'message' => 'The combination of Attribute ID and Relative ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'attribute_id' => Yii::t('backend', 'Attribute ID'),
            'relative_id' => Yii::t('backend', 'Relative ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute()
    {
        return $this->hasOne(CsmAttributeDB::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelative()
    {
        return $this->hasOne(CsmAttributeDB::className(), ['id' => 'relative_id']);
    }
}

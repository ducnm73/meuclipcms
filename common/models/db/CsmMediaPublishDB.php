<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_media_publish".
 *
 * @property string $id
 * @property string $media_id
 * @property string $client_id
 * @property string $published_at
 * @property string $published_by
 * @property string $status
 *
 * @property ApiClientDB $client
 * @property CsmMediaDB $media
 */
class CsmMediaPublishDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_media_publish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'client_id', 'published_by', 'status'], 'integer'],
            [['published_at'], 'safe'],
            [['media_id', 'client_id'], 'unique', 'targetAttribute' => ['media_id', 'client_id'], 'message' => 'The combination of Media ID and Client ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'client_id' => Yii::t('backend', 'Client ID'),
            'published_at' => Yii::t('backend', 'Published At'),
            'published_by' => Yii::t('backend', 'Published By'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(ApiClientDB::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(CsmMediaDB::className(), ['id' => 'media_id']);
    }
}

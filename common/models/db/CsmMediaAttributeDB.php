<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_media_attribute".
 *
 * @property string $id
 * @property string $media_id
 * @property string $attribute_id
 * @property string $created_at
 * @property string $created_by
 * @property string $info
 * @property integer $position
 *
 * @property CsmMediaDB $media
 * @property CsmAttributeDB $attribute
 */
class CsmMediaAttributeDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_media_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'attribute_id', 'created_by', 'position'], 'integer'],
            [['created_at'], 'safe'],
            [['info'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'attribute_id' => Yii::t('backend', 'Attribute ID'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'info' => Yii::t('backend', 'Info'),
            'position' => Yii::t('backend', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(CsmMediaDB::className(), ['id' => 'media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes0()
    {
        return $this->hasOne(CsmAttributeDB::className(), ['id' => 'attribute_id']);
    }
}

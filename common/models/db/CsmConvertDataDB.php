<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_convert_data".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image_path
 * @property string $censored_info
 * @property string $meta_info
 * @property integer $need_censored
 *
 * @property CsmMediaDB[] $csmMedia
 */
class CsmConvertDataDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_convert_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['need_censored'], 'integer'],
            [['name', 'slug', 'image_path'], 'string', 'max' => 255],
            [['description', 'censored_info', 'meta_info'], 'string', 'max' => 4096]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'slug' => Yii::t('backend', 'Slug'),
            'description' => Yii::t('backend', 'Description'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'censored_info' => Yii::t('backend', 'Censored Info'),
            'meta_info' => Yii::t('backend', 'Meta Info'),
            'need_censored' => Yii::t('backend', 'Need Censored'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMedia()
    {
        return $this->hasMany(CsmMediaDB::className(), ['convert_data_id' => 'id']);
    }
}

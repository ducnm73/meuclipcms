<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_media".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $short_desc
 * @property string $description
 * @property integer $status
 * @property string $price_download
 * @property string $price_play
 * @property integer $type
 * @property integer $max_quantity
 * @property string $published_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $published_at
 * @property integer $duration
 * @property string $resolution
 * @property string $attributes
 * @property string $cp_id
 * @property string $cp_info
 * @property string $original_path
 * @property string $image_path
 * @property string $poster_path
 * @property integer $file_type
 * @property integer $convert_status
 * @property string $convert_path
 * @property integer $convert_priority
 * @property string $convert_start_time
 * @property string $convert_end_time
 * @property string $convert_data_id
 * @property string $convert_images
 * @property string $meta_info
 * @property string $censored_info
 * @property string $logo_path
 * @property integer $need_censored
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $is_crawler
 * @property string $crawler_id
 * @property string $crawler_info
 * @property string $created_by
 * @property string $updated_by
 * @property string $reviewed_by
 * @property string $published_list
 * @property string $tag
 * @property integer $need_encryption
 * @property string $resource_id
 * @property integer $drm_id
 * @property string $convert_server
 * @property string $media_info
 * @property string $copyright_id
 * @property string $copyright_info
 * @property string $last_sync_at
 * @property integer $multi_language
 * @property string $subtitle_path
 * @property string $audio_path
 * @property string $convert_audio
 * @property string $multiple_language
 *
 * @property CsmCopyrightDB $csmCopyright
 * @property CsmCpDB $cp
 * @property CsmConvertDataDB $convertData
 * @property CsmMediaAttributeDB[] $csmMediaAttributes
 */
class CsmMediaDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description', 'attributes', 'convert_path', 'media_info'], 'string'],
            [['status', 'price_download', 'price_play', 'type', 'max_quantity', 'published_by', 'duration', 'cp_id', 'file_type', 'convert_status', 'convert_priority', 'convert_data_id', 'need_censored', 'is_crawler', 'crawler_id', 'created_by', 'updated_by', 'reviewed_by', 'need_encryption', 'drm_id', 'copyright_id', 'multi_language'], 'integer'],
            [['created_at', 'updated_at', 'published_at', 'convert_start_time', 'convert_end_time', 'last_sync_at', 'multiple_language'], 'safe'],
            [['name', 'slug', 'original_path', 'image_path', 'poster_path', 'seo_title', 'tag'], 'string', 'max' => 512],
            [['description', 'short_desc', 'convert_images', 'seo_description', 'seo_keywords', 'published_list'], 'string', 'max' => 2048],
            [['resolution'], 'string', 'max' => 20],
            [['cp_info', 'censored_info', 'logo_path', 'resource_id', 'convert_server', 'copyright_info'], 'string', 'max' => 255],
            [['meta_info'], 'string', 'max' => 4096],
            [['crawler_info'], 'string', 'max' => 1024],
            [['price_download', 'price_play'],'integer', 'min' => 0],
            [['price_download', 'price_play'],'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'slug' => Yii::t('backend', 'Slug'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'description' => Yii::t('backend', 'Description'),
            'status' => Yii::t('backend', 'Status'),
            'price_download' => Yii::t('backend', 'Price Download'),
            'price_play' => Yii::t('backend', 'Price Play'),
            'type' => Yii::t('backend', 'Type'),
            'max_quantity' => Yii::t('backend', 'Max Quantity'),
            'published_by' => Yii::t('backend', 'Published By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'published_at' => Yii::t('backend', 'Published At'),
            'duration' => Yii::t('backend', 'Duration'),
            'resolution' => Yii::t('backend', 'Resolution'),
            'attributes' => Yii::t('backend', 'Attributes'),
            'cp_id' => Yii::t('backend', 'Cp ID'),
            'cp_info' => Yii::t('backend', 'Cp Info'),
            'original_path' => Yii::t('backend', 'Original Path'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'poster_path' => Yii::t('backend', 'Poster Path'),
            'file_type' => Yii::t('backend', 'File Type'),
            'convert_status' => Yii::t('backend', 'Convert Status'),
            'convert_path' => Yii::t('backend', 'Convert Path'),
            'convert_priority' => Yii::t('backend', 'Convert Priority'),
            'convert_start_time' => Yii::t('backend', 'Convert Start Time'),
            'convert_end_time' => Yii::t('backend', 'Convert End Time'),
            'convert_data_id' => Yii::t('backend', 'Convert Data ID'),
            'convert_images' => Yii::t('backend', 'Convert Images'),
            'meta_info' => Yii::t('backend', 'Meta Info'),
            'censored_info' => Yii::t('backend', 'Censored Info'),
            'logo_path' => Yii::t('backend', 'Logo Path'),
            'need_censored' => Yii::t('backend', 'Need Censored'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'is_crawler' => Yii::t('backend', 'Is Crawler'),
            'crawler_id' => Yii::t('backend', 'Crawler ID'),
            'crawler_info' => Yii::t('backend', 'Crawler Info'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'reviewed_by' => Yii::t('backend', 'Reviewed By'),
            'published_list' => Yii::t('backend', 'Published List'),
            'tag' => Yii::t('backend', 'Tag'),
            'need_encryption' => Yii::t('backend', 'Need Encryption'),
            'resource_id' => Yii::t('backend', 'Resource ID'),
            'drm_id' => Yii::t('backend', 'Drm ID'),
            'convert_server' => Yii::t('backend', 'Convert Server'),
            'media_info' => Yii::t('backend', 'Media Info'),
            'copyright_id' => Yii::t('backend', 'Copyright ID'),
            'copyright_info' => Yii::t('backend', 'Copyright Info'),
            'last_sync_at' => Yii::t('backend', 'Last Sync At'),
            'multi_language' => Yii::t('backend', 'Multi Language'),
            'subtitle_path' => Yii::t('backend', 'Subtitle Path'),
            'audio_path' => Yii::t('backend', 'Audio Path'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmCopyright()
    {
        return $this->hasOne(CsmCopyrightDB::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCp()
    {
        return $this->hasOne(CsmCpDB::className(), ['id' => 'cp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConvertData()
    {
        return $this->hasOne(CsmConvertDataDB::className(), ['id' => 'convert_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMediaAttributes()
    {
        return $this->hasMany(CsmMediaAttributeDB::className(), ['media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMediaPublishes()
    {
        return $this->hasMany(CsmMediaPublishDB::className(), ['media_id' => 'id']);
    }
}

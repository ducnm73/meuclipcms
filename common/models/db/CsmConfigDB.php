<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_config".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $key
 * @property string $value
 */
class CsmConfigDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'value'], 'string'],
            [['name', 'key'], 'string', 'max' => 255],
            [['key'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'key' => Yii::t('backend', 'Key'),
            'value' => Yii::t('backend', 'Value'),
        ];
    }
}

<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_attribute".
 *
 * @property string $id
 * @property string $name
 * @property string $std_name
 * @property string $slug
 * @property string $description
 * @property string $image_path
 * @property string $second_image
 * @property string $type
 * @property integer $is_active
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $media_list
 * @property string $meta_info
 * @property string $parent_id
 * @property string $multiple_language
 *
 * @property CsmAttributeTypeDB $type0
 * @property CsmAttributeRelativeDB[] $csmAttributeRelatives
 * @property CsmMediaAttributeDB[] $csmMediaAttributes
 */
class CsmAttributeDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_attribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description', 'media_list', 'meta_info'], 'string'],
            [['type', 'is_active', 'created_by', 'updated_by', 'parent_id'], 'integer'],
            [['created_at', 'updated_at', 'multiple_language'], 'safe'],
            [['name', 'std_name', 'slug', 'image_path', 'second_image'], 'string', 'max' => 255],
            [['slug', 'type'], 'unique', 'targetAttribute' => ['slug', 'type'], 'message' => 'The combination of Slug and Type has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'std_name' => Yii::t('backend', 'Std Name'),
            'slug' => Yii::t('backend', 'Slug'),
            'description' => Yii::t('backend', 'Description'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'second_image' => Yii::t('backend', 'Second Image'),
            'type' => Yii::t('backend', 'Type'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'media_list' => Yii::t('backend', 'Media List'),
            'meta_info' => Yii::t('backend', 'Meta Info'),
            'parent_id' => Yii::t('backend', 'Parent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(CsmAttributeTypeDB::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmAttributeRelatives()
    {
        return $this->hasMany(CsmAttributeRelativeDB::className(), ['relative_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMediaAttributes()
    {
        return $this->hasMany(CsmMediaAttributeDB::className(), ['attribute_id' => 'id']);
    }
}

<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_convert_tasks".
 *
 * @property string $id
 * @property string $media_id
 * @property integer $part_id
 * @property string $original_path
 * @property string $convert_path
 * @property string $start_time
 * @property string $end_time
 * @property integer $process_id
 * @property string $convert_server
 * @property integer $status
 * @property integer $num_part
 * @property integer $height
 * @property integer $progress
 * @property integer $type
 * @property integer $priority
 * @property string $cp_id
 */
class CsmConvertTasksDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_convert_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'part_id', 'original_path', 'num_part'], 'required'],
            [['media_id', 'part_id', 'process_id', 'status', 'num_part', 'height', 'progress', 'type', 'priority', 'cp_id'], 'integer'],
            [['convert_path'], 'string'],
            [['start_time', 'end_time'], 'safe'],
            [['original_path'], 'string', 'max' => 512],
            [['convert_server'], 'string', 'max' => 50],
            [['media_id', 'part_id'], 'unique', 'targetAttribute' => ['media_id', 'part_id'], 'message' => 'The combination of Media ID and Part ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'part_id' => Yii::t('backend', 'Part ID'),
            'original_path' => Yii::t('backend', 'Original Path'),
            'convert_path' => Yii::t('backend', 'Convert Path'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'process_id' => Yii::t('backend', 'Process ID'),
            'convert_server' => Yii::t('backend', 'Convert Server'),
            'status' => Yii::t('backend', 'Status'),
            'num_part' => Yii::t('backend', 'Num Part'),
            'height' => Yii::t('backend', 'Height'),
            'progress' => Yii::t('backend', 'Progress'),
            'type' => Yii::t('backend', 'Type'),
            'priority' => Yii::t('backend', 'Priority'),
            'cp_id' => Yii::t('backend', 'Cp ID'),
        ];
    }
}

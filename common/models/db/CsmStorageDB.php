<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_storage".
 *
 * @property integer $id
 * @property string $bucket
 * @property string $access_key
 * @property string $secret_key
 * @property string $endpoint
 * @property string $endpoint_public
 */
class CsmStorageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bucket', 'access_key', 'secret_key', 'endpoint', 'endpoint_public'], 'required'],
            [['bucket'], 'string', 'max' => 100],
            [['access_key', 'secret_key', 'endpoint', 'endpoint_public'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'bucket' => Yii::t('backend', 'Bucket'),
            'access_key' => Yii::t('backend', 'Access Key'),
            'secret_key' => Yii::t('backend', 'Secret Key'),
            'endpoint' => Yii::t('backend', 'Endpoint'),
            'endpoint_public' => Yii::t('backend', 'Endpoint Public'),
        ];
    }
}

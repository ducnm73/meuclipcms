<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_crawler_controller".
 *
 * @property string $id
 * @property string $name
 * @property integer $type
 * @property string $source
 * @property string $main_class
 * @property string $media_class
 * @property string $media_source
 * @property string $params
 *
 * @property CsmCrawlerDB[] $csmCrawlers
 */
class CsmCrawlerControllerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_crawler_controller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'source', 'main_class'], 'required'],
            [['type'], 'integer'],
            [['params'], 'string'],
            [['name', 'main_class', 'media_class', 'media_source'], 'string', 'max' => 255],
            [['source'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'type' => Yii::t('backend', 'Type'),
            'source' => Yii::t('backend', 'Source'),
            'main_class' => Yii::t('backend', 'Main Class'),
            'media_class' => Yii::t('backend', 'Media Class'),
            'media_source' => Yii::t('backend', 'Media Source'),
            'params' => Yii::t('backend', 'Params'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmCrawlers()
    {
        return $this->hasMany(CsmCrawlerDB::className(), ['controller_id' => 'id']);
    }
}

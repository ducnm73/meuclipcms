<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_profile_detail".
 *
 * @property string $id
 * @property string $profile_id
 * @property string $pro_key
 * @property string $pro_value
 *
 * @property CsmProfileDB $profile
 */
class CsmProfileDetailDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_profile_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id'], 'integer'],
            [['pro_key'], 'string', 'max' => 255],
            [['pro_value'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'profile_id' => Yii::t('backend', 'Profile ID'),
            'pro_key' => Yii::t('backend', 'Pro Key'),
            'pro_value' => Yii::t('backend', 'Pro Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(CsmProfileDB::className(), ['id' => 'profile_id']);
    }
}

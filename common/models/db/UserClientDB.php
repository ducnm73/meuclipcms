<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "user_client".
 *
 * @property string $id
 * @property string $user_id
 * @property string $client_id
 *
 * @property UserDB $user
 * @property ApiClientDB $client
 */
class UserClientDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'client_id' => Yii::t('backend', 'Client ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDB::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(ApiClientDB::className(), ['id' => 'client_id']);
    }
}

<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_cp".
 *
 * @property string $id
 * @property string $name
 * @property string $cp_code
 * @property string $description
 * @property string $contact
 * @property string $address
 * @property string $start_time
 * @property string $end_time
 * @property integer $is_active
 *
 * @property CsmCopyrightDB[] $csmCopyrights
 * @property CsmCpClientDB[] $csmCpClients
 * @property CsmMediaDB[] $csmMedia
 * @property UserDB[] $users
 */
class CsmCpDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_cp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['start_time', 'end_time'], 'safe'],
            [['is_active'], 'integer'],
            [['name', 'contact', 'address'], 'string', 'max' => 255],
            [['cp_code'], 'string', 'max' => 30],
            [['cp_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'cp_code' => Yii::t('backend', 'Cp Code'),
            'description' => Yii::t('backend', 'Description'),
            'contact' => Yii::t('backend', 'Contact'),
            'address' => Yii::t('backend', 'Address'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'is_active' => Yii::t('backend', 'Is Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmCopyrights()
    {
        return $this->hasMany(CsmCopyrightDB::className(), ['cp_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmCpClients()
    {
        return $this->hasMany(CsmCpClientDB::className(), ['cp_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMedia()
    {
        return $this->hasMany(CsmMediaDB::className(), ['cp_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(UserDB::className(), ['cp_id' => 'id']);
    }
}

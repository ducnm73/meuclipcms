<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_media_action_log".
 *
 * @property string $id
 * @property string $media_id
 * @property integer $action
 * @property string $created_at
 * @property integer $created_by
 * @property string $note
 * @property string $updated_fields
 * @property string $all_fields
 * @property string $all_old_fields
 * @property integer $watched_duration
 */
class CsmMediaActionLogDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_media_action_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'action', 'created_by', 'watched_duration'], 'integer'],
            [['created_at'], 'safe'],
            [['note', 'updated_fields', 'all_fields', 'all_old_fields'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'action' => Yii::t('backend', 'Action'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'note' => Yii::t('backend', 'Note'),
            'updated_fields' => Yii::t('backend', 'Updated Fields'),
            'all_fields' => Yii::t('backend', 'All Fields'),
            'all_old_fields' => Yii::t('backend', 'All Old Fields'),
            'watched_duration' => Yii::t('backend', 'Watched Duration'),
        ];
    }
}

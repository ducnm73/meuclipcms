<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_media_history".
 *
 * @property string $id
 * @property string $media_id
 * @property string $updated_fields
 * @property string $all_fields
 * @property string $updated_at
 * @property string $updated_by
 * @property integer $client_id
 * @property integer $is_publish
 * @property string $published_at
 * @property integer $type
 */
class CsmMediaHistoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_media_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'updated_by', 'client_id', 'is_publish', 'type'], 'integer'],
            [['updated_fields', 'all_fields'], 'string'],
            [['updated_at', 'published_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'updated_fields' => Yii::t('backend', 'Updated Fields'),
            'all_fields' => Yii::t('backend', 'All Fields'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'client_id' => Yii::t('backend', 'Client ID'),
            'is_publish' => Yii::t('backend', 'Is Publish'),
            'published_at' => Yii::t('backend', 'Published At'),
            'type' => Yii::t('backend', 'Type')
        ];
    }
}

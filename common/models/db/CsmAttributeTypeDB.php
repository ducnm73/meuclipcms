<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_attribute_type".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property integer $is_need_approved
 * @property integer $is_active
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property CsmAttributeDB[] $csmAttributes
 */
class CsmAttributeTypeDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_attribute_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['is_need_approved', 'is_active', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'slug' => Yii::t('backend', 'Slug'),
            'is_need_approved' => Yii::t('backend', 'Is Need Approved'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmAttributes()
    {
        return $this->hasMany(CsmAttributeDB::className(), ['type' => 'id']);
    }
}

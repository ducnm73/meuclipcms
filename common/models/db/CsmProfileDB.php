<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_profile".
 *
 * @property string $id
 * @property string $name
 * @property integer $is_active
 * @property integer $min_org_res
 *
 * @property CsmProfileDetailDB[] $csmProfileDetails
 */
class CsmProfileDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'min_org_res'], 'required'],
            [['is_active', 'min_org_res'], 'integer'],
            [['name'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'min_org_res' => Yii::t('backend', 'Min Org Res'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmProfileDetails()
    {
        return $this->hasMany(CsmProfileDetailDB::className(), ['profile_id' => 'id']);
    }
}

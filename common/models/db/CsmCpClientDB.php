<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_cp_client".
 *
 * @property string $id
 * @property string $cp_id
 * @property string $client_id
 *
 * @property CsmCpDB $cp
 * @property ApiClientDB $client
 */
class CsmCpClientDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_cp_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cp_id', 'client_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cp_id' => Yii::t('backend', 'Cp ID'),
            'client_id' => Yii::t('backend', 'Client ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCp()
    {
        return $this->hasOne(CsmCpDB::className(), ['id' => 'cp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(ApiClientDB::className(), ['id' => 'client_id']);
    }
}

<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "api_client".
 *
 * @property string $id
 * @property string $name
 * @property string $client_id
 * @property string $client_secret
 * @property string $permission
 * @property string $description
 *
 * @property CsmCpClientDB[] $csmCpClients
 * @property CsmMediaPublishDB[] $csmMediaPublishes
 * @property UserClientDB[] $userClients
 */
class ApiClientDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['client_id', 'client_secret'], 'string', 'max' => 32],
            [['permission'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'client_id' => Yii::t('backend', 'Client ID'),
            'client_secret' => Yii::t('backend', 'Client Secret'),
            'permission' => Yii::t('backend', 'Permission'),
            'description' => Yii::t('backend', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmCpClients()
    {
        return $this->hasMany(CsmCpClientDB::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCsmMediaPublishes()
    {
        return $this->hasMany(CsmMediaPublishDB::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClients()
    {
        return $this->hasMany(UserClientDB::className(), ['client_id' => 'id']);
    }
}

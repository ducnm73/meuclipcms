<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_crawler_history".
 *
 * @property string $id
 * @property string $crawler_id
 * @property string $info
 * @property integer $state
 * @property string $media_info
 * @property string $created_at
 * @property string $created_by
 * @property integer $error_code
 * @property string $item_id
 */
class CsmCrawlerHistoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_crawler_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crawler_id', 'state', 'created_by', 'error_code', 'item_id'], 'integer'],
            [['media_info'], 'string'],
            [['created_at'], 'safe'],
            [['info'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'crawler_id' => Yii::t('backend', 'Crawler ID'),
            'info' => Yii::t('backend', 'Info'),
            'state' => Yii::t('backend', 'State'),
            'media_info' => Yii::t('backend', 'Media Info'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'error_code' => Yii::t('backend', 'Error Code'),
            'item_id' => Yii::t('backend', 'Item ID'),
        ];
    }
}

<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_crawler".
 *
 * @property string $id
 * @property string $name
 * @property string $url
 * @property string $params
 * @property integer $type
 * @property integer $is_authenticated
 * @property integer $state
 * @property string $username
 * @property string $password
 * @property string $token
 * @property string $controller_id
 * @property string $controller_info
 * @property integer $level_crawler
 * @property string $start_time
 * @property string $end_time
 * @property integer $is_active
 * @property integer $is_need_approved
 * @property string $auto_client
 * @property string $max_items
 * @property integer $allow_null_date
 * @property string $min_date
 * @property string $max_date
 * @property string $parent_id
 * @property integer $schedule_type
 * @property string $cron_syntax
 * @property integer $status
 * @property string $crawler_time
 * @property integer $next_time
 *
 * @property CsmCrawlerControllerDB $controller
 */
class CsmCrawlerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_crawler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'controller_info'], 'required'],
            [['type', 'is_authenticated', 'state', 'controller_id', 'level_crawler', 'is_active', 'is_need_approved', 'max_items', 'allow_null_date', 'parent_id', 'schedule_type', 'status', 'next_time'], 'integer'],
            [['controller_info'], 'string'],
            [['start_time', 'end_time', 'min_date', 'max_date', 'crawler_time'], 'safe'],
            [['name', 'username', 'password', 'token'], 'string', 'max' => 255],
            [['url', 'auto_client'], 'string', 'max' => 512],
            [['params'], 'string', 'max' => 2048],
            [['cron_syntax'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'url' => Yii::t('backend', 'Url'),
            'params' => Yii::t('backend', 'Params'),
            'type' => Yii::t('backend', 'Type'),
            'is_authenticated' => Yii::t('backend', 'Is Authenticated'),
            'state' => Yii::t('backend', 'State'),
            'username' => Yii::t('backend', 'Username'),
            'password' => Yii::t('backend', 'Password'),
            'token' => Yii::t('backend', 'Token'),
            'controller_id' => Yii::t('backend', 'Controller ID'),
            'controller_info' => Yii::t('backend', 'Controller Info'),
            'level_crawler' => Yii::t('backend', 'Level Crawler'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'is_need_approved' => Yii::t('backend', 'Is Need Approved'),
            'auto_client' => Yii::t('backend', 'Auto Client'),
            'max_items' => Yii::t('backend', 'Max Items'),
            'allow_null_date' => Yii::t('backend', 'Allow Null Date'),
            'min_date' => Yii::t('backend', 'Min Date'),
            'max_date' => Yii::t('backend', 'Max Date'),
            'parent_id' => Yii::t('backend', 'Parent ID'),
            'schedule_type' => Yii::t('backend', 'Schedule Type'),
            'cron_syntax' => Yii::t('backend', 'Cron Syntax'),
            'status' => Yii::t('backend', 'Status'),
            'crawler_time' => Yii::t('backend', 'Crawler Time'),
            'next_time' => Yii::t('backend', 'Next Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getController()
    {
        return $this->hasOne(CsmCrawlerControllerDB::className(), ['id' => 'controller_id']);
    }
}

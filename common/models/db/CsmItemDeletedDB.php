<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_item_deleted".
 *
 * @property string $id
 * @property integer $type
 * @property string $item_id
 * @property string $item_data
 * @property string $deleted_at
 * @property string $deleted_by
 */
class CsmItemDeletedDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_item_deleted';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'item_id', 'deleted_by'], 'integer'],
            [['item_data'], 'string'],
            [['deleted_at'], 'safe'],
            [['type', 'item_id'], 'unique', 'targetAttribute' => ['type', 'item_id'], 'message' => 'The combination of Type and Item ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'type' => Yii::t('backend', 'Type'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'item_data' => Yii::t('backend', 'Item Data'),
            'deleted_at' => Yii::t('backend', 'Deleted At'),
            'deleted_by' => Yii::t('backend', 'Deleted By'),
        ];
    }
}

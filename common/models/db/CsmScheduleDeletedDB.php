<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "csm_schedule_deleted".
 *
 * @property string $id
 * @property string $type
 * @property string $item_id
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 */
class CsmScheduleDeletedDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'csm_schedule_deleted';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'item_id', 'status', 'created_by'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'type' => Yii::t('backend', 'Type'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
        ];
    }
}

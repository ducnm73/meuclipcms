<?php

namespace common\models;

use Yii;

class CsmConvertTasksBase extends \common\models\db\CsmConvertTasksDB {

    public static function deleteItemByMediaId($mediaId){
        self::deleteAll('media_id = :MEDIA_ID',[':MEDIA_ID' => $mediaId]);
    }

}
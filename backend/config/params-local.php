<?php

return [
    'mainLanguage' => 'pt',
    'languagepool' => ['vi', 'en', 'pt'],
    'delimiter' => '#',
    'storage-url' => [
        'image' => [
            'bucket' => 'image1',
            'access-key' => 'cdn-vttvas',
            'secret-key' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
            'end-point' => '10.60.117.250',
            'show_url' => 'http://cdn-vttvas.public.cloudstorage.com.vn'
        ],
        'video' => [
            'bucket' => 'video1',
            'access-key' => 'cdn-vttvas',
            'secret-key' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
            'end-point' => '10.60.117.250',
            'show_url' => 'http://cdn-vttvas.public.cloudstorage.com.vn'
        ],
        'audio' => [
            'bucket' => 'audio',
            'access-key' => 'cdn-vttvas',
            'secret-key' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
            'end-point' => '10.60.117.250',
            'show_url' => 'http://cdn-vttvas.public.cloudstorage.com.vn'
        ],
        'subtitle' => [
            'bucket' => 'audio',
            'access-key' => 'cdn-vttvas',
            'secret-key' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
            'end-point' => '10.60.117.250',
            'show_url' => 'http://cdn-vttvas.public.cloudstorage.com.vn'
        ],

    ],
    's3' => [
        'accessKey' => 'cdn-vttvas',
        'secretKey' => 'PkwYl4/buTMCaXsp/2jsoccLtvE5pgTzOEXiF3/y',
        'video.bucket' => 'video1',
        'static.bucket' => 'image1',
        'endPoint' => 'http://10.60.117.250',

//        'accessKey' => 'CZ434KBF6TTR35PDZBX2',
//        'secretKey' => 'TOR2fBW7PxXA+ADGJtylc2zRlXDFO54DFIZeGLjG',
//        'video.bucket' => 's2-video3',
//        'static.bucket' => 's2-image3',
//        'endPoint' => 'http://s2.cloudstorage.com.vn',

        'PathStyle' => true,
        'proxy.host' => null,
        'proxy.username' => null,
        'proxy.password' => null,

        'maxDuration' => 5 * 60,

        'vodcdn.encrypt' => false,
        'vodcdn.tokenKey' => 'lkjc34xYMp@@@X3434z',
        'vodcdn.encryptWithIp' => false,
        'vodcdn.timeout' => 1800, #giay

        'vodcdn.template' => [
            'VODCDN' => 'http://%domain_name%/%encrypt_string%/playlist.m3u8',
            'OBJCDN' => 'http://%domain_name%/%encrypt_string%'
        ],
        'webcdn.template' => 'http://%domain_name%/%path%',

        #map Bucket to CDN
        'cdn.mapping' => [
            #Link image
            'mobitv_image1' => 'cdn1obj.mobitv.vn',

            'mobitv_video1' => [
                'VODCDN' => 'cdn1vod.mobitv.vn',
                'OBJCDN' => 'cdn1obj.mobitv.vn'
            ],
        ]
    ],
    'media-action-log' => [
        MEDIA_ACTION_DECLINE => Yii::t('backend', 'Từ chối duyệt'),
    ],
    'media-approved-status' => [
        0 => Yii::t('backend', "Giữ nguyên"),
        STATUS_MEDIA_APPROVED => Yii::t('backend', "Phê duyệt"),
        STATUS_MEDIA_REJECTED => Yii::t('backend', "Từ chối")
    ],
    'media-status' => [
        STATUS_MEDIA_DRAFT => Yii::t('backend', "Mới tạo"),
        STATUS_MEDIA_APPROVED_PROPOSAL => Yii::t('backend', "Chưa duyệt"),
        STATUS_MEDIA_REJECTED => Yii::t('backend', "Từ chối"),
        STATUS_MEDIA_APPROVED => Yii::t('backend', "Đã duyệt"),
        STATUS_MEDIA_TRANSFER_PENDING => Yii::t('backend', "Đang đẩy lên storage"),
        STATUS_MEDIA_TRANSFER_SUCCESS => Yii::t('backend', "Hoàn thành đẩy lên storage"),
        STATUS_MEDIA_PUBLISHED => Yii::t('backend', "Đang published"),
        STATUS_MEDIA_LOCKED => Yii::t('backend', "Bị khóa"),
        STATUS_MEDIA_DELETED => Yii::t('backend', "Đã xóa"),
        STATUS_MEDIA_WAIT_CONTRACT_APPROVE => Yii::t('backend', "Chờ duyệt hợp đồng"),
        STATUS_MEDIA_CONTRACT_REJECT => Yii::t('backend', "Từ chối hợp đồng"),
    ],
    'convert-status' => [
        STATUS_NOT_YET_CONVERTED => Yii::t('backend', 'NOT_CONVERTED'),
        STATUS_CONVERTED_SUCCESS => Yii::t('backend', 'SUCCESS'),
        STATUS_IS_CONVERTING => Yii::t('backend', 'CONVERTING'),
        STATUS_GET_FILE_FAIL => Yii::t('backend', 'GET_FILE_FAIL'),
        STATUS_PUT_FILE_FAIL => Yii::t('backend', 'PUT_FILE_FAIL'),
        STATUS_CONVERTED_FAIL => Yii::t('backend', 'CONVERTED_FAIL'),
        STATUS_FILE_INFO_FAIL => Yii::t('backend', 'FILE_INFO_FAIL'),
        STATUS_SPLIT_FAIL => Yii::t('backend', 'SPLIT_FAIL'),
        STATUS_MERGE_FAIL => Yii::t('backend', 'MERGE_FAIL'),
        STATUS_CHECK_FOR_MERGE => Yii::t('backend', 'CHECK_FOR_MERGE'),
        STATUS_WAIT_FOR_MERGING => Yii::t('backend', 'WAIT_FOR_MERGING'),
        STATUS_IS_MERGING => Yii::t('backend', 'MERGING'),
        STATUS_DUR_DEVIATION => Yii::t('backend', 'DUR_DEVIATION')
    ],
    'service-status' => [
        STATUS_MEDIA_DRAFT => Yii::t('backend', 'Mới tạo'),
        STATUS_MEDIA_APPROVED_PROPOSAL => Yii::t('backend', "Chưa duyệt"),
        STATUS_MEDIA_REJECTED => Yii::t('backend', "Từ chối"),
        STATUS_MEDIA_APPROVED => Yii::t('backend', "Đã duyệt"),
        STATUS_MEDIA_WAIT_CONTRACT_APPROVE => Yii::t('backend', "Chờ duyệt hợp đồng"),
        STATUS_MEDIA_CONTRACT_REJECT => Yii::t('backend', "Từ chối hợp đồng"),
    ],

    'approved-action-type' => [
        0 => Yii::t('backend', "Giữ nguyên"),
        MEDIA_ACTION_APPROVE => Yii::t('backend', "Phê duyệt"),
        MEDIA_ACTION_DECLINE => Yii::t('backend', "Từ chối")
    ],
    'media-film-type' => [
        TYPE_MEDIA_FILM => Yii::t('backend', "Film lẻ"),
        TYPE_MEDIA_TRAILER => Yii::t('backend', "Trailer"),
    ],
    'media-film-series-type' => [
        TYPE_MEDIA_FILM_SERIES => Yii::t('backend', "Film bộ"),
        TYPE_MEDIA_TRAILER => Yii::t('backend', "Trailer"),
    ],
    'yes-no-option' => [
        0 => Yii::t('backend', 'Không'),
        1 => Yii::t('backend', 'Có')
    ],
    'media-file-type' => [
        FILE_TYPE_MEDIA_LOCAL => Yii::t('backend', "Server local"),
        FILE_TYPE_MEDIA_STORAGE => Yii::t('backend', "S3 Storage"),
        FILE_TYPE_MEDIA_LINK => Yii::t('backend', "Link"),
    ],
    'csm-attribute-type' => [
        'category' => 1,
    ],
    'upload' => [
        'basePath' => '/u01/khonoidungos/apps/cms_peru/backend/web/uploads',
        'baseUrl' => 'http://192.168.146.252:9402/uploads',
        'storePath' => '/u01/khonoidungos/apps/cms_peru/backend/web/uploads2',
        'storeUrl' => 'http://192.168.146.252:9402/uploads2',
//        'basePath' => 'D:/Projects/VAS/movitel/cms_movitel/backend/web/uploads',
//        'baseUrl' => 'http://127.0.0.1:8080/uploads',
//        'storePath' => 'D:/Projects/VAS/movitel/cms_movitel/backend/web/uploads2',
//        'storeUrl' => 'http://127.0.0.1:8080/uploads2',
        'video' => [
            'basePath' => '/media2/videos',
            'extensions' => ['avi', 'mp4', 'wmv', 'mkv', 'm4v'],
            'maxSize' => 10737418240, //file size 10GB
            'minQuality' => 360
        ],
        'video-image' => [
            'basePath' => '/media2/images/video',
            'extensions' => ['gif', 'jpg', 'png'],
            'maxSize' => 104857600, //file size 100 MB
            'size' => [
                'width' => 100,
                'height' => 100,
            ]
        ],
        'video-poster-image' => [
            'basePath' => '/media2/images/video-poster',
            'extensions' => ['gif', 'jpg', 'png'],
            'maxSize' => 104857600, //file size
            'size' => [
                'width' => 100,
                'height' => 100,
            ]
        ],
        'song' => [
            'basePath' => '/media2/songs',
            'extensions' => ['flac', 'ape', 'ape', 'wv', 'm4a', 'mp3', 'pek', 'wav', 'mp4'],
            'maxSize' => 524288000, //file size 500 MB
        ],
        'attribute-image' => [
            'basePath' => '/media2/images/attribute',
            'extensions' => ['gif', 'jpg', 'jpeg', 'png'],
            'maxSize' => 104857600, //file size 100 MB
        ],
        'audio' => [
            'basePath' => '/media2/audio',
            'extensions' => ['flac', 'ape', 'ape', 'wv', 'm4a', 'mp3', 'pek', 'wav', 'mp4'],
            'maxSize' => 1073741824, //file size 1GB
        ],
        'subtitle' => [
            'basePath' => '/media2/subtitle',
            'extensions' => ['vtt', 'srt'],
            'maxSize' => 10485760, //file size 10 MB
        ],
        'custom-video' => [
            'basePath' => '/media2/videos',
            'extensions' => ['avi', 'mp4', 'wmv', 'mkv', 'm4v'],
            'maxSize' => 10737418240, // 10GB = 10 * 1024 * 1024 * 1024 byte
            'minSize' => 5242880, // 5 MB = 5 * 1024 * 1024 byte
            'minWidth' => 1280,
            'minHeight' => 720,
            'minDuration' => 5
        ],
        'custom-video-image' => [
            'basePath' => '/media2/images/video',
            'extensions' => ['gif', 'jpg', 'png'],
            'maxSize' => 104857600, //file size 100 MB
            'minSize' => 1048576, // 1 MB = 1024 * 1024 byte
            'minWidth' => 1280,
            'minHeight' => 720
        ],
        'custom-video-poster-image' => [
            'basePath' => '/media2/images/video-poster',
            'extensions' => ['gif', 'jpg', 'png']
        ]
    ],
    'ftp' => [
        'upload_path' => '/ftp',
    ],
    'login-failed' => [
        "limit_username" => 5,
        "limit_username_duration" => 10 * 60,
        "locked_username_duration" => 10 * 60,
        "limit_ip" => 20,
        "limit_ip_duration" => 20 * 60,
        "locked_ip_duration" => 20 * 60,
    ],
    'get-ip-method' => 'REMOTE_ADDR', // REMOTE_ADDR | HTTP_X_FORWARDED_FOR
    'menu-icon' => [
        "icon-user-female", "icon-user-follow", "icon-user-following", "icon-user-unfollow", "icon-trophy",
        "icon-screen-smartphone", "icon-screen-desktop", "icon-plane", "icon-notebook", "icon-moustache",
        "icon-mouse", "icon-magnet", "icon-energy", "icon-emoticon-smile", "icon-disc", "icon-cursor-move",
        "icon-crop", "icon-credit-card", "icon-chemistry", "icon-user", "icon-speedometer", "icon-social-youtube",
        "icon-social-twitter", "icon-social-tumblr", "icon-social-facebook", "icon-social-dropbox",
        "icon-social-dribbble", "icon-shield", "icon-screen-tablet", "icon-magic-wand", "icon-hourglass",
        "icon-graduation", "icon-ghost", "icon-game-controller", "icon-fire", "icon-eyeglasses", "icon-envelope-open",
        "icon-envelope-letter", "icon-bell", "icon-badge", "icon-anchor", "icon-wallet", "icon-vector",
        "icon-speech", "icon-puzzle", "icon-printer", "icon-present", "icon-playlist", "icon-pin", "icon-picture",
        "icon-map", "icon-layers", "icon-handbag", "icon-globe-alt", "icon-globe", "icon-frame", "icon-folder-alt",
        "icon-film", "icon-feed", "icon-earphones-alt", "icon-earphones", "icon-drop", "icon-drawer", "icon-docs",
        "icon-directions", "icon-direction", "icon-diamond", "icon-cup", "icon-compass", "icon-call-out",
        "icon-call-in", "icon-call-end", "icon-calculator", "icon-bubbles", "icon-briefcase", "icon-book-open",
        "icon-basket-loaded", "icon-basket", "icon-bag", "icon-action-undo", "icon-action-redo", "icon-wrench",
        "icon-umbrella", "icon-trash", "icon-tag", "icon-support", "icon-size-fullscreen", "icon-size-actual",
        "icon-shuffle", "icon-share-alt", "icon-share", "icon-rocket", "icon-question", "icon-pie-chart",
        "icon-pencil", "icon-note", "icon-music-tone-alt", "icon-music-tone", "icon-microphone", "icon-loop",
        "icon-logout", "icon-login", "icon-list", "icon-like", "icon-home", "icon-grid", "icon-graph",
        "icon-equalizer", "icon-dislike", "icon-cursor", "icon-control-start", "icon-control-rewind",
        "icon-control-play", "icon-control-pause", "icon-control-forward", "icon-control-end", "icon-calendar",
        "icon-bulb", "icon-bar-chart", "icon-arrow-up", "icon-arrow-right", "icon-arrow-left", "icon-arrow-down",
        "icon-ban", "icon-bubble", "icon-camcorder", "icon-camera", "icon-check", "icon-clock", "icon-close",
        "icon-cloud-download", "icon-cloud-upload", "icon-doc", "icon-envelope", "icon-eye", "icon-flag",
        "icon-folder", "icon-heart", "icon-info", "icon-key", "icon-link", "icon-lock", "icon-lock-open",
        "icon-magnifier", "icon-magnifier-add", "icon-magnifier-remove", "icon-paper-clip", "icon-paper-plane",
        "icon-plus", "icon-pointer", "icon-power", "icon-refresh", "icon-reload", "icon-settings", "icon-star",
        "icon-symbol-female", "icon-symbol-male", "icon-target", "icon-volume-1", "icon-volume-2", "icon-volume-off",
        "icon-users"
    ],
    'revenua_per_minute_approve' => 2800

];

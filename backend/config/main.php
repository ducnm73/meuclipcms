<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/meta_country.php'),
    require(__DIR__ . '/meta_language.php'),
    require(__DIR__ . '/meta_year.php'),
    require(__DIR__ . '/meta_content_filter.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'vi',
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => 7200, // 2 * 60 * 60 = 7200 // 2 hours
            'identityCookie' => [
                'name' => '_cmsIdentity',
                'httpOnly' => true,
            ],
        ],
//        'redis' => [
//            'class' => 'yii\redis\Connection',
//            'hostname' => '10.30.141.120',
//            'password' => 'Redis%@4Csm',
//            'port' => 8378,
//            'database' => 14,
//        ],
//        'file-upload-redis' => [
//            'class' => 'yii\redis\Connection',
//            'hostname' => '10.30.141.120',
//            'password' => 'Redis%@4Csm',
//            'port' => 8378,
//            'database' => 7,
//        ],

        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '192.168.146.231',
            'port' => 8379,
            'database' => 11,
            'password' => 'Password@2020'
        ],
        'file-upload-redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => '192.168.146.231',
            'port' => 8379,
            'database' => 12,
            'password' => 'Password@2020'
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/site/index',
                'login' => '/site/login',
                'logout' => '/site/logout',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logFile' => '@logs/cms/error.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logFile' => '@logs/cms/warning.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => '@logs/cms/info.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'logFile' => '@logs/cms/queries.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['s3'],
                    'logFile' => '@logs/cms/s3.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['i18n'],
                    'logFile' => '@logs/i18n/translate.log',
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 100,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logVars' => [],
                    'categories' => ['debug'],
                    'maxFileSize' => 51200, // 50MB
                    'maxLogFiles' => 50,
                    'logFile' => '@logs/cms/debug.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['upload'],
                    'maxFileSize' => 51200, // 50MB
                    'maxLogFiles' => 50,
                    'logFile' => '@logs/cms/upload.error.log',
                ],

            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
//            '*',
            'site/*',
            'gii/*'
        ]
    ],
    'params' => $params,
];

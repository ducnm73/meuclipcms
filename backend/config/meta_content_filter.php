<?php

return [
    'meta_content_filter' => [
        'P' => Yii::t('backend','Popular'),
        'C13' => Yii::t('backend','For audiences over 13 years old'),
        'C16' => Yii::t('backend','For audiences over 16 years old'),
        'C18' => Yii::t('backend','For audiences over 18 years old'),
    ]
];
$(document).ready(function () {

    $(".pft-directory A").click(function () {
        handleDirectoryClick($(this));
    });

    $(".pft-file A").click(function () {
        handleFileClick($(this));
    });
    $('.modal').on('hidden.bs.modal', function () {
        reValidateField();
    })


});

function reValidateField() {
    var form = $("#my-form"),
        form_data = form.data("yiiActiveForm");
    $.each(form_data.attributes, function () {
        this.status = 3;
    });
    form.yiiActiveForm("validate");
}

function validateImage(url, callback) {
    var img = new Image();
    img.onload = function () {
        var width = this.width;
        var height = this.height;
        if (width == 1280 && height == 720) {
            callback(true);
        } else callback(false);
    };
    img.src = url;
}

function getFileFromPath(url, path, type, callback) {
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
            path: path,
            type: type
        },
        success: function (result) {
            callback(result);
        }
    })

}


function handleFileClick(aTag) {

    var currentPath = aTag.data('path');
    var fileName = aTag.html();
    var fileUrl = $('#ftp_base_url').val() + encodeURIComponent(currentPath);
    var modal = aTag.parents().closest(".modal.fade");
    var modalId = modal.attr('id');
    switch (modalId) {
        case 'video-modal': {
            $('#csmmedia-original_path').val(currentPath);
            $('#csmmedia-name').val(fileName);
            $('#videoPlayer').attr('src', fileUrl);
            $('#video-modal').modal('toggle');

            break;
        }
        case 'image-path-modal': {
            validateImage(fileUrl, function (result) {
                if (result) {
                    $('#csmmedia-image_path').val(currentPath);
                    $("#img_model_id").show().attr("src", fileUrl);
                    $('#image-path-modal').modal('toggle');
                } else {
                    alert('image size must be 1280 x 720 px');
                }
            })
            break;
        }
        case 'poster-path-modal': {
            $('#csmmedia-poster_path').val(currentPath);
            $("#img_poster_model_id").show().attr("src", fileUrl);
            $('#poster-path-modal').modal('toggle');
            break;
        }
    }

}

function handleDirectoryClick(element) {
    if (element.attr('data-is-loaded') == 1) {
        element.parent().find("UL:first").slideToggle("medium");
    } else if (element.attr('data-is-loaded') == 0) {
        var postUrl = $('#get_ftp_file_url').val();
        var currentPath = element.data('path');
        var mediaType = '';
        var modal = element.parents().closest(".modal.fade");
        var modalId = modal.attr('id');
        switch (modalId) {
            case 'video-modal': {
                mediaType = 'video';
                break;
            }
            case 'image-path-modal': {
                mediaType = 'video-image';
                break;
            }
            case 'poster-path-modal': {
                mediaType = 'video-poster-image';
                break;
            }
        }

        if (postUrl && currentPath && mediaType) {
            getFileFromPath(postUrl, currentPath, mediaType, function (result) {
                if (result.responseCode == 200) {
                    element.parent().append(result.data);
                    element.attr('data-is-loaded', '1');
                    var appendFolder = element.next();
                    appendFolder.find('.pft-directory A').click(function () {
                        handleDirectoryClick($(this));
                    })

                    appendFolder.find('.pft-file A').click(function () {
                        handleFileClick($(this));
                    })
                }
            });
        }
    }
    if ($(this).parent().attr('className') == "pft-directory") return false;


}



$(document).ready(function () {

});

function validateMultiLanguage(elementId) {
    var mLanguageCode = $('[name=' + elementId + '_language_code]').val();
    if (!mLanguageCode) {
        alert('Please choose language ');
        return false;
    }
    return true;
}
function afterUpload(type, elementId, filePath, fileUrl) {

    var newObj = {
        'language_code': $('[name=' + elementId + '_language_code]').val(),
        'original_path': filePath,
        'file_url': fileUrl
    }
    var currentValue = null;
    if (type == 'audio') {
        currentValue = $('#csmmedia-audio_path').val();
    } else if (type == 'subtitle') {
        currentValue = $('#csmmedia-subtitle_path').val();
    }
    var elementPaths = [];
    if (currentValue) {
        var currentValue = JSON.parse(currentValue);
        currentValue.push(newObj);
        elementPaths = currentValue;
    } else {
        elementPaths = [newObj];
    }

    var jsonString = JSON.stringify(elementPaths);
    if (type == 'audio') {
        $('#csmmedia-audio_path').val(jsonString);
    } else if (type == 'subtitle') {
        $('#csmmedia-subtitle_path').val(jsonString)
    }

    updateTable(type, elementId, jsonString);
}

function updateTable(type, elementId, jsonData) {
    jsonData = JSON.parse(jsonData);
    var allLanguage = getAllLanguageCode(elementId);
    var table_id = elementId + '_table';
    $('#' + table_id).find("tr:gt(0)").remove();

    if (type == 'audio') {
        for (var i = 0; i < jsonData.length; i++) {
            var row = '<tr id = "' + type + '_row_' + i + '">'
                + '<td>' + (i+1) + '</td>'
                + '<td>' + 'Language ' + allLanguage[jsonData[i].language_code] + ' - ' + jsonData[i].language_code + '</td>'
                + '<td>' + '<audio controls><source src="' + jsonData[i].file_url + '" type="audio/mpeg">Your browser does not support the audio</audio>' + '</td>'
                + '<td>' + '<button type = "button" class = "btn btn-danger" onclick = "removeRow(\'' + elementId + '\',\'' + type + '\',' + i + ')"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></button>' + '</td>'
                + '</tr>';
            $('#' + table_id + ' > tbody:last-child').append(row);
        }

    } else if (type == 'subtitle') {
        for (var i = 0; i < jsonData.length; i++) {
            var row = '<tr id = "' + type + '_row_' + i + '">'
                + '<td>' + (i+1) + '</td>'
                + '<td>' + 'Language ' + allLanguage[jsonData[i].language_code] + ' - ' + jsonData[i].language_code + '</td>'
                + '<td>' + '<a href="' + jsonData[i].file_url + '" target="_blank">Download file</a>' + '</td>'
                + '<td>' + '<button type = "button" class = "btn btn-danger" onclick = "removeRow(\'' + elementId + '\',\'' + type + '\',\'' + i + '\')"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></button>' + '</td>'
                + '</tr>';
            $('#' + table_id + ' > tbody:last-child').append(row);
        }

    }


    // clear option
    $('#' + elementId + '_language_code').prop('selectedIndex', 0);

}

function getAllLanguageCode(elementId) {
    var allOption = [];
    $('#' + elementId + '_language_code > option').each(function () {
        if ($(this).val()) {
            allOption[$(this).val()] = $(this).text();
        }
    });
    return allOption;
}

function removeRow(elementId, type, rowIndex) {
    var cf = confirm("Do you want to remove this item ?");
    if (!cf) return;
    var currentVal = getCurrentHiddenData(type);
    if (currentVal) {
        currentVal.splice(rowIndex, 1);
        var jsonString = JSON.stringify(currentVal);
        if (type == 'audio') {
            $('#csmmedia-audio_path').val(jsonString);
        } else if (type == 'subtitle') {
            $('#csmmedia-subtitle_path').val(jsonString)
        }
        showSnackbar('remove successfuly',2000);
        updateTable(type, elementId, jsonString);
    }
}

function getCurrentHiddenData(type) {
    var currentValue = null;
    if (type == 'audio') {
        currentValue = $('#csmmedia-audio_path').val();
    } else if (type == 'subtitle') {
        currentValue = $('#csmmedia-subtitle_path').val();
    }

    if (currentValue) {
        return JSON.parse(currentValue);
    } else {
        return [];
    }
}

function showSnackbar(mess, timeInMiliSec){
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");
    x.innerHTML = mess ;

    // Add the "show" class to DIV
    x.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, timeInMiliSec);
}
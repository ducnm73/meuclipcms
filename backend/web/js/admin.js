$(document).ready(function () {
//    $(document).keypress(function (e) {
//        if (e.which == 13) {
//            return false;
//        }
//    });

    $('*[data-toggle="toggle-visible"]').change(function() {
        if($(this).val() == $(this).data('toggle-value')) {
            $($(this).data('target')).show();
        } else {
            $($(this).data('target')).hide();
        }
    });

    $('#btn-add-upload-video').click(function () {
        $('#list-avaliable option:selected').each(function () {
            $('#list-assigned').append($('<option>', {
                value: $(this).val(),
                text: $(this).text()
            }));
            $(this).remove();
        }
        );
    });

    $('#btn-remove-upload-video').click(function () {
        $('#list-assigned option:selected').each(function () {
            $('#list-avaliable').append($('<option>', {
                value: $(this).val(),
                text: $(this).text()
            }));
            $(this).remove();
        }
        );
    });

    $('#frm-btn-submit').click(function () {
        $("#list-assigned option").prop("selected", "selected");
        $("form:first").submit();
    });

    $('#frm-btn-song-submit').click(function () {
        $("#list-assigned option").prop("selected", "selected");
        if (!$('#editorTags').val()) {
            alert('Ca sỹ không được để trống');
            $('#editorTags').focus();
            return;
        }
        if (!$('#list-assigned').val()) {
            alert('Thể loại không được để trống');
            $('#list-assigned').focus();
            return;
        }
        $("form:first").submit();
    });

    $('#frm-btn-video-submit').click(function () {
        $("#list-assigned option").prop("selected", "selected");
        if (!$('#file-name-video').val()) {
            alert('Tên không được để trống');
            $('#file-name-video').focus();
            return;
        }
        if (!$('#editorTags').val()) {
            alert('Ca sỹ không được để trống');
            $('#editorTags').focus();
            return;
        }
        if (!$('#list-assigned').val()) {
            alert('Thể loại không được để trống');
            $('#list-assigned').focus();
            return;
        }
        $("form:first").submit();
    });

    $('#video-active-id').click(function () {
        var ids = '';
        $('input:checkbox:checked').each(function () {
            ids = ids + $(this).val() + ',';
        });
        if (ids != '') {
            if (confirm('Ban co chac muon kiem duyet?')) {
                $.ajax({
                    method: "POST",
                    url: "/video/active",
                    data: {ids: ids}
                });
            }
        } else {
            alert('Ban chua chon ban ghi nao!');
        }
    });

    $('#song-active-id').click(function () {
        var ids = '';
        $('input:checkbox:checked').each(function () {
            ids = ids + $(this).val() + ',';
        });
        if (ids != '') {
            if (confirm('Ban co chac muon kiem duyet?')) {
                $.ajax({
                    method: "POST",
                    url: "/song/active",
                    data: {ids: ids}
                });
            }
        } else {
            alert('Ban chua chon ban ghi nao!');
        }
    });

    $('#banneritem-target_type').change(function () {
        var itemId = $(this).val();
        if (itemId == 1) {
            $('#song-item-type').show();
            $('#video-item-type').hide();
            $('#playlist-item-type').hide();
        }
        if (itemId == 2) {
            $('#song-item-type').hide();
            $('#video-item-type').show();
            $('#playlist-item-type').hide();
        }
        if (itemId == 3) {
            $('#song-item-type').hide();
            $('#video-item-type').hide();
            $('#playlist-item-type').show();
        }
    });

    $('#banneritem-song_id').change(function () {
        $('#banneritem-target_id').val($(this).val());
    });

    $('#banneritem-video_id').change(function () {
        $('#banneritem-target_id').val($(this).val());
    });

    $('#banneritem-playlist_id').change(function () {
        $('#banneritem-target_id').val($(this).val());
    });


    $('#video_upload_1').on('change', function (event) {
        var file = this.files[0];
        var type = file.type;
        var videoNode = document.querySelector('#video1');
        var canPlay = videoNode.canPlayType(type);
        if (canPlay === '')
            canPlay = 'no';
        var isError = canPlay === 'no';
        alert(isError);
        if (isError) {
            return;
        }
        videoNode.src = createObjectURL(file);
        videoNode.controls = true;
    });

});

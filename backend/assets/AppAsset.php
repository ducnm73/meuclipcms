<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/font-opensans/css/font-opensans.css',
        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/simple-line-icons/simple-line-icons.min.css',
        'plugins/bootstrap/css/bootstrap.min.css',
//        'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'css/components-md.min.css',
        'css/plugins-md.min.css',
        'css/layout.min.css',
        'css/themes/darkblue.min.css',
        'css/custom.css'
    ];
    public $js = [
//        'plugins/jquery.min.js',
        'plugins/bootstrap/js/bootstrap.min.js',
//        'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
//        'plugins/js.cookie.min.js',
        'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
//        'plugins/jquery.blockui.min.js',
//        'plugins/uniform/jquery.uniform.min.js',
//        'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'js/metronic/app.min.js',
        'js/metronic/layout.min.js',
//        'js/metronic/demo.min.js',
//        'js/src/jquery.picture.cut.js',
        'js/metronic/quick-sidebar.min.js',
        'js/admin.js',
        'js/jquery.blockUI.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}

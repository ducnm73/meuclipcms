<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UploadAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/fine-uploader-new.css'
        'css/upload.css',
        'plugins/file_tree/styles/default/default.css',
    ];
    public $js = [
        'js/upload.js',
        'plugins/file_tree/php_file_tree_jquery.js',
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];


}

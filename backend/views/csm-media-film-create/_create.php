<?php

use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use common\helpers\I18nHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'my-form']); ?>

<div class="portlet light portlet-fit portlet-form bordered csm-media-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <a class="btn btn-transparent black btn-outline btn-circle btn-sm"
               href="<?= Url::to(['index']) ?>">
                <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div id="divUploadVideo">
                <div id="video_upload">
                </div>
                <?php
                echo FineUploader::widget([
                    'url' => Url::to(['upload/video-upload', 'type' => 'video']),
                    'id' => 'video_upload',
                    'csrf' => true,
                    'renderTag' => false,
                    'preMergeFile' => true,
                    'jsOptions' => [
                        'chunking' => [
                            'enabled' => true,
                            'partSize' => 20 * 1024 * 1024, // 20MB
                            'concurrent' => [ 'enabled' => true ],
                            'success' => [ 'endpoint' => Url::to(['upload/video-upload', 'type' => 'video', 'done' => 1]) ]
                        ],
                        'callbacks' => [
                            'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    alert('The file ' + name + ' could not be uploaded: ' + errorReason);
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                            ),
                            'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        $('#video-tabs').show();
        $('#divUploadVideo').hide();
        $('#csmmedia-original_path').val(data.filePath);
        $('#csmmedia-name').val(data.uploadName).focus();
        $('#videoPlayer').attr('src',data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                            )
                        ],
                    ]
                ]);
                ?>
            </div>
            <div class="tabbable-line" id="video-tabs" style="display: none;">
                <?= $this->render('//partials/film_tab_content', [
                    'model' => $model,
                    'form' => $form
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

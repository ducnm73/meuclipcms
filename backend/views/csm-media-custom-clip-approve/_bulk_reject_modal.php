<?php

use awesome\backend\form\AwsActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<?php

$formId = 'reject-reason-popup-form';
// $gridId = 'csm_media_grid'
// $toggleButtonSelector = '.toggle-reject-modal'
// 'location' => Url::to('/csm-media-custom-clip-approve/index')
// $modalId = reject-reason-modal
$mess_1 = Yii::t('backend', 'Please select one or more items from the list.');
?>

    <div id="reject-reason-modal" class="fade modal in" role="dialog" tabindex="-1"
         style="display: none; padding-right: 17px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div id="modal-pjax" data-pjax-container="" data-pjax-timeout="1000">
                    <div class="form-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h5>Deny Approve</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group form-md-line-input form-md-floating-label field-mediarejectform-reject_reason required">
                                    <textarea id="mediarejectform-reject_reason" class="form-control"
                                              name="MediaRejectForm[reject_reason]"></textarea>
                                <label class="control-label" for="mediarejectform-reject_reason">Reason</label>

                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btn-submit" class="btn btn-default">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->registerJs(<<<EOF
   $('#btn_reject').click(function (event) {
    event.preventDefault();
    var ids = $('#$gridId').yiiGridView('getSelectedRows');
    if (!ids.length) {
        alert('$mess_1');
    } else {
        $('#$modalId').show();

    }
});

$('.close').click(function (e) {
    $('#$modalId').hide();
})

$('#btn-submit').click(function (e) {
    var reason = $('#mediarejectform-reject_reason').val();
    var ids = $('#$gridId').yiiGridView('getSelectedRows');
    console.log(reason);
    if (reason) {

        $.ajax({
            method: "POST",
            url: '/csm-media-custom-clip-approve/bulk-reject-media',
            dataType: 'json',
            data: {
                'items_id': ids,
                'reason': reason
            },
            success: function (response) {
            console.log(response);
                $('#$modalId').hide();
                if (response['responseCode'] == 200) {
                    alert(response['message']);
                    location.reload();
                } else {
                    alert(response['message']);
                    location.reload();
                }
            },
            error: function (data) {
                alert('error!!!!');
                location.reload();
            }
        });

    } else {
        alert('reason must not be empty');
    }

})
EOF
)
?>
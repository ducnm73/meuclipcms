<?php

use awesome\backend\widgets\AwsBaseHtml;
use kartik\select2\Select2;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Video Clip'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?= AwsBaseHtml::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                        ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm'])
                    ?>
                    <?= AwsBaseHtml::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-transparent red btn-outline btn-circle btn-sm',
                        'data' => [
                            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-md-offset-9">
                <?= Select2::widget([
                    'data' => common\helpers\I18nHelper::getI18nBackendParams(
                        array_intersect_key(Yii::$app->params['language_code'], array_flip(Yii::$app->params['languagepool']))
                    ),
                    'name' => 'kv-repo-template',
                    'value' => Yii::$app->params['mainLanguage'],
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Chọn ngôn ngữ'),
                        'id' => 'formLangSelect',
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ]);

                $this->registerJs("
                    const recordId = '" . $model->id . "';
                    $('#formLangSelect').on('change', function() {
                        $.ajax({
                            type:'GET', 
                            url: `/csm-media-custom-clip-approve/view?id=\${recordId}&lang=\${this.value}`, 
                            success: function(data) {
                                $('.portlet-body').replaceWith($('.portlet-body', data));
                            }, 
                            error: function(jqXHR, textStatus, errorThrown) {
                                alert(jqXHR.status);
                            },
                            dataType: 'html'
                        });
                    });
                "); ?>
            </div>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
//                        'slug',
                        'short_desc',
                        'description:ntext',
                        'status',
//                        'price_download',
//                        'price_play',
//                        'type',
                        'created_at',
                        'updated_at',
//                        'published_at',
                        'duration',
                        'resolution',
//                        'attributes:ntext',
                        'cp_id',
//                    'cp_info',
//                    'image_path',
                        [
                            'attribute' => 'image_path',
                            'value' => $model->getLocalImagePath(),
                            'format' => ['image', ['width' => '240']]
                        ],
//                        'file_type',
//                        'convert_path',
//                        'convert_priority',
//                        'convert_start_time',
//                        'convert_end_time',
                        'tag:ntext',
                        'seo_title',
                        'seo_description',
                        'seo_keywords',
//                        'is_crawler:boolean',
//                        'crawler_id',
//                        'crawler_info',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

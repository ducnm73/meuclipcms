<?php 

$formId = 'reject-reason-popup-form';

?>
<div id="<?php echo $modalId ?>" class="fade modal in" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <?php yii\widgets\Pjax::begin(['id' => 'modal-pjax', 'formSelector' => '#' . $formId, 'enablePushState' => false]) ?>
            <div class="form-content"></div>
            <?php yii\widgets\Pjax::end() ?>
        </div>
    </div>
</div>

<?php $this->registerJs("
    $('" . $toggleButtonSelector . "').click(function(event){
        event.preventDefault();
        var ids = $('#" . $gridId . "').yiiGridView('getSelectedRows');
        if (!ids.length) {
            alert('" . Yii::t('backend', 'Please select one or more items from the list.') . "');
        } else {
            App.blockUI({
                zIndex: 2000,
                target: '.page-content',
                boxed: !0
            });

            $('#" . $modalId . "').find('.form-content').load($(this).attr('href') + '?ids=' + ids.join(',') + '&formId=" . $formId . "', function(){
                $($('" . $toggleButtonSelector . "').data('target')).modal('show');
                App.unblockUI('.page-content');
            });
        }
    });

    //var handle = function (event, jqXHR, settings){
    //    event.preventDefault();
    //    $.ajax({
    //        url: $(this).attr('action'),
    //        type: $(this).attr('method'),
    //        data: new FormData($(this)[0]),
    //        mimeType: 'multipart/form-data',
    //        contentType: false,
    //        cache: false,
    //        processData: false,
    //        dataType: 'json',
    //        success: function (data) {
    //            console.log(data);
    //        },
    //        error: function (data) {
    //            console.log(data);
    //        }
    //    });
    //    return false;
    //};
    //$(document).on('submit', '#" . $formId . "', handle);
    
    $('#" . $modalId . "').on('hide.bs.modal', function (e) {
        window.location = '" . $location . "';
    });

    $(document).on('pjax:start', '#modal-pjax', function() {
        console.log('start');
        App.blockUI({
            zIndex: 10051,
            target: '#" . $modalId . "',
            boxed: !0
        });
    });

    $(document).on('pjax:end', '#modal-pjax', function() {
        console.log('end');
        App.unblockUI('#" . $modalId . "');
    });
") ?>
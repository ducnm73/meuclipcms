<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Csm Media',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Csm Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-create">
    <div class="col-md-12">
        <?= $this->render('_create', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

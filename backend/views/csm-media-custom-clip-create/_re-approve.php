<?php
use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
$this->registerJsFile("/js/upload.utils.js");
?>

<?php $form = AwsActiveForm::begin(['id' => 'my-form']); ?>

<div class="portlet light portlet-fit portlet-form bordered csm-media-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Phê duyệt lại'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                    onclick="window.location.replace('<?= Url::to(['csm-media-custom-clip-cp/index']) ?>');">
                <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div class="tabbable-line" id="video-tabs">
                <ul class="nav nav-tabs nav-tabs-lg">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">

                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <video id="videoPlayer" src="<?= $model->getLocalFilePath() ?>" width="100%"
                                                   controls="controls"></video>
                                            <?= $form->field($model, 'original_path', ['selectors' => ['input' => '#csmmedia-original_path']])->hiddenInput(['id' => 'csmmedia-original_path'])->label(Yii::t('backend','Video Path')) ?>
                                            <div id="divUploadVideo">
                                                <div id="video_upload">
                                                </div>
                                                <?php
                                                echo FineUploader::widget([
                                                    'url' => Url::to(['upload/video-upload', 'type' => 'custom-video']),
                                                    'id' => 'video_upload',
                                                    'csrf' => true,
                                                    'renderTag' => false,
                                                    'preMergeFile' => true,
                                                    'jsOptions' => [
                                                        'validation' => [
                                                            'minSizeLimit' => Yii::$app->params['upload']['custom-video']['minSize'],
                                                            'sizeLimit' => Yii::$app->params['upload']['custom-video']['maxSize'],
                                                            'allowedExtensions' => Yii::$app->params['upload']['custom-video']['extensions'],
                                                        ],
                                                        'messages' => [
                                                            'minSizeError' => Yii::t('backend', 'Kích thước file phải lớn hơn') . ' '.Yii::$app->params['upload']['custom-video']['minSize']/1024/1024 . ' MB',
                                                            'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['custom-video']['maxSize']/1024/1024 . ' MB',
                                                            'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                                        ],
                                                        'chunking' => [
                                                            'enabled' => true,
                                                            'partSize' => 20971520, // 20MB
                                                            'concurrent' => ['enabled' => true],
                                                            'success' => ['endpoint' => Url::to(['upload/video-upload', 'type' => 'video', 'done' => 1])]
                                                        ],
                                                        'callbacks' => [
                                                            'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                            ),
                                                            'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        $('.qq-upload-success').hide();
        $('#csmmedia-original_path').val(data.filePath);
        $('#videoPlayer').attr('src',data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                            )
                                                        ],
                                                    ]
                                                ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <img width="100%" src="<?= $model->getLocalImagePath() ?>"

                                                         id="img_model_id"/>
                                                    <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend', 'Ảnh ngang') . ' 1280*720 px' . '<span style="color: red; margin-left: 2px">*</span>') ?>
                                                    <div id="image_upload">
                                                    </div>
                                                    <?php
                                                    echo FineUploader::widget([
                                                        'url' => Url::to(['upload/image-upload', 'type' => 'custom-video-image']),
                                                        'id' => 'image_upload',
                                                        'csrf' => true,
                                                        'isBase' => true,
                                                        'renderTag' => false,
                                                        'jsOptions' => [
                                                            'validation' => [
                                                                'image' => [
                                                                    'minWidth' => Yii::$app->params['upload']['custom-video-image']['minWidth'],
                                                                    'minHeight' => Yii::$app->params['upload']['custom-video-image']['minHeight'],
                                                                ],
                                                                'minSizeLimit' => Yii::$app->params['upload']['custom-video-image']['minSize'],
                                                                'sizeLimit' => Yii::$app->params['upload']['custom-video-image']['maxSize'],
                                                                'allowedExtensions' => Yii::$app->params['upload']['custom-video-image']['extensions'],
                                                            ],
                                                            'messages' => [
                                                                'minWidthImageError' => Yii::t('backend', 'Phải chọn ảnh có chiều rộng tối thiểu' . ' ' .Yii::$app->params['upload']['custom-video-image']['minWidth'].'px'),
                                                                'minHeightImageError' => Yii::t('backend', 'Phải chọn ảnh có chiều cao tối thiểu' . ' ' .Yii::$app->params['upload']['custom-video-image']['minHeight'].'px'),
                                                                'minSizeError' => Yii::t('backend', 'Kích thước file phải lớn hơn') . ' '.Yii::$app->params['upload']['custom-video-image']['minSize']/1024/1024 . ' MB',
                                                                'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['custom-video-image']['maxSize']/1024/1024 . ' MB',
                                                                'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                                            ],
                                                            'callbacks' => [
                                                                'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                ),
                                                                'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#csmmedia-image_path').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
        var form = $("#my-form"), 
        form_data = form.data("yiiActiveForm");
        $.each(form_data.attributes, function() {
             this.status = 3;
        });
        form.yiiActiveForm("validate");
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                )],
                                                        ]
                                                    ]);
                                                    ?>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <img width="100%" src="<?= $model->getLocalPosterPath() ?>"

                                                         id="img_poster_model_id"/>
                                                    <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path']) ?>
                                                    <div id="image_poster_upload">
                                                    </div>
                                                    <?php
                                                    echo FineUploader::widget([
                                                        'url' => Url::to(['upload/image-upload', 'type' => 'custom-video-poster-image']),
                                                        'id' => 'image_poster_upload',
                                                        'csrf' => true,
                                                        'isBase' => true,
                                                        'renderTag' => false,
                                                        'jsOptions' => [
                                                            'callbacks' => [
                                                                'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                ),
                                                                'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_poster_upload').hide();
        $('#csmmedia-poster_path').val(data.filePath);
        $("#img_poster_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                )],
                                                        ]
                                                    ]);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'name',['selectors' => ['input' => '#csmmedia-name']])->textInput(['id' => 'csmmedia-name'])->label(Yii::t('backend', 'Tên') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'short_desc')->textarea(['row' => 3]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'description')->textarea(['row' => 5]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_download')->textInput(); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_play')->textInput(); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => Yii::t('backend','Chọn thời gian xuất bản ...'),
                                                    'value' => ($model->published_at) ? $model->published_at : '',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                                    'todayHighlight' => true,
                                                    'todayBtn' => true
                                                ]
                                            ])->label(Yii::t('backend', 'Thời gian xuất bản')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'category_list')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getCategoryByIdsActive($model->category_list),
                                                    'id', 'name'
                                                ),
                                                'size' => Select2::MEDIUM,
                                                'url' => Url::to(['attribute/search-category-active']),
                                                'options' => ['placeholder' => Yii::t('backend','Chọn danh mục'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label('Category'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                        </div>

                                        <?php if (Yii::$app->user->can('video-convert-priority')) { ?>
                                            <div class="static-info">
                                                <?= $form->field($model, 'convert_priority')->textInput()->label(Yii::t('backend', 'Độ ưu tiên convert')); ?>
                                            </div>
                                        <?php } ?>

                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_content_filter')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_content_filter'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn loại nội dung')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Phân loại nội dung')); ?>
                                        </div>

                                        <div class="static-info">
                                            <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php AwsActiveForm::end(); ?>

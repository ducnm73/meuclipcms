<?php

use awesome\backend\widgets\AwsBaseHtml;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Csm Media'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?=  AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?=                     AwsBaseHtml::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                        ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm'])
                    ?>
                    <?=                     AwsBaseHtml::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-transparent red btn-outline btn-circle btn-sm',
                        'data' => [
                            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <div class="portlet-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
            'name',
            'slug',
            'short_desc',
            'description:ntext',
            'status',
            'price_download',
            'price_play',
            'type',
            'max_quantity',
            'published_by',
            'created_at',
            'updated_at',
            'published_at',
            'duration',
            'resolution',
            'attributes:ntext',
            'cp_id',
            'cp_info',
            'original_path',
            'image_path',
            'poster_path',
            'file_type',
            'convert_status',
            'convert_path:ntext',
            'convert_priority',
            'convert_start_time',
            'convert_end_time',
            'convert_data_id',
            'convert_images',
            'meta_info',
            'censored_info',
            'logo_path',
            'need_censored',
            'seo_title',
            'seo_description',
            'seo_keywords',
            'is_crawler',
            'crawler_id',
            'crawler_info',
            'created_by',
            'updated_by',
            'reviewed_by',
            'published_list',
            'tag:ntext',
            'need_encryption',
            'resource_id',
            'drm_id',
            'convert_server',
            'media_info:ntext',
            'copyright_id',
            'copyright_info',
                ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

<?php


use awesome\backend\grid\AwsGridView;
use awesome\backend\toast\AwsAlertToast;
use awesome\backend\widgets\AwsBaseHtml;
use yii\widgets\Pjax;

$this->title = Yii::t('backend', 'Báo cáo doanh thu');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row csm-media-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
            </div>

            <?php echo $this->render('_search', ['model' => $searchModel]); ?>


            <div class="portlet-body">
                <div class="table-container">
                    <?= AwsGridView::widget([
                        'showFooter' => true,
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['style' => 'width:3%'],],
                            [
                                'attribute' => 'published_at',
                                'header' => Yii::t('backend', 'Ngày Duyệt'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:10%;',
                                    'class' => 'text-center'],
                                'contentOptions' => ['class' => 'text-center', 'style' => 'width:10%'],
                            ],
                            [
                                'attribute' => 'user_id',
                                'header' => Yii::t('backend', 'User ID'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                            ],
                            [
                                'attribute' => 'user_name',
                                'header' => Yii::t('backend', 'BTV'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                            ],
                            [
                                'attribute' => 'media_id',
                                'header' => Yii::t('backend', 'ID Nội Dung'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                            ],
                            [
                                'attribute' => 'media_name',
                                'header' => Yii::t('backend', 'Tên Nội Dung'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:15%;',
                                    'class' => 'text-center'],
                            ],
                            [
                                'attribute' => 'action',
                                'header' => Yii::t('backend', 'Trạng Thái'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                                'content' => function ($data) {
                                    return Yii::t('backend', YII::$app->params['approved-action-type'][$data->action]);

                                },
                                'footer' => Yii::t('backend', 'Tổng') . ':'
                            ],
                            [
                                'attribute' => 'video_duration',
                                'header' => Yii::t('backend', 'Thời Lượng Video'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],

                            ],
                            [
                                'attribute' => 'watched_duration',
                                'header' => Yii::t('backend', 'Thời Lượng Duyệt'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                                'enableSorting' => false,
                                'contentOptions' => ['class' => 'text-center', 'style' => 'width:5%'],
                                'footer' => \backend\models\CpApproveReport::getTotalData($searchModel)['watched_duration'],

                            ],
                            [
                                'attribute' => 'revenue',
                                'header' => Yii::t('backend', 'Doanh Thu'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:5%;',
                                    'class' => 'text-center'],
                                'enableSorting' => false,
                                'contentOptions' => ['class' => 'text-center', 'style' => 'width:10%'],
                                'footer' => \backend\models\CpApproveReport::getTotalData($searchModel)['revenue'] . ' VND'

                            ],
                            [
                                'attribute' => 'note',
                                'header' => Yii::t('backend', 'Ghi Chú'),
                                'headerOptions' => [
                                    'style' => 'vertical-align:top; width:15%;',
                                    'class' => 'text-center'],
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>


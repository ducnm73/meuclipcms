<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Film Trailer FTP',
]) . ' ' . Html::encode($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Film Trailer FTP'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update') . ' ' . $model->name;
?>
<div class="row csm-media-update">
    <div class="col-md-12">

    <?= $this->render('//partials/_form_film_ftp', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>

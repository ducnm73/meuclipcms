<?php

use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use common\helpers\I18nHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\assets\UploadAsset;
use common\helpers\UploadHelper;
use yii\bootstrap\Modal;
use kartik\datetime\DateTimePicker;
use yii\widgets\ActiveForm;

UploadAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'my-form']); ?>

<div class="portlet light portlet-fit portlet-form bordered csm-media-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <a class="btn btn-transparent black btn-outline btn-circle btn-sm"
               href="<?= Url::to(['index']) ?>">
                <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div class="tabbable-line" id="video-tabs">
                <ul class="nav nav-tabs nav-tabs-lg">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#tab_3" data-toggle="tab"><?= Yii::t('backend', 'Film Information') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#tab_4" data-toggle="tab"><?= Yii::t('backend', 'Audio Information') ?>
                        </a>
                    </li>
                    <li>
                        <a href="#tab_5" data-toggle="tab"><?= Yii::t('backend', 'Subtitle Information') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <video id="videoPlayer" width="100%" controls="controls"
                                                   src="<?= $model->getLocalFilePath() ?>"></video>
                                            <?= $form->field($model, 'original_path', ['selectors' => ['input' => '#csmmedia-original_path']])->hiddenInput(['id' => 'csmmedia-original_path'])->label(Yii::t('backend', 'Video File') . '<span style="color: red; margin-left: 2px">*</span>'); ?>

                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#video-modal">
                                                Select Video File
                                            </button>
                                            <?= Html::hiddenInput('', Url::toRoute('upload/get-ftp-file-from-path'), ['id' => 'get_ftp_file_url']) ?>
                                            <?= Html::hiddenInput('', UploadHelper::getFtpBaseUrl(), ['id' => 'ftp_base_url']) ?>
                                            <!-- Modal -->
                                            <div class="modal fade" id="video-modal" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Select File</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php
                                                            $user_name = Yii::$app->user->identity->username;
                                                            if ($user_name) {
                                                                echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video']['extensions']);
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="static-info">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <img id="img_model_id" width="100%"
                                                         src="<?= $model->getLocalImagePath() ?>"/>
                                                    <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend', 'Ảnh ngang') . ' 1280*720 px' . '<span style="color: red; margin-left: 2px">*</span>') ?>
                                                    <?php if (Yii::$app->controller->action->id != 're-upload') { ?>

                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary"
                                                                data-toggle="modal"
                                                                data-target="#image-path-modal">
                                                            Select Horizontal Image
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="image-path-modal" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close"><span
                                                                                    aria-hidden="true">&times;</span>
                                                                        </button>
                                                                        <h4 class="modal-title" id="myModalLabel">Select
                                                                            File</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $user_name = Yii::$app->user->identity->username;
                                                                        if ($user_name) {
                                                                            echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video-image']['extensions']);
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">Close
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <img id="img_poster_model_id" width="100%"
                                                         src="<?= $model->getLocalPosterPath() ?>"/>
                                                    <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path'])->label(Yii::t('backend', 'Ảnh dọc')) ?>
                                                    <?php if (Yii::$app->controller->action->id != 're-upload') { ?>
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary"
                                                                data-toggle="modal"
                                                                data-target="#poster-path-modal">
                                                            Select Vertical Image
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="poster-path-modal" tabindex="-1"
                                                             role="dialog"
                                                             aria-labelledby="myModalLabel">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal"
                                                                                aria-label="Close"><span
                                                                                    aria-hidden="true">&times;</span>
                                                                        </button>
                                                                        <h4 class="modal-title" id="myModalLabel">Select
                                                                            File</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <?php
                                                                        $user_name = Yii::$app->user->identity->username;
                                                                        if ($user_name) {
                                                                            echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video-poster-image']['extensions']);
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">Close
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'name', ['selectors' => ['input' => '#csmmedia-name'],])->textInput(['id' => 'csmmedia-name'])->label(Yii::t('backend', 'Tên') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'short_desc')->textarea(['row' => 3])->label(Yii::t('backend', 'Mô tả ngắn')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'description')->textarea(['row' => 5])->label(Yii::t('backend', 'Mô tả')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_download')->textInput()->label(Yii::t('backend', 'Giá tải')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_play')->textInput()->label(Yii::t('backend', 'Giá xem')); ?>
                                        </div>

                                        <div class="static-info">
                                            <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => Yii::t('backend', 'Chọn thời gian xuất bản ...'),
                                                    'value' => ($model->published_at) ? $model->published_at : '',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                                    'todayHighlight' => true,
                                                    'todayBtn' => true
                                                ]
                                            ])->label(Yii::t('backend', 'Thời gian xuất bản')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(
                                                    ApiClient::getUserAccessClients(),
                                                    'id', 'name'
                                                ),
                                                'size' => Select2::MEDIUM,
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn dịch vụ phân phối'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Dịch vụ được phân phối') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'category_list_film')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film),
                                                    'id', 'name'
                                                ),
                                                'size' => Select2::MEDIUM,
                                                'url' => Url::to(['attribute/search-category-film-active']),
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn danh mục'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Category')); ?>
                                        </div>
                                        <?php if (Yii::$app->user->can('film-convert-priority')) { ?>
                                            <div class="static-info">
                                                <?= $form->field($model, 'convert_priority')->textInput()->label(Yii::t('backend', 'Độ ưu tiên convert')); ?>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'film_list')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getFilmByIdsActive($model->film_list),
                                                    'id', 'name'
                                                ),
                                                'url' => Url::to(['attribute/search-film-active']),
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn phim lẻ')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Bộ phim') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'actor_list')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getActorByIdsActive($model->actor_list),
                                                    'id', 'name'
                                                ),
                                                'url' => Url::to(['attribute/search-actor-active']),
                                                'size' => Select2::MEDIUM,
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn diễn viên'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Diễn viên')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'director_list')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getDirectorByIdsActive($model->director_list),
                                                    'id', 'name'
                                                ),
                                                'url' => Url::to(['attribute/search-director-active']),
                                                'size' => Select2::MEDIUM,
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn đạo diễn'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Đạo diễn')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_content_filter')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_content_filter'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn loại nội dung')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Phân loại nội dung')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_year')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_year'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn năm sản xuất')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Năm sản xuất')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_imdb_rating')->input('number', [
                                                'min' => 0,
                                                'max' => 10,
                                                'step' => 0.1
                                            ])->label('IMDB Rating'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_country')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_country'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn nước sản xuất')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Nước sản xuất')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_language')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_language'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn ngôn ngữ phim')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Ngôn ngữ phim')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_subtitle_language')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_language'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn ngôn ngữ phụ đề')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Ngôn ngữ phụ đề')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_copyright')->textInput()->label('Copyright'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_author')->textInput()->label(Yii::t('backend', 'Tác giả')); ?>
                                        </div>

                                        <div class="static-info">
                                            <?php
                                            if (!$model->drm_id || !$model->need_encryption) {
                                                $model->drm_id = 0;
                                            }
                                            ?>
                                            <?= $form->field($model, 'drm_id')->radioList([
                                                0 => Yii::t('backend', 'NO'),
                                                1 => Yii::t('backend', 'DRM'),
                                                2 => Yii::t('backend', 'Multi DRM')
                                            ])->label(Yii::t('backend', 'Is DRM ?')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_4">
                        <?= $form->field($model, 'audio_path')->hiddenInput(['id' => 'csmmedia-audio_path'])->label(false) ?>
                        <div class="form-group">
                            <?= Html::dropDownList('audio_upload_language_code', null, Yii::$app->params['language_code'], ['id' => 'audio_upload_language_code', 'prompt' => Yii::t('backend', '-- Choose audio language --'), 'class' => 'form-control', 'style' => 'width:350px']) ?>
                        </div>
                        <br>
                        <div id="audio_upload">
                        </div>
                        <?php
                        echo FineUploader::widget([
                            'url' => Url::to(['upload/audio-upload', 'type' => 'audio']),
                            'id' => 'audio_upload',
                            'csrf' => true,
                            'renderTag' => false,
                            'preMergeFile' => true,
                            'manualTrigger' => true,
                            'jsOptions' => [
                                'autoUpload' => false,
                                'multiple' => false,
                                'validation' => [
                                    'sizeLimit' => Yii::$app->params['upload']['audio']['maxSize'],
                                    'allowedExtensions' => Yii::$app->params['upload']['audio']['extensions'],
                                ],
                                'messages' => [
                                    'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' ' . Yii::$app->params['upload']['audio']['maxSize'] / 1024 / 1024 . ' MB',
                                    'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                ],
                                'chunking' => [
                                    'enabled' => true,
                                    'partSize' => 20 * 1024 * 1024, // 20MB
                                    'concurrent' => ['enabled' => true],
                                    'success' => ['endpoint' => Url::to(['upload/audio-upload', 'type' => 'audio', 'done' => 1])]
                                ],
                                'resume' => [
                                    'enabled' => true,
                                ],
                                'callbacks' => [
                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                    ),
                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        afterUpload('audio', 'audio_upload', data.filePath, data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                    )
                                ],
                            ]
                        ]);
                        ?>
                        <br>

                        <table class="table table-bordered" id='audio_upload_table'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Language info</th>
                                <th>File preview</th>
                                <th style="width: 110px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach (json_decode($model->audio_path, JSON_UNESCAPED_UNICODE) as $key => $value): ?>
                                <tr id="audio_row_<?= $key ?>">
                                    <td><?= (int)($key + 1) ?></td>
                                    <td>
                                        Language <?= Yii::$app->params['language_code'][$value['language_code']] . ' - ' . $value['language_code'] ?></td>
                                    <td>
                                        <audio controls="">
                                            <source src="<?= $value['file_url'] ?>" type="audio/mpeg">
                                            Your browser does not support the audio
                                        </audio>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger"
                                                onclick="removeRow('audio_upload','audio',<?= $key ?>)"><i
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab_5">
                        <?= $form->field($model, 'subtitle_path')->hiddenInput(['id' => 'csmmedia-subtitle_path'])->label(false) ?>
                        <div class="form-group">
                            <?= Html::dropDownList('subtitle_upload_language_code', null, Yii::$app->params['language_code'], ['id' => 'subtitle_upload_language_code', 'prompt' => Yii::t('backend', '-- Choose subtitle language --'), 'class' => 'form-control', 'style' => 'width:350px']) ?>
                        </div>
                        <br>
                        <div id="subtitle_upload">
                        </div>
                        <?php
                        echo FineUploader::widget([
                            'url' => Url::to(['upload/subtitle-upload', 'type' => 'subtitle']),
                            'id' => 'subtitle_upload',
                            'csrf' => true,
                            'renderTag' => false,
                            'preMergeFile' => true,
                            'manualTrigger' => true,
                            'jsOptions' => [
                                'autoUpload' => false,
                                'multiple' => false,
                                'validation' => [
                                    'sizeLimit' => Yii::$app->params['upload']['subtitle']['maxSize'],
                                    'allowedExtensions' => Yii::$app->params['upload']['subtitle']['extensions'],
                                ],
                                'messages' => [
                                    'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' ' . Yii::$app->params['upload']['subtitle']['maxSize'] / 1024 / 1024 . ' MB',
                                    'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                ],
                                'callbacks' => [
                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                    ),
                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        afterUpload('subtitle', 'subtitle_upload', data.filePath, data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                    )
                                ],
                            ]
                        ]);
                        ?>
                        <br>

                        <table class="table table-bordered" id='subtitle_upload_table'>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Language info</th>
                                <th>File preview</th>
                                <th style="width: 110px">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach (json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE) as $key => $value): ?>
                                <tr id="subtitle_row_<?= $key ?>">
                                    <td><?= (int)($key + 1) ?></td>
                                    <td>
                                        Language <?= Yii::$app->params['language_code'][$value['language_code']] . ' - ' . $value['language_code'] ?></td>
                                    <td><a href="<?= $value['file_url'] ?>" target="_blank">Download file</a></td>
                                    <td>
                                        <button type="button" class="btn btn-danger"
                                                onclick="removeRow('subtitle_upload','subtitle',<?= $key ?>)"><i
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="snackbar"></div>

<?php ActiveForm::end(); ?>

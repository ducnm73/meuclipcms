<?php

use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use common\helpers\I18nHelper;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\Html;
use backend\assets\UploadAsset;
use common\helpers\Helpers ;
use kartik\datetime\DateTimePicker;
UploadAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<ul class="nav nav-tabs nav-tabs-lg">
    <li class="active">
        <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
    </li>
    <li>
        <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
        </a>
    </li>
    <li>
        <a href="#tab_3" data-toggle="tab"><?= Yii::t('backend', 'TV Show Series Information') ?>
        </a>
    </li>
    <li>
        <a href="#tab_4" data-toggle="tab"><?= Yii::t('backend', 'Audio Information') ?>
        </a>
    </li>
    <li>
        <a href="#tab_5" data-toggle="tab"><?= Yii::t('backend', 'Subtitle Information') ?>
        </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab_1">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <video id="videoPlayer" src="<?= $model->getLocalFilePath() ?>"
                                   width="100%"
                                   controls="controls"></video>
                            <?= $form->field($model, 'original_path', ['selectors' => ['input' => '#csmmedia-original_path']])->hiddenInput(['id' => 'csmmedia-original_path'])->label(false); ?>
                        </div>
                        <div class="static-info">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <img width="100%" src="<?= $model->getLocalImagePath() ?>"
                                         id="img_model_id"/>
                                    <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend','Ảnh ngang').' 1280*720 px') ?>
                                    <div id="image_upload">
                                    </div>
                                    <?php
                                    echo FineUploader::widget([
                                        'url' => Url::to(['upload/image-upload', 'type' => 'video-image']),
                                        'id' => 'image_upload',
                                        'csrf' => true,
                                        'isBase' => true,
                                        'renderTag' => false,
                                        'jsOptions' => [
                                            'validation' => [
                                                'image' => [
                                                    'maxWidth' => 1280,
                                                    'minWidth' => 1280,
                                                    'maxHeight' => 720,
                                                    'minHeight' => 720,
                                                ],
                                                'sizeLimit' => Yii::$app->params['upload']['video-image']['maxSize'],
                                                'allowedExtensions' => Yii::$app->params['upload']['video-image']['extensions']
                                            ],
                                            'messages' => [
                                                'minWidthImageError' => Yii::t('backend', 'Phải chọn ảnh có kích thước 1280*720 pixel'),
                                                'maxWidthImageError' => Yii::t('backend', 'Phải chọn ảnh có kích thước 1280*720 pixel'),
                                                'minHeightImageError' => Yii::t('backend', 'Phải chọn ảnh có kích thước 1280*720 pixel'),
                                                'maxHeightImageError' => Yii::t('backend', 'Phải chọn ảnh có kích thước 1280*720 pixel'),
                                                'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['video-image']['maxSize']/1024/1024 . ' MB',
                                                'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                            ],
                                            'callbacks' => [
                                                'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                ),
                                                'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#csmmedia-image_path').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
        var form = $("#my-form"), 
        form_data = form.data("yiiActiveForm");
        $.each(form_data.attributes, function() {
             this.status = 3;
        });
        form.yiiActiveForm("validate");
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                )],
                                        ]
                                    ]);
                                    ?>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <img width="100%" src="<?= $model->getLocalPosterPath() ?>"
                                         id="img_poster_model_id"/>
                                    <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path'])->label(Yii::t('backend','Ảnh dọc')) ?>
                                    <div id="image_poster_upload">
                                    </div>
                                    <?php
                                    echo FineUploader::widget([
                                        'url' => Url::to(['upload/image-upload', 'type' => 'video-poster-image']),
                                        'id' => 'image_poster_upload',
                                        'csrf' => true,
                                        'isBase' => true,
                                        'renderTag' => false,
                                        'jsOptions' => [
                                            'callbacks' => [
                                                'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                ),
                                                'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_poster_upload').hide();
        $('#csmmedia-poster_path').val(data.filePath);
        $("#img_poster_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                )],
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <?= $form->field($model, 'name')->textInput()->label(Yii::t('backend', 'Tên') . '*'); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'short_desc')->textarea(['row' => 3])->label(Yii::t('backend','Mô tả ngắn')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'description')->textarea(['row' => 5])->label(Yii::t('backend', 'Mô tả')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'price_download')->textInput()->label(Yii::t('backend', 'Giá tải')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'price_play')->textInput()->label(Yii::t('backend', 'Giá xem')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                                'options' => [
                                    'placeholder' => Yii::t('backend','Chọn thời gian xuất bản ...'),
                                    'value' => ($model->published_at) ? $model->published_at : '',
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                    'todayHighlight' => true,
                                    'todayBtn' => true
                                ]
                            ])->label(Yii::t('backend', 'Thời gian xuất bản')); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab_2">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(
                                    ApiClient::getUserAccessClients(),
                                    'id', 'name'
                                ),
                                'size' => Select2::MEDIUM,
                                'options' => ['placeholder' => Yii::t('backend','Chọn dịch vụ phân phối'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Dịch vụ được phân phối')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'category_list_film')->widget(Select2Ajax::classname(), [
                                'data' => ArrayHelper::map(
                                    CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film),
                                    'id', 'name'
                                ),
                                'size' => Select2::MEDIUM,
                                'url' => Url::to(['attribute/search-category-film-active']),
                                'options' => ['placeholder' => Yii::t('backend','Chọn danh mục'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label('Category'); ?>
                        </div>
                        <?php if(Yii::$app->user->can('tv-show-convert-priority')){?>
                            <div class="static-info">
                                <?= $form->field($model, 'convert_priority')->textInput()->label(Yii::t('backend','Độ ưu tiên convert')); ?>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab_3">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <?= $form->field($model, 'tv_show_list')->widget(Select2::classname(), [
                                'data' => CsmAttribute::getTreeByType(0, TYPE_ATTRIBUTE_TV_SHOW_SERIES),
                                'options' => ['placeholder' => Yii::t('backend','Chọn TV Show Series')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','TV Show Series')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'episode_no')->textInput()->label(Yii::t('backend','Số tập')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'episode_name')->textInput()->label(Yii::t('backend','Tên tập')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_content_filter')->widget(Select2::classname(), [
                                'data' => Yii::$app->params['meta_content_filter'],
                                'options' => ['placeholder' => Yii::t('backend','Chọn loại nội dung')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Phân loại nội dung')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_year')->widget(Select2::classname(), [
                                'data' => Yii::$app->params['meta_year'],
                                'options' => ['placeholder' => Yii::t('backend','Chọn năm sản xuất')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Năm sản xuất')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_imdb_rating')->input('number', [
                                'min' => 0,
                                'max' => 10,
                                'step' => 0.1
                            ])->label('IMDB Rating'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="portlet box">
                    <div class="portlet-body">
                        <div class="static-info">
                            <?= $form->field($model, 'actor_list')->widget(Select2Ajax::classname(), [
                                'data' => ArrayHelper::map(
                                    CsmAttribute::getActorByIdsActive($model->actor_list),
                                    'id', 'name'
                                ),
                                'url' => Url::to(['attribute/search-actor-active']),
                                'size' => Select2::MEDIUM,
                                'options' => ['placeholder' => Yii::t('backend','Chọn diễn viên'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Diễn viên')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'director_list')->widget(Select2Ajax::classname(), [
                                'data' => ArrayHelper::map(
                                    CsmAttribute::getDirectorByIdsActive($model->director_list),
                                    'id', 'name'
                                ),
                                'url' => Url::to(['attribute/search-director-active']),
                                'size' => Select2::MEDIUM,
                                'options' => ['placeholder' => Yii::t('backend','Chọn đạo diễn'), 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Đạo diễn')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_country')->widget(Select2::classname(), [
                                'data' => Yii::$app->params['meta_country'],
                                'options' => ['placeholder' => Yii::t('backend','Chọn nước sản xuất')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Nước sản xuất')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_language')->widget(Select2::classname(), [
                                'data' => Yii::$app->params['meta_language'],
                                'options' => ['placeholder' => Yii::t('backend','Chọn ngôn ngữ phim')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Ngôn ngữ phim')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_subtitle_language')->widget(Select2::classname(), [
                                'data' => Yii::$app->params['meta_language'],
                                'options' => ['placeholder' => Yii::t('backend','Chọn ngôn ngữ phụ đề')],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                    ]
                                ],
                            ])->label(Yii::t('backend','Ngôn ngữ phụ đề')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_copyright')->textInput()->label(Yii::t('backend','Copyright')); ?>
                        </div>
                        <div class="static-info">
                            <?= $form->field($model, 'meta_author')->textInput()->label(Yii::t('backend','Tác giả')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab_4">
        <?= $form->field($model, 'audio_path')->hiddenInput(['id' => 'csmmedia-audio_path'])->label(false) ?>
        <div class="form-group">
            <?= Html::dropDownList('audio_upload_language_code', null,Helpers::shortArrayAsc(Yii::$app->params['language_code']), ['id' => 'audio_upload_language_code' ,'prompt' => '-- Choose audio language --', 'class' => 'form-control', 'style'=>'width:350px']) ?>
        </div>
        <br>
        <div id="audio_upload">
        </div>
        <?php
        echo FineUploader::widget([
            'url' => Url::to(['upload/audio-upload', 'type' => 'audio']),
            'id' => 'audio_upload',
            'csrf' => true,
            'renderTag' => false,
            'preMergeFile' => true,
            'manualTrigger' => true,
            'jsOptions' => [
                'autoUpload' => false,
                'multiple' => false,
                'validation' => [
                    'sizeLimit' => Yii::$app->params['upload']['audio']['maxSize'],
                    'allowedExtensions' => Yii::$app->params['upload']['audio']['extensions'],
                ],
                'messages' => [
                    'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['audio']['maxSize']/1024/1024 . ' MB',
                    'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                ],
                'chunking' => [
                    'enabled' => true,
                    'partSize' => 20 * 1024 * 1024, // 20MB
                    'concurrent' => [ 'enabled' => true ],
                    'success' => [ 'endpoint' => Url::to(['upload/audio-upload', 'type' => 'audio', 'done' => 1]) ]
                ],
                'resume' => [
                    'enabled' => true,
                ],
                'callbacks' => [
                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                    ),
                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        afterUpload('audio', 'audio_upload', data.filePath, data.fileUrl);
        showSnackbar('Upload successful!',2000);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                    )
                ],
            ]
        ]);
        ?>
        <br>

        <table class="table table-bordered" id = 'audio_upload_table'>
            <thead>
            <tr>
                <th>#</th>
                <th>Language info</th>
                <th>File preview</th>
                <th style="width: 110px">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach (json_decode($model->audio_path, JSON_UNESCAPED_UNICODE) as $key => $value): ?>
                <tr id="audio_row_<?= $key ?>">
                    <td><?= (int)($key + 1) ?></td>
                    <td>Language <?= Yii::$app->params['language_code'][$value['language_code']] . ' - ' . $value['language_code']  ?></td>
                    <td><audio controls=""><source src="<?= $value['file_url'] ?>" type="audio/mpeg">Your browser does not support the audio</audio></td>
                    <td><button type="button" class="btn btn-danger" onclick="removeRow('audio_upload','audio',<?= $key ?>)"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></button></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="tab_5">
        <?= $form->field($model, 'subtitle_path')->hiddenInput(['id' => 'csmmedia-subtitle_path'])->label(false) ?>
        <div class="form-group">
            <?= Html::dropDownList('subtitle_upload_language_code', null,Helpers::shortArrayAsc(Yii::$app->params['language_code']), ['id' => 'subtitle_upload_language_code', 'prompt' => '-- Choose subtitle language --', 'class' => 'form-control', 'style'=>'width:350px']) ?>
        </div>
        <br>
        <div id="subtitle_upload">
        </div>
        <?php
        echo FineUploader::widget([
            'url' => Url::to(['upload/subtitle-upload', 'type' => 'subtitle']),
            'id' => 'subtitle_upload',
            'csrf' => true,
            'renderTag' => false,
            'preMergeFile' => true,
            'manualTrigger' => true,
            'jsOptions' => [
                'autoUpload' => false,
                'multiple' => false,
                'validation' => [
                    'sizeLimit' => Yii::$app->params['upload']['subtitle']['maxSize'],
                    'allowedExtensions' => Yii::$app->params['upload']['subtitle']['extensions'],
                ],
                'messages' => [
                    'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['subtitle']['maxSize']/1024/1024 . ' MB',
                    'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                ],
                'callbacks' => [
                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                    ),
                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        afterUpload('subtitle', 'subtitle_upload', data.filePath, data.fileUrl);
        showSnackbar('Upload successful!',2000);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                    )
                ],
            ]
        ]);
        ?>
        <br>

        <table class="table table-bordered" id = 'subtitle_upload_table'>
            <thead>
            <tr>
                <th>#</th>
                <th>Language info</th>
                <th>File preview</th>
                <th style="width: 110px">Action</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach (json_decode($model->subtitle_path,JSON_UNESCAPED_UNICODE) as $key => $value): ?>
                <tr id="subtitle_row_<?= $key ?>">
                    <td><?= (int)($key + 1) ?></td>
                    <td>Language <?= Yii::$app->params['language_code'][$value['language_code']] . ' - ' . $value['language_code']  ?></td>
                    <td><a href="<?= $value['file_url'] ?>" target="_blank">Download file</a></td>
                    <td><button type="button" class="btn btn-danger" onclick="removeRow('subtitle_upload','subtitle',<?= $key ?>)"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></button></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>

<div id="snackbar"></div>
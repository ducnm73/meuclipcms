<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */

?>

<?php 
$options = ['action' => Url::to('/csm-media-clip-approve/bulk-reject-media')]; 

if(isset($formId) && $formId) {
    $options['options'] = ['id' => $formId];
}

?>

<?php $form = AwsActiveForm::begin($options); ?>
    <div class="portlet light portlet-fit portlet-form bordered csm-media-form">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?= Yii::t('backend', 'Xác nhận từ chối duyệt') ?>
            </span>
            </div>
            <div class="actions">
                <?= AwsBaseHtml::submitButton(Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                        onclick="history.back(-1)">
                    <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet box">
                            <div class="portlet-body">
                                <?php foreach ($model->media_ids as $media_id): ?>
                                    <?= Html::activeLabel($model, 'reject_reason') ?>
                                    <?= Html::hiddenInput($model->formName() . '[media_ids][]', $media_id) ?>
                                <?php endforeach ?>
                                <?= $form->field($model, 'reject_reason')->textInput() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php AwsActiveForm::end(); ?>
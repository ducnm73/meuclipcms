<?php

use awesome\backend\form\AwsActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5><?php echo Yii::t('backend', 'Từ chối duyệt') ?></h5>
</div>
<?php if (!isset($result)): ?>
    <?php $form = AwsActiveForm::begin([
        'id' => $formId,
        'action' => Url::to('/csm-media-clip-approve/bulk-reject-media'),
        'enableClientScript' => false,
        // 'enableAjaxValidation' => true,
        // 'options' => [
        //     'data-pjax' => true,
        // ]
    ]); ?>
        <div class="modal-body">
            <?php foreach ($model->media_ids as $media_id): ?>
                <?= Html::hiddenInput($model->formName() . '[media_ids][]', $media_id) ?>
            <?php endforeach ?>
            <?= $form->field($model, 'reject_reason')->textArea() ?>
        </div>
        <div class="modal-footer">
            <?= Html::submitButton(Yii::t('backend','Lưu'), ['class' => 'btn btn-default']) ?>
        </div>
    <?php AwsActiveForm::end(); ?>
<?php else: ?>
        <div class="modal-body">
            <?php echo $result['message'] ?>
        </div>
        <div class="modal-footer">
            <?= Html::button(Yii::t('backend', 'Đóng'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
        </div>
<?php endif ?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMediaClipApproveSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="csm-media-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

<!--    --><?//= $form->field($model, 'slug') ?>

<!--    --><?//= $form->field($model, 'short_desc') ?>
<!---->
<!--    --><?//= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'price_download') ?>

    <?php // echo $form->field($model, 'price_play') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'max_quantity') ?>

    <?php // echo $form->field($model, 'published_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'published_at') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'resolution') ?>

    <?php // echo $form->field($model, 'attributes') ?>

    <?php // echo $form->field($model, 'cp_id') ?>

    <?php // echo $form->field($model, 'cp_info') ?>

    <?php // echo $form->field($model, 'original_path') ?>

    <?php // echo $form->field($model, 'image_path') ?>

    <?php // echo $form->field($model, 'poster_path') ?>

    <?php // echo $form->field($model, 'file_type') ?>

    <?php // echo $form->field($model, 'convert_status') ?>

    <?php // echo $form->field($model, 'convert_path') ?>

    <?php // echo $form->field($model, 'convert_priority') ?>

    <?php // echo $form->field($model, 'convert_start_time') ?>

    <?php // echo $form->field($model, 'convert_end_time') ?>

    <?php // echo $form->field($model, 'convert_data_id') ?>

    <?php // echo $form->field($model, 'convert_images') ?>

    <?php // echo $form->field($model, 'meta_info') ?>

    <?php // echo $form->field($model, 'censored_info') ?>

    <?php // echo $form->field($model, 'logo_path') ?>

    <?php // echo $form->field($model, 'need_censored') ?>

    <?php // echo $form->field($model, 'seo_title') ?>

    <?php // echo $form->field($model, 'seo_description') ?>

    <?php // echo $form->field($model, 'seo_keywords') ?>

    <?php // echo $form->field($model, 'is_crawler') ?>

    <?php // echo $form->field($model, 'crawler_id') ?>

    <?php // echo $form->field($model, 'crawler_info') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'reviewed_by') ?>

    <?php // echo $form->field($model, 'published_list') ?>

    <?php // echo $form->field($model, 'tag') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

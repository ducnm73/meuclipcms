<?php
use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\components\common\MultipleLanguage;
use backend\models\CsmAttribute;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
$this->registerJsFile("/js/upload.utils.js");
?>

<?php $form = AwsActiveForm::begin(['id' => 'my-form']); ?>

    <div class="portlet light portlet-fit portlet-form bordered csm-media-form">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
            </div>
            <div class="actions">
                <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                        onclick="history.back(-1)">
                    <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="tabbable-line" id="video-tabs">
                    <ul class="nav nav-tabs nav-tabs-lg">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-md-offset-9">
                                <?= Select2::widget([
                                    'data' => common\helpers\I18nHelper::getI18nBackendParams(
                                        array_intersect_key(Yii::$app->params['language_code'], array_flip(Yii::$app->params['languagepool']))
                                    ),
                                    'name' => 'kv-repo-template',
                                    'value' => Yii::$app->params['mainLanguage'],
                                    'options' => [
                                        'placeholder' => Yii::t('backend', 'Chọn ngôn ngữ'),
                                        'id' => 'formLangSelect',
                                    ],
                                    'pluginOptions' => ['allowClear' => false]
                                ]); ?>
                            </div>
                        </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <video id="videoPlayer" src="<?= $model->getLocalFilePath() ?>"
                                                       width="100%"
                                                       controls="controls">

                                                </video>
                                            </div>

                                            <?= $form->field($model, 'watched_duration')->hiddenInput(['id' => 'watched_duration_id','value'=> 0])->label(false)?>
                                            <div class="static-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <img width="100%" src="<?= $model->getLocalImagePath() ?>"

                                                             id="img_model_id"/>
                                                        <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend', 'Ảnh ngang') . ' 1280*720 px' . '<span style="color: red; margin-left: 2px">*</span>') ?>
                                                        <div id="image_upload">
                                                        </div>
                                                        <?php
                                                        echo FineUploader::widget([
                                                            'url' => Url::to(['upload/image-upload', 'type' => 'video-image']),
                                                            'id' => 'image_upload',
                                                            'csrf' => true,
                                                            'isBase' => true,
                                                            'renderTag' => false,
                                                            'jsOptions' => [
                                                                'callbacks' => [
                                                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                    ),
                                                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#csmmedia-image_path').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
        var form = $("#my-form"), 
        form_data = form.data("yiiActiveForm");
        $.each(form_data.attributes, function() {
             this.status = 3;
        });
        form.yiiActiveForm("validate");
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                    )],
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <img width="100%" src="<?= $model->getLocalPosterPath() ?>"

                                                             id="img_poster_model_id"/>
                                                        <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path']) ?>
                                                        <div id="image_poster_upload">
                                                        </div>
                                                        <?php
                                                        echo FineUploader::widget([
                                                            'url' => Url::to(['upload/image-upload', 'type' => 'video-poster-image']),
                                                            'id' => 'image_poster_upload',
                                                            'csrf' => true,
                                                            'isBase' => true,
                                                            'renderTag' => false,
                                                            'jsOptions' => [
                                                                'callbacks' => [
                                                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                    ),
                                                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_poster_upload').hide();
        $('#csmmedia-poster_path').val(data.filePath);
        $("#img_poster_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                    )],
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= MultipleLanguage::widget([
                                                    'model' => $model,
                                                    'form' => $form,
                                                    'formOptions' => [
                                                        'selectors' => ['input' => '#csmmedia-name']
                                                    ],
                                                    'attribute' => 'name',
                                                    'label' => Yii::t('backend', 'Name'),
                                                    'inputType' => [
                                                        'type' => MultipleLanguage::TEXT_INPUT,
                                                        'options' => ['id' => 'csmmedia-name']
                                                    ]
                                                ]) ?>
                                            </div>
                                            <div class="static-info">
                                                <?= MultipleLanguage::widget([
                                                    'model' => $model,
                                                    'form' => $form,
                                                    'attribute' => 'short_desc',
                                                    'label' => Yii::t('backend', 'Short Desc'),
                                                    'inputType' => [
                                                        'type' => MultipleLanguage::TEXT_AREA,
                                                        'options' => ['row' => 3]
                                                    ]
                                                ]) ?>
                                            </div>
                                            <div class="static-info">
                                                <?= MultipleLanguage::widget([
                                                    'model' => $model,
                                                    'form' => $form,
                                                    'attribute' => 'description',
                                                    'label' => Yii::t('backend', 'Description'),
                                                    'inputType' => [
                                                        'type' => MultipleLanguage::TEXT_AREA,
                                                        'options' => ['row' => 5]
                                                    ]
                                                ]) ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'price_download')->textInput(); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'price_play')->textInput(); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                                                    'options' => [
                                                        'placeholder' => Yii::t('backend','Chọn thời gian xuất bản ...'),
                                                        'value' => ($model->published_at) ? $model->published_at : '',
                                                    ],
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'yyyy-mm-dd hh:ii:ss',
                                                        'todayHighlight' => true,
                                                        'todayBtn' => true
                                                    ]
                                                ])->label(Yii::t('backend', 'Thời gian xuất bản')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'media_client_status')->widget(Select2::classname(), [
                                                    'data' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-approved-status']),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => [
                                                        'placeholder' => Yii::t('backend', 'Trạng thái'),
                                                        'multiple' => false,
                                                        'data-toggle' => 'toggle-visible',
                                                        'data-toggle-value' => STATUS_MEDIA_REJECTED,
                                                        'data-target' => '.reject-reason',
                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend', 'Trạng thái')); ?>
                                            </div>
                                            <div class="static-info reject-reason" style='display:none;'>
                                                <?= $form->field($model, 'reject_reason')->textInput()->label(Yii::t('backend', 'Lý do')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'category_list')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(
                                                        CsmAttribute::getAllCategoryActive(),
                                                        'id', 'name'
                                                    ),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => ['placeholder' => Yii::t('backend', 'Chọn danh mục'), 'multiple' => true],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend', 'Category')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_content_filter')->widget(Select2::classname(), [
                                                    'data' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['meta_content_filter']),
                                                    'options' => ['placeholder' => Yii::t('backend', 'Chọn loại nội dung')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend', 'Phân loại nội dung')); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= MultipleLanguage::widget([
                                                    'model' => $model,
                                                    'form' => $form,
                                                    'attribute' => 'seo_title',
                                                    'label' => Yii::t('backend', 'Seo Title'),
                                                    'inputType' => [
                                                        'type' => MultipleLanguage::TEXT_AREA,
                                                        'options' => ['row' => 3]
                                                    ]
                                                ]) ?>
                                            </div>
                                            <div class="static-info">
                                                <?= MultipleLanguage::widget([
                                                    'model' => $model,
                                                    'form' => $form,
                                                    'attribute' => 'seo_description',
                                                    'label' => Yii::t('backend', 'Seo Description'),
                                                    'inputType' => [
                                                        'type' => MultipleLanguage::TEXT_AREA,
                                                        'options' => ['row' => 7]
                                                    ]
                                                ]) ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php AwsActiveForm::end(); ?>

<script>
    var player = document.getElementById("videoPlayer");
    var watchedDuration = 0;
    var timestamps = [];
    var curTime = 0;
    player.ontimeupdate = function () {
        curTime = +player.currentTime.toFixed(0);
        if (timestamps.indexOf(curTime) < 0) {
            timestamps.push(curTime);
        }
        watchedDuration = timestamps.length - 1;
        $('#watched_duration_id').val(watchedDuration);
    };
</script>

        <?php
        $nameArray = explode("\\", get_class($model));
        $this->registerJs("
const attrArray = ['multiple_language', 'name','short_desc', 'description', 'seo_title', 'seo_description'];
const modelClassName = '" . end($nameArray) . "';

const selectId = '#formLangSelect';
const defaultLang = '" . Yii::$app->params['mainLanguage'] . "';
var langPool = '" . Yii::$app->params['languagepool'] . "';
var formLang = '" . Yii::$app->params['mainLanguage'] . "';

function displayTranslateField(changeLang) {
    let fieldList = $('.form-group').filter(function () {
        let className = $(this).attr('class');
        for (const ele of attrArray) {
            if (className.includes(modelClassName.toLowerCase() + '-' + ele))
                return $(this);
        }
    });
    fieldList.each(function () { 
        $(this).hide();
        let lastClassName = $(this).attr('class');
        if (changeLang == defaultLang) {
            if (!lastClassName.includes(modelClassName.toLowerCase() + '-multiple_language'))
                $(this).show();
        } else {
            if (lastClassName.includes(modelClassName.toLowerCase() + '-multiple_language-' + changeLang))
                $(this).show();
        }
    });
}
displayTranslateField(formLang);
$(selectId).on('change', function() {
//    if( !$('#csmattribute-name').val() ) {
//        $(selectId).val(formLang);
//        alert('Empty name');
//    }

    displayTranslateField(this.value);
    formLang = this.value;
});"
);
?>

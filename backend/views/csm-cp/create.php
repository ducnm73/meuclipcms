<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmCp */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Csm Cp',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Csm Cps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-cp-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

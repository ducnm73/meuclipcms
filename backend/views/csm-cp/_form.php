<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use backend\models\ApiClient;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmCp */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = AwsActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered csm-cp-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
                </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                    onclick="history.back(-1)">
                <i class="fa fa-angle-left"></i> Back
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="portlet box">
                        <div class="portlet-body">
                            <div class="static-info">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
                            </div>
                            <div class="static-info">
                                <?= $form->field($model, 'cp_code')->textInput(['maxlength' => 30]) ?>
                            </div>
                            <div class="static-info">
                                <?= $form->field($model, 'contact')->textInput(['maxlength' => 255]) ?>
                            </div>
                            <div class="static-info">
                                <?= $form->field($model, 'address')->textInput(['maxlength' => 255]) ?>
                            </div>
                            <div class="static-info">
                                <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet box">
                            <div class="portlet-body">
                                <div class="static-info">
                                    <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(
                                            ApiClient::getAll(),
                                            'id', 'name'
                                        ),
                                        'size' => Select2::MEDIUM,
                                        'options' => ['placeholder' => Yii::t('backend','Chọn dịch vụ phân phối'), 'multiple' => true],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                        'addon' => [
                                            'prepend' => [
                                                'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                            ]
                                        ],
                                    ])->label(Yii::t('backend','Dịch vụ được phân phối')); ?>
                                </div>
                                <div class="static-info">
                                    <?= $form->field($model, 'start_time')->widget(DatePicker::classname(), [
                                        'language' => Yii::$app->language,
                                        'dateFormat' => 'yyyy-MM-dd',
                                    ])->label('Start time'); ?>
                                </div>
                                <div class="static-info">
                                    <?= $form->field($model, 'end_time')->widget(DatePicker::classname(), [
                                        'language' => Yii::$app->language,
                                        'dateFormat' => 'yyyy-MM-dd',
                                    ])->label('End time'); ?>
                                </div>
                                <div class="static-info">
                                    <?= $form->field($model, 'is_active')->checkbox() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php AwsActiveForm::end(); ?>

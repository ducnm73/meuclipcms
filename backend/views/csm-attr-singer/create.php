<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmAttribute */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Singer',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Singers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-attribute-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

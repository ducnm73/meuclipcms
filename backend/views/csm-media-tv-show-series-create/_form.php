<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php  $form = AwsActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered csm-media-form">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?=  $title ?>
                </span>
            </div>
            <div class="actions">
                <?=  AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                        onclick="history.back(-1)">
                    <i class="fa fa-angle-left"></i> Back
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'short_desc')->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'price_download')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'price_play')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'max_quantity')->textInput() ?>

    <?= $form->field($model, 'published_by')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'published_at')->textInput() ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'resolution')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'attributes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cp_id')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'cp_info')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'original_path')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'image_path')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'poster_path')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'file_type')->textInput() ?>

    <?= $form->field($model, 'convert_status')->textInput() ?>

    <?= $form->field($model, 'convert_path')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'convert_priority')->textInput() ?>

    <?= $form->field($model, 'convert_start_time')->textInput() ?>

    <?= $form->field($model, 'convert_end_time')->textInput() ?>

    <?= $form->field($model, 'convert_data_id')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'convert_images')->textInput(['maxlength' => 2560]) ?>

    <?= $form->field($model, 'meta_info')->textInput(['maxlength' => 4096]) ?>

    <?= $form->field($model, 'censored_info')->textInput(['maxlength' => 4096]) ?>

    <?= $form->field($model, 'logo_path')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'need_censored')->textInput() ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'seo_description')->textInput(['maxlength' => 500]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 2048]) ?>

    <?= $form->field($model, 'is_crawler')->textInput() ?>

    <?= $form->field($model, 'crawler_id')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'crawler_info')->textInput(['maxlength' => 2048]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'reviewed_by')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'published_list')->textInput(['maxlength' => 2048]) ?>

    <?= $form->field($model, 'tag')->textarea(['rows' => 6]) ?>

            </div>
        </div>
    </div>

<?php AwsActiveForm::end(); ?>

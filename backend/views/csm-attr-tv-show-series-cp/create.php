<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmAttribute */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'TV Show Series',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'TV Show Series'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-attribute-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

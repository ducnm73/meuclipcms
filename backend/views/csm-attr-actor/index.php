<?php

use awesome\backend\actionBar\ActionBar;
use awesome\backend\grid\AwsGridView;
use awesome\backend\toast\AwsAlertToast;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmAttrActorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Actors');
$this->params['breadcrumbs'][] = $this->title;
$gridId = 'csm_media_grid';
$pjaxContainer = 'mainGridPjax';
?>
<div class="row csm-attribute-index">
    <div class="col-md-12">
        <?php
        Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => $pjaxContainer]);
        ?>
        <?=
        AwsAlertToast::widget([
        ]);
        ?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => 'Actor',
                    ]),
                        ['create'], ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                    <?= ActionBar::widget([
                        'grid' => $gridId,
                        'pjax' => true,
                        'pjaxContainer' => '#' . $pjaxContainer,
                        'options' => ['class' => 'btn-group'],
                        'containerOptions' => ['class' => ''],
                        'templates' => [
                            '{bulk-actions}' => ['class' => 'btn-group'],
                        ],
                        'bulkActionsItems' => [
                            'Update Status' => [
                                'status-activate' => 'Activate',
                                'status-deactivate' => 'Deactivate',
                            ],
                            'General' => [
                                'general-delete' => 'Delete'
                            ],
                        ],
                        'bulkActionsOptions' => [
                            'options' => [
                                'status-activate' => [
                                    'url' => Url::toRoute(['update-status', 'status' => 'status-activate']),
                                    'data-confirm' => 'Are you sure?',
                                ],
                                'status-deactivate' => [
                                    'url' => Url::toRoute(['update-status', 'status' => 'status-deactivate']),
                                    'data-confirm' => 'Are you sure?',
                                ],
                                'general-delete' => [
                                    'url' => Url::toRoute('delete-multiple'),
                                    'data-confirm' => 'Are you sure?',
                                ],
                            ],
                            'class' => 'form-control',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?= AwsGridView::widget([
                        'id' => $gridId,
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\CheckboxColumn'],
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'name',
                            'image_path:imageStorage',
                            'second_image:imageStorage',
                            'is_active:bool',
                            'created_at:vnDatetime',
                            'updated_at:vnDatetime',

                            ['class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'update' => function ($model, $key, $index) {
                                        return $model->is_active === INACTIVE ;
                                    },
                                    'delete' => function ($model, $key, $index) {
                                        return $model->is_active === INACTIVE ;
                                    }
                                ]
                                ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <?php
        Pjax::end();
        ?>
    </div>
</div>

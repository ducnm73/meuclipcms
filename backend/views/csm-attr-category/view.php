<?php

use awesome\backend\widgets\AwsBaseHtml;
use kartik\select2\Select2;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmAttribute */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-attribute-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-md-offset-9">
                <?= Select2::widget([
                    'data' => common\helpers\I18nHelper::getI18nBackendParams(
                        array_intersect_key(Yii::$app->params['language_code'], array_flip(Yii::$app->params['languagepool']))
                    ),
                    'name' => 'kv-repo-template',
                    'value' => Yii::$app->params['mainLanguage'],
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Chọn ngôn ngữ'),
                        'id' => 'formLangSelect',
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                ]);

                $this->registerJs("
                    const recordId = '" . $model->id . "';
                    $('#formLangSelect').on('change', function() {
                        $.ajax({
                            type:'GET', 
                            url: `/csm-attr-category/view?id=\${recordId}&lang=\${this.value}`, 
                            success: function(data) {
                                $('.portlet-body').replaceWith($('.portlet-body', data));
                            }, 
                            error: function(jqXHR, textStatus, errorThrown) {
                                alert(jqXHR.status);
                            },
                            dataType: 'html'
                        });
                    });
                "); ?>
            </div>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
//                        'slug',
                        'description:ntext',
//                        'image_path',
                        [
                            'attribute' => 'image_path',
                            'value' => $model->bindImagePath(),
                            'format' => ['image', ['width' => '240']]
                        ],
//                        'type',
                        'is_active:boolean',
                        'created_at',
//                        'created_by',
                        'updated_at',
//                        'updated_by',
//                        'media_list:ntext',
//                        'parent_id',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

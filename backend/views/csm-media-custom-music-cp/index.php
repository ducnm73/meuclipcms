<?php


use awesome\backend\grid\AwsGridView;
use awesome\backend\widgets\AwsBaseHtml;
use backend\models\ApiClient;
use backend\models\User;
use backend\models\CsmMediaActionLog;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaMusicCpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Csm Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => true, 'id' => 'mainGridPjax']);
                    ?>

                    <?= AwsGridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'header' => Yii::t('backend','Media ID'),
                                'attribute' => 'id',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return $data->media->id;
                                }

                            ],
                            [
                                'header' => Yii::t('backend','Tên'),
                                'attribute' => 'name',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return Html::encode($data->media->name);
                                }
                            ],
                            [
                                'attribute' => 'image_path',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return "<img width='80' src='" . $data->media->getLocalImagePath() . "' />";
                                }
                            ],
                            [
                                'header' => Yii::t('backend','Trạng thái file'),
                                'attribute' => 'status',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status'][$data->media->status]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'attribute' => 'updated_at',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return date('d/m/Y', strtotime($data->media->updated_at));
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value' => date('Y-m-d 0:i:s') . ' - ' . date('Y-m-d H:i:s'),
                                    'convertFormat' => true,
                                    'options' => [
                                        'placeholder' => Yii::t('backend','Select range')
                                    ],
                                    'pluginOptions' => [
                                        'timePicker' => true,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'header' => Yii::t('backend','Trạng thái convert'),
                                'attribute' => 'convert_status',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['convert-status'][$data->media->convert_status]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['convert-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
//                            [
//                                'header' => Yii::t('backend','Tự động?'),
//                                'attribute' => 'is_crawler',
//                                'content' => function ($data) {
//                                    /* @var \backend\models\CsmMediaPublish $data */
//                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option'][$data->media->is_crawler]);
//                                },
//                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
//                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option']),
//                                'filterWidgetOptions' => [
//                                    'pluginOptions' => ['allowClear' => true],
//                                ],
//                                'filterInputOptions' => [
//                                    'placeholder' => Yii::t('backend','Tự động?'),
////                                    'multiple' => true
//                                ],
//                            ],
//                            'duration:mediaDuration',
                            // 'price_download',
                            // 'price_play',
                            // 'type',
//                            [
//                                'header' => Yii::t('backend','Chất lượng'),
//                                'attribute' => 'max_quantity',
//                                'content' => function ($data) {
//                                    /* @var \backend\models\CsmMediaPublish $data */
//                                    return $data->media->max_quantity;
//                                }
//                            ],
                            // 'published_by',

                            [
                                'header' => Yii::t('backend','Resolution'),
                                'attribute' => 'resolution',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return $data->media->resolution;
                                }

                            ],
//                            'updated_at',
//                            'published_at',
//                             'resolution',
//                            'attributes:jsonNameType',
                            // 'cp_id',
                            // 'cp_info',
                            // 'original_path',
                            // 'file_type',
                            // 'convert_path',
                            // 'convert_priority',
                            // 'convert_start_time',
                            // 'convert_end_time',
                            // 'tag:ntext',
                            // 'seo_title',
                            // 'seo_description',
                            // 'seo_keywords',
//                            'is_crawler:bool',
                            // 'crawler_id',
                            // 'crawler_info',
                            // 'created_by',
                            // 'reviewed_by',
//                            [
//                                'header' => 'Category',
//                                'attribute' => 'category_list',
//                                'content' => function ($data) {
//                                    /* @var \backend\models\CsmMediaPublish $data */
//                                    if ($value = $data->media->attributes) {
//                                        $decode = json_decode($value, true);
//                                        if (count($decode)) {
//                                            $res = [];
//                                            foreach ($decode as $obj) {
//                                                if ($obj['type'] == TYPE_ATTRIBUTE_CATEGORY)
//                                                    $res[] = $obj['name'];
//                                            }
//                                            return implode(", ", $res);
//                                        } else if ($decode) {
//                                            return $decode['name'];
//                                        }
//                                    }
//                                    return "";
//                                },
//                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
//                                'filter' => ArrayHelper::map(CsmAttribute::getAllCategoryActive(), 'id', 'name'),
//                                'filterWidgetOptions' => [
//                                    'pluginOptions' => ['allowClear' => true],
//                                ],
//                                'filterInputOptions' => [
//                                    'placeholder' => 'Chọn danh mục',
////                                    'multiple' => true
//                                ]
//                            ],
                            [
                                'header' => Yii::t('backend','Dịch vụ'),
                                'attribute' => 'client_list',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return $data->client->name;
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => ArrayHelper::map(ApiClient::getAll(), 'id', 'name'),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn dịch vụ'),
//                                    'multiple' => true
                                ]
                            ],
                            [
                                'header' => Yii::t('backend','Trạng thái DV'),
                                'attribute' => 'media_client_status',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['service-status'][$data->status]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['service-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
//                            'published_list:jsonName',
//                            ['class' => 'yii\grid\ActionColumn'],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{viewReason}',
                                'buttons' => [
                                    'viewReason' => function ($url, $model) {
                                        if($model->status != STATUS_MEDIA_REJECTED) {
                                            return '';
                                        }

                                        $log = CsmMediaActionLog::find()->where(['media_id' => $model->media->id, 'created_by' => $model->published_by])->orderBy(['created_at' => SORT_DESC])->one();

                                        if($log) {
                                            $url = Url::to(['/site/view-reject-reason', 'id' => $log->id]);
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                                    'title' => Yii::t('backend','Xem lý do từ chối'),
                                                    'data-pjax' => '0',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#view-reject-reason-modal',
                                            ]);
                                        } else {
                                            return '';
                                        }
                                    },
                                ],
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->render('/csm-media-clip-cp/_view_reject_reason') ?>
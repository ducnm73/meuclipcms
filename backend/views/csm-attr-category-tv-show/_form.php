<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\CsmAttribute;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmAttribute */
/* @var $title string */
/* @var $form AwsActiveForm */

$parentItems = $model->isNewRecord ? CsmAttribute::getTreeByType(0, TYPE_ATTRIBUTE_CATEGORY_TV_SHOW) :
    CsmAttribute::getTreeByType($_REQUEST['id'], TYPE_ATTRIBUTE_CATEGORY_TV_SHOW);
//$parentItems = [];
?>

<?php $form = AwsActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered csm-attribute-form">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
                </span>
            </div>
            <div class="actions">
                <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                        onclick="history.back(-1)">
                    <i class="fa fa-angle-left"></i> Back
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'is_active')->checkbox() ?>

                        <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
                            'data' => $parentItems,
                            'options' => ['placeholder' => Yii::t('backend','Chọn thuộc tính cha')],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'addon' => [
                                'prepend' => [
                                    'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                ]
                            ],
                        ])->label(Yii::t('backend','Thuộc tính cha')); ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                        <div style="row">
                            <div class="col-md-6 col-sm-12">
                                <label class="control-label" for="csmattribute-image_path" style="width: 100%;"><?=Yii::t('backend','Ảnh đại diện')?></label>
                                <img id="img_model_id" src="<?= $model->bindImagePath() ?>"
                                     width="100px" <?= $model->image_path ? '' : 'style="display: none"' ?>/>
                                <?= $form->field($model, 'image_path')->hiddenInput(['id' => 'image_path_id'])->label("") ?>
                                <div id="image_upload">
                                </div>
                                <?php
                                echo FineUploader::widget([
                                    'url' => Url::to(['upload/image-upload', 'type' => 'attribute-image']),
                                    'id' => 'image_upload',
                                    'csrf' => true,
                                    'isBase' => true,
                                    'renderTag' => false,
                                    'jsOptions' => [
                                        'callbacks' => [
                                            'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                            ),
                                            'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#image_path_id').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                            )],
                                    ]
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php AwsActiveForm::end(); ?>
<?php

use yii\widgets\DetailView;
use backend\models\User;

?>
<div class="row csm-media-action-log">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-body">
            	<?php if ($model): ?>
            		
            	<?php else: ?>
            		<p><?php echo Yii::t('backend', 'Không có dữ liệu') ?></p>
            	<?php endif ?>
                <?= DetailView::widget([
	                'model' => $model,
	                'attributes' => [
	                    'media_id',
						[
							'attribute' => 'action',
							'value' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-action-log'][$model->action]),
						],
						'created_at',
						[
							'attribute' => 'created_by',
							'value' => !($user = User::findOne($model->created_by)) ? '' : $user->username,
						],
						'note',
	                ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
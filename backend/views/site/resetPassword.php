<?php

/* @var $this yii\web\View */
/* @var $form awesome\backend\form\AwsActiveForm */
/* @var $model \backend\models\ResetPasswordForm */


use awesome\backend\form\AwsActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = Yii::t('backend', 'Reset Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = AwsActiveForm::begin(['id' => 'change-password-form']); ?>
<?= $form->field($model, 'username')->textInput([
    "class" => "form-control form-control-solid placeholder-no-fix",
    "autocomplete" => "off",
    "disabled" => "disabled",
    "placeholder" => Html::encode(Yii::t('backend', "Username"))
])->label(false); ?>
<?= $form->field($model, 'password')->passwordInput([
    "class" => "form-control form-control-solid placeholder-no-fix",
    "autocomplete" => "off",
    "placeholder" => Html::encode(Yii::t('backend', "Password"))
])->label(false); ?>
<?= $form->field($model, 're_password')->passwordInput([
    "class" => "form-control form-control-solid placeholder-no-fix",
    "autocomplete" => "off",
    "placeholder" => Html::encode(Yii::t('backend', "Re Type Password"))
])->label(false); ?>
<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
    'template' => '{input} {image}',
    'options' => [
        "class" => 'form-control form-control-solid placeholder-no-fix',
        "autocomplete" => "off",
        "style" => "float:left; width:100px;",
        "placeholder" => Html::encode(Yii::t('backend', "Captcha")),
    ],
])->label(false); ?>
<div class="form-actions noborder">
    <?= Html::submitButton(Yii::t('backend', 'Confirm'), ['class' => 'btn red uppercase', 'name' => 'login-button']) ?>
</div>
<?php AwsActiveForm::end(); ?>

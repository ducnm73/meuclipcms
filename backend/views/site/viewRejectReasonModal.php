<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h5><?php echo Yii::t('backend', 'Xem lý do từ chối duyệt') ?></h5>
</div>
<div class="modal-body">
    <?php echo $this->render('viewRejectReason', ['model' => $model]) ?>
</div>
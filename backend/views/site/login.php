<?php

/* @var $this yii\web\View */
/* @var $form awesome\backend\form\AwsActiveForm */
/* @var $model \backend\models\LoginForm */

//use awesome\backend\form\AwsActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('backend', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<!---->
<!--<div class="pull-left lock-avatar-block">-->
<!--    <img src="/img/photo3.jpg" class="lock-avatar">-->
<!--</div>-->
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
<?= $form->field($model, 'username')->textInput([
    "class" => "form-control form-control-solid placeholder-no-fix",
    "autocomplete" => "off",
    "placeholder" => Html::encode(Yii::t('backend', "Username"))
])->label(false) ?>
<?= $form->field($model, 'password')->passwordInput([
    "class" => "form-control form-control-solid placeholder-no-fix",
    "autocomplete" => "off",
    "placeholder" => Html::encode(Yii::t('backend', "Password"))
])->label(false); ?>
<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
    'template' => '{input} {image}',
    'options' => [
        "class" => 'form-control form-control-solid placeholder-no-fix',
        "autocomplete" => "off",
        'enableClientScript' => false,
        "placeholder" => Html::encode(Yii::t('backend', "Captcha")),
        "style" => "float:left; width: 50%;"
    ],
])->label(false); ?>
<div class="form-actions noborder">
    <?= Html::submitButton('Login', ['class' => 'btn red uppercase', 'name' => 'login-button', "style" => "width:70px; float:left"]) ?>
</div>
<?php ActiveForm::end(); ?>

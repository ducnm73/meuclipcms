<?php

use awesome\backend\actionBar\ActionBar;
use awesome\backend\grid\AwsGridView;
use awesome\backend\toast\AwsAlertToast;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaTrailerSeriesCreateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->registerJs('plugins/jquery-ui/jquery-ui.min.js');
$this->registerJsFile('/plugins/bootstrap-table/bootstrap-table.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/table-bootstrap.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('backend', 'Film Series Trailer FTP');
$this->params['breadcrumbs'][] = $this->title;
$gridId = 'csm_media_grid';
$pjaxContainer = 'mainGridPjax';

?>
<div class="row csm-media-index" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <?php
        Pjax::begin(['formSelector' => 'form', 'enablePushState' => true, 'id' => $pjaxContainer]);
        ?>
        <?=
        AwsAlertToast::widget([
        ]);
        ?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <div class="actions">
                        <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                            'modelClass' => 'Film Series',
                        ]),
                            ['create'], ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                        <?= ActionBar::widget([
                            'grid' => $gridId,
                            'pjax' => true,
                            'pjaxContainer' => '#' . $pjaxContainer,
                            'options' => ['class' => 'btn-group'],
                            'containerOptions' => ['class' => ''],
                            'templates' => [
                                '{bulk-actions}' => ['class' => 'btn-group'],
                            ],
                            'bulkActionsItems' => [
                                'Update Status' => [
                                    'status-reviewed' => 'Reviewed Proposal',
                                ],
                            ],
                            'bulkActionsOptions' => [
                                'options' => [
                                    'status-reviewed' => [
                                        'url' => Url::toRoute(['update-status', 'status' => 'reviewed-proposal']),
                                        'data-confirm' => 'Are you sure?',
                                    ],
                                ],
                                'class' => 'form-control',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <?= AwsGridView::widget([
                        'id' => $gridId,
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['class' => 'yii\grid\CheckboxColumn'],
                            'id',
                            [
                                'attribute' => 'name',
                                'headerOptions' => ['style' => 'max-width:300px;word-wrap: break-word;'],
                                'contentOptions' => ['style' => 'max-width:300px;word-wrap: break-word;'],
                            ],
                            [
                                'attribute' => 'image_path',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return '<img width=\'80\' src=\'' . $data->getLocalImagePath() . '\' />';
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'content' => function ($data) {
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status'][$data->status]);
                                }
                            ],
                            // 'price_download',
                            // 'price_play',
                            // 'type',
//                            'max_quantity',
                            // 'published_by',
                            [
                                'attribute' => 'created_at',
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value' => date('Y-m-d 0:i:s') . ' - ' . date('Y-m-d H:i:s'),
                                    'convertFormat' => true,
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => Yii::t('backend','Select range')
                                    ],
                                    'pluginOptions' => [
                                        'timePicker' => false,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ],
                            ],
                            [
                                'attribute' => 'updated_at',
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value' => date('Y-m-d 0:i:s') . ' - ' . date('Y-m-d H:i:s'),
                                    'convertFormat' => true,
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => Yii::t('backend','Select range')
                                    ],
                                    'pluginOptions' => [
                                        'timePicker' => false,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ],
                            ],
                            'duration:mediaDuration',
                            'resolution',
//                            'published_at',

                            'attributes:jsonNameType',
                            // 'cp_id',
                            // 'cp_info',
                            // 'original_path',
                            // 'file_type',
                            // 'convert_path',
                            // 'convert_priority',
                            // 'convert_start_time',
                            // 'convert_end_time',
                            // 'tag:ntext',
                            // 'seo_title',
                            // 'seo_description',
                            // 'seo_keywords',
//                            'is_crawler:bool',
                            // 'crawler_id',
                            // 'crawler_info',
                            // 'created_by',
                            // 'reviewed_by',
                            'published_list:jsonName',
                            ['class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'view' => function ($model, $key, $index) {
                                        return 0;
                                    }
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="modal fade" id="publishedModal" tabindex="-1" role="publishedModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="<?= Url::toRoute(['published-for']) ?>" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?= Yii::t('backend', 'Chọn hệ thống xuất bản') ?></h4>
                        </div>
                        <div class="modal-body">
                            <table id="table-clients" data-toggle="table" data-url="<?= Url::to('/site/get-clients') ?>"
                                   data-pagination="true" data-search="true" data-page-size="10" data-id-field="id"
                                   data-unique-id="id" data-select-item-name="listPublishedFor">
                                <thead>
                                <tr>
                                    <th data-field="state" data-checkbox="true"></th>
                                    <th data-field="name" data-align="center" data-sortable="true">
                                        <?= Yii::t('backend', 'Hệ thống') ?>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                                <?= Yii::t('backend', 'Hủy bỏ') ?></button>
                            <button id="btnPublishedFor" data-url="<?= Url::toRoute(['published-for']) ?>"
                                    type="button" class="btn green" data-dismiss="modal">
                                <?= Yii::t('backend', 'Xác nhận') ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <?php
            $this->registerJs("$('#btnPublishedFor').click(function() {
                            var ids = $('#" . $gridId . "').yiiGridView('getSelectedRows'),
                                url = $(this).data('url');
                                clients = [];
                            $('input:checkbox[name=listPublishedFor]:checked').each(function(){
                                clients.push($(this).val());
                            });
                            if (!ids.length) {
                                alert('" . Yii::t('backend', 'Please select one or more items from the list.') . "');
                            } else if (url) {
                                var csrfParam = $('meta[name=csrf-param]').prop('content'),
                                    csrfToken = $('meta[name=csrf-token]').prop('content');
                                $.pjax({
                                    type: 'POST',
                                    url: url,
                                    container: '#" . $pjaxContainer . "',
                                    data: {
                                        csrfParam: csrfToken,
                                        listPublishedFor: clients,
                                        ids: ids,
                                    },
                                    'timeout': false,
                                    'push': false,                                    
                                    'scrollTo': 0
                                });
                            }
                        });
                        $(document).on('pjax:complete', function() {
                            //$('#table-clients').bootstrapTable('refresh');
                            $('#table-clients').bootstrapTable({});
                        });
                        ");
            ?>
            <!-- /.modal-dialog -->
        </div>
        <?php
        Pjax::end();
        ?>
    </div>
</div>

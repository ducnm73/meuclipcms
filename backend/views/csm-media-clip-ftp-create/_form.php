<?php

use awesome\backend\select2Ajax\Select2Ajax;
use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use common\helpers\UploadHelper;
use backend\assets\UploadAsset;
UploadAsset::register($this);


/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
$this->registerJsFile("/js/upload.utils.js");
?>

<?php $form = AwsActiveForm::begin(['id' => 'my-form']); ?>
<div class="portlet light portlet-fit portlet-form bordered csm-media-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                    onclick="history.back(-1)">
                <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div class="tabbable-line" id="video-tabs">
                <ul class="nav nav-tabs nav-tabs-lg">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <video id="videoPlayer" src="<?= $model->getLocalFilePath() ?>" width="100%"
                                                   controls="controls"></video>
                                            <?= $form->field($model, 'original_path', ['selectors' => ['input' => '#csmmedia-original_path']])->hiddenInput(['id' => 'csmmedia-original_path'])->label(Yii::t('backend', 'Video File') . '<span style="color: red; margin-left: 2px">*</span>'); ?>

                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#video-modal">
                                                Select Video File
                                            </button>
                                            <?= Html::hiddenInput('', Url::toRoute('upload/get-ftp-file-from-path'), ['id' => 'get_ftp_file_url']) ?>
                                            <?= Html::hiddenInput('', UploadHelper::getFtpBaseUrl(), ['id' => 'ftp_base_url']) ?>
                                            <!-- Modal -->
                                            <div class="modal fade" id="video-modal" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close"><span
                                                                        aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Select File</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php
                                                            $user_name = Yii::$app->user->identity->username;
                                                            if ($user_name) {
                                                                echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video']['extensions']);
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="static-info">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <img width="100%" src="<?= $model->getLocalImagePath() ?>"
                                                         id="img_model_id"/>
                                                    <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend', 'Ảnh ngang') . ' 1280*720 px' . '<span style="color: red; margin-left: 2px">*</span>') ?>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#image-path-modal">
                                                        Select Horizontal Image
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="image-path-modal" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close"><span
                                                                                aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel">Select
                                                                        File</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $user_name = Yii::$app->user->identity->username;
                                                                    if ($user_name) {
                                                                        echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video-image']['extensions']);
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <img width="100%" src="<?= $model->getLocalPosterPath() ?>"
                                                         id="img_poster_model_id"/>
                                                    <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path'])->label(Yii::t('backend', 'Ảnh dọc')) ?>
                                                    <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                            data-target="#poster-path-modal">
                                                        Select Vertical Image
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="poster-path-modal" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close"><span
                                                                                aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel">Select
                                                                        File</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <?php
                                                                    $user_name = Yii::$app->user->identity->username;
                                                                    if ($user_name) {
                                                                        echo UploadHelper::php_file_tree($user_name, '/', true, Yii::$app->params['upload']['video-poster-image']['extensions']);
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'name',['selectors' => ['input' => '#csmmedia-name']])->textInput(['id' => 'csmmedia-name'])->label(Yii::t('backend', 'Tên') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'short_desc')->textarea(['row' => 3]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'description')->textarea(['row' => 5]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_download')->textInput(); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'price_play')->textInput(); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                                                'options' => [
                                                    'placeholder' => Yii::t('backend', 'Chọn thời gian xuất bản ...'),
                                                    'value' => ($model->published_at) ? $model->published_at : '',
                                                ],
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'format' => 'yyyy-mm-dd hh:ii:ss',
                                                    'todayHighlight' => true,
                                                    'todayBtn' => true
                                                ]
                                            ])->label(Yii::t('backend', 'Thời gian xuất bản')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(
                                                    ApiClient::getUserAccessClients(),
                                                    'id', 'name'
                                                ),
                                                'size' => Select2::MEDIUM,
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn dịch vụ phân phối'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Dịch vụ được phân phối')); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'category_list')->widget(Select2Ajax::classname(), [
                                                'data' => ArrayHelper::map(
                                                    CsmAttribute::getCategoryByIdsActive($model->category_list),
                                                    'id', 'name'
                                                ),
                                                'size' => Select2::MEDIUM,
                                                'url' => Url::to(['attribute/search-category-active']),
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn danh mục'), 'multiple' => true],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label('Category'); ?>
                                        </div>

                                        <?php if (Yii::$app->user->can('video-convert-priority')) { ?>
                                            <div class="static-info">
                                                <?= $form->field($model, 'convert_priority')->textInput()->label(Yii::t('backend', 'Độ ưu tiên convert')); ?>
                                            </div>
                                        <?php } ?>

                                        <div class="static-info">
                                            <?= $form->field($model, 'meta_content_filter')->widget(Select2::classname(), [
                                                'data' => Yii::$app->params['meta_content_filter'],
                                                'options' => ['placeholder' => Yii::t('backend', 'Chọn loại nội dung')],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                                'addon' => [
                                                    'prepend' => [
                                                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                    ]
                                                ],
                                            ])->label(Yii::t('backend', 'Phân loại nội dung')); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                        </div>
                                        <div class="static-info">
                                            <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php AwsActiveForm::end(); ?>

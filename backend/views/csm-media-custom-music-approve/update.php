<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Music Clip',
]) . ' ' . Html::encode($model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Music Clip'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update') . ' ' . $model->name;
?>
<div class="row csm-media-update">
    <div class="col-md-12">

    <?= $this->render('_update', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>

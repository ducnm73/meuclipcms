<?php


use awesome\backend\actionBar\ActionBar;
use awesome\backend\grid\AwsGridView;
use awesome\backend\toast\AwsAlertToast;
use awesome\backend\widgets\AwsBaseHtml;
use backend\models\CsmAttribute;
use backend\models\CsmCp;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaMusicApproveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Music Clip');
$this->params['breadcrumbs'][] = $this->title;
$gridId = 'csm_media_grid';
$pjaxContainer = 'mainGridPjax';
$modalId = 'reject-reason-modal';

?>
<div class="row csm-media-index">
    <div class="col-md-12">

        <?=
        AwsAlertToast::widget([
        ]);
        ?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?= \yii\helpers\Html::a(Yii::t('backend', 'Từ chối duyệt'), Url::to('/csm-media-clip-approve/bulk-reject-media'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm toggle-reject-modal', 'data-pjax' => 'false', 'data-target' => '#reject-reason-modal']) ?>
                    <?= ActionBar::widget([
                        'grid' => $gridId,
                        'pjax' => true,
                        'pjaxContainer' => '#' . $pjaxContainer,
                        'options' => ['class' => 'btn-group'],
                        'containerOptions' => ['class' => ''],
                        'templates' => [
                            '{bulk-actions}' => ['class' => 'btn-group'],
                        ],
                        'bulkActionsItems' => [
                            'Update Status' => [
                                'status-approved' => 'Approved',
                                // 'status-rejected' => 'Rejected',
                            ],
                            'General' => [
                                'general-delete' => 'Delete'
                            ],
                        ],
                        'bulkActionsOptions' => [
                            'options' => [
                                'status-approved' => [
                                    'url' => Url::toRoute(['update-status', 'status' => 'approved-accepted']),
                                    'data-confirm' => 'Are you sure?',
                                ],
                                // 'status-rejected' => [
                                //     'url' => Url::toRoute(['update-status', 'status' => 'approved-rejected']),
                                //     'data-confirm' => 'Are you sure?',
                                // ],
                                'general-delete' => [
                                    'url' => Url::toRoute('delete-multiple'),
                                    'data-confirm' => 'Are you sure?',
                                ],
                            ],
                            'class' => 'form-control',
                        ],
                    ]) ?>
                </div>
            </div>


            <?php
            Pjax::begin(['formSelector' => 'form', 'enablePushState' => true, 'id' => $pjaxContainer]);
            ?>
            <div class="portlet-body">
                <div class="table-container">
                    <?= AwsGridView::widget([
                        'id' => $gridId,
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\CheckboxColumn'],
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'name',
                            [
                                'attribute' => 'image_path',
                                'content' => function ($data) {
                                    return AwsBaseHtml::img($data->getLocalImagePath(), [
                                        'width' => 80,
                                    ]);
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'header' => Yii::t('backend','Trạng thái file'),
                                'content' => function ($data) {
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status'][$data->status]);
                                }
                            ],
                            [
                                'attribute' => 'updated_at',
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value' => date('Y-m-d 0:i:s') . ' - ' . date('Y-m-d H:i:s'),
                                    'convertFormat' => true,
                                    'options' => [
                                        'autocomplete' => 'off',
                                        'placeholder' => Yii::t('backend','Select range')
                                    ],
                                    'pluginOptions' => [
                                        'timePicker' => true,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ]
                            ],
//                            'attributes:jsonNameType',
                            'duration:mediaDuration',
                            'resolution',
                            [
                                'header' => 'Category',
                                'attribute' => 'category_list_music',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    if ($value = $data->attributes) {
                                        $decode = json_decode($value, true);
                                        if (count($decode)) {
                                            $res = [];
                                            foreach ($decode as $obj) {
                                                if ($obj['type'] == TYPE_ATTRIBUTE_CATEGORY_MUSIC)
                                                    $res[] = $obj['name'];
                                            }
                                            return implode(", ", $res);
                                        } else if ($decode) {
                                            return $decode['name'];
                                        }
                                    }
                                    return "";
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => ArrayHelper::map(CsmAttribute::getAllCategoryMusicActive(), 'id', 'name'),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn Category'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'header' => 'CP',
                                'attribute' => 'cp_id',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    $cp = CsmCp::findOne($data->cp_id);
                                    if ($cp) {
                                        return $cp->name;
                                    }
                                    return "";
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => ArrayHelper::map(CsmCp::find()->orderBy('name')->all(), 'id', 'name'),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn CP')
                                ],
                            ],
                            'published_list:jsonName',
                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <input id = "redirect_url" type="hidden" value="<?= Url::to(array_merge(['csm-media-custom-music-approve/index'],Yii::$app->session->get(\backend\controllers\CsmMediaCustomMusicApproveController::className() . 'queryParams'))) ?>"></input>
        <?php
        Pjax::end();
        ?>
    </div>
</div>

<?php echo $this->render('//csm-media-clip-approve/_bulk_reject_modal', ['gridId' => $gridId, 'toggleButtonSelector' => '.toggle-reject-modal', 'location' => Url::to(array_merge(['csm-media-custom-music-approve/index'],Yii::$app->session->get(\backend\controllers\CsmMediaCustomMusicApproveController::className() . 'queryParams'))), 'modalId' => $modalId]) ?>
<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\ApiClient;
use backend\models\CsmAttribute;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
$this->registerJsFile("/js/upload.utils.js");
?>


<?php $form = AwsActiveForm::begin(['id' => 'my-form']); ?>

    <div class="portlet light portlet-fit portlet-form bordered csm-media-form">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
            </div>
            <div class="actions">
                <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                        onclick="history.back(-1)">
                    <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="tabbable-line" id="video-tabs">
                    <ul class="nav nav-tabs nav-tabs-lg">
                        <li class="active">
                            <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#tab_3" data-toggle="tab"><?= Yii::t('backend', 'Music Information') ?>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <video id="videoPlayer" src="<?= $model->getLocalFilePath() ?>"
                                                       width="100%"
                                                       controls="controls"></video>
                                            </div>
                                            <?= $form->field($model, 'watched_duration')->hiddenInput(['id' => 'watched_duration_id','value'=> 0])->label(false)?>
                                            <div class="static-info">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <img width="100%" src="<?= $model->getLocalImagePath() ?>"
                                                             id="img_model_id"/>
                                                        <?= $form->field($model, 'image_path', ['selectors' => ['input' => '#csmmedia-image_path']])->hiddenInput(['id' => 'csmmedia-image_path'])->label(Yii::t('backend', 'Ảnh ngang') . ' 1280*720 px' . '<span style="color: red; margin-left: 2px">*</span>') ?>
                                                        <div id="image_upload">
                                                        </div>
                                                        <?php
                                                        echo FineUploader::widget([
                                                            'url' => Url::to(['upload/image-upload', 'type' => 'custom-video-image']),
                                                            'id' => 'image_upload',
                                                            'csrf' => true,
                                                            'isBase' => true,
                                                            'renderTag' => false,
                                                            'jsOptions' => [
                                                                'validation' => [
                                                                    'image' => [
                                                                        'minWidth' => Yii::$app->params['upload']['custom-video-image']['minWidth'],
                                                                        'minHeight' => Yii::$app->params['upload']['custom-video-image']['minHeight'],
                                                                    ],
                                                                    'minSizeLimit' => Yii::$app->params['upload']['custom-video-image']['minSize'],
                                                                    'sizeLimit' => Yii::$app->params['upload']['custom-video-image']['maxSize'],
                                                                    'allowedExtensions' => Yii::$app->params['upload']['custom-video-image']['extensions'],
                                                                ],
                                                                'messages' => [
                                                                    'minWidthImageError' => Yii::t('backend', 'Phải chọn ảnh có chiều rộng tối thiểu' . ' ' .Yii::$app->params['upload']['custom-video-image']['minWidth'].'px'),
                                                                    'minHeightImageError' => Yii::t('backend', 'Phải chọn ảnh có chiều cao tối thiểu' . ' ' .Yii::$app->params['upload']['custom-video-image']['minHeight'].'px'),
                                                                    'minSizeError' => Yii::t('backend', 'Kích thước file phải lớn hơn') . ' '.Yii::$app->params['upload']['custom-video-image']['minSize']/1024/1024 . ' MB',
                                                                    'sizeError' => Yii::t('backend', 'Kích thước file không được quá') . ' '.Yii::$app->params['upload']['custom-video-image']['maxSize']/1024/1024 . ' MB',
                                                                    'typeError' => Yii::t('backend', 'Định dạng file không đúng') . ' !!!'
                                                                ],
                                                                'callbacks' => [
                                                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                    ),
                                                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#csmmedia-image_path').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
        var form = $("#my-form"), 
        form_data = form.data("yiiActiveForm");
        $.each(form_data.attributes, function() {
             this.status = 3;
        });
        form.yiiActiveForm("validate");
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                    )],
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12">
                                                        <img width="100%" src="<?= $model->getLocalPosterPath() ?>"
                                                             id="img_poster_model_id"/>
                                                        <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path']) ?>
                                                        <div id="image_poster_upload">
                                                        </div>
                                                        <?php
                                                        echo FineUploader::widget([
                                                            'url' => Url::to(['upload/image-upload', 'type' => 'custom-video-poster-image']),
                                                            'id' => 'image_poster_upload',
                                                            'csrf' => true,
                                                            'isBase' => true,
                                                            'renderTag' => false,
                                                            'jsOptions' => [
                                                                'callbacks' => [
                                                                    'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                                    ),
                                                                    'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_poster_upload').hide();
        $('#csmmedia-poster_path').val(data.filePath);
        $("#img_poster_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                                    )],
                                                            ]
                                                        ]);
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'name',['selectors' => ['input' => '#csmmedia-name']])->textInput(['id' => 'csmmedia-name'])->label(Yii::t('backend', 'Tên') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'short_desc')->textarea(['row' => 3])->label(Yii::t('backend','Mô tả ngắn')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'description')->textarea(['row' => 5]); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'price_download')->textInput(); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'price_play')->textInput(); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'media_client_status')->widget(Select2::classname(), [
                                                    'data' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-approved-status']),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => [
                                                        'placeholder' => Yii::t('backend','Trạng thái'),
                                                        'multiple' => false,
                                                        'data-toggle' => 'toggle-visible',
                                                        'data-toggle-value' => STATUS_MEDIA_REJECTED,
                                                        'data-target' => '.reject-reason',
                                                    ],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Trạng thái')); ?>
                                            </div>
                                            <div class="static-info reject-reason" style='display:none;'>
                                                <?= $form->field($model, 'reject_reason')->textInput()->label(Yii::t('backend','Lý do')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(
                                                        ApiClient::getUserAccessClients(),
                                                        'id', 'name'
                                                    ),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn dịch vụ phân phối'), 'multiple' => true],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Dịch vụ được phân phối')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'category_list_music')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(
                                                        CsmAttribute::getAllCategoryMusicActive(),
                                                        'id', 'name'
                                                    ),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn danh mục'), 'multiple' => true],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label('Category'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'singer_list')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(
                                                        CsmAttribute::getAllSingerActive(),
                                                        'id', 'name'
                                                    ),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn ca sĩ'), 'multiple' => true],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Ca sĩ')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'composer_list')->widget(Select2::classname(), [
                                                    'data' => ArrayHelper::map(
                                                        CsmAttribute::getAllComposerActive(),
                                                        'id', 'name'
                                                    ),
                                                    'size' => Select2::MEDIUM,
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn nhạc sĩ'), 'multiple' => true],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Nhạc sĩ')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_year')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->params['meta_year'],
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn năm sản xuất')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Năm sản xuất')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_copyright')->textInput()->label('Copyright'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_country')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->params['meta_country'],
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn nước sản xuất')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Nước sản xuất')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_language')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->params['meta_language'],
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn ngôn ngữ music clip')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Ngôn ngữ music clip')); ?>
                                            </div>
                                            <div class="static-info">
                                                <?= $form->field($model, 'meta_subtitle_language')->widget(Select2::classname(), [
                                                    'data' => Yii::$app->params['meta_language'],
                                                    'options' => ['placeholder' => Yii::t('backend','Chọn ngôn ngữ phụ đề')],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                                                        ]
                                                    ],
                                                ])->label(Yii::t('backend','Ngôn ngữ phụ đề')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php AwsActiveForm::end(); ?>

<script>
    var player = document.getElementById("videoPlayer");
    var watchedDuration = 0;
    var timestamps = [];
    var curTime = 0;
    player.ontimeupdate = function () {
        curTime = +player.currentTime.toFixed(0);
        if (timestamps.indexOf(curTime) < 0) {
            timestamps.push(curTime);
        }
        watchedDuration = timestamps.length - 1;
        $('#watched_duration_id').val(watchedDuration);
    };
</script>

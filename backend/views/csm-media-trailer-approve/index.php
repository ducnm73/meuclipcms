<?php

use awesome\backend\actionBar\ActionBar;
use awesome\backend\grid\AwsGridView;
use awesome\backend\toast\AwsAlertToast;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\ArrayHelper;
use backend\models\CsmCp;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaTrailerApproveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile('/plugins/bootstrap-table/bootstrap-table.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/table-bootstrap.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = Yii::t('backend', 'Film Trailer Approve');
$this->params['breadcrumbs'][] = $this->title;
$gridId = 'csm_media_grid';
$pjaxContainer = 'mainGridPjax';
$modalId = 'reject-reason-modal';

?>
<div class="row csm-media-index" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">

        <?=
        AwsAlertToast::widget([
        ]);
        ?>
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <div class="actions">
                        <?= \yii\helpers\Html::a(Yii::t('backend', 'Từ chối duyệt'), Url::to('/csm-media-clip-approve/bulk-reject-media'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm toggle-reject-modal', 'data-pjax' => 'false', 'data-target' => '#reject-reason-modal']) ?>
                        <?= ActionBar::widget([
                            'grid' => $gridId,
                            'pjax' => true,
                            'pjaxContainer' => '#' . $pjaxContainer,
                            'options' => ['class' => 'btn-group'],
                            'containerOptions' => ['class' => ''],
                            'templates' => [
                                '{bulk-actions}' => ['class' => 'btn-group'],
                            ],
                            'bulkActionsItems' => [
                                'Update Status' => [
                                    'status-approved' => 'Approved',
                                    // 'status-rejected' => 'Rejected',
                                ],
                                'General' => [
                                    'general-delete' => 'Delete'
                                ],
                            ],
                            'bulkActionsOptions' => [
                                'options' => [
                                    'status-approved' => [
                                        'url' => Url::toRoute(['update-status', 'status' => 'approved-accepted']),
                                        'data-confirm' => 'Are you sure?',
                                    ],
                                    // 'status-rejected' => [
                                    //     'url' => Url::toRoute(['update-status', 'status' => 'approved-rejected']),
                                    //     'data-confirm' => 'Are you sure?',
                                    // ],
                                    'general-delete' => [
                                        'url' => Url::toRoute('delete-multiple'),
                                        'data-confirm' => 'Are you sure?',
                                    ],
                                ],
                                'class' => 'form-control',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>


            <?php
            Pjax::begin(['formSelector' => 'form', 'enablePushState' => true, 'id' => $pjaxContainer]);
            ?>
            <div class="portlet-body">
                <div class="table-container">

                    <?= AwsGridView::widget([
                        'id' => $gridId,
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['class' => 'yii\grid\CheckboxColumn'],
                            'id',
                            'name',
                            [
                                'attribute' => 'image_path',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return '<img width=\'80\' src=\'' . $data->getLocalImagePath() . '\' />';
                                }
                            ],
//                            [
//                                'header' => Yii::t('backend','Tự động?'),
//                                'attribute' => 'is_crawler',
//                                'content' => function ($data) {
//                                    /* @var \backend\models\CsmMedia $data */
//                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option'][$data->is_crawler]);
//                                },
//                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
//                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option']),
//                                'filterWidgetOptions' => [
//                                    'pluginOptions' => ['allowClear' => true],
//                                ],
//                                'filterInputOptions' => [
//                                    'placeholder' => Yii::t('backend','Tự động?'),
////                                    'multiple' => true
//                                ],
//                            ],
//                            [
//                                'attribute' => 'status',
//                                'content' => function ($data) {
//                                    return Yii::$app->params['media-status'][$data->status];
//                                }
//                            ],
                            'duration:mediaDuration',
                            'resolution',
                            // 'price_download',
                            // 'price_play',
                            // 'type',
//                            'max_quantity',
                            // 'published_by',
//                            'created_at',

                            [
                                'attribute' => 'updated_at',
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value'=> date('Y-m-d 0:i:s').' - '. date('Y-m-d H:i:s'),
                                    'convertFormat'=>true,
                                    'pluginOptions' => [
                                        'timePicker' => false,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ],
                            ],
//                            'published_at',

                            'attributes:jsonNameType',
                            // 'cp_id',
                            // 'cp_info',
                            // 'original_path',
                            // 'file_type',
                            // 'convert_path',
                            // 'convert_priority',
                            // 'convert_start_time',
                            // 'convert_end_time',
                            // 'tag:ntext',
                            // 'seo_title',
                            // 'seo_description',
                            // 'seo_keywords',
//                            'is_crawler:bool',
                            // 'crawler_id',
                            // 'crawler_info',
                            // 'created_by',
                            // 'reviewed_by',
                            [
                                'header' => 'CP',
                                'attribute' => 'cp_id',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    $cp = CsmCp::findOne($data->cp_id);
                                    if ($cp) {
                                        return $cp->name;
                                    }
                                    return "";
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => ArrayHelper::map(CsmCp::find()->orderBy('name')->all(), 'id', 'name'),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn CP')
                                ],
                            ],
                            'published_list:jsonName',
                            ['class' => 'yii\grid\ActionColumn',
                                'visibleButtons' => [
                                    'view' => function ($model, $key, $index) {
                                        return 0;
                                    }
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="modal fade" id="publishedModal" tabindex="-1" role="publishedModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="<?= Url::toRoute(['published-for']) ?>" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?= Yii::t('backend', 'Chọn hệ thống xuất bản') ?></h4>
                        </div>
                        <div class="modal-body">
                            <table id="table-clients" data-toggle="table" data-url="<?= Url::to('/site/get-clients') ?>"
                                   data-pagination="true" data-search="true" data-page-size="10" data-id-field="id"
                                   data-unique-id="id" data-select-item-name="listPublishedFor">
                                <thead>
                                <tr>
                                    <th data-field="state" data-checkbox="true"></th>
                                    <th data-field="name" data-align="center" data-sortable="true">
                                        <?= Yii::t('backend', 'Hệ thống') ?>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                                <?= Yii::t('backend', 'Hủy bỏ') ?></button>
                            <button id="btnPublishedFor" data-url="<?= Url::toRoute(['published-for']) ?>"
                                    type="button" class="btn green" data-dismiss="modal">
                                <?= Yii::t('backend', 'Xác nhận') ?></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
            <?php
            $this->registerJs("$('#btnPublishedFor').click(function() {
                            var ids = $('#" . $gridId . "').yiiGridView('getSelectedRows'),
                                url = $(this).data('url');
                                clients = [];
                            $('input:checkbox[name=listPublishedFor]:checked').each(function(){
                                clients.push($(this).val());
                            });
                            if (!ids.length) {
                                alert('" . Yii::t('backend', 'Please select one or more items from the list.') . "');
                            } else if (url) {
                                var csrfParam = $('meta[name=csrf-param]').prop('content'),
                                    csrfToken = $('meta[name=csrf-token]').prop('content');
                                $.pjax({
                                    type: 'POST',
                                    url: url,
                                    container: '#" . $pjaxContainer . "',
                                    data: {
                                        csrfParam: csrfToken,
                                        listPublishedFor: clients,
                                        ids: ids,
                                    },
                                    'timeout': false,
                                    'push': false,                                    
                                    'scrollTo': 0
                                });
                            }
                        });
                        $(document).on('pjax:complete', function() {
                            //$('#table-clients').bootstrapTable('refresh');
                            $('#table-clients').bootstrapTable({});
                        });
                        ");
            ?>
            <!-- /.modal-dialog -->
        </div>
        <input id = "redirect_url" type="hidden" value="<?= Url::to(array_merge(['csm-media-trailer-approve/index'],Yii::$app->session->get(\backend\controllers\CsmMediaTrailerApproveController::className() . 'queryParams'))) ?>"></input>
        <?php
        Pjax::end();
        ?>
    </div>
</div>

<?php echo $this->render('//csm-media-clip-approve/_bulk_reject_modal', ['gridId' => $gridId, 'toggleButtonSelector' => '.toggle-reject-modal', 'location' => Url::to(array_merge(['csm-media-trailer-approve/index'],Yii::$app->session->get(\backend\controllers\CsmMediaTrailerApproveController::className() . 'queryParams'))), 'modalId' => $modalId]) ?>
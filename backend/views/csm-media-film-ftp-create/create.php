<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Film FTP',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Film FTP'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-create">
    <div class="col-md-12">
        <?= $this->render('//partials/_form_film_ftp', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

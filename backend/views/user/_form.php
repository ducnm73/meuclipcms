<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use backend\models\ApiClient;
use backend\models\CsmCp;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = AwsActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered user-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
                <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
                </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                    onclick="history.back(-1)">
                <i class="fa fa-angle-left"></i> Back
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'password_hash')->passwordInput() ?>

            <?= $form->field($model, 're_password')->passwordInput() ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'status')->checkbox() ?>

            <?= $form->field($model, 'cp_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    CsmCp::getAll(),
                    'id', 'name'
                ),
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => Yii::t('backend','Chọn CP')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                    ]
                ],
            ])->label('CP'); ?>


            <?= $form->field($model, 'client_list')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(
                    ApiClient::getAll(),
                    'id', 'name'
                ),
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => Yii::t('backend','Chọn dịch vụ quản lý'), 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-folder-open"></i>'
                    ]
                ],
            ])->label(Yii::t('backend','Dịch vụ được quản lý')); ?>
        </div>
    </div>
</div>

<?php AwsActiveForm::end(); ?>

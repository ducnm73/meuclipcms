<?php


use awesome\backend\grid\AwsGridView;
use awesome\backend\widgets\AwsBaseHtml;
use backend\models\CsmAttribute;
use backend\models\CsmCp;
use backend\models\CsmMediaActionLog;
use backend\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaFilmAdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Csm Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => true, 'id' => 'mainGridPjax']);
                    ?>

                    <?= AwsGridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'name',
                            [
                                'attribute' => 'image_path',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return "<img width='80' src='" . $data->getLocalImagePath() . "' />";
                                }
                            ],
                            [
                                'attribute' => Yii::t('backend','status'),
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status'][$data->status]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'header' => Yii::t('backend','Trạng thái convert'),
                                'attribute' => 'convert_status',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMediaPublish $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['convert-status'][$data->convert_status]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['convert-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'attribute' => 'updated_at',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return date('d/m/Y', strtotime($data->updated_at));
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_DATE_RANGE,
                                'filterWidgetOptions' => [
                                    'value' => date('Y-m-d 0:i:s') . ' - ' . date('Y-m-d H:i:s'),
                                    'convertFormat' => true,
                                    'pluginOptions' => [
                                        'timePicker' => true,
                                        'timePicker24Hour' => true,
                                        'timePickerIncrement' => 30,
                                        'locale' => [
                                            'format' => 'Y-m-d H:i:s'
                                        ]
                                    ]
                                ]
                            ],
                            [
                                'attribute' => Yii::t('backend','is_crawler'),
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option'][$data->is_crawler]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Crawler?'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'attribute' => 'need_encryption',
                                'header' => Yii::t('backend','Is DRM ?'),
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option'][$data->need_encryption]);
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['yes-no-option']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => 'DRM ?',
//                                    'multiple' => true
                                ],
                            ],
//                            'duration:mediaDuration',
                            // 'price_download',
                            // 'price_play',
                            // 'type',
//                            'max_quantity',
                            // 'published_by',

//                            'updated_at',
//                            'published_at',
                            'resolution',
//                            'attributes:jsonNameType',
                            // 'cp_id',
                            // 'cp_info',
                            // 'original_path',
                            // 'file_type',
                            // 'convert_path',
                            // 'convert_priority',
                            // 'convert_start_time',
                            // 'convert_end_time',
                            // 'tag:ntext',
                            // 'seo_title',
                            // 'seo_description',
                            // 'seo_keywords',
//                            'is_crawler:bool',
                            // 'crawler_id',
                            // 'crawler_info',
                            // 'created_by',
                            // 'reviewed_by',
                            [
                                'header' => Yii::t('backend','Trạng thái DV'),
                                'attribute' => 'media_client_status',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    $clients = User::getClientList();
                                    foreach ($data->csmMediaPublishes as $published) {
                                        if (in_array($published->client_id, $clients)) {
                                            return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['service-status'][$published->status]);
                                        }
                                    }
                                    return "";
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['service-status']),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn trạng thái'),
//                                    'multiple' => true
                                ],
                            ],
                            [
                                'header' => 'CP',
                                'attribute' => 'cp_id',
                                'content' => function ($data) {
                                    /* @var \backend\models\CsmMedia $data */
                                    $cp = CsmCp::findOne($data->cp_id);
                                    if ($cp) {
                                        return $cp->name;
                                    }
                                    return "";
                                },
                                'filterType' => AwsGridView::FILTER_TYPE_SELECT2,
                                'filter' => ArrayHelper::map(CsmCp::find()->orderBy('name')->all(), 'id', 'name'),
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                                'filterInputOptions' => [
                                    'placeholder' => Yii::t('backend','Chọn CP')
                                ],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{viewReason}',
                                'buttons' => [
                                    'viewReason' => function ($url, $model) {
                                        $clients = User::getClientList();
                                        $publishObj = null;

                                        foreach ($model->csmMediaPublishes as $published) {
                                            if (in_array($published->client_id, $clients)) {
                                                $publishObj = $published;
                                                break;
                                            }
                                        }

                                        if(!$publishObj || $publishObj->status != STATUS_MEDIA_REJECTED) {
                                            return '';
                                        }
                                        
                                        $log = CsmMediaActionLog::find()->where(['media_id' => $model->id, 'created_by' => $publishObj->published_by])->orderBy(['created_at' => SORT_DESC])->one();
                                        
                                        if($log) {
                                            $url = Url::to(['/site/view-reject-reason', 'id' => $log->id]);
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                                    'title' => Yii::t('backend','Xem lý do từ chối'),
                                                    'data-pjax' => '0',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#view-reject-reason-modal',
                                            ]);
                                        } else {
                                            return '';
                                        }
                                    },
                                ],
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->render('/csm-media-clip-cp/_view_reject_reason') ?>
<?php


use awesome\backend\grid\AwsGridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CsmMediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Csm Media');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row csm-media-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title" style="z-index: 2; position: relative;">
                <div class="caption">
                    <i class="icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => 'Csm Media',
                    ]),
                        ['create'], ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
                    <div class="btn-group">
                        <a class="btn btn-circle btn-default dropdown-toggle" href="javascript:;"
                           data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs"><?= Yii::t('backend', 'Tools') ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;"><?= Yii::t('backend', 'Publish for') ?></a>
                            </li>
                            <li>
                                <a href="javascript:;"><?= Yii::t('backend', 'Reject') ?></a>
                            </li>
                            <li>
                                <a href="javascript:;"><?= Yii::t('backend', 'Delete') ?></a>
                            </li>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body" style="z-index: 1; position: relative;">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= AwsGridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['class' => 'yii\grid\CheckboxColumn'],

                            'name',
                            'image_path:imageStorage',
                            [
                                'attribute' => 'status',
                                'content' => function ($data) {
                                    return common\helpers\I18nHelper::getI18nBackendParams(Yii::$app->params['media-status'][$data->status]);
                                }
                            ],
                            'duration:mediaDuration',
                            // 'price_download',
                            // 'price_play',
                            // 'type',
                            'max_quantity',
                            // 'published_by',
                            // 'created_at',
                            'updated_at',
                            'published_at',
                            // 'resolution',
                            'attributes:jsonName',
                            // 'cp_id',
                            // 'cp_info',
                            // 'original_path',
                            // 'file_type',
                            // 'convert_path',
                            // 'convert_priority',
                            // 'convert_start_time',
                            // 'convert_end_time',
                            // 'tag:ntext',
                            // 'seo_title',
                            // 'seo_description',
                            // 'seo_keywords',
                            'is_crawler:bool',
                            // 'crawler_id',
                            // 'crawler_info',
                            // 'created_by',
                            // 'reviewed_by',
                            'published_list:jsonName',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use awesome\fineuploader\FineUploader;
use backend\models\CsmAttribute;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\CsmMedia */
/* @var $title string */
/* @var $form AwsActiveForm */
$this->registerJsFile("/js/upload.utils.js");
?>

<?php $form = AwsActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered csm-media-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane font-green"></i>
            <span class="caption-subject font-green sbold uppercase">
                <?= $title ?>
            </span>
        </div>
        <div class="actions">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green btn-outline btn-circle btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black btn-outline btn-circle btn-sm"
                    onclick="history.back(-1)">
                <i class="fa fa-angle-left"></i><?= Yii::t('backend', 'Back') ?>
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div id="divUploadVideo">
                <div id="video_upload">
                </div>
                <?php
                echo FineUploader::widget([
                    'url' => Url::to(['upload/video-upload', 'type' => 'video']),
                    'id' => 'video_upload',
                    'csrf' => true,
                    'renderTag' => false,
                    'jsOptions' => [
                        'chunking' => [
                            'enabled' => true,
                            'partSize' => 20 * 1024 * 1024, // 20MB
                            'concurrent' => [ 'enabled' => true ],
                            'success' => [ 'endpoint' => Url::to(['upload/video-upload', 'type' => 'video', 'done' => 1]) ]
                        ],
                        'callbacks' => [
                            'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                            ),
                            'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(data.error);
    } else if (data.success)  {
        $('#video-tabs').show();
        $('#divUploadVideo').hide();
        $('#csmmedia-original_path').val(data.filePath);
        $('#csmmedia-name').val(data.uploadName).focus();
        $('#videoPlayer').attr('src',data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                            )
                        ],
                    ]
                ]);
                ?>
            </div>
            <div class="tabbable-line" id="video-tabs" style="display: none;">
                <ul class="nav nav-tabs nav-tabs-lg">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab"><?= Yii::t('backend', 'Base Information') ?></a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab"><?= Yii::t('backend', 'Detail Information') ?>
                        </a>
                    </li>
                    <li style="<?= $model->isNewRecord ? "display:none;" : "" ?>">
                        <a href="#tab_4" data-toggle="tab"><?= Yii::t('backend', 'Convert') ?>
                            <span class="badge badge-danger"><?= $model->isNewRecord ? "" : $model->getNumConvertPath() ?></span>
                        </a>
                    </li>
                    <li style="<?= $model->isNewRecord ? "display:none;" : "" ?>">
                        <a href="#tab_5" data-toggle="tab"><?= Yii::t('backend', 'History') ?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <video id="videoPlayer" width="100%" controls="controls"></video>
                                            <?= $form->field($model, 'original_path', ['selectors' => ['input' => '#csmmedia-original_path']])->hiddenInput(['id' => 'csmmedia-original_path'])->label(false); ?>
                                        </div>
                                        <div class="row static-info">
                                            <img src="" width="80%" id="img_model_id" style="display: none"/>
                                            <?= $form->field($model, 'image_path')->hiddenInput(['id' => 'image_path_id']) ?>
                                            <div id="image_upload">
                                            </div>
                                            <?php
                                            echo FineUploader::widget([
                                                'url' => Url::to(['upload/image-upload', 'type' => 'video-image']),
                                                'id' => 'image_upload',
                                                'csrf' => true,
                                                'isBase' => true,
                                                'renderTag' => false,
                                                'jsOptions' => [
                                                    'callbacks' => [
                                                        'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                        ),
                                                        'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_upload').hide();
        $('#image_path_id').val(data.filePath);
        $("#img_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                        )],
                                                ]
                                            ]);
                                            ?>
                                        </div>
                                        <div class="row static-info">
                                            <img src="" width="80%" id="img_poster_model_id" style="display: none"/>
                                            <?= $form->field($model, 'poster_path')->hiddenInput(['id' => 'csmmedia-poster_path']) ?>
                                            <div id="image_poster_upload">
                                            </div>
                                            <?php
                                            echo FineUploader::widget([
                                                'url' => Url::to(['upload/image-upload', 'type' => 'video-poster-image']),
                                                'id' => 'image_poster_upload',
                                                'csrf' => true,
                                                'isBase' => true,
                                                'renderTag' => false,
                                                'jsOptions' => [
                                                    'callbacks' => [
                                                        'onError' => new JsExpression(<<<EOF
function(id, name, errorReason) {
    console.log('The file ' + name + ' could not be uploaded: ' + errorReason);
}
EOF
                                                        ),
                                                        'onComplete' => new JsExpression(<<<EOF
function(id, name, responseJSON) {
    var data = responseJSON;
    if (data.error) {
        alert(data.error);
        console.log(responseJSON);
    } else if (data.success) {
//        $('#image_poster_upload').hide();
        $('#csmmedia-poster_path').val(data.filePath);
        $("#img_poster_model_id").show().attr("src", data.fileUrl);
        console.log(data.fileUrl);
        console.log(data.filePath);
    } else {
        console.log(responseJSON);
    }
}
EOF
                                                        )],
                                                ]
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div style="width: 100%"></div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'name',['selectors' => ['input' => '#csmmedia-name']])->textInput(['id' => 'csmmedia-name'])->label(Yii::t('backend', 'Tên') . '<span style="color: red; margin-left: 2px">*</span>'); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'short_desc')->textarea(['row' => 3]); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'description')->textarea(['row' => 5]); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'price_download')->textInput(); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'price_play')->textInput(); ?>
                                    </div>
                                    <div class="row static-info">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="row static-info">
                                        <div class="row">
                                            <?= $form->field($model, 'category_list')->checkboxList(ArrayHelper::map(
                                                CsmAttribute::getAllCategoryActive(),
                                                'id', 'name'
                                            ), [
                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                    return "<div class='col-md-6 col-sm-12 md-checkbox'>
                                                        <input id='cbxAttribute{$index}' name='{$name}' value='{$value}' class=\"md-check\" {$checked} type=\"checkbox\">
                                                        <label for='cbxAttribute{$index}'>
                                                            <span class=\"inc\"></span>
                                                            <span class=\"check\"></span>
                                                            <span class=\"box\"></span>{$label}</label>
                                                    </div>";
//                                                return "<label class='checkbox col-md-4' style='font-weight: normal;'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
                                                }
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet box">
                                    <div class="row static-info">
                                        <?= $form->field($model, 'seo_title')->textarea(['row' => 3]); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'seo_description')->textarea(['row' => 7]); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'seo_keywords')->textarea(['row' => 5]); ?>
                                    </div>
                                    <div class="row static-info">
                                        <?= $form->field($model, 'tag')->textarea(['row' => 3]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">

                    </div>
                    <div class="tab-pane" id="tab_4">

                    </div>
                    <div class="tab-pane" id="tab_5">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php AwsActiveForm::end(); ?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ApiClient */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Api Client',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Api Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row api-client-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>

<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Media reject form
 */
class MediaRejectForm extends Model {
	public $media_ids = array();
	public $reject_reason;

	public function rules() {
		return [
			['media_ids', 'in', 'range' => ArrayHelper::map(CsmMediaPublish::find()->where([
                'client_id' => User::getClientList(),
                'status' => STATUS_MEDIA_APPROVED_PROPOSAL
			])->all(), 'id', 'id')],
			[['media_ids', 'reject_reason'], 'required'],
			[['reject_reason'], 'string'],
		];
	}

	public function attributeLabels() {
		return [
			'media_ids' => false,
			'reject_reason' => Yii::t('backend', 'Lý do'),
		];
	}
}
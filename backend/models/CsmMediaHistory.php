<?php

namespace backend\models;

use common\models\db\CsmMediaHistoryDB;
use Yii;
use yii\db\Exception;
use yii\db\Expression;

class CsmMediaHistory extends \common\models\CsmMediaHistoryBase {
    public static $STATUS_NOT_PUBLISH = 0 ;

    public static function insertMediaHistory($media_id, $all_field, $updated_by, $client_id, $is_publish, $type){
        $rows = [];
        $update_at = new Expression('NOW()');
        foreach ($client_id as $client){
            $rows[] = [
                'media_id' => $media_id,
                'all_fields' => $all_field,
                'updated_at' => $update_at,
                'updated_by' => $updated_by,
                'client_id' => $client,
                'is_publish' => $is_publish,
                'type' => $type
            ] ;
        }
        try {
            $result = Yii::$app->db->createCommand()->batchInsert(CsmMediaHistoryDB::tableName(), ['media_id', 'all_fields', 'updated_at', 'updated_by', 'client_id', 'is_publish', 'type'], $rows)->execute();

        } catch (Exception $e) {
            return false ;
        }
        return $result;

    }

}
<?php

namespace backend\models;

use Yii;

class CsmConfig extends \common\models\CsmConfigBase {
    public static function getConfig($key){
        $config = self::find()
            ->asArray()
            ->select('value')
            ->where('name = :config_key', [':config_key' => $key])
            ->one();
        if (!empty($config) && $config['value'] != NULL) {
            return trim($config['value']);
        } else {
            return '';
        }
    }

    public static function getListClientNeedApproveContract(){
        $config = self::getConfig('CLIENT_NEED_APPROVE_CONTRACT');
        if($config){
            $listClient = explode(',', $config);
            if($listClient) return $listClient ;
        }
        return null ;
    }


}
<?php

namespace backend\models;

use DateTime;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CsmMediaClipApproveSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CpApproveReportSearch extends CpApproveReport
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;

        $query = CpApproveReport::find()
            ->select(CsmMediaActionLog::tableName() . '.created_at as published_at, '
                . CsmMediaActionLog::tableName() . '.created_by as user_id,'
                . User::tableName() . '.username AS user_name, '
                . CsmMediaActionLog::tableName() . '.media_id AS media_id,'
                . CsmMedia::tableName() . '.`name` AS media_name, '
                . CsmMediaActionLog::tableName() . '.watched_duration AS watched_duration,'
                . CsmMediaActionLog::tableName() . '.action AS action,'
                . CsmMedia::tableName() . '.duration as video_duration, '
                . CsmMediaActionLog::tableName() . '.note AS note')
            ->leftJoin(CsmMedia::tableName(), CsmMediaActionLog::tableName() . '.media_id = ' . CsmMedia::tableName() . '.id')
            ->leftJoin(User::tableName(), CsmMediaActionLog::tableName() . '.created_by = ' . User::tableName() . '.id')
            ->where(CsmMediaActionLog::tableName() . '.created_by = ' . $user->id)
            ->andWhere([CsmMediaActionLog::tableName() . '.action' => [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE]]);
        $this->load($params);
        if (strpos($this->published_at, ' - ') > 0) {
            $start_dates = self::splitDate($this->published_at, 'd-m-Y');
            $query->andWhere(CsmMediaActionLog::tableName() . '.created_at >= :fromDate', [':fromDate' => $start_dates[0]])
                ->andWhere(CsmMediaActionLog::tableName() . '.created_at <= :toDate', [':toDate' => $start_dates[1]]);

        }
        //print_r($query->createCommand()->getRawSql());die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['published_at' => SORT_ASC]]
        ]);

        $allModel = $dataProvider->getModels();
        foreach ($allModel as $item) {
            if (!in_array($item->action, [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE])) {
                $item->watched_duration = 0;
            } else {
                if ($item->watched_duration === NULL) {
                    $item->watched_duration = ($item->video_duration) ? $item->video_duration : 0;
                }
            }

            $item['revenue'] = number_format($item->watched_duration * Yii::$app->params['revenua_per_minute_approve'] / 60, 2);
        }
        $dataProvider->setModels($allModel);

        return $dataProvider;
    }


}

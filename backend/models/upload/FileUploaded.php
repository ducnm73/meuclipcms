<?php

namespace backend\models\upload;

use Yii;
use yii\redis\ActiveRecord;

/**
 * This is the model class for table "FileUploaded" in redis.
 *
 * @property string $id
 * @property string $url
 * @property string $type
 * @property string $upload_date
 */
class FileUploaded extends ActiveRecord
{
    public static function getDb()
    {
        try {
            return Yii::$app->get('file-upload-redis');
        } catch (\yii\base\InvalidConfigException $e) {
            Yii::error('FileUploaded configuration "file-upload-redis" is not found on main.php');
            return parent::getDb();
        }
    }

    public function attributes()
    {
        return ['id', 'url', 'type', 'upload_date'];
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public static function countByUrlAndType($url, $type)
    {
        return FileUploaded::find()
            ->where([
                'username' => $url,
                'type' => $type,
            ])
            ->count();
    }

    public static function deleteByUrlAndType($url)
    {
        return ;
        // Xử lý những luồng upload không lưu, tạm thời off
//        FileUploaded::deleteAll([
//            'id' => md5($url)
//        ]);
    }

    public static function log($url, $type) {
        // Xử lý những luồng upload không lưu, tạm thời off
        return;
//        $fileUpload = new FileUploaded();
//        $fileUpload->id = md5($url);
//        $fileUpload->type = $type;
//        $fileUpload->url = $url;
//        $fileUpload->upload_date = date('Y-m-d H:i:s', time());
//        $fileUpload->save();
    }
}
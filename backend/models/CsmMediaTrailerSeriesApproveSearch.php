<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaTrailerSeriesApproveSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaTrailerSeriesApproveSearch extends CsmMediaFilmSeries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'price_download', 'price_play', 'type', 'max_quantity', 'published_by', 'cp_id', 'file_type', 'convert_status', 'convert_priority', 'convert_data_id', 'need_censored', 'is_crawler', 'crawler_id', 'created_by', 'updated_by', 'reviewed_by'], 'integer'],
            [['name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'cp_info', 'original_path', 'poster_path', 'convert_path', 'convert_start_time', 'convert_end_time', 'convert_images', 'meta_info', 'censored_info', 'logo_path', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info', 'tag'], 'safe'],
            [['category_list_film', 'film_series_list', 'actor_list', 'director_list', 'client_list'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsmMediaFilmSeries::find()
            ->distinct()
            ->select('csm_media.*')
            ->where([
                CsmMedia::tableName() . '.type' => TYPE_MEDIA_TRAILER,
                CsmMedia::tableName() . '.status' => [STATUS_MEDIA_PUBLISHED],
            ])->andWhere(CsmMedia::tableName() . '.cp_id IS NOT NULL');
        $query->innerJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.media_id = ' .
            CsmMedia::tableName() . '.id')
            ->innerJoin(['cma' => CsmMediaAttribute::tableName()], CsmMedia::tableName() . '.id = ' .
                'cma.media_id')
            ->innerJoin(['ca' => CsmAttribute::tableName()], 'ca.id = cma.attribute_id')
            ->andWhere([
                CsmMediaPublish::tableName() . '.client_id' => User::getClientList(),
                CsmMediaPublish::tableName() . '.status' => [STATUS_MEDIA_APPROVED_PROPOSAL],
                'ca.type' => TYPE_ATTRIBUTE_FILM_SERIES
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        // process value of category_list_film change type int to int[]
        if ($params[$this->formName()]['category_list_film'] && !is_array($params[$this->formName()]['category_list_film'])) {
            $params[$this->formName()]['category_list_film'] = [$params[$this->formName()]['category_list_film']];
        }

        // process value of film_series_list change type int to int[]
        if ($params[$this->formName()]['film_series_list'] && !is_array($params[$this->formName()]['film_series_list'])) {
            $params[$this->formName()]['film_series_list'] = [$params[$this->formName()]['film_series_list']];
        }

        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $split = explode(' - ', $params[$this->formName()]['created_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.created_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }
        if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
            $split = explode(' - ', $params[$this->formName()]['updated_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.updated_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            CsmMedia::tableName() . '.status' => $this->status,
            CsmMedia::tableName() . '.price_download' => $this->price_download,
            CsmMedia::tableName() . '.price_play' => $this->price_play,
            CsmMedia::tableName() . '.type' => $this->type,
            CsmMedia::tableName() . '.max_quantity' => $this->max_quantity,
            CsmMedia::tableName() . '.published_by' => $this->published_by,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'published_at' => $this->published_at,
            CsmMedia::tableName() . '.duration' => $this->duration,
            CsmMedia::tableName() . '.cp_id' => $this->cp_id,
            CsmMedia::tableName() . '.file_type' => $this->file_type,
            CsmMedia::tableName() . '.convert_status' => $this->convert_status,
            CsmMedia::tableName() . '.convert_priority' => $this->convert_priority,
            CsmMedia::tableName() . '.convert_start_time' => $this->convert_start_time,
            CsmMedia::tableName() . '.convert_end_time' => $this->convert_end_time,
            CsmMedia::tableName() . '.convert_data_id' => $this->convert_data_id,
            CsmMedia::tableName() . '.need_censored' => $this->need_censored,
            CsmMedia::tableName() . '.is_crawler' => $this->is_crawler,
            CsmMedia::tableName() . '.crawler_id' => $this->crawler_id,
            CsmMedia::tableName() . '.created_by' => $this->created_by,
            CsmMedia::tableName() . '.updated_by' => $this->updated_by,
            CsmMedia::tableName() . '.reviewed_by' => $this->reviewed_by,
        ]);

        $query->andFilterWhere(['like', CsmMedia::tableName() . '.name', trim($this->name)])
            ->andFilterWhere(['like', CsmMedia::tableName().'.id', trim($this->id)])
//            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.short_desc', $this->short_desc])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.description', $this->description])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.resolution', $this->resolution])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.attributes', $this->attributes])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.cp_info', $this->cp_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.original_path', $this->original_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.image_path', $this->image_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.poster_path', $this->poster_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.convert_path', $this->convert_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.convert_images', $this->convert_images])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.meta_info', $this->meta_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.censored_info', $this->censored_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.logo_path', $this->logo_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_title', $this->seo_title])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_description', $this->seo_description])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.published_list', $this->published_list])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.tag', $this->tag]);

        if ($this->category_list_film && count($this->category_list_film)) {
            $query->innerJoin(CsmMediaAttribute::tableName(), CsmMedia::tableName() . '.id = ' .
                CsmMediaAttribute::tableName() . '.media_id')
                ->innerJoin(CsmAttribute::tableName(), CsmAttribute::tableName() . '.id = ' .
                    CsmMediaAttribute::tableName() . '.attribute_id')
                ->andWhere([CsmMediaAttribute::tableName() . '.attribute_id' => $this->category_list_film])
                ->andWhere([CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_CATEGORY_FILM]);
        }

        if ($this->film_series_list && count($this->film_series_list)) {
            $query->innerJoin(CsmMediaAttribute::tableName(), CsmMedia::tableName() . '.id = ' .
                CsmMediaAttribute::tableName() . '.media_id')
                ->innerJoin(CsmAttribute::tableName(), CsmAttribute::tableName() . '.id = ' .
                    CsmMediaAttribute::tableName() . '.attribute_id')
                ->andWhere([CsmMediaAttribute::tableName() . '.attribute_id' => $this->film_series_list])
                ->andWhere([CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_FILM_SERIES]);
        }

        return $dataProvider;
    }
}

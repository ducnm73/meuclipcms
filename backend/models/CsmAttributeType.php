<?php

namespace backend\models;

use backend\components\behaviors\VietnameseSlugBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Html;

class CsmAttributeType extends \common\models\CsmAttributeTypeBase {
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => VietnameseSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ]
        ];
    }

    public static function getAllSelect2() {
        $items = CsmAttributeType::find()->where(['is_active' => ACTIVE])->orderBy('name')->all();
        $arrItem = [];
        foreach ($items as $item) {
            $arrItem[$item->id] = Html::encode($item->name);
        }
        return $arrItem;
    }
}
<?php

namespace backend\models;

use awesome\backend\behavior\ManyToManyBehavior;
use backend\components\behaviors\TrimBehavior;
use backend\components\behaviors\VietnameseSlugBehavior;
use MongoDB\Driver\Query;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is updated the model class for table "csm_media".
 *
 * @property int[] $client_list
 * @property int[] $category_list
 * @property int[] $category_list_music
 * @property int[] $category_list_film
 * @property int[] $category_list_tv_show
 * @property int[] $film_series_list
 * @property int[] $tv_show_series_list
 * @property int[] $composer_list
 * @property int[] $singer_list
 * @property int[] $actor_list
 * @property int[] $director_list
 * @property int[] $tv_show_list
 * @property int[] $film_list
 * @property int[] $sitcom_list
 *
 * @property CsmCopyright $csmCopyright
 * @property CsmCp $cp
 * @property CsmConvertData $convertData
 * @property CsmMediaAttribute[] $csmMediaAttributes
 * @property CsmMediaPublish[] $csmMediaPublishes
 */
class CsmMediaTvShow extends \common\models\CsmMediaBase
{
    public $watched_duration;
    public $meta_album;
    public $meta_year;
    public $meta_track;
    public $meta_comment;
    public $meta_copyright;
    public $meta_author;
    public $meta_country;
    public $meta_language;
    public $meta_subtitle_language;
    public $meta_content_filter;
    public $meta_imdb_rating;
    public $episode_no;
    public $episode_name;
    public $media_client_status;
    public $reject_reason;
//    public $media_type = TYPE_MEDIA_CLIP;
    public $validate_media_status = [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED,
        STATUS_MEDIA_APPROVED, STATUS_MEDIA_TRANSFER_PENDING, STATUS_MEDIA_TRANSFER_SUCCESS, STATUS_MEDIA_PUBLISHED];
    public $validate_media_type = [TYPE_MEDIA_TV, TYPE_MEDIA_TV_SERIES];

    public function rules()
    {
        return array_merge([
            [['category_list_tv_show', 'tv_show_series_list', 'actor_list', 'director_list', 'tv_show_list', 'client_list'], 'each', 'rule' => ['integer']],
            [['meta_album', 'meta_year', 'meta_track', 'meta_comment', 'meta_copyright', 'meta_author',
                'meta_country', 'meta_language', 'meta_subtitle_language', 'meta_content_filter',
                'meta_imdb_rating', 'episode_name', 'media_client_status', 'reject_reason', 'watched_duration', 'subtitle_path', 'audio_path', 'convert_audio'], 'safe'],
            [['meta_copyright', 'meta_author'], 'string', 'max' => 128],
            [['episode_name'], 'string', 'max' => 50],
            [['watched_duration', 'episode_no', 'convert_priority'], 'integer'],
            ['status', 'in', 'range' => $this->validate_media_status],
            ['type', 'in', 'range' => $this->validate_media_type],
            [['name'], 'required'],
            [['image_path'], 'required', 'message' => Yii::t('backend', 'Horizontal image can not be blank')],
            [['original_path'], 'required', 'message' => Yii::t('backend', 'Video file can not be blank')],
            ['client_list', 'required', 'message' => Yii::t('backend', 'Distributed service can not be blank')],
            [['name'], 'match', 'pattern' => '/^.*[a-zA-Z0-9].*$/i', 'message' => Yii::t('backend', 'Name must contain at least 1 alphabet character or number')],
            [['episode_no'], 'compare', 'compareValue' => 0, 'operator' => '>=', 'message' => Yii::t('backend', 'Số tập phải lớn hơn 0')],
            [['convert_priority'], 'integer', 'min' => 0],
            [['convert_priority'], 'compare', 'compareValue' => 10000, 'operator' => '<=', 'message' => Yii::t('backend', 'Độ ưu tiên convert không được quá 10000')],
            [['episode_no'], 'compare', 'compareValue' => 10000, 'operator' => '<=', 'message' => Yii::t('backend', 'Số tập không được quá 10000')],
            ['is_crawler', 'default', 'value' => 0],
            [['published_at'], 'required', 'when' => function ($model) {
                return $model->status == STATUS_MEDIA_PUBLISHED;
            }, 'whenClient' => "function (attribute, value) {
                    return false;
                }"],
//            [['published_at'],'required', 'on' => self::SCENARIO_FILM, 'message' => Yii::t('backend', 'Thời gian xuất bản không được để trống')],
//            [['published_at'],'required', 'on' => self::SCENARIO_FILM_SERIES, 'message' => Yii::t('backend', 'Thời gian xuất bản không được để trống')],
        ], parent::rules()
        );
    }

//    public function init()
//    {
//        $this->type = $this->media_type;
//        parent::init();
//    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => VietnameseSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
            [
                // trim tất cả các trường
                'class' => TrimBehavior::className()
            ],
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'client_list' => [
                        'attrClients',
                        'viaTableValues' => [
                            'published_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'published_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            },
                            'status' => function ($model, $relationName, $attributeName, $relatedPk) {
                                /* @var CsmMedia $model */
                                return $model->media_client_status;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(ApiClient::find()
                                ->select(ApiClient::tableName() . '.id')
                                ->leftJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.client_id=' .
                                    ApiClient::tableName() . '.id')
                                ->where([
                                    CsmMediaPublish::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaPublish::tableName() . '.client_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'category_list_tv_show' => [
                        'attrTvShowCategories',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'tv_show_list' => [
                        'attrTvShows',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_TV_SHOW,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'tv_show_series_list' => [
                        'attrTvShowSeries',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            },
                            'info' => function ($model, $relationName, $attributeName, $relatedPk) {
                                /* @var CsmMedia $model */
                                return $model->episode_name;
                            },
                            'position' => function ($model, $relationName, $attributeName, $relatedPk) {
                                /* @var CsmMedia $model */
                                return $model->episode_no;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'actor_list' => [
                        'attrActors',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_ACTOR,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'director_list' => [
                        'attrDirectors',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_DIRECTOR,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                ]
            ]
        ];
    }

    /**
     * Bind metadata
     */
    public function afterFind()
    {
        $meta = $this->meta_info;
        if ($meta) {
            $meta = json_decode($meta, true);
            if ($meta[META_ALBUM]) {
                $this->meta_album = $meta[META_ALBUM];
            }
            if ($meta[META_YEAR]) {
                $this->meta_year = $meta[META_YEAR];
            }
            if ($meta[META_TRACK]) {
                $this->meta_track = $meta[META_TRACK];
            }
            if ($meta[META_COMMENT]) {
                $this->meta_comment = $meta[META_COMMENT];
            }
            if ($meta[META_COPYRIGHT]) {
                $this->meta_copyright = $meta[META_COPYRIGHT];
            }
            if ($meta[META_AUTHOR]) {
                $this->meta_author = $meta[META_AUTHOR];
            }
            if ($meta[META_COUNTRY]) {
                $this->meta_country = $meta[META_COUNTRY];
            }
            if ($meta[META_LANGUAGE]) {
                $this->meta_language = $meta[META_LANGUAGE];
            }
            if ($meta[META_SUBTITLE_LANGUAGE]) {
                $this->meta_subtitle_language = $meta[META_SUBTITLE_LANGUAGE];
            }
            if ($meta[META_CONTENT_FILTER]) {
                $this->meta_content_filter = $meta[META_CONTENT_FILTER];
            }
            if ($meta[META_IMDB_RATING]) {
                $this->meta_imdb_rating = $meta[META_IMDB_RATING];
            }
        }
        if ($this->type == TYPE_MEDIA_TV_SERIES) {
            $attributes = json_decode($this->attributes, true);
            foreach ($attributes as $attr) {
                if ($attr['type'] == TYPE_ATTRIBUTE_TV_SHOW_SERIES) {
                    $this->episode_no = $attr['position'];
                    $this->episode_name = $attr['info'];
                    break;
                }
            }
        }
        parent::afterFind();
    }

    public function getAttrClients()
    {
        return $this->hasMany(ApiClient::className(), ['id' => 'client_id'])
            ->viaTable('csm_media_publish', ['media_id' => 'id']);
    }


    public function getAttrTvShowCategories()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrTvShows()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_TV_SHOW])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrTvShowSeries()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrActors()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_ACTOR])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrDirectors()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_DIRECTOR])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getNumConvertPath()
    {
        return $this->convert_path ? count(json_decode($this->convert_path, true)) : 0;
    }



    public function cloneForReUpload()
    {
        $newObj = new CsmMediaTvShow();
        $newObj->name = $this->name;
        $newObj->short_desc = $this->short_desc;
        $newObj->description = $this->description;
        $newObj->price_download = $this->price_download;
        $newObj->price_play = $this->price_play;
        $newObj->attributes = $this->attributes;
        $newObj->cp_id = $this->cp_id;
        $newObj->cp_info = $this->cp_info;
        $newObj->meta_info = $this->meta_info;
        $newObj->censored_info = $this->censored_info;
        $newObj->logo_path = $this->logo_path;
        $newObj->need_censored = $this->need_censored;
        $newObj->seo_title = $this->seo_title;
        $newObj->seo_description = $this->seo_description;
        $newObj->seo_keywords = $this->seo_keywords;
        $newObj->published_list = $this->published_list;
        $newObj->tag = $this->tag;
        $newObj->need_encryption = $this->need_encryption;
        $newObj->drm_id = $this->drm_id;
        $newObj->media_info = $this->media_info;
        $newObj->copyright_id = $this->copyright_id;
        $newObj->copyright_info = $this->copyright_info;
        $newObj->client_list = $this->client_list;
        $newObj->category_list = $this->category_list;
        $newObj->category_list_music = $this->category_list_music;
        $newObj->category_list_film = $this->category_list_film;
        $newObj->category_list_tv_show = $this->category_list_tv_show;
        $newObj->film_series_list = $this->film_series_list;
        $newObj->tv_show_series_list = $this->tv_show_series_list;
        $newObj->composer_list = $this->composer_list;
        $newObj->singer_list = $this->singer_list;
        $newObj->actor_list = $this->actor_list;
        $newObj->director_list = $this->director_list;
        $newObj->tv_show_list = $this->tv_show_list;
        $newObj->film_list = $this->film_list;
        $newObj->sitcom_list = $this->sitcom_list;
        $newObj->watched_duration = $this->watched_duration;
        $newObj->meta_album = $this->meta_album;
        $newObj->meta_year = $this->meta_year;
        $newObj->meta_track = $this->meta_track;
        $newObj->meta_comment = $this->meta_comment;
        $newObj->meta_copyright = $this->meta_copyright;
        $newObj->meta_author = $this->meta_author;
        $newObj->meta_country = $this->meta_country;
        $newObj->meta_language = $this->meta_language;
        $newObj->meta_subtitle_language = $this->meta_subtitle_language;
        $newObj->meta_content_filter = $this->meta_content_filter;
        $newObj->meta_imdb_rating = $this->meta_imdb_rating;
        $newObj->episode_no = $this->episode_no;
        $newObj->episode_name = $this->episode_name;
        $newObj->media_client_status = $this->media_client_status;
        $newObj->validate_media_status = $this->validate_media_status;
        $newObj->validate_media_type = $this->validate_media_type;
        return $newObj;
    }

    public static function updateStatus($ids, $status, $currentStatus, $cpId)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], [
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])
            ->execute();
    }

    public static function checkAndUpdateStatus($ids, $status, $currentStatus, $cpId)
    {
        if (CsmMedia::find()->where([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])->count() > 0
        ) {
            Yii::$app->db->createCommand()
                ->update(CsmMedia::tableName(), ['status' => $status], [
                    'id' => $ids,
                    'status' => $currentStatus,
                    'cp_id' => $cpId
                ])
                ->execute();
            return true;
        }
        return false;
    }

    public static function updateStatusAdmin($ids, $status, $currentStatus, $more = array())
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus
            ], $more))
            ->execute();
    }

    public static function updateStatusMore($ids, $status, $currentStatus, $cpId, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ], $more))
            ->execute();
    }

    public static function updateStatusAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function approvedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId(),
                'published_by' => Yii::$app->user->getIdentity()->getId(),
                'published_at' => date('Y-m-d H:i:s', time())
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function reviewedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId()
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function getCreatedById($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = self::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED],
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    // lấy ra các video status 6
    public static function getMediaPublish($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = self::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => STATUS_MEDIA_PUBLISHED,
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    public static function checkExistMedia($name, $type, $duration, $maxQuantity)
    {
        return CsmMedia::find()->where([
            'name' => $name,
            'type' => $type,
            'duration' => $duration,
            'max_quantity' => $maxQuantity,
        ])->count();
    }

    public static function getByIds($ids)
    {
        return CsmMedia::find()
            ->where([
                'id' => $ids,
            ])
            ->all();
    }
}
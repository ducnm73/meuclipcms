<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Exception;

/**
 * @property ApiClient $client
 * @property CsmMedia $media
 */
class CsmMediaPublish extends \common\models\CsmMediaPublishBase
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'published_at',
                'updatedAtAttribute' => 'published_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'published_by',
                'updatedByAttribute' => 'published_by',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(ApiClient::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(CsmMedia::className(), ['id' => 'media_id']);
    }

    public static function updateStatus($ids, $status, $currentStatus, $clientIds)
    {
        try {
            return Yii::$app->db->createCommand()
                ->update(CsmMediaPublish::tableName(),
                    [
                        'status' => $status,
                        'published_by' => User::getUserId(),
                        'published_at' => new \yii\db\Expression('NOW()')
                    ],
                    [
                        'media_id' => $ids,
                        'client_id' => $clientIds,
                        'status' => $currentStatus
                    ])
                ->execute();
        } catch (Exception $e) {
            Yii::error("updateStatus - error: " . $e->getTraceAsString());
        }
        return false;
    }

    public static function updateStatusCp($ids, $status, $currentStatus)
    {
        try {
            return Yii::$app->db->createCommand()
                ->update(CsmMediaPublish::tableName(),
                    [
                        'status' => $status,
                        'published_by' => User::getUserId(),
                        'published_at' => new \yii\db\Expression('NOW()')
                    ],
                    [
                        'media_id' => $ids,
                        'status' => $currentStatus
                    ])
                ->execute();
        } catch (Exception $e) {
            Yii::error("updateStatus - error: " . $e->getTraceAsString());
        }
        return false;
    }

    // nhũng client nào có trong cấu hình cần phê duyệt hợp đồng sẽ chuyển status -> 11
    public static function updateWithNeedContractApprove($ids, $currentStatus)
    {
        $normal_status = STATUS_MEDIA_APPROVED_PROPOSAL;
        $wait_contract_approve_status = STATUS_MEDIA_WAIT_CONTRACT_APPROVE;
        $list_client = CsmConfig::getConfig('CLIENT_NEED_APPROVE_CONTRACT');
        try {
            if ($list_client) {
                $q = Yii::$app->db->createCommand()
                    ->update(CsmMediaPublish::tableName(),
                        [
                            'status' => new Expression('CASE WHEN client_id IN (' . $list_client . ') THEN '. $wait_contract_approve_status .' ELSE ' . $normal_status . ' END'),
                            'published_by' => User::getUserId(),
                            'published_at' => new \yii\db\Expression('NOW()')
                        ],
                        [
                            'media_id' => $ids,
                            'status' => $currentStatus
                        ]);
            } else {
                $q = Yii::$app->db->createCommand()
                    ->update(CsmMediaPublish::tableName(),
                        [
                            'status' => $normal_status,
                            'published_by' => User::getUserId(),
                            'published_at' => new \yii\db\Expression('NOW()')
                        ],
                        [
                            'media_id' => $ids,
                            'status' => $currentStatus
                        ]);
            }

            return $q->execute();
        } catch (Exception $e) {
            Yii::error("updateStatus - error: " . $e->getTraceAsString());
        }
        return false;
    }


    public static function updateAllStatus($ids, $status)
    {
        try {
            return Yii::$app->db->createCommand()
                ->update(CsmMediaPublish::tableName(),
                    [
                        'status' => $status,
                        'published_by' => User::getUserId(),
                        'published_at' => new \yii\db\Expression('NOW()')
                    ],
                    [
                        'media_id' => $ids
                    ])
                ->execute();
        } catch (Exception $e) {
            Yii::error("updateStatus - error: " . $e->getTraceAsString());
        }
        return false;
    }

}
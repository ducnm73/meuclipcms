<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Exception;

/**
 * @property ApiClient $client
 * @property CsmMedia $media
 */
class CpApproveReport extends \common\models\CsmMediaActionLogBase
{
    public $user_id;
    public $user_name;
    public $media_name;
    public $video_duration;
    public $revenue;
    public $client_name;
    public $published_at;

    public function rules()
    {
        return [
            [['media_id', 'action', 'created_by', 'watched_duration'], 'integer'],
            [['created_at'], 'safe'],
            [['note', 'updated_fields', 'all_fields', 'all_old_fields'], 'string'],
            [['user_id', 'user_name', 'media_name', 'watched_duration', 'video_duration', 'note', 'revenua', 'client_name', 'published_at'], 'safe']
        ];
    }


    public static function getTotalData($searchModel)
    {
        $user = Yii::$app->user->identity;
        $query = CpApproveReport::find()
            ->select(
                CsmMediaActionLog::tableName() . '.watched_duration AS watched_duration,'
                . CsmMediaActionLog::tableName() . '.action AS action,'
                . CsmMedia::tableName() . '.duration as video_duration')
            ->leftJoin(CsmMedia::tableName(), CsmMediaActionLog::tableName() . '.media_id = ' . CsmMedia::tableName() . '.id')
            ->leftJoin(User::tableName(), CsmMediaActionLog::tableName() . '.created_by = ' . User::tableName() . '.id')
            ->where(CsmMediaActionLog::tableName() . '.created_by = ' . $user->id)
            ->andWhere([CsmMediaActionLog::tableName() . '.action' => [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE]]);

        if (strpos($searchModel->published_at, ' - ') > 0) {
            $start_dates = self::splitDate($searchModel->published_at, 'd-m-Y');
            $query->andWhere(CsmMediaActionLog::tableName() . '.created_at >= :fromDate', [':fromDate' => $start_dates[0]])
                ->andWhere(CsmMediaActionLog::tableName() . '.created_at <= :toDate', [':toDate' => $start_dates[1]]);

        } else {
            $query->andFilterWhere(['published_at' => $searchModel->published_at]);
        }

        $allData = $query->all();
        $totalWatchedDuration = 0;

        foreach ($allData as $item) {
            if (in_array($item->action, [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE])) {
                if ($item->watched_duration === NULL) {
                    $item->watched_duration = ($item->video_duration) ? $item->video_duration : 0;
                }

            }
            $totalWatchedDuration = $totalWatchedDuration + $item->watched_duration;

        }

        $totalRevenue = number_format($totalWatchedDuration * Yii::$app->params['revenua_per_minute_approve'] / 60, 2);

        return [
            'watched_duration' => $totalWatchedDuration,
            'revenue' => $totalRevenue
        ];
    }


    public static function getTotalCpData($searchModel)
    {
        $query = CpApproveReport::find()
            ->select(CsmMediaActionLog::tableName() . '.watched_duration AS watched_duration,'
                . CsmMediaActionLog::tableName() . '.action AS action,'
                . CsmMedia::tableName() . '.duration as video_duration')
            ->join('INNER JOIN', CsmMedia::tableName(), CsmMediaActionLog::tableName() . '.media_id = ' . CsmMedia::tableName() . '.id')
            ->join('INNER JOIN', User::tableName(), CsmMediaActionLog::tableName() . '.created_by = ' . User::tableName() . '.id')
            ->where([CsmMediaActionLog::tableName() . '.action' => [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE]]);

        if ($searchModel->user_name) {
            // neu co filter
            $query->andFilterWhere([
                User::tableName() . '.username' => $searchModel->user_name
            ]);
        }else{
            $queryGetListUser = UserClient::find()
                ->distinct()
                ->asArray()
                ->select('user_id')
                ->where(['client_id' => User::getClientList()]);

            $allUserIdHaveClientId = $queryGetListUser->all();
            $allUserIdHaveClientId = array_column($allUserIdHaveClientId, 'user_id');
            $query->andWhere([CsmMediaActionLog::tableName() . '.created_by' => $allUserIdHaveClientId]);
        }

        if (strpos($searchModel->published_at, ' - ') > 0) {
            $start_dates = self::splitDate($searchModel->published_at, 'd-m-Y');
            $query->andWhere(CsmMediaActionLog::tableName() . '.created_at >= :fromDate', [':fromDate' => $start_dates[0]])
                ->andWhere(CsmMediaActionLog::tableName() . '.created_at <= :toDate', [':toDate' => $start_dates[1]]);

        }

        $allData = $query->all();
        $totalWatchedDuration = 0;
        foreach ($allData as $item) {
            if (in_array($item->action, [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE])) {

                if ($item->watched_duration === null) {

                    $item->watched_duration = ($item->video_duration) ? $item->video_duration : 0;

                }
            }
            $totalWatchedDuration = $totalWatchedDuration + $item->watched_duration;

        }

        $totalRevenue = number_format($totalWatchedDuration * Yii::$app->params['revenua_per_minute_approve'] / 60, 2);

        return [
            'watched_duration' => $totalWatchedDuration,
            'revenue' => $totalRevenue
        ];
    }

    public static function splitDate($date, $display_format)
    {
        $dates = explode(' - ', $date);
        $dates[0] = date_create_from_format($display_format, $dates[0])->format('Y-m-d 00:00:00');
        $dates[1] = date_create_from_format($display_format, $dates[1])->format('Y-m-d 23:59:59');
        return $dates;
    }

}
<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaFilmSeriesCreateSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaFilmSeriesCreateSearch extends CsmMediaFilmSeries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_download', 'price_play', 'type', 'published_by', 'cp_id', 'file_type', 'convert_status', 'convert_priority', 'convert_data_id', 'need_censored', 'is_crawler', 'crawler_id', 'created_by', 'updated_by', 'reviewed_by', 'need_encryption'], 'integer'],
            [['id', 'name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'cp_info', 'original_path', 'poster_path', 'convert_path', 'convert_start_time', 'convert_end_time', 'convert_images', 'meta_info', 'censored_info', 'logo_path', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info', 'tag'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = CsmMediaFilmSeries::find()
            ->where([
                'type' => TYPE_MEDIA_FILM_SERIES,
                'status' => [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED],
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $split = explode(' - ', $params[$this->formName()]['created_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.created_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }
        if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
            $split = explode(' - ', $params[$this->formName()]['updated_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.updated_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['need_encryption' => $this->need_encryption]);
        $query->andFilterWhere(['like', 'name', trim($this->name)])
            ->andFilterWhere(['like', 'id', trim($this->id)])
           // ->andFilterWhere(['like', 'created_at', trim($this->created_at)])
           // ->andFilterWhere(['like', 'updated_at', trim($this->updated_at)])
//            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'attributes', $this->attributes])
            ->andFilterWhere(['like', 'cp_info', $this->cp_info])
            ->andFilterWhere(['like', 'original_path', $this->original_path])
            ->andFilterWhere(['like', 'image_path', $this->image_path])
            ->andFilterWhere(['like', 'poster_path', $this->poster_path])
            ->andFilterWhere(['like', 'convert_path', $this->convert_path])
            ->andFilterWhere(['like', 'convert_images', $this->convert_images])
            ->andFilterWhere(['like', 'meta_info', $this->meta_info])
            ->andFilterWhere(['like', 'censored_info', $this->censored_info])
            ->andFilterWhere(['like', 'logo_path', $this->logo_path])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', 'published_list', $this->published_list])
            ->andFilterWhere(['like', 'tag', $this->tag]);

        return $dataProvider;
    }
}

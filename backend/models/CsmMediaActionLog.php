<?php

namespace backend\models;

use common\models\CsmMediaActionLogBase;
use Yii;

class CsmMediaActionLog extends \common\models\CsmMediaActionLogBase
{
    public static function saveActionLog($mediaId, $action, $note, $createBy, $watchedDuration)
    {
        $model = new CsmMediaActionLog(['media_id' => $mediaId, 'action' => $action, 'note' => $note, 'created_by' => $createBy, 'watched_duration' => $watchedDuration]);
        $model->save(false);
    }
}
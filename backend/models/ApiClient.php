<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class ApiClient extends \common\models\ApiClientBase
{
    public function rules()
    {
        return array_merge([
            [['name'], 'required'],
        ],
            parent::rules());
    }

    public static function getAll()
    {
        return ApiClient::find()->all();
    }
    public static function getUserClient()
    {
        $userClientList = self::getUserAccessClients();
        return ApiClient::find()
            ->where(['id' => $userClientList])->all();
    }

    public static function getByIds($ids)
    {
        return ApiClient::find()->where(['id' => $ids])->all();
    }


    // user được upload cho dịch vụ nào
    public static function getUserAccessClients() {
        if (!Yii::$app->user->isGuest) {
            $clients = Yii::$app->session->get('client_list');
            if ($clients && count($clients)) {
                return ArrayHelper::toArray($clients);
            }
        }
        return [];
    }

    public function generateJsonObj() {
        return $this->getAttributes(array('id', 'name', 'client_id'));
    }
}
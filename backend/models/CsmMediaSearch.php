<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaSearch extends CsmMedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'price_download', 'price_play', 'type', 'max_quantity', 'published_by', 'duration', 'cp_id', 'file_type', 'convert_priority', 'is_crawler', 'crawler_id', 'created_by', 'reviewed_by'], 'integer'],
            [['name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'resolution', 'attributes', 'cp_info', 'original_path', 'image_path', 'convert_path', 'convert_start_time', 'convert_end_time', 'tag', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info', 'published_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CsmMedia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'price_download' => $this->price_download,
            'price_play' => $this->price_play,
            'type' => $this->type,
            'max_quantity' => $this->max_quantity,
            'published_by' => $this->published_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'published_at' => $this->published_at,
            'duration' => $this->duration,
            'cp_id' => $this->cp_id,
            'file_type' => $this->file_type,
            'convert_priority' => $this->convert_priority,
            'convert_start_time' => $this->convert_start_time,
            'convert_end_time' => $this->convert_end_time,
            'is_crawler' => $this->is_crawler,
            'crawler_id' => $this->crawler_id,
            'created_by' => $this->created_by,
            'reviewed_by' => $this->reviewed_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'attributes', $this->attributes])
            ->andFilterWhere(['like', 'cp_info', $this->cp_info])
            ->andFilterWhere(['like', 'original_path', $this->original_path])
            ->andFilterWhere(['like', 'image_path', $this->image_path])
            ->andFilterWhere(['like', 'convert_path', $this->convert_path])
            ->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', 'published_list', $this->published_list]);

        return $dataProvider;
    }
}

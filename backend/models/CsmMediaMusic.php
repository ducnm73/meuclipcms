<?php

namespace backend\models;

use awesome\backend\behavior\ManyToManyBehavior;
use backend\components\behaviors\TrimBehavior;
use backend\components\behaviors\VietnameseSlugBehavior;
use MongoDB\Driver\Query;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is updated the model class for table "csm_media".
 *
 * @property int[] $client_list
 * @property int[] $category_list
 * @property int[] $category_list_music
 * @property int[] $category_list_film
 * @property int[] $category_list_tv_show
 * @property int[] $film_series_list
 * @property int[] $tv_show_series_list
 * @property int[] $composer_list
 * @property int[] $singer_list
 * @property int[] $actor_list
 * @property int[] $director_list
 * @property int[] $tv_show_list
 * @property int[] $film_list
 * @property int[] $sitcom_list
 *
 * @property CsmCopyright $csmCopyright
 * @property CsmCp $cp
 * @property CsmConvertData $convertData
 * @property CsmMediaAttribute[] $csmMediaAttributes
 * @property CsmMediaPublish[] $csmMediaPublishes
 */
class CsmMediaMusic extends \common\models\CsmMediaBase
{
    public $watched_duration;
    public $meta_album;
    public $meta_year;
    public $meta_track;
    public $meta_comment;
    public $meta_copyright;
    public $meta_author;
    public $meta_country;
    public $meta_language;
    public $meta_subtitle_language;
    public $meta_content_filter;
    public $meta_imdb_rating;
    public $episode_no;
    public $episode_name;
    public $media_client_status;
    public $reject_reason;
    public $validate_media_status = [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED,
        STATUS_MEDIA_APPROVED, STATUS_MEDIA_TRANSFER_PENDING, STATUS_MEDIA_TRANSFER_SUCCESS, STATUS_MEDIA_PUBLISHED];
    public $validate_media_type = [TYPE_MEDIA_MUSIC];

    public function rules()
    {
        return array_merge([
            [['category_list_music', 'composer_list', 'singer_list'], 'each', 'rule' => ['integer']],
            [['meta_album', 'meta_year', 'meta_track', 'meta_comment', 'meta_copyright', 'meta_author',
                'meta_country', 'meta_language', 'meta_subtitle_language', 'meta_content_filter',
                'meta_imdb_rating', 'episode_name', 'media_client_status', 'reject_reason', 'watched_duration', 'subtitle_path', 'audio_path', 'convert_audio'], 'safe'],
            [['watched_duration', 'episode_no', 'convert_priority'], 'integer'],
            ['status', 'in', 'range' => $this->validate_media_status],
            ['type', 'in', 'range' => $this->validate_media_type],
            [['name'], 'required'],
            [['image_path'], 'required', 'message' => Yii::t('backend', 'Horizontal image can not be blank')],
            [['original_path'], 'required', 'message' => Yii::t('backend', 'Video file can not be blank')],
            ['client_list', 'required', 'message' => Yii::t('backend', 'Distributed service can not be blank')],
            [['name'], 'match', 'pattern' => '/^.*[a-zA-Z0-9].*$/i', 'message' => Yii::t('backend', 'Name must contain at least 1 alphabet character or number')],
            [['convert_priority'], 'integer', 'min' => 0],
            [['convert_priority'], 'compare', 'compareValue' => 10000, 'operator' => '<=', 'message' => Yii::t('backend', 'Độ ưu tiên convert không được quá 10000')],
            ['is_crawler', 'default', 'value' => 0],
            [['published_at'], 'required', 'when' => function ($model) {
                return $model->status == STATUS_MEDIA_PUBLISHED;
            }, 'whenClient' => "function (attribute, value) {
                    return false;
                }"],
        ], parent::rules()
        );
    }

//    public function init()
//    {
//        $this->type = $this->media_type;
//        parent::init();
//    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => VietnameseSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
            [
                // trim tất cả các trường
                'class' => TrimBehavior::className()
            ],
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'client_list' => [
                        'attrClients',
                        'viaTableValues' => [
                            'published_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'published_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            },
                            'status' => function ($model, $relationName, $attributeName, $relatedPk) {
                                /* @var CsmMedia $model */
                                return $model->media_client_status;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(ApiClient::find()
                                ->select(ApiClient::tableName() . '.id')
                                ->leftJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.client_id=' .
                                    ApiClient::tableName() . '.id')
                                ->where([
                                    CsmMediaPublish::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaPublish::tableName() . '.client_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'category_list_music' => [
                        'attrMusicCategories',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'composer_list' => [
                        'attrComposers',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_COMPOSER,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'singer_list' => [
                        'attrSingers',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_SINGER,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                ]
            ]
        ];
    }
    /**
     * Bind metadata
     */
    public function afterFind()
    {
        $meta = $this->meta_info;
        if ($meta) {
            $meta = json_decode($meta, true);
            if ($meta[META_ALBUM]) {
                $this->meta_album = $meta[META_ALBUM];
            }
            if ($meta[META_YEAR]) {
                $this->meta_year = $meta[META_YEAR];
            }
            if ($meta[META_TRACK]) {
                $this->meta_track = $meta[META_TRACK];
            }
            if ($meta[META_COMMENT]) {
                $this->meta_comment = $meta[META_COMMENT];
            }
            if ($meta[META_COPYRIGHT]) {
                $this->meta_copyright = $meta[META_COPYRIGHT];
            }
            if ($meta[META_AUTHOR]) {
                $this->meta_author = $meta[META_AUTHOR];
            }
            if ($meta[META_COUNTRY]) {
                $this->meta_country = $meta[META_COUNTRY];
            }
            if ($meta[META_LANGUAGE]) {
                $this->meta_language = $meta[META_LANGUAGE];
            }
            if ($meta[META_SUBTITLE_LANGUAGE]) {
                $this->meta_subtitle_language = $meta[META_SUBTITLE_LANGUAGE];
            }
            if ($meta[META_CONTENT_FILTER]) {
                $this->meta_content_filter = $meta[META_CONTENT_FILTER];
            }
            if ($meta[META_IMDB_RATING]) {
                $this->meta_imdb_rating = $meta[META_IMDB_RATING];
            }
        }
        parent::afterFind();
    }


    public function getAttrClients()
    {
        return $this->hasMany(ApiClient::className(), ['id' => 'client_id'])
            ->viaTable('csm_media_publish', ['media_id' => 'id']);
    }

    public function getAttrMusicCategories()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrComposers()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_COMPOSER])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getAttrSingers()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_SINGER])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }


    public function getNumConvertPath()
    {
        return $this->convert_path ? count(json_decode($this->convert_path, true)) : 0;
    }



    public static function updateStatus($ids, $status, $currentStatus, $cpId)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], [
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])
            ->execute();
    }

    public static function checkAndUpdateStatus($ids, $status, $currentStatus, $cpId)
    {
        if (CsmMedia::find()->where([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])->count() > 0
        ) {
            Yii::$app->db->createCommand()
                ->update(CsmMedia::tableName(), ['status' => $status], [
                    'id' => $ids,
                    'status' => $currentStatus,
                    'cp_id' => $cpId
                ])
                ->execute();
            return true;
        }
        return false;
    }

    public static function updateStatusAdmin($ids, $status, $currentStatus, $more = array())
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus
            ], $more))
            ->execute();
    }

    public static function updateStatusMore($ids, $status, $currentStatus, $cpId, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ], $more))
            ->execute();
    }

    public static function updateStatusAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function approvedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId(),
                'published_by' => Yii::$app->user->getIdentity()->getId(),
                'published_at' => date('Y-m-d H:i:s', time())
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function reviewedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId()
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function getCreatedById($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = self::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED],
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    // lấy ra các video status 6
    public static function getMediaPublish($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = CsmMedia::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => STATUS_MEDIA_PUBLISHED,
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    public static function checkExistMedia($name, $type, $duration, $maxQuantity)
    {
        return CsmMedia::find()->where([
            'name' => $name,
            'type' => $type,
            'duration' => $duration,
            'max_quantity' => $maxQuantity,
        ])->count();
    }

    public static function getByIds($ids)
    {
        return CsmMedia::find()
            ->where([
                'id' => $ids,
            ])
            ->all();
    }
}
<?php

namespace backend\models;

use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaClipApproveSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaCustomClipApproveSearch extends CsmMediaClip
{
    public $updatedAtRange;
    public $updatedAtStart;
    public $updatedAtEnd;
    public $client_id;

    public function behaviors()
    {
        return array_merge([
            [
                'class' => DateRangeBehavior::className(),
                'attribute' => 'updatedAtRange',
                'dateStartAttribute' => 'updatedAtStart',
                'dateEndAttribute' => 'updatedAtEnd',
            ]
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_download', 'price_play', 'type', 'max_quantity', 'published_by', 'file_type', 'convert_status', 'convert_priority', 'convert_data_id', 'need_censored', 'is_crawler', 'crawler_id', 'created_by', 'updated_by', 'reviewed_by'], 'integer'],
            [['client_id', 'id', 'name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'attributes', 'cp_info', 'original_path', 'poster_path', 'convert_path', 'convert_start_time', 'convert_end_time', 'convert_images', 'meta_info', 'censored_info', 'logo_path', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info', 'tag'], 'safe'],
            [['updatedAtRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['category_list', 'client_list'], 'each', 'rule' => ['integer']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $user = Yii::$app->user->identity;
        $query = CsmMedia::find()
            ->distinct()
            ->select('csm_media.*')
            ->innerJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.media_id = ' .
            CsmMedia::tableName() . '.id')
            ->where([
                CsmMedia::tableName() . '.type' => TYPE_MEDIA_CLIP,
                CsmMedia::tableName() . '.status' => STATUS_MEDIA_PUBLISHED,
                CsmMedia::tableName() . '.cp_id' => $user->cp_id,
                CsmMediaPublish::tableName() . '.client_id' => User::getClientList(),
                CsmMediaPublish::tableName() . '.status' => STATUS_MEDIA_APPROVED_PROPOSAL,
            ])
            ->andWhere(CsmMedia::tableName() . '.is_crawler = 0 OR ' . CsmMedia::tableName() . '.is_crawler IS NULL');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        // process value of film_list change type int to int[]
        if ($params[$this->formName()]['category_list'] && !is_array($params[$this->formName()]['category_list'])) {
            $params[$this->formName()]['category_list'] = [$params[$this->formName()]['category_list']];
        }

        if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
            $split = explode(' - ', $params[$this->formName()]['updated_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.updated_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }

        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $split = explode(' - ', $params[$this->formName()]['created_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.created_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'price_download' => $this->price_download,
            'price_play' => $this->price_play,
            'type' => $this->type,
            'max_quantity' => $this->max_quantity,
            'published_by' => $this->published_by,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'published_at' => $this->published_at,
            'duration' => $this->duration,
            'cp_id' => $this->cp_id,
            'file_type' => $this->file_type,
            'convert_status' => $this->convert_status,
            'convert_priority' => $this->convert_priority,
            'convert_start_time' => $this->convert_start_time,
            'convert_end_time' => $this->convert_end_time,
            'convert_data_id' => $this->convert_data_id,
            'need_censored' => $this->need_censored,
            'is_crawler' => $this->is_crawler,
            'crawler_id' => $this->crawler_id,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'reviewed_by' => $this->reviewed_by,
        ]);

        $query->andFilterWhere(['like', CsmMedia::tableName().'.name', trim($this->name)])
            ->andFilterWhere(['like', CsmMedia::tableName().'.id', trim($this->id)])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'attributes', $this->attributes])
            ->andFilterWhere(['like', 'cp_info', $this->cp_info])
            ->andFilterWhere(['like', 'original_path', $this->original_path])
            ->andFilterWhere(['like', 'image_path', $this->image_path])
            ->andFilterWhere(['like', 'poster_path', $this->poster_path])
            ->andFilterWhere(['like', 'convert_path', $this->convert_path])
            ->andFilterWhere(['like', 'convert_images', $this->convert_images])
            ->andFilterWhere(['like', 'meta_info', $this->meta_info])
            ->andFilterWhere(['like', 'censored_info', $this->censored_info])
            ->andFilterWhere(['like', 'logo_path', $this->logo_path])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', 'tag', $this->tag]);

        if ($this->category_list && count($this->category_list)) {
            $query->innerJoin(CsmMediaAttribute::tableName(), CsmMedia::tableName() . '.id = ' .
                CsmMediaAttribute::tableName() . '.media_id')
                ->innerJoin(CsmAttribute::tableName(), CsmAttribute::tableName() . '.id = ' .
                    CsmMediaAttribute::tableName() . '.attribute_id')
                ->andWhere([CsmMediaAttribute::tableName() . '.attribute_id' => $this->category_list])
                ->andWhere([CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_CATEGORY]);
        }

        //var_dump($query->createCommand()->getRawSql());die();

        return $dataProvider;
    }
}

<?php

namespace backend\models;

use awesome\backend\grid\AwsFormatter;
use backend\components\behaviors\TrimBehavior;
use backend\components\behaviors\VietnameseSlugBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Html;

class CsmAttribute extends \common\models\CsmAttributeBase
{
    const SCENARIO_FILM_ATTRIBUTE = 'scenario_film_attribute';
    public function rules()
    {
        return array_merge([
            [['image_path', 'second_image'], 'required', 'on' => self::SCENARIO_FILM_ATTRIBUTE]

        ], parent::rules());
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => VietnameseSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
            [
                // trim tất cả các trường
                'class' => TrimBehavior::className()
            ],
        ];
    }

    public function getImageUrl()
    {
        if ($this->image_path) {
            $formater = new AwsFormatter();
            return $formater->asImageStorage($this->getImageUrl());
        }
        return "";
    }

    public function generateJsonObj() {
        return $this->getAttributes(array('id', 'name', 'slug', 'description',
            'image_path', 'second_image', 'type', 'parent_id', 'multiple_language'));
    }

    public function getAttrMedia($mediaId) {
        return CsmMediaAttribute::find()->where([
            'media_id' => $mediaId,
            'attribute_id' => $this->id,
        ])->one();
    }

    public static function getAllCategoryActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_CATEGORY
        ])->orderBy('name')->all();
    }

    public static function getAllCategoryMusicActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC
        ])->orderBy('name')->all();
    }

    public static function getAllCategoryFilmActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_CATEGORY_FILM
        ])->orderBy('name')->all();
    }

    public static function getAllCategoryTvShowActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW
        ])->orderBy('name')->all();
    }

    public static function getAllFilmActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_FILM
        ])->orderBy('name')->all();
    }

    public static function getAllFilmSeriesActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_FILM_SERIES
        ])->orderBy('name')->all();
    }

    public static function getAllTvShowActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_TV_SHOW
        ])->orderBy('name')->all();
    }

    public static function getAllTvShowSeriesActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES
        ])->orderBy('name')->all();
    }

    public static function getAllTvSitcomActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_SITCOM
        ])->orderBy('name')->all();
    }

    public static function getAllComposerActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_COMPOSER
        ])->orderBy('name')->all();
    }

    public static function getAllSingerActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_SINGER
        ])->orderBy('name')->all();
    }

    public static function getAllActorActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_ACTOR
        ])->orderBy('name')->all();
    }

    public static function getAllDirectorActive()
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => TYPE_ATTRIBUTE_DIRECTOR
        ])->orderBy('name')->all();
    }

    public static function getCategoryByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_CATEGORY
        ])->orderBy('name')->all();
    }

    public static function getCategoryMusicByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC
        ])->orderBy('name')->all();
    }

    public static function getCategoryFilmByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_CATEGORY_FILM
        ])->orderBy('name')->all();
    }

    public static function getCategoryTvShowByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW
        ])->orderBy('name')->all();
    }

    public static function getFilmByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_FILM
        ])->orderBy('name')->all();
    }

    public static function getFilmSeriesByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_FILM_SERIES
        ])->orderBy('name')->all();
    }

    public static function getTvShowByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_TV_SHOW
        ])->orderBy('name')->all();
    }

    public static function getTvShowSeriesByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES
        ])->orderBy('name')->all();
    }

    public static function getSitcomByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_SITCOM
        ])->orderBy('name')->all();
    }

    public static function getComposerByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_COMPOSER
        ])->orderBy('name')->all();
    }

    public static function getSingerByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_SINGER
        ])->orderBy('name')->all();
    }

    public static function getActorByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_ACTOR
        ])->orderBy('name')->all();
    }

    public static function getDirectorByIdsActive($ids)
    {
        return CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'id' => $ids,
            'type' => TYPE_ATTRIBUTE_DIRECTOR
        ])->orderBy('name')->all();
    }

    public static function updateStatus($ids, $status)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmAttribute::tableName(), ['is_active' => $status], ['id' => $ids])
            ->execute();
    }


    public static function getAllParentCategory($excludeId = 0)
    {
        $q = CsmAttribute::find()->where(['is_active' => ACTIVE])->orderBy('name');
        if ($excludeId) {
            $q->andWhere(['not in', 'id', [$excludeId]]);
        }
        $cats = $q->all();
        $cats = self::sortCategories($excludeId, $cats);
        $menu = [];
        if (count($cats) > 0) {
            foreach ($cats as $cat) {
                self::bindCatItems($menu, $cat);
            }
        }
        return $menu;
    }

    public static function getTreeByType($excludeId = 0, $type)
    {
        $q = CsmAttribute::find()->where([
            'is_active' => ACTIVE,
            'type' => $type,
        ])->orderBy('name');
        if ($excludeId) {
            $q->andWhere(['not in', 'id', [$excludeId]]);
        }
        $cats = $q->all();
//        var_dump($cats); die;
        $cats = self::sortCategories($cats);
        $menu = [];
        if (count($cats) > 0) {
            foreach ($cats as $cat) {
                self::bindCatItems($menu, $cat);
            }
        }
        return $menu;
    }

    //todo fix loop
    public static function sortCategories($cats, $parent_id = 0, $level = 0)
    {
        $return = array();
        foreach ($cats as $key => $cat) {
            /* @var CsmAttribute $cat */
            if ($parent_id == 0 && ($cat->parent_id == 0 || $cat->parent_id == null)) {
                $level = 0;
                $parent_id = 0;
            }
            if ($cat->parent_id == $parent_id) {
                unset($cats[$key]);
                $return[] = [
                    'id' => $cat->id,
                    'name' => Html::encode($cat->name),
                    'slug' => $cat->slug,
                    'level' => $level,
                    'child' => self::sortCategories($cats, $cat->id, $level + 1)
                ];
            }
        }
        return $return;
    }

    public function bindImagePath()
    {
//        if ($this->image_path) {
//            $decode = json_decode($this->image_path, true);
//            return $decode['domain'] . '/' . $decode['bucket'] . $decode['path'];
//        }
        // fix path ảnh peru, các path lưu ở local
        if ($this->image_path) {
            return Yii::$app->params['upload']['baseUrl'] . $this->image_path ;
        }
        return null;
    }

    public function bindSecondImage()
    {
//        if ($this->second_image) {
//            $decode = json_decode($this->second_image, true);
//            return $decode['domain'] . '/' . $decode['bucket'] . $decode['path'];
//        }
        // fix path ảnh peru, các path lưu ở local
        if ($this->second_image) {
            return Yii::$app->params['upload']['baseUrl'] . $this->second_image ;
        }
        return null;
    }

    public static function bindCatItems(&$menu, $item)
    {
        $menu[$item['id']] = $item['level'] > 0 ? str_repeat('--', $item['level']) . $item['name'] : $item['name'];
        if ($item['child'] != null) {
            foreach ($item['child'] as $child) {
                self::bindCatItems($menu, $child);
            }
        }
    }

    public function afterFind()
    {
        $multiple_language = $this->multiple_language;
        if ($multiple_language) {
            $this->multiple_language = json_decode($multiple_language, true);
        }

        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // multiple_language
        $multiple_language = array();
        foreach ((object)$this->multiple_language as $key => $item) {
            $item['slug'] = '';
            if ($item['name']) {
                $item['name'] = trim($item['name']);
                $item['slug'] = VietnameseSlugBehavior::slugify($item['name']);
            }
            $multiple_language[$key] = $item;
        }
        $this->multiple_language = json_encode($multiple_language, JSON_UNESCAPED_UNICODE);

        return parent::beforeSave($insert);
    }

    public static function checkExistAttribute($name, $type)
    {
        return CsmAttribute::find()->where([
            'name' => $name,
            'type' => $type
        ])->count();
    }
}
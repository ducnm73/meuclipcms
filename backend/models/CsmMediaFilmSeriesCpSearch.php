<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaFilmSeriesCpSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaFilmSeriesCpSearch extends CsmMediaFilmSeries
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'price_download', 'price_play', 'type', 'max_quantity', 'published_by', 'duration', 'cp_id', 'file_type', 'convert_status', 'convert_priority', 'convert_data_id', 'need_censored', 'is_crawler', 'crawler_id', 'created_by', 'updated_by', 'reviewed_by', 'need_encryption', 'drm_id', 'copyright_id', 'media_client_status'], 'integer'],
            [['id', 'name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'attributes', 'cp_info', 'original_path', 'poster_path', 'convert_path', 'convert_start_time', 'convert_end_time', 'convert_images', 'meta_info', 'censored_info', 'logo_path', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info', 'published_list', 'tag', 'resource_id', 'convert_server', 'media_info', 'copyright_info'], 'safe'],
            [['category_list_film', 'film_series_list', 'actor_list', 'director_list', 'client_list'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = CsmMediaPublish::find()
//            ->leftJoin(CsmMedia::tableName(), CsmMedia::tableName() . '.id=' .
//            CsmMediaPublish::tableName() . '.media_id')
            ->joinWith('media')
            ->where([
                CsmMedia::tableName() . '.type' => TYPE_MEDIA_FILM_SERIES,
            ]);

        if ($user->cp_id) {
            $query->andWhere([
                CsmMedia::tableName() . '.cp_id' => $user->cp_id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['media_id' => SORT_DESC]]
        ]);

        // process value of actor_list change type int to int[]
        if ($params[$this->formName()]['actor_list'] && !is_array($params[$this->formName()]['actor_list'])) {
            $params[$this->formName()]['actor_list'] = [$params[$this->formName()]['actor_list']];
        }

        // process value of director_list change type int to int[]
        if ($params[$this->formName()]['director_list'] && !is_array($params[$this->formName()]['director_list'])) {
            $params[$this->formName()]['director_list'] = [$params[$this->formName()]['director_list']];
        }

        // process value of film_series_list change type int to int[]
        if ($params[$this->formName()]['film_series_list'] && !is_array($params[$this->formName()]['film_series_list'])) {
            $params[$this->formName()]['film_series_list'] = [$params[$this->formName()]['film_series_list']];
        }

        // process value of client_list change type int to int[]
        if ($params[$this->formName()]['client_list'] && !is_array($params[$this->formName()]['client_list'])) {
            $params[$this->formName()]['client_list'] = [$params[$this->formName()]['client_list']];
        }

        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $split = explode(' - ', $params[$this->formName()]['created_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.created_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }
        if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
            $split = explode(' - ', $params[$this->formName()]['updated_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.updated_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            CsmMedia::tableName() . '.status' => $this->status,
            CsmMedia::tableName() . '.price_download' => $this->price_download,
            CsmMedia::tableName() . '.price_play' => $this->price_play,
            CsmMedia::tableName() . '.type' => $this->type,
            CsmMedia::tableName() . '.max_quantity' => $this->max_quantity,
            CsmMedia::tableName() . '.published_by' => $this->published_by,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
            CsmMedia::tableName() . '.published_at' => $this->published_at,
            CsmMedia::tableName() . '.duration' => $this->duration,
            CsmMedia::tableName() . '.cp_id' => $this->cp_id,
            CsmMedia::tableName() . '.file_type' => $this->file_type,
            CsmMedia::tableName() . '.convert_status' => $this->convert_status,
            CsmMedia::tableName() . '.convert_priority' => $this->convert_priority,
            CsmMedia::tableName() . '.convert_start_time' => $this->convert_start_time,
            CsmMedia::tableName() . '.convert_end_time' => $this->convert_end_time,
            CsmMedia::tableName() . '.convert_data_id' => $this->convert_data_id,
            CsmMedia::tableName() . '.need_censored' => $this->need_censored,
            CsmMedia::tableName() . '.is_crawler' => $this->is_crawler,
            CsmMedia::tableName() . '.crawler_id' => $this->crawler_id,
            CsmMedia::tableName() . '.created_by' => $this->created_by,
            CsmMedia::tableName() . '.updated_by' => $this->updated_by,
            CsmMedia::tableName() . '.reviewed_by' => $this->reviewed_by,
            CsmMedia::tableName() . '.need_encryption' => $this->need_encryption,
            CsmMedia::tableName() . '.drm_id' => $this->drm_id,
            CsmMedia::tableName() . '.copyright_id' => $this->copyright_id,
            CsmMediaPublish::tableName() . '.status' => $this->media_client_status,
        ]);

        $query->andFilterWhere(['like', CsmMedia::tableName() . '.name', trim($this->name)])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.id', trim($this->id)])
//            ->andFilterWhere(['like', CsmMedia::tableName() . '.slug', $this->slug])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.short_desc', $this->short_desc])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.description', $this->description])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.resolution', $this->resolution])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.attributes', $this->attributes])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.cp_info', $this->cp_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.original_path', $this->original_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.image_path', $this->image_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.poster_path', $this->poster_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.convert_path', $this->convert_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.convert_images', $this->convert_images])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.meta_info', $this->meta_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.censored_info', $this->censored_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.logo_path', $this->logo_path])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_title', $this->seo_title])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_description', $this->seo_description])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.published_list', $this->published_list])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.tag', $this->tag])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.resource_id', $this->resource_id])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.convert_server', $this->convert_server])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.media_info', $this->media_info])
            ->andFilterWhere(['like', CsmMedia::tableName() . '.copyright_info', $this->copyright_info]);

        if ($this->client_list && count($this->client_list)) {
            $query->andWhere([CsmMediaPublish::tableName() . '.client_id' => $this->client_list]);
        }

        if ($this->actor_list && count($this->actor_list)) {
            $query->innerJoin(['ma1' => CsmMediaAttribute::tableName()], CsmMedia::tableName() . '.id = ' .
                'ma1.media_id')
                ->innerJoin(['a1' => CsmAttribute::tableName()], 'a1.id = ma1.attribute_id')
                ->andWhere(['ma1.attribute_id' => $this->actor_list])
                ->andWhere(['a1.type' => TYPE_ATTRIBUTE_ACTOR]);
        }

        if ($this->director_list && count($this->director_list)) {
            $query->innerJoin(['ma2' => CsmMediaAttribute::tableName()], CsmMedia::tableName() . '.id = ' .
                'ma2.media_id')
                ->innerJoin(['a2' => CsmAttribute::tableName()], 'a2.id = ma2.attribute_id')
                ->andWhere(['ma2.attribute_id' => $this->director_list])
                ->andWhere(['a2.type' => TYPE_ATTRIBUTE_DIRECTOR]);
        }

        if ($this->film_series_list && count($this->film_series_list)) {
            $query->innerJoin(['ma2' => CsmMediaAttribute::tableName()], CsmMedia::tableName() . '.id = ' .
                'ma2.media_id')
                ->innerJoin(['a2' => CsmAttribute::tableName()], 'a2.id = ma2.attribute_id')
                ->andWhere(['ma2.attribute_id' => $this->film_series_list])
                ->andWhere(['a2.type' => TYPE_ATTRIBUTE_FILM_SERIES]);
        }
        //var_dump($query->createCommand()->getRawSql());die();

        return $dataProvider;
    }
}

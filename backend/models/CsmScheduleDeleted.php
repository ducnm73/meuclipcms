<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class CsmScheduleDeleted extends \common\models\CsmScheduleDeletedBase
{
    public static function deleteMedia($mediaIds)
    {
        $user = Yii::$app->get('user', false);
        $userId = $user && !$user->isGuest ? $user->id : null;
        $rows = array();
        foreach ($mediaIds as $mediaId) {
            $rows[] = [
                'type' => TYPE_DELETE_MEDIA,
                'item_id' => $mediaId,
                'status' => STATUS_DELETE_NOT_RUNNING,
                'created_at' => date('Y-m-d H:i:s', time()),
                'created_by' => $userId
            ];
        }

        Yii::$app->db->createCommand()->batchInsert(CsmScheduleDeleted::tableName(), [
            'type', 'item_id', 'status', 'created_at', 'created_by'
        ], $rows)->execute();
    }
    public static function deleteAttribute($attrIds)
    {
        $user = Yii::$app->get('user', false);
        $userId = $user && !$user->isGuest ? $user->id : null;
        $rows = array();
        foreach ($attrIds as $attrId) {
            $rows[] = [
                'type' => TYPE_DELETE_ATTRIBUTE,
                'item_id' => $attrId,
                'status' => STATUS_DELETE_NOT_RUNNING,
                'created_at' => date('Y-m-d H:i:s', time()),
                'created_by' => $userId
            ];
        }
        Yii::$app->db->createCommand()->batchInsert(CsmScheduleDeleted::tableName(), [
            'type', 'item_id', 'status', 'created_at', 'created_by'
        ], $rows)->execute();
    }
}
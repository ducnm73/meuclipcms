<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CsmMedia;

/**
 * CsmMediaClipCreateSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class CsmMediaCustomClipCreateSearch extends CsmMediaClip
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_download', 'price_play', 'type', 'published_by', 'cp_id', 'file_type', 'convert_priority', 'is_crawler', 'crawler_id', 'created_by', 'reviewed_by'], 'integer'],
            [['id', 'name', 'slug', 'short_desc', 'description', 'created_at', 'updated_at', 'published_at', 'cp_info', 'convert_start_time', 'convert_end_time', 'tag', 'seo_title', 'seo_description', 'seo_keywords', 'crawler_info'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = CsmMediaClip::find()
            ->where([
                'type' => TYPE_MEDIA_CLIP,
                'status' => [STATUS_MEDIA_DRAFT],
            ]);
        if ($user->id) {
            $query->andWhere([
                'created_by' => $user->id
            ]);
        }
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [1, 200]
            ],
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        if (isset($params[$this->formName()]['created_at']) && !empty($params[$this->formName()]['created_at'])) {
            $split = explode(' - ', $params[$this->formName()]['created_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.created_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }
        if (isset($params[$this->formName()]['updated_at']) && !empty($params[$this->formName()]['updated_at'])) {
            $split = explode(' - ', $params[$this->formName()]['updated_at']);
            $beginDate = trim($split[0]);
            $endDate = trim($split[1]);

            $query->andWhere(CsmMedia::tableName() . '.updated_at between :beginTime and :endTime', [
                ':beginTime' => $beginDate,
                ':endTime' => $endDate
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', trim($this->name)])
            ->andFilterWhere(['like', 'id', trim($this->id)])
            ->andFilterWhere(['like', 'short_desc', $this->short_desc])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'resolution', $this->resolution])
            ->andFilterWhere(['like', 'attributes', $this->attributes])
            ->andFilterWhere(['like', 'cp_info', $this->cp_info])
            ->andFilterWhere(['like', 'original_path', $this->original_path])
            ->andFilterWhere(['like', 'image_path', $this->image_path])
            ->andFilterWhere(['like', 'convert_path', $this->convert_path])
            ->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'crawler_info', $this->crawler_info])
            ->andFilterWhere(['like', 'published_list', $this->published_list]);

        return $dataProvider;
    }
}

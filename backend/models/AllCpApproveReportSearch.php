<?php

namespace backend\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CsmMediaClipApproveSearch represents the model behind the search form about `backend\models\CsmMedia`.
 */
class AllCpApproveReportSearch extends CpApproveReport
{
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $query = UserClient::find()
            ->distinct()
            ->asArray()
            ->select('user_id')
            ->where(['client_id' => User::getClientList()]);

        $allUserIdHaveClientId = $query->all();
        $allUserIdHaveClientId = array_column($allUserIdHaveClientId, 'user_id');
        $query = CpApproveReport::find()
            ->select(CsmMediaActionLog::tableName() . '.created_at as published_at, '
                . CsmMediaActionLog::tableName() . '.created_by as user_id,'
                . User::tableName() . '.username AS user_name, '
                . CsmMediaActionLog::tableName() . '.media_id AS media_id,'
                . CsmMedia::tableName() . '.`name` AS media_name, '
                . CsmMediaActionLog::tableName() . '.watched_duration AS watched_duration,'
                . CsmMediaActionLog::tableName() . '.action AS action,'
                . CsmMedia::tableName() . '.duration as video_duration, '
                . CsmMediaActionLog::tableName() . '.note AS note')
            ->join('INNER JOIN', CsmMedia::tableName(), CsmMediaActionLog::tableName() . '.media_id = ' . CsmMedia::tableName() . '.id')
            ->join('INNER JOIN', User::tableName(), CsmMediaActionLog::tableName() . '.created_by = ' . User::tableName() . '.id')
            ->where([CsmMediaActionLog::tableName() . '.created_by' => $allUserIdHaveClientId])
            ->andWhere([CsmMediaActionLog::tableName() . '.action' => [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE]]);
        //print_r($query->createCommand()->getRawSql());die();
        $this->load($params);

        if ($this->user_name) {
            // neu co filter
            $query->andFilterWhere([
                User::tableName() . '.username' => $this->user_name
            ]);
        }

        if (strpos($this->published_at, ' - ') > 0) {
            $start_dates = self::splitDate($this->published_at, 'd-m-Y');
            $query->andWhere(CsmMediaActionLog::tableName() . '.created_at >= :fromDate', [':fromDate' => $start_dates[0]])
                ->andWhere(CsmMediaActionLog::tableName() . '.created_at <= :toDate', [':toDate' => $start_dates[1]]);

        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['user_name' => SORT_ASC]]
        ]);
       // print_r($query->createCommand()->getRawSql());die();



        $allModel = $dataProvider->getModels();
        //var_dump($allModel);die();

        foreach ($allModel as $item) {
            if (!in_array($item->action, [MEDIA_ACTION_APPROVE, MEDIA_ACTION_DECLINE])) {
                $item->watched_duration = 0;
            } else {
                if ($item->watched_duration === NULL) {
                    $item->watched_duration = ($item->video_duration) ? $item->video_duration : 0;
                }
            }

            $item['revenue'] = number_format($item->watched_duration * Yii::$app->params['revenua_per_minute_approve'] / 60, 2);
        }

        $dataProvider->setModels($allModel);

        return $dataProvider;
    }


}

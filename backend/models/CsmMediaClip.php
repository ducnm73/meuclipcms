<?php

namespace backend\models;

use awesome\backend\behavior\ManyToManyBehavior;
use backend\components\behaviors\TrimBehavior;
use backend\components\behaviors\VietnameseSlugBehavior;
use MongoDB\Driver\Query;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is updated the model class for table "csm_media".
 *
 * @property int[] `$client_list`
 * @property int[] $category_list
 * @property int[] $category_list_music
 * @property int[] $category_list_film
 * @property int[] $category_list_tv_show
 * @property int[] $film_series_list
 * @property int[] $tv_show_series_list
 * @property int[] $composer_list
 * @property int[] $singer_list
 * @property int[] $actor_list
 * @property int[] $director_list
 * @property int[] $tv_show_list
 * @property int[] $film_list
 * @property int[] $sitcom_list
 *
 * @property CsmCopyright $csmCopyright
 * @property CsmCp $cp
 * @property CsmConvertData $convertData
 * @property CsmMediaAttribute[] $csmMediaAttributes
 * @property CsmMediaPublish[] $csmMediaPublishes
 */
class CsmMediaClip extends \common\models\CsmMediaBase
{
    public $watched_duration;
    public $meta_album;
    public $meta_year;
    public $meta_track;
    public $meta_comment;
    public $meta_copyright;
    public $meta_author;
    public $meta_country;
    public $meta_language;
    public $meta_subtitle_language;
    public $meta_content_filter;
    public $meta_imdb_rating;
    public $episode_no;
    public $episode_name;
    public $media_client_status;
    public $reject_reason;
//    public $media_type = TYPE_MEDIA_CLIP;
    public $validate_media_status = [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED,
        STATUS_MEDIA_APPROVED, STATUS_MEDIA_TRANSFER_PENDING, STATUS_MEDIA_TRANSFER_SUCCESS, STATUS_MEDIA_PUBLISHED];
    public $validate_media_type = [TYPE_MEDIA_CLIP];

    public function rules()
    {
        return array_merge([
            [['category_list'], 'each', 'rule' => ['integer']],
            [['meta_album', 'meta_year', 'meta_track', 'meta_comment', 'meta_copyright', 'meta_author',
                'meta_country', 'meta_language', 'meta_subtitle_language', 'meta_content_filter',
                'meta_imdb_rating', 'episode_name', 'media_client_status', 'reject_reason', 'watched_duration', 'subtitle_path', 'audio_path', 'convert_audio'], 'safe'],
            [['watched_duration', 'episode_no', 'convert_priority'], 'integer'],
            ['status', 'in', 'range' => $this->validate_media_status],
            ['type', 'in', 'range' => $this->validate_media_type],
            [['name'], 'required'],
            [['image_path'], 'required', 'message' => Yii::t('backend', 'Horizontal image can not be blank')],
            [['original_path'], 'required', 'message' => Yii::t('backend', 'Video file can not be blank')],
            ['client_list', 'required', 'message' => Yii::t('backend', 'Distributed service can not be blank')],
            [['name'], 'match', 'pattern' => '/^.*[a-zA-Z0-9].*$/i', 'message' => Yii::t('backend', 'Name must contain at least 1 alphabet character or number')],
            [['convert_priority'], 'integer', 'min' => 0],
            [['convert_priority'], 'compare', 'compareValue' => 10000, 'operator' => '<=', 'message' => Yii::t('backend', 'Độ ưu tiên convert không được quá 10000')],
            ['is_crawler', 'default', 'value' => 0],
            [['published_at'], 'required', 'when' => function ($model) {
                return $model->status == STATUS_MEDIA_PUBLISHED;
            }, 'whenClient' => "function (attribute, value) {
                    return false;
                }"],
        ], parent::rules()
        );
    }

//    public function init()
//    {
//        $this->type = $this->media_type;
//        parent::init();
//    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => VietnameseSlugBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug'
            ],
            [
                // trim tất cả các trường
                'class' => TrimBehavior::className()
            ],
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'client_list' => [
                        'attrClients',
                        'viaTableValues' => [
                            'published_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'published_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            },
                            'status' => function ($model, $relationName, $attributeName, $relatedPk) {
                                /* @var CsmMedia $model */
                                return $model->media_client_status;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(ApiClient::find()
                                ->select(ApiClient::tableName() . '.id')
                                ->leftJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.client_id=' .
                                    ApiClient::tableName() . '.id')
                                ->where([
                                    CsmMediaPublish::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaPublish::tableName() . '.client_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ],
                    'category_list' => [
                        'attrCategories',
                        'viaTableValues' => [
                            'created_at' => function () {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => function () {
                                $user = Yii::$app->get('user', false);
                                return $user && !$user->isGuest ? $user->id : null;
                            }
                        ],
                        'delete_addition' => function ($bindingKeys) {
                            return ['id' => ArrayHelper::getColumn(CsmMediaAttribute::find()
                                ->select(CsmMediaAttribute::tableName() . '.id')
                                ->leftJoin(CsmAttribute::tableName(), CsmMediaAttribute::tableName() . '.attribute_id=' .
                                    CsmAttribute::tableName() . '.id')
                                ->where([
                                    CsmAttribute::tableName() . '.type' => TYPE_ATTRIBUTE_CATEGORY,
                                    CsmMediaAttribute::tableName() . '.media_id' => $this->id,
                                ])
                                ->andWhere(['not in', CsmMediaAttribute::tableName() . '.attribute_id', $bindingKeys])
                                ->asArray()
                                ->all(), 'id', false)];
                        },
                    ]
                ]
            ]
        ];
    }

    public function afterFind()
    {
        $meta = $this->meta_info;
        if ($meta) {
            $meta = json_decode($meta, true);
            if ($meta[META_CONTENT_FILTER]) {
                $this->meta_content_filter = $meta[META_CONTENT_FILTER];
            }
        }

        $multiple_language = $this->multiple_language;
        if ($multiple_language) {
            $this->multiple_language = json_decode($multiple_language, true);
        }

        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        $meta = array();

        if ($this->meta_content_filter) {
            $meta[META_CONTENT_FILTER] = trim($this->meta_content_filter);
        }

        $this->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);

        // multiple_language
        $multiple_language = array();
        foreach ((object)$this->multiple_language as $key => $item) {
            $item['slug'] = '';
            if ($item['name']) {
                $item['name'] = trim($item['name']);
                $item['slug'] = VietnameseSlugBehavior::slugify($item['name']);
            }
            $multiple_language[$key] = $item;
        }
        $this->multiple_language = json_encode($multiple_language, JSON_UNESCAPED_UNICODE);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }


    public function getAttrClients()
    {
        return $this->hasMany(ApiClient::className(), ['id' => 'client_id'])
            ->viaTable('csm_media_publish', ['media_id' => 'id']);
    }

    public function getAttrCategories()
    {
        return $this->hasMany(CsmAttribute::className(), ['id' => 'attribute_id'])
            ->andOnCondition(['type' => TYPE_ATTRIBUTE_CATEGORY])
            ->viaTable('csm_media_attribute', ['media_id' => 'id']);
    }

    public function getNumConvertPath()
    {
        return $this->convert_path ? count(json_decode($this->convert_path, true)) : 0;
    }

    public static function updateStatus($ids, $status, $currentStatus, $cpId)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], [
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])
            ->execute();
    }

    public static function checkAndUpdateStatus($ids, $status, $currentStatus, $cpId)
    {
        if (CsmMedia::find()->where([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ])->count() > 0
        ) {
            Yii::$app->db->createCommand()
                ->update(CsmMedia::tableName(), ['status' => $status], [
                    'id' => $ids,
                    'status' => $currentStatus,
                    'cp_id' => $cpId
                ])
                ->execute();
            return true;
        }
        return false;
    }

    public static function updateStatusAdmin($ids, $status, $currentStatus, $more = array())
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus
            ], $more))
            ->execute();
    }

    public static function updateStatusMore($ids, $status, $currentStatus, $cpId, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
                'cp_id' => $cpId
            ], $more))
            ->execute();
    }

    public static function updateStatusAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), ['status' => $status], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function approvedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId(),
                'published_by' => Yii::$app->user->getIdentity()->getId(),
                'published_at' => date('Y-m-d H:i:s', time())
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function reviewedAdminMore($ids, $status, $currentStatus, $more)
    {
        return Yii::$app->db->createCommand()
            ->update(CsmMedia::tableName(), [
                'status' => $status,
                'reviewed_by' => Yii::$app->user->getIdentity()->getId()
            ], array_merge([
                'id' => $ids,
                'status' => $currentStatus,
            ], $more))
            ->execute();
    }

    public static function getCreatedById($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = self::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => [STATUS_MEDIA_DRAFT, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_REJECTED],
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    // lấy ra các video status 6
    public static function getMediaPublish($id, $type)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $query = CsmMedia::find()
            ->where([
                'id' => $id,
                'type' => $type,
                'status' => STATUS_MEDIA_PUBLISHED,
            ]);
        if ($user->cp_id) {
            $query->andWhere([
                'cp_id' => $user->cp_id
            ]);
        }
        return $query->one();
    }

    public static function checkExistMedia($name, $type, $duration, $maxQuantity)
    {
        return CsmMedia::find()->where([
            'name' => $name,
            'type' => $type,
            'duration' => $duration,
            'max_quantity' => $maxQuantity,
        ])->count();
    }

    public static function getByIds($ids)
    {
        return CsmMedia::find()
            ->where([
                'id' => $ids,
            ])
            ->all();
    }
}
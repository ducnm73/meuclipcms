<?php

namespace backend\models;

use awesome\backend\behavior\ManyToManyBehavior;
use Yii;

/**
 * This is updated the model class for table "csm_cp".
 *
 * @property int[] $client_list
 */
class CsmCp extends \common\models\CsmCpBase
{
    public function rules()
    {
        return array_merge([
            [['client_list'], 'each', 'rule' => ['integer']],
        ], parent::rules()
        );
    }

    public function behaviors()
    {
        return [
            [
                'class' => ManyToManyBehavior::className(),
                'relations' => [
                    'client_list' => [
                        'clients',
                    ],
                ]
            ]
        ];
    }

    public function getClients()
    {
        return $this->hasMany(ApiClient::className(), ['id' => 'client_id'])
            ->viaTable(CsmCpClient::tableName(), ['cp_id' => 'id']);
    }

    public static function getByUserName($userName)
    {
        return CsmCp::find()
            ->leftJoin(User::tableName(), User::tableName() . '.cp_id = ' . CsmCp::tableName() . '.id')
            ->where(User::tableName() . '.username = :username', ['username' => $userName])
            ->one();
    }

    public static function getAll() {
        return CsmCp::find()->all();
    }
}
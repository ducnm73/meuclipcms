<?php

namespace backend\components\common;

use Yii;
use yii\db\Expression;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_IOFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Utility {

    public static function getFilePathToSave($filePath, $type) {
        if (is_file($filePath)) {
            $config = Yii::$app->params['upload'][$type]['basePath'];
            $file = explode($config, $filePath);
            return $config . $file[1];
        }
        return '';
    }

    public static function getBasePathUpload($type) {
        return Yii::$app->params['upload']['basePath'] . Yii::$app->params['upload'][$type]['basePath'];
    }

    public static function exportExcel($queryParams, $searchClass, $title, $fields, $fileName) {
        $searchModel = new $searchClass();
        $dataProvider = $searchModel->search($queryParams);
        $sort = $dataProvider->getSort();
        $sort->params = ['sort' => $queryParams['sort']];
        $dataProvider->setSort($sort);
        $styleArray = array(
            'font'  => array(
                'size'  => 14,
                'name'  => 'Times NewRoman'
            ));

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);

        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setDescription($title);

        $objPHPExcel->setActiveSheetIndex(0);
        $activeSheet = $objPHPExcel->getActiveSheet();

        $activeSheet->setCellValue('A1', mb_strtoupper($title, 'UTF-8'));
        $lastColumnName = PHPExcel_Cell::stringFromColumnIndex(sizeof($fields));
        $activeSheet->mergeCells('A1:' . $lastColumnName . '1');
        $activeSheet->getRowDimension('1')->setRowHeight(40);
        $activeSheet->getStyle('A1')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 18
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);

        $exportData = $dataProvider;
        $exportData->setPagination(false);
        $dataStartRowIndex = 2;
        $objPHPExcel->getActiveSheet()->setCellValue("A$dataStartRowIndex", 'STT');
        $columnIndex = 1;

        foreach ($fields as $field) {
            $columnName = PHPExcel_Cell::stringFromColumnIndex($columnIndex ++);
            $headerText = isset($field['headerText']) ? $field['headerText'] : $searchClass::attributeLabels()[$field['name']];
            $objPHPExcel->getActiveSheet()->setCellValue("$columnName$dataStartRowIndex", $headerText);
        }

        $objPHPExcel->getActiveSheet()->getStyle("A$dataStartRowIndex:$lastColumnName$dataStartRowIndex")->applyFromArray(
            [
                'font' => [
                    'bold' => true
                ],
                'alignment' => [
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ]
            ]
        );

        $row = 2;
        $order = 1;

        foreach ($exportData->models as $model) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . strval($row + 1), $order ++);
            $columnIndex = 1;

            foreach ($fields as $field) {
                $columnName = PHPExcel_Cell::stringFromColumnIndex($columnIndex ++);
                $fieldName = $field['name'];

                if(isset($field['callback'])) {
                    $value = call_user_func($field['callback'], $model);
                } else {
                    $value = $model->$fieldName;
                }

                $objPHPExcel->getActiveSheet()->setCellValue($columnName . strval($row + 1), $value);
            }

            $row++;
        }

        // bôi đạm tiêu đề các cột
        $objPHPExcel->getActiveSheet()->getStyle('A2:A' . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);

        $objPHPExcel->getActiveSheet()->getRowDimension($dataStartRowIndex)->setRowHeight(40);

        foreach ($fields as $field) {
            $columnName = PHPExcel_Cell::stringFromColumnIndex($columnIndex ++);
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnName)->setWidth(30);
        }

        $objPHPExcel->getActiveSheet()->getStyle('A' . ($dataStartRowIndex + 1) . ':' . $lastColumnName . $objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true);
        $objPHPExcel->getActiveSheet()->getStyle('B' . ($dataStartRowIndex + 1) . ':' . $lastColumnName . $objPHPExcel->getActiveSheet()->getHighestRow())->applyFromArray([
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
            ]
        ]);

        // $objPHPExcel->getActiveSheet()->getStyle('H3:H' . $objPHPExcel->getActiveSheet()->getHighestRow())
        //     ->getNumberFormat()
        //     ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $sysdate = new Expression(date('d/m/Y'));
        $objPHPExcel
            ->getActiveSheet()
            ->getStyle("A$dataStartRowIndex:$lastColumnName" . $objPHPExcel->getActiveSheet()->getHighestRow())
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '_'. $sysdate . '.xls"');
        $objWriter->save('php://output');
    }
}

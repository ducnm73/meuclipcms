<?php

namespace backend\components\common;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class MultipleLanguage extends Widget
{
    const TEXT_INPUT = 'textInput';
    const TEXT_AREA = 'textArea';

    public $model;
    public $form;
    public $formOptions = [];
    public $attribute;
    public $label;

    /**
     * @var array
     * - type: text
     * - options: array
     */
    public $inputType = [];

    public $languagePool;
    public $mainLanguage;

    const LANGUAGE_FIELD = 'multiple_language';

    public function init()
    {
        parent::init();
        if ($this->languagePool === null) {
            $this->languagePool = Yii::$app->params['languagepool'];
        }
        $this->mainLanguage = Yii::$app->params['mainLanguage'];

        if ($this->formOptions === null) {
            $this->languagePool = [];
        }
    }

    private function getInputType($form, $options = [], $type = null)
    {
        switch ($type) {
            case self::TEXT_INPUT:
                $output = $form->textInput($options);
                break;
            case self::TEXT_AREA:
                $output = $form->textarea($options);
                break;
            default:
                $output = $form->textInput(['maxlength' => 255]);
                break;
        }
        return $output;
    }

    private function groupRender()
    {
        $output = '';
        $output .= $this->getInputType(
            $this->form->field($this->model, $this->attribute, $this->formOptions)->label($this->label),
            $this->inputType['options'],
            $this->inputType['type']
        );

        // Remove main lang in pool
        $tempPool = array_diff($this->languagePool, [$this->mainLanguage]);
        foreach ($tempPool as $lang) {
            $output .= $this->getInputType(
                $this->form
                    ->field($this->model, self::LANGUAGE_FIELD . "[$lang][$this->attribute]", $this->formOptions)
                    ->label($this->label),
                $this->inputType['options'],
                $this->inputType['type']
            );
        }
        return $output;
    }

    public function run()
    {
        return $this->groupRender();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: cuongtt11
 * Date: 12/31/2020
 * Time: 9:54 AM
 */

namespace backend\components\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class TrimBehavior extends Behavior
{

    public $listAttributes;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate'
        ];
    }

    public function beforeValidate($event)
    {
        if ($this->listAttributes) {
            foreach ($this->listAttributes as $key => $value) { //For all model attributes
                if (!is_array($this->owner->$key && isset($this->owner->$key))) {
                    $this->owner->$key = trim($this->owner->$key);
                }
            }
        } else {
            $attributes = $this->owner;
            foreach ($attributes as $key => $value) { //For all model attributes
                if (!is_array($this->owner->$key) && isset($this->owner->$key)) {
                    $this->owner->$key = trim($this->owner->$key);
                }

            }


        }

    }

}
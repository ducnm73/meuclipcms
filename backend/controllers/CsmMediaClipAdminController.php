<?php

namespace backend\controllers;

use awesome\backend\grid\AwsFormatter;
use backend\models\CsmMediaClip;
use Yii;
use backend\models\CsmCp;
use backend\models\User;
use backend\models\CsmMedia;
use backend\models\CsmMediaClipAdminSearch;
use backend\controllers\AppController;
use backend\components\common\Utility;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaClipAdminController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaClipAdminController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaClipAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionExport() {

        $queryParams = Yii::$app->session->get(self::className() . 'queryParams');
        if(!$queryParams) {
            $this->redirect('index');
        }
        $searchModel = new CsmMediaClipAdminSearch();
        $dataProvider = $searchModel->search($queryParams);
        $max = 10000;

        if($dataProvider->totalCount > $max) {
            Yii::$app->session->setFlash('error', Yii::t('backend', 'Không thể thực hiện do số lượng dữ liệu vượt quá {max}. Vui lòng lọc thêm dữ liệu', ['max' => $max]));
            return $this->redirect('index');
        }



        $fields = [
            [
                'name' => 'media_id',
                'headerText' => 'Video ID',
            ],
            [
                'name' => 'name',
                'headerText' => 'Tên video',
                'callback' => function ($model) {
                    return $model->media->name;
                }
            ],
            [
                'name' => 'duration',
                'headerText' => 'Thời lượng',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asMediaDuration($model->media->duration);
                }
            ],
            [
                'name' => 'resolution',
                'headerText' => 'Độ phân giải',
                'callback' => function ($model) {
                    return $model->media->resolution;
                }
            ],
            [
                'name' => 'status',
                'headerText' => 'Trạng thái file',
                'callback' => function ($model) {
                    return Yii::$app->params['media-status'][$model->media->status];
                }
            ],
            [
                'name' => 'created_at',
                'headerText' => 'Ngày tạo',
                'callback' => function ($model) {
                    return $model->media->created_at;
                }
            ],
            [
                'name' => 'updated_at',
                'headerText' => 'Ngày update',
                'callback' => function ($model) {
                    return $model->media->updated_at;
                }
            ],
            [
                'name' => 'published_at',
                'headerText' => 'Ngày duyệt',
                'callback' => function ($model) {
                    return $model->published_at;
                }
            ],
            [
                'name' => 'published_by',
                'headerText' => 'Người duyệt',
                'callback' => function ($model) {
                    $user = User::findOne($model->published_by);
                    if ($user) {
                        return $user->username;
                    } else {
                        return '';
                    }
                }
            ],

            [
                'name' => 'cp_id',
                'headerText' => 'CP',
                'callback' => function ($model) {
                    $cp = CsmCp::findOne($model->media->cp_id);
                    if ($cp) {
                        return $cp->name;
                    }
                    return "";
                }
            ],

            [
                'name' => 'attributes',
                'headerText' => 'Thuộc tính',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asJsonNameType($model->media->attributes);
                }
            ],

            [
                'name' => 'published_list',
                'headerText' => 'Upload cho dịch vụ',
                'callback' => function ($model) {
                    return $model->client->name ;

                }
            ],
            [
                'name' => 'service_status',
                'headerText' => 'Trạng thái dịch vụ',
                'callback' => function ($model) {
                    return Yii::$app->params['media-status'][$model->status];

                }
            ],

        ];

        Utility::exportExcel($queryParams, CsmMediaClipAdminSearch::className(), 'danh sách video', $fields, 'export danh sach video');
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmMediaClip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

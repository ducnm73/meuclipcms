<?php

namespace backend\controllers;

use backend\models\ApiClient;
use backend\models\CsmAttribute;
use backend\models\CsmMediaPublish;
use backend\models\CsmMediaTvShow;
use backend\models\CsmScheduleDeleted;
use backend\models\upload\FileUploaded;
use common\helpers\MediaHelper;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaTvShowCreateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaTvShowCreateController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaTvShowCreateController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaTvShowCreateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsmMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmMediaTvShow();

        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $values = Yii::$app->request->post();
        // process value of tv_show_list change type int to int[]
        if ($values[$model->formName()]['tv_show_list'] && !is_array($values[$model->formName()]['tv_show_list'])) {
            $values[$model->formName()]['tv_show_list'] = [$values[$model->formName()]['tv_show_list']];
        }
        if ($model->load($values)) {
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->type = TYPE_MEDIA_TV;
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;
            if ($user->cp_id) {
                $model->cp_id = $user->cp_id;
                $model->cp_info = json_encode($user->generateCpJsonObj());
            }

            // fix add duration max_quantity resolution before convert
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Processing of the video failed. Upload again please."));
                return $this->redirect('index');
            }
            $model->duration = $duration;
            $model->max_quantity = min($width, $height);
            $model->resolution = $width . 'x' . $height;
            // category attributes
            $attributes = array();
            $categories = null;
            $clients = array();
            if (count($model->category_list_tv_show)) {
                $categories = CsmAttribute::getCategoryTvShowByIdsActive($model->category_list_tv_show);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - category attributes
            // tv attributes
            if (count($model->tv_show_list)) {
                $csmAttributes = CsmAttribute::getTvShowByIdsActive($model->tv_show_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - tv attributes
            // actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - actor attributes
            // director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = $model->meta_album;
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = $model->meta_year;
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = $model->meta_track;
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = $model->meta_comment;
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = $model->meta_copyright;
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = $model->meta_author;
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = $model->meta_country;
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = $model->meta_language;
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = $model->meta_subtitle_language;
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = $model->meta_content_filter;
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = $model->meta_imdb_rating;
            }
            if ($model->name) {
                $meta[META_TITLE] = $model->name;
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = $model->description;
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            // end - update metadata

            if (count($attributes)) {
                $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            }
            if (count($clients)) {
                $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            }
            $model->is_crawler = 0;
            if ($model->save()) {
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);

                if ($model->drm_id) {
                    $model->need_encryption = 1;
                    $model->resource_id = 'CSM_' . $model->id;
                    $model->save();
                }else{
                    $model->need_encryption = 0;
                    $model->resource_id = null;
                    $model->save();
                }

                return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_published_list = json_decode($model->published_list);
        $values = Yii::$app->request->post();
        // process value of film_list change type int to int[]
        if ($values[$model->formName()]['tv_show_list'] && !is_array($values[$model->formName()]['tv_show_list'])) {
            $values[$model->formName()]['tv_show_list'] = [$values[$model->formName()]['tv_show_list']];
        }
        if ($model->load($values)) {
            $attributes = array();
            $categories = array();
            $clients = array();
            // update category attributes
            if (count($model->category_list_tv_show)) {
                $categories = CsmAttribute::getCategoryTvShowByIdsActive($model->category_list_tv_show);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update category attributes
            // tv attributes
            if (count($model->tv_show_list)) {
                $csmAttributes = CsmAttribute::getTvShowByIdsActive($model->tv_show_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - tv attributes
            // update actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update actor attributes
            // update director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // done - update director attributes
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = $model->meta_album;
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = $model->meta_year;
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = $model->meta_track;
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = $model->meta_comment;
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = $model->meta_copyright;
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = $model->meta_author;
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = $model->meta_country;
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = $model->meta_language;
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = $model->meta_subtitle_language;
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = $model->meta_content_filter;
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = $model->meta_imdb_rating;
            }
            if ($model->name) {
                $meta[META_TITLE] = $model->name;
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = $model->description;
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;

            if ($model->drm_id) {
                $model->need_encryption = 1;
                $model->resource_id = 'CSM_' . $model->id;
            }else{
                $model->need_encryption = 0;
                $model->resource_id = null;
            }

            if ($model->save()) {
                $old_client_list = array_map(function ($item){
                    return is_object($item) ? $item->id : $item['id'];
                },$old_published_list);
                $diff_client_id = array_diff($old_client_list,$model->client_list);
                if($diff_client_id){
                    // xóa các dịch vụ không được phân phối
                    CsmMediaPublish::deleteAll(['media_id' => $id,'client_id' => $diff_client_id]);
                }
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if (CsmMedia::checkAndUpdateStatus([$id], STATUS_MEDIA_DELETED,
                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
            ) {
                CsmScheduleDeleted::deleteMedia([$id]);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete item failed! Item invalid!"));
            }
        }
        return $this->redirect('index');
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmMediaTvShow::getCreatedById($id, TYPE_MEDIA_TV)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if ($status == 'reviewed-proposal') {
                $ids = Yii::$app->getRequest()->post('ids');
                // Update status to ready for upload
                if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_TRANSFER_SUCCESS,
                    [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
                ) {
                    // Update status pending approved
                    CsmMediaPublish::updateStatusCp($ids, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_DRAFT);
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to need approved successful"));
                } else {
                    Yii::$app->session->setFlash('warning', Yii::t('backend', "Check status invalid!"));
                }
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Status is invalid"));
            }
        }
        return $this->redirect('index');
    }

//    public function actionDeleteMultiple()
//    {
//        $user = Yii::$app->user->identity;
//        /* @var \backend\models\User $user */
//        if ($user->cp_id) {
//            $ids = Yii::$app->getRequest()->post('ids');
//            if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_DELETED,
//                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
//            ) {
//                CsmScheduleDeleted::deleteMedia($ids);
//                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
//            } else {
//                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete items failed! List of items invalid!"));
//            }
//        }
//        $searchModel = new CsmMediaTvShowCreateSearch();
//        $dataProvider = $searchModel->search(Yii::$app->session->get(self::className() . 'queryParams'));
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
}

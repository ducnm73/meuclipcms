<?php

namespace backend\controllers;

use backend\models\ApiClient;
use backend\models\CsmAttribute;
use backend\models\CsmMediaAttribute;
use backend\models\CsmMediaFilmSeries;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use backend\models\CsmMediaActionLog;
use backend\models\upload\FileUploaded;
use backend\models\User;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaFilmSeriesQuickApproveSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaFilmSeriesQuickApproveController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaFilmSeriesQuickApproveController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaFilmSeriesQuickApproveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $values = Yii::$app->request->post();
        $model->validate_media_type = [TYPE_MEDIA_FILM_SERIES];
        // process value of film_series_list change type int to int[]
        if ($values[$model->formName()]['film_series_list'] && !is_array($values[$model->formName()]['film_series_list'])) {
            $values[$model->formName()]['film_series_list'] = [$values[$model->formName()]['film_series_list']];
        }
        if ($model->load($values)) {
            $attributes = array();
            $categories = array();
            $clients = array();
            // update category attributes
            if (count($model->category_list_film)) {
                $categories = CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update category attributes
            // update film series attributes
            if (count($model->film_series_list)) {
                $attributes = self::processUpdateAttr($attributes, $model->film_series_list, $model->id);
            }
            // done - update film series attributes
            // update actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update actor attributes
            // update director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = $model->meta_album;
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = $model->meta_year;
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = $model->meta_track;
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = $model->meta_comment;
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = $model->meta_copyright;
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = $model->meta_author;
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = $model->meta_country;
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = $model->meta_language;
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = $model->meta_subtitle_language;
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = $model->meta_content_filter;
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = $model->meta_imdb_rating;
            }
            if ($model->name) {
                $meta[META_TITLE] = $model->name;
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = $model->description;
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
//            $model->status = STATUS_MEDIA_DRAFT;
            if ($model->save()) {
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);

                // update status
                $watched_duration = $model->watched_duration;
                if ($model->media_client_status == STATUS_MEDIA_APPROVED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_APPROVE, '', User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_APPROVED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                } else if ($model->media_client_status == STATUS_MEDIA_REJECTED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_DECLINE, $model->reject_reason, User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_REJECTED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                }
                Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                return $this->redirect('index');
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Update item failed"));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        CsmMedia::updateStatusAdmin($id, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia([$id]);
        CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_DELETE, '', User::getUserId(), null);
        CsmMediaPublish::updateStatus($id, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete item successful"));
        return $this->redirect('index');
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = CsmMediaFilmSeries::find()->where([
            CsmMedia::tableName() . '.id' => $id,
            CsmMedia::tableName() . '.type' => TYPE_MEDIA_FILM_SERIES,
            CsmMedia::tableName() . '.status' => [STATUS_MEDIA_APPROVED, STATUS_MEDIA_TRANSFER_PENDING, STATUS_MEDIA_TRANSFER_SUCCESS, STATUS_MEDIA_PUBLISHED]
        ])->innerJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.media_id = ' .
            CsmMedia::tableName() . '.id')
//            ->andWhere(CsmMedia::tableName() . '.is_crawler = 0 OR ' . CsmMedia::tableName() . '.is_crawler IS NULL')
            ->andWhere([
                CsmMediaPublish::tableName() . '.client_id' => User::getClientList(),
                CsmMediaPublish::tableName() . '.status' => [STATUS_MEDIA_APPROVED_PROPOSAL]
            ])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        if ($status == 'approved-accepted') {
            $ids = Yii::$app->getRequest()->post('ids');

            foreach ($ids as $id) {
                CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_APPROVE, '', User::getUserId(), null);
            }

            CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_APPROVED,
                STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to approved successful"));
        } else if ($status == 'approved-rejected') {
            $ids = Yii::$app->getRequest()->post('ids');
            foreach ($ids as $id) {
                CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_DECLINE, '', User::getUserId(), null);
            }
            CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_REJECTED,
                STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to rejected successful"));
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('backend', "Invalid status"));
        }
        return $this->redirect('index');
    }

    public function actionDeleteMultiple()
    {
        $ids = Yii::$app->getRequest()->post('ids');
//        CsmMedia::updateStatusAdmin($ids, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia($ids);
        CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
        foreach ($ids as $media_id) {
            $log = new CsmMediaActionLog(['media_id' => $media_id, 'action' => MEDIA_ACTION_DELETE, 'note' => '', 'created_by' => User::getUserId()]);
            $rows[] = $log->attributes;
        }
        $logModel = new CsmMediaActionLog();
        Yii::$app->db->createCommand()->batchInsert(CsmMediaActionLog::tableName(), $logModel->attributes(), $rows)->execute();
        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
        return $this->redirect('index');
    }

    private function processUpdateAttr($attributes, $ids, $mediaId)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attrMedia = $csmAttribute->getAttrMedia($mediaId);
            /* @var CsmMediaAttribute $attrMedia */
            $attributes[] = array_merge($csmAttribute->generateJsonObj(), [
                'info' => $attrMedia->info,
                'position' => $attrMedia->position
            ]);
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }

    private function processAttrParent($attributes, $ids)
    {
        $csmAttributes = CsmAttribute::getFilmSeriesByIdsActive($ids);
        foreach ($csmAttributes as $csmAttribute) {
            /* @var CsmAttribute $csmAttribute */
            $attributes[] = array_merge($csmAttribute->generateJsonObj());
            if ($csmAttribute->parent_id > 0) {
                return self::processAttrParent($attributes, [$csmAttribute->parent_id]);
            }
        }
        return $attributes;
    }
}

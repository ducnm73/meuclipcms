<?php


namespace backend\controllers;


use backend\models\CsmAttribute;
use Yii;
use yii\helpers\Json;

class AttributeController extends AppController
{
    private $pageSize = 30;

    public function actionSearchCategoryActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_CATEGORY, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchFilmSeriesActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_FILM_SERIES, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_FILM_SERIES, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_FILM_SERIES, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchTvShowActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_TV_SHOW, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_TV_SHOW, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_TV_SHOW, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchTvShowSeriesActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_TV_SHOW_SERIES, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchFilmActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_FILM, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_FILM, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_FILM, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchSitcomActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_SITCOM, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_SITCOM, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_SITCOM, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchComposerActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_COMPOSER, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_COMPOSER, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_COMPOSER, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchSingerActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_SINGER, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_SINGER, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_SINGER, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchActorActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_ACTOR, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_ACTOR, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_ACTOR, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchDirectorActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_DIRECTOR, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_DIRECTOR, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%' OR `std_name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_DIRECTOR, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name,
                        'image_path' => $model->bindImagePath(),
                        'description' => $model->std_name
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchCategoryFilmActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_CATEGORY_FILM, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_FILM, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_FILM, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchCategoryTvShowActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_TV_SHOW, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }

    public function actionSearchCategoryMusicActive($q, $page = 1)
    {
        if (Yii::$app->request->isAjax) {
            $results = [];
            if (is_numeric($q)) {
                /** @var CsmAttribute $model */
                $model = CsmAttribute::findOne(['id' => $q, 'type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC, 'is_active' => ACTIVE]);
                if ($model) {
                    $results['total_count'] = 1;
                    $results['items'] = [[
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ]];
                }
            } else {
                $q = addslashes($q);
                /** @var CsmAttribute $model */
                $items = [];
                $results['total_count'] = CsmAttribute::find()->where("(`name` like '%{$q}%')")
                    ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC, 'is_active' => ACTIVE])->count();
                foreach (CsmAttribute::find()->where("(`name` like '%{$q}%')")
                             ->andWhere(['type' => TYPE_ATTRIBUTE_CATEGORY_MUSIC, 'is_active' => ACTIVE])
                             ->offset(($page - 1) * $this->pageSize)->limit($this->pageSize)->all() as $model) {
                    $items[] = [
                        'id' => $model->id,
                        'name' => $model->name . ' (id: ' . $model->id . ')',
                    ];
                }
                $results['items'] = $items;
            }
            echo Json::encode($results);
        }
    }
}
<?php

namespace backend\controllers;

use backend\models\ApiClient;
use backend\models\CsmAttribute;
use backend\models\CsmConvertTasks;
use backend\models\CsmMediaMusic;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use backend\models\User;
use common\helpers\MediaHelper;
use backend\models\upload\FileUploaded;
use common\helpers\S3Service;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaCustomMusicCreateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaMusicCreateController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaCustomMusicCreateController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaCustomMusicCreateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsmMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmMedia();

        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $values = Yii::$app->request->post();
        if ($model->load($values)) {
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->type = TYPE_MEDIA_MUSIC;
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());
            Yii::info("MEDIAINFO MUSIC: id = " . $model->id . "; filepath=" . $model->getFullOriginalPath() .
                "; duration=" . $duration . "; width=" . $width . "; height=" . $height);

            if ($duration >= Yii::$app->params['upload']['custom-video']['minDuration']
                && $width >= Yii::$app->params['upload']['custom-video']['minWidth']
                && $height >= Yii::$app->params['upload']['custom-video']['minHeight']) {
                $model->duration = $duration;
                $model->max_quantity = min($width, $height);
                $model->resolution = $width . 'x' . $height;
                if (CsmMedia::checkExistMedia($model->name, $model->type, $model->duration, $model->max_quantity) > 0) {
                    Yii::$app->session->setFlash('error', Yii::t('backend', "Upload failed: Duplicate upload."));
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
                if ($user->cp_id) {
                    $model->cp_id = $user->cp_id;
                    $model->cp_info = json_encode($user->generateCpJsonObj());
                }
                // category attributes
                $attributes = array();
                $categories = null;
                $clients = array();
                if (count($model->category_list_music)) {
                    $categories = CsmAttribute::getCategoryMusicByIdsActive($model->category_list_music);
                    foreach ($categories as $csmAttribute) {
                        /* @var CsmAttribute $csmAttribute */
                        $attributes[] = $csmAttribute->generateJsonObj();
                    }
                }
                // end - category attributes
                // singer attributes
                if (count($model->singer_list)) {
                    $csmAttributes = CsmAttribute::getSingerByIdsActive($model->singer_list);
                    foreach ($csmAttributes as $csmAttribute) {
                        $attributes[] = $csmAttribute->generateJsonObj();
                    }
                }
                // end - singer attributes
                // composer attributes
                if (count($model->composer_list)) {
                    $csmAttributes = CsmAttribute::getComposerByIdsActive($model->composer_list);
                    foreach ($csmAttributes as $csmAttribute) {
                        $attributes[] = $csmAttribute->generateJsonObj();
                    }
                }
                // end - composer attributes
                // clients
                if (count($model->client_list)) {
                    $clientList = ApiClient::getByIds($model->client_list);
                    foreach ($clientList as $client) {
                        /* @var ApiClient $client */
                        $clients[] = $client->generateJsonObj();
                    }
                }
                // end - clients
                // update metadata
                $meta = array();
                if ($model->meta_album) {
                    $meta[META_ALBUM] = $model->meta_album;
                }
                if ($model->meta_year) {
                    $meta[META_YEAR] = $model->meta_year;
                }
                if ($model->meta_track) {
                    $meta[META_TRACK] = $model->meta_track;
                }
                if ($model->meta_comment) {
                    $meta[META_COMMENT] = $model->meta_comment;
                }
                if ($model->meta_copyright) {
                    $meta[META_COPYRIGHT] = $model->meta_copyright;
                }
                if ($model->meta_author) {
                    $meta[META_AUTHOR] = $model->meta_author;
                }
                if ($model->meta_country) {
                    $meta[META_COUNTRY] = $model->meta_country;
                }
                if ($model->meta_language) {
                    $meta[META_LANGUAGE] = $model->meta_language;
                }
                if ($model->meta_subtitle_language) {
                    $meta[META_SUBTITLE_LANGUAGE] = $model->meta_subtitle_language;
                }
                if ($model->meta_content_filter) {
                    $meta[META_CONTENT_FILTER] = $model->meta_content_filter;
                }
                if ($model->meta_imdb_rating) {
                    $meta[META_IMDB_RATING] = $model->meta_imdb_rating;
                }
                if ($model->name) {
                    $meta[META_TITLE] = $model->name;
                }
                if ($model->description) {
                    $meta[META_DESCRIPTION] = $model->description;
                }
                $genres = array();
                if ($categories) {
                    foreach ($categories as $csmAttribute) {
                        /* @var CsmAttribute $csmAttribute */
                        $genres[] = $csmAttribute->name;
                    }
                    $meta[META_GENRE] = implode(", ", $genres);
                }
                $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
                // end - update metadata

                if (count($attributes)) {
                    $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
                }
                if (count($clients)) {
                    $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
                }
                $model->is_crawler = 0;
                if ($model->save()) {
                    FileUploaded::deleteByUrlAndType($model->original_path);
                    FileUploaded::deleteByUrlAndType($model->image_path);
                    FileUploaded::deleteByUrlAndType($model->poster_path);
                    return $this->redirect('index');
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('backend', 'Tạo thất bại, Video phải có thời lượng tối thiểu '
                    .Yii::$app->params['upload']['custom-video']['minDuration'].' giây, resolution tối thiểu '
                    .Yii::$app->params['upload']['custom-video']['minWidth'].'x'.Yii::$app->params['upload']['custom-video']['minHeight'].' px'));
                return $this->redirect('index');
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_published_list = json_decode($model->published_list);

        $values = Yii::$app->request->post();
        if ($model->load($values)) {
            $attributes = array();
            $categories = array();
            $clients = array();
            // update category attributes
            if (count($model->category_list_music)) {
                $categories = CsmAttribute::getCategoryMusicByIdsActive($model->category_list_music);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update category attributes
            // singer attributes
            if (count($model->singer_list)) {
                $csmAttributes = CsmAttribute::getSingerByIdsActive($model->singer_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - singer attributes
            // composer attributes
            if (count($model->composer_list)) {
                $csmAttributes = CsmAttribute::getComposerByIdsActive($model->composer_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - composer attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // done - update director attributes
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = $model->meta_album;
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = $model->meta_year;
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = $model->meta_track;
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = $model->meta_comment;
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = $model->meta_copyright;
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = $model->meta_author;
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = $model->meta_country;
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = $model->meta_language;
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = $model->meta_subtitle_language;
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = $model->meta_content_filter;
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = $model->meta_imdb_rating;
            }
            if ($model->name) {
                $meta[META_TITLE] = $model->name;
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = $model->description;
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            if ($model->save()) {
                $old_client_list = array_map(function ($item){
                    return is_object($item) ? $item->id : $item['id'];
                },$old_published_list);
                $diff_client_id = array_diff($old_client_list,$model->client_list);
                if($diff_client_id){
                    // xóa các dịch vụ không được phân phối
                    CsmMediaPublish::deleteAll(['media_id' => $id,'client_id' => $diff_client_id]);
                }
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if (CsmMedia::checkAndUpdateStatus([$id], STATUS_MEDIA_DELETED,
                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
            ) {
                CsmScheduleDeleted::deleteMedia([$id]);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete item failed! Item invalid!"));
            }
        }
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmMediaMusic::getCreatedById($id, TYPE_MEDIA_MUSIC)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if ($status == 'reviewed-proposal') {
                $ids = Yii::$app->getRequest()->post('ids');
                // Update status to ready for upload
                if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_TRANSFER_SUCCESS,
                    [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
                ) {
                    // Update status pending approved
                    CsmMediaPublish::updateStatusCp($ids, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_DRAFT);
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to need approved successful"));
                } else {
                    Yii::$app->session->setFlash('warning', Yii::t('backend', "Check status invalid!"));
                }
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Status is invalid"));
            }
        }
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

//    public function actionDeleteMultiple()
//    {
//        $user = Yii::$app->user->identity;
//        /* @var \backend\models\User $user */
//        if ($user->cp_id) {
//            $ids = Yii::$app->getRequest()->post('ids');
//            if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_DELETED,
//                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
//            ) {
//                CsmScheduleDeleted::deleteMedia($ids);
//                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
//            } else {
//                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete items failed! List of items invalid!"));
//            }
//        }
//        $searchModel = new CsmMediaMusicCreateSearch();
//        $dataProvider = $searchModel->search(Yii::$app->session->get(self::className() . 'queryParams'));
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
}

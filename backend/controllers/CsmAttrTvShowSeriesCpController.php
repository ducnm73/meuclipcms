<?php

namespace backend\controllers;

use backend\models\CsmScheduleDeleted;
use backend\models\User;
use common\helpers\S3Service;
use Yii;
use backend\models\CsmAttribute;
use backend\models\CsmAttrTvSeriesShowCpSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmAttrTvShowSeriesCpController implements the CRUD actions for CsmAttribute model.
 */
class CsmAttrTvShowSeriesCpController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmAttribute models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmAttrTvSeriesShowCpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmAttribute model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsmAttribute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmAttribute();
        $model->scenario = CsmAttribute::SCENARIO_FILM_ATTRIBUTE ;
        $model->type = TYPE_ATTRIBUTE_TV_SHOW_SERIES;
        $model->is_active = INACTIVE;

        if ($model->load(Yii::$app->request->post())) {
//            if ($model->image_path) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//            }
//            if ($model->second_image) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->second_image;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->second_image = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->second_image = null;
//                }
//            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmAttribute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
//            if ($model->isAttributeChanged('image_path')) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if ($model->image_path && is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_SLASHES);
//                        if ($model->getOldAttribute('image_path')) {
//                            $imagePath = json_decode($model->getOldAttribute('image_path'), true);
//                            if ($imagePath['path']) {
//                                $imagePath = substr($imagePath['path'], 1);
//                                S3Service::deleteObject(Yii::$app->params['s3']['static.bucket'], $imagePath);
//                            }
//                        }
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//            }
//            if ($model->isAttributeChanged('second_image')) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->second_image;
//                if ($model->second_image && is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->second_image = json_encode($upload['image_path'], JSON_UNESCAPED_SLASHES);
//                        if ($model->getOldAttribute('second_image')) {
//                            $imagePath = json_decode($model->getOldAttribute('second_image'), true);
//                            if ($imagePath['path']) {
//                                $imagePath = substr($imagePath['path'], 1);
//                                S3Service::deleteObject(Yii::$app->params['s3']['static.bucket'], $imagePath);
//                            }
//                        }
//                    }
//                } else {
//                    $model->second_image = null;
//                }
//            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmAttribute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        CsmScheduleDeleted::deleteAttribute([$model->id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the CsmAttribute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmAttribute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $userId = User::getUserId();
        if (($model = CsmAttribute::findOne(['id' => $id, 'created_by' => $userId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

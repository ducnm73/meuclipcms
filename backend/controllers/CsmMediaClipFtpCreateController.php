<?php

namespace backend\controllers;

use backend\models\ApiClient;
use backend\models\CsmAttribute;
use backend\models\CsmMediaClip;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use common\helpers\MediaHelper;
use backend\models\upload\FileUploaded;
use common\helpers\UploadHelper;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaClipCreateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaClipCreateController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaClipFtpCreateController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaClipCreateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsmMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmMediaClip();
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($model->load(Yii::$app->request->post())) {
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->type = TYPE_MEDIA_CLIP;
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;

            if ($model->original_path) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }

            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            Yii::info("MEDIAINFO VCLIP: id = " . $model->id . "; filepath=" . $model->getFullOriginalPath() .
                "; duration=" . $duration . "; width=" . $width . "; height=" . $height);
            $model->duration = $duration;
            if ($model->duration > 0 && $width > 0 && $height > 0) {
                if ($model->image_path) {
                    $model->image_path = UploadHelper::getFtpBasePath($model->image_path);
                }

                if ($model->poster_path) {
                    $model->poster_path = UploadHelper::getFtpBasePath($model->poster_path);
                }
                $model->max_quantity = min($width, $height);
                $model->resolution = $width . 'x' . $height;
                if (CsmMedia::checkExistMedia($model->name, $model->type, $model->duration, $model->max_quantity) > 0) {
                    Yii::$app->session->setFlash('error', Yii::t('backend', "File upload is duplicated."));
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
                if ($user->cp_id) {
                    $model->cp_id = $user->cp_id;
                    $model->cp_info = json_encode($user->generateCpJsonObj());
                }
                // category attributes
                $attributes = array();
                $clients = array();
                if (count($model->category_list)) {
                    $csmAttributes = CsmAttribute::getCategoryByIdsActive($model->category_list);
                    foreach ($csmAttributes as $csmAttribute) {
                        /* @var CsmAttribute $csmAttribute */
                        $attributes[] = $csmAttribute->generateJsonObj();
                    }
                }
                if (count($attributes)) {
                    $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
                }
                // end - category attributes
                // clients
                if (count($model->client_list)) {
                    $clientList = ApiClient::getByIds($model->client_list);
                    foreach ($clientList as $client) {
                        /* @var ApiClient $client */
                        $clients[] = $client->generateJsonObj();
                    }
                }
                if (count($clients)) {
                    $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
                }
                // end - clients
                $model->is_crawler = 0;
                $model->status = STATUS_MEDIA_DRAFT;
                if ($model->save()) {
                    // clear file_upload when save finish
                    FileUploaded::deleteByUrlAndType($model->original_path);
                    FileUploaded::deleteByUrlAndType($model->image_path);
                    FileUploaded::deleteByUrlAndType($model->poster_path);
//                    return $this->redirect(['update', 'id' => $model->id]);
                    return $this->redirect('index');
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Processing of the video failed. Upload again please."));
                $model->original_path = '';
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_published_list = json_decode($model->published_list);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->original_path && $model->isAttributeChanged('original_path')) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }

            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Wrong duration or width or height of video"));
                $model->original_path = '';
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            if ($model->image_path && $model->isAttributeChanged('image_path')) {
                $model->image_path = UploadHelper::getFtpBasePath($model->image_path);
            }

            if ($model->poster_path && $model->isAttributeChanged('poster_path')) {
                $model->poster_path = UploadHelper::getFtpBasePath($model->poster_path);
            }

            $model->duration = $duration;
            $model->max_quantity = min($width, $height);
            $model->resolution = $width . 'x' . $height;

            $attributes = array();
            $clients = array();
            // update category attributes
            if (count($model->category_list)) {
                $csmAttributes = CsmAttribute::getCategoryByIdsActive($model->category_list);
                foreach ($csmAttributes as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            $attrs = json_decode($model->attributes, true);
            foreach ($attrs as $value) {
                if ($value['type'] != TYPE_ATTRIBUTE_CATEGORY) {
                    $attributes[] = $value;
                }
            }
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            // done - update category attributes
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            if ($model->save()) {
                $old_client_list = array_map(function ($item){
                    return is_object($item) ? $item->id : $item['id'];
                },$old_published_list);
                $diff_client_id = array_diff($old_client_list,$model->client_list);
                if($diff_client_id){
                    // xóa các dịch vụ không được phân phối
                    CsmMediaPublish::deleteAll(['media_id' => $id,'client_id' => $diff_client_id]);
                }
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
            }else{
                Yii::$app->session->setFlash('error', Yii::t('backend', "Update item failed"));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if (CsmMedia::checkAndUpdateStatus([$id], STATUS_MEDIA_DELETED,
                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
            ) {
                CsmScheduleDeleted::deleteMedia([$id]);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete item failed! Item invalid!"));
            }
        }
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
//        return $this->redirect('index');
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmMediaClip::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if ($status == 'reviewed-proposal') {
                $ids = Yii::$app->getRequest()->post('ids');
                // Update status to ready for upload
                if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_TRANSFER_SUCCESS,
                    [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
                ) {

                    //CsmMediaPublish::updateStatusCp($ids, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_DRAFT);
                    // Update status with contract approve
                    CsmMediaPublish::updateWithNeedContractApprove($ids, STATUS_MEDIA_DRAFT);

                    Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to need approved successful"));
                } else {
                    Yii::$app->session->setFlash('warning', Yii::t('backend', "Check status invalid!"));
                }
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Status is invalid"));
            }
        }
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
//        return $this->redirect('index');
    }

//    public function actionDeleteMultiple()
//    {
//        $user = Yii::$app->user->identity;
//        /* @var \backend\models\User $user */
//        if ($user->cp_id) {
//            $ids = Yii::$app->getRequest()->post('ids');
//            if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_DELETED,
//                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
//            ) {
//                CsmScheduleDeleted::deleteMedia($ids);
//                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
//            } else {
//                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete items failed! List of items invalid!"));
//            }
//        }
//        $searchModel = new CsmMediaClipCreateSearch();
//        $dataProvider = $searchModel->search(Yii::$app->session->get(self::className() . 'queryParams'));
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }
}

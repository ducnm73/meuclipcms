<?php

/**
 * Created by PhpStorm.
 * User: HoangL
 * Date: 8/10/2015
 * Time: 4:29 PM
 */

namespace backend\controllers;

use Yii;
use yii\web\Controller;

class AppController extends Controller {

      public function beforeAction($action) {
        if (Yii::$app->session->has('lang_code')) {
            Yii::$app->language = Yii::$app->session->get('lang_code');
        } else {
            Yii::$app->language = Yii::$app->params['mainLanguage'];
        }
        return parent::beforeAction($action);
    }

}

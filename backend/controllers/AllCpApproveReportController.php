<?php

namespace backend\controllers;

use backend\models\AllCpApproveReportSearch;
use backend\models\User;
use backend\models\UserClient;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * CsmMediaClipApproveController implements the CRUD actions for CsmMedia model.
 */
class AllCpApproveReportController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        $allUserInClientList = Yii::$app->session->get(self::className().'client_list') ;
        if(!$allUserInClientList){
            $query = User::find()
                ->distinct()
                ->asArray()
                ->select(User::tableName() . '.id, '.User::tableName() . '.username, ')
                ->Leftjoin(UserClient::tableName(), User::tableName() . '.id = ' . UserClient::tableName() . '.user_id')
                ->where([UserClient::tableName() . '.client_id' => User::getClientList()]);
            $allUserInClientList = $query->all();
            Yii::$app->session->set(self::className() . 'client_list', $allUserInClientList);
        }
        //var_dump($allUserInClientList);die();

        $searchModel = new AllCpApproveReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allUserInClientList' => ArrayHelper::map($allUserInClientList, 'username', 'username')
        ]);
    }

}

<?php

namespace backend\controllers;

use backend\models\CsmAttribute;
use backend\models\CsmMediaCustomMusicApproveSearch;
use backend\models\CsmMediaMusic;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use backend\models\CsmMediaActionLog;
use backend\models\User;
use common\helpers\S3Service;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaMusicApproveSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaMusicApproveController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaCustomMusicApproveController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaCustomMusicApproveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /* @var CsmMedia $model */
        $model = $this->findModel($id);
//        $old_image_path = $model->image_path;
//        $old_poster_path = $model->poster_path;
        if ($model->load(Yii::$app->request->post())) {
//            if ($model->image_path != $old_image_path){
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//
//            }
//            if ($model->poster_path != $old_poster_path){
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->poster_path;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->poster_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->poster_path = null;
//                }
//            }
            $attributes = array();
            // update category attributes
            if (count($model->category_list)) {
                $csmAttributes = CsmAttribute::getCategoryMusicByIdsActive($model->category_list);
                foreach ($csmAttributes as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            $attrs = json_decode($model->attributes, true);
            foreach ($attrs as $value) {
                if ($value['type'] != TYPE_ATTRIBUTE_CATEGORY_MUSIC) {
                    $attributes[] = $value;
                }
            }
            // end - clients
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            if ($model->save()) {
                // update status
                $watched_duration = $model->watched_duration;
                if ($model->media_client_status == STATUS_MEDIA_APPROVED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_APPROVE, '', User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_APPROVED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                } else if ($model->media_client_status == STATUS_MEDIA_REJECTED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_DECLINE, $model->reject_reason, User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_REJECTED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                }
//                return $this->redirect('index');
                return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        CsmMedia::updateStatusAdmin($id, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia([$id]);
        CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_DELETE, '', User::getUserId(), null);
        CsmMediaPublish::updateStatus($id, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete item successful"));
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return array|\yii\db\ActiveRecord
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = CsmMediaMusic::find()->where([
            CsmMedia::tableName() . '.id' => $id,
            CsmMedia::tableName() . '.type' => TYPE_MEDIA_MUSIC,
            CsmMedia::tableName() . '.status' => [STATUS_MEDIA_PUBLISHED],
            CsmMedia::tableName() . '.is_crawler' => 0
        ])->innerJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.media_id = ' .
            CsmMedia::tableName() . '.id')
            ->andWhere([
                CsmMediaPublish::tableName() . '.client_id' => User::getClientList(),
                CsmMediaPublish::tableName() . '.status' => [STATUS_MEDIA_APPROVED_PROPOSAL]
            ])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        if ($status == 'approved-accepted') {
            $ids = Yii::$app->getRequest()->post('ids');

            foreach ($ids as $id) {
                CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_APPROVE, '', User::getUserId(), null);
            }
            CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_APPROVED,
                STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to approved successful"));
        } else if ($status == 'approved-rejected') {
            $ids = Yii::$app->getRequest()->post('ids');

            foreach ($ids as $id) {
                CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_DECLINE, '', User::getUserId(), null);
            }
            CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_REJECTED,
                STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to rejected successful"));
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('backend', "Invalid status"));
        }
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

    public function actionDeleteMultiple()
    {
        $ids = Yii::$app->getRequest()->post('ids');
//        CsmMedia::updateStatusAdmin($ids, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia($ids);

        CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
        foreach ($ids as $media_id) {
            $log = new CsmMediaActionLog(['media_id' => $media_id, 'action' => MEDIA_ACTION_DELETE, 'note' => '', 'created_by' => User::getUserId()]);
            $rows[] = $log->attributes;
        }
        $logModel = new CsmMediaActionLog();
        Yii::$app->db->createCommand()->batchInsert(CsmMediaActionLog::tableName(), $logModel->attributes(), $rows)->execute();
        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }
}

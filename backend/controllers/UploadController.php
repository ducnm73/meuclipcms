<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 12/28/2016
 * Time: 11:01 PM
 */

namespace backend\controllers;

use awesome\fineuploader\UploadAction;
use common\helpers\FileHelper;
use common\helpers\Helpers;
use common\helpers\MediaHelper;
use common\helpers\UploadHelper;
use backend\models\upload\FileUploaded;
use Yii;

class UploadController extends AppController
{
    public function actions()
    {
        Yii::info('upload-something');
        $type = Yii::$app->request->getQueryParam('type');
        return [
            'video-upload' => [
                'class' => UploadAction::className(),
                'redisConfig' => 'uploadChunks',
                'moduleName' => 'fineuploader',
                'timeout' => 2 * 60 * 60, // 2 hours
                'basePath' => UploadHelper::getBasePathUpload($type),
                'baseUrl' => Yii::$app->params['upload']['baseUrl'],
                'postFieldName' => 'qqfile', // default
                'format' => function (UploadAction $action) {
                    $fileext = $action->getExtension();
                    $filehash = Helpers::generateStructurePath();
                    return "{$filehash}.{$fileext}";
                },
                'validateOptions' => [
                    'extensions' => \Yii::$app->params['upload'][$type]['extensions'],
                    'maxSize' => \Yii::$app->params['upload'][$type]['maxSize'],
                ],
                'afterSave' => function (UploadAction $action) {
                    $type = Yii::$app->request->getQueryParam('type');
                    $action->output['fileUrl'] = $action->baseUrl . Yii::$app->params['upload'][$type]['basePath'] .
                        '/' . $action->getFilename();

                    if (isset($action->output['filePath'])) {
                        $action->output['filePath'] = Yii::$app->params['upload'][$type]['basePath'] . '/' .
                            $action->output['filePath'];
                    }
//                    $totalParts = isset($_REQUEST['qqtotalparts']) ? (int)$_REQUEST['qqtotalparts'] : 1;
//                    if (($totalParts > 1 && isset($_GET['done'])) || $totalParts == 1) {
//                        # chunked upload & has 'done' in query OR non-chunked upload
//                        $height = MediaHelper::getHeight($action->getSavePath());
//                        $width = MediaHelper::getWidth($action->getSavePath());
//                        $maxQuantity = min($width, $height);
//                        if ($maxQuantity < Yii::$app->params['upload'][$type]['minQuality']) {
//                            FileHelper::deleteFile($action->getSavePath());
//                            $action->output = array('error' => 'Your upload quality is lower than ' .
//                                Yii::$app->params['upload'][$type]['minQuality'] . 'p');
//                            $action->output['getSavePath'] =  $action->getSavePath();
//                        } else {
                    $action->output['fileUrl'] = $action->baseUrl . Yii::$app->params['upload'][$type]['basePath'] .
                        '/' . $action->getFilename();
                    $action->output['filePath'] = UploadHelper::getFilePathToSave($action->getSavePath(), $type);
                    $action->output['getSavePath'] = $action->getSavePath();
                    FileUploaded::log($action->output['filePath'], $type);
//                        }
//                    }
                },
            ],
            'image-upload' => [
                'class' => UploadAction::className(),
                'basePath' => UploadHelper::getBasePathUpload($type),
                'baseUrl' => Yii::$app->params['upload']['baseUrl'],
                'postFieldName' => 'qqfile', // default
                'format' => function (UploadAction $action) {
                    $fileext = $action->getExtension();
                    $filehash = Helpers::generateStructurePath();
                    return "{$filehash}.{$fileext}";
                },
                'validateOptions' => [
                    'extensions' => \Yii::$app->params['upload'][$type]['extensions'],
                    'maxSize' => \Yii::$app->params['upload'][$type]['maxSize'],
                ],
                'afterSave' => function (UploadAction $action) {
                    $type = Yii::$app->request->getQueryParam('type');
                    $action->output['fileUrl'] = $action->baseUrl . Yii::$app->params['upload'][$type]['basePath'] .
                        '/' . $action->getFilename();
                    $action->output['filePath'] = UploadHelper::getFilePathToSave($action->getSavePath(), $type);
                    FileUploaded::log($action->output['filePath'], $type);
                },
            ],
            'audio-upload' => [
                'class' => UploadAction::className(),
                'redisConfig' => 'uploadChunks',
                'moduleName' => 'fineuploader',
                'timeout' => 2 * 60 * 60, // 2 hours
                'basePath' => UploadHelper::getBasePathUpload($type),
                'baseUrl' => Yii::$app->params['upload']['baseUrl'],
                'postFieldName' => 'qqfile', // default
                'format' => function (UploadAction $action) {
                    $fileext = $action->getExtension();
                    $filehash = Helpers::generateStructurePath();
                    return "{$filehash}.{$fileext}";
                },
                'validateOptions' => [
                    'extensions' => \Yii::$app->params['upload'][$type]['extensions'],
                    'maxSize' => \Yii::$app->params['upload'][$type]['maxSize'],
                ],
                'afterSave' => function (UploadAction $action) {
                    $type = Yii::$app->request->getQueryParam('type');
                    $action->output['fileUrl'] = $action->baseUrl . Yii::$app->params['upload'][$type]['basePath'] .
                        '/' . $action->getFilename();
                    $action->output['filePath'] = UploadHelper::getFilePathToSave($action->getSavePath(), $type);
                    FileUploaded::log($action->output['filePath'], $type);
                },
            ],
            'subtitle-upload' => [
                'class' => UploadAction::className(),
                'basePath' => UploadHelper::getBasePathUpload($type),
                'baseUrl' => Yii::$app->params['upload']['baseUrl'],
                'postFieldName' => 'qqfile', // default
                'format' => function (UploadAction $action) {
                    $fileext = $action->getExtension();
                    $filehash = Helpers::generateStructurePath();
                    return "{$filehash}.{$fileext}";
                },
                'validateOptions' => [
                    'extensions' => \Yii::$app->params['upload'][$type]['extensions'],
                    'maxSize' => \Yii::$app->params['upload'][$type]['maxSize'],
                ],
                'afterSave' => function (UploadAction $action) {
                    $type = Yii::$app->request->getQueryParam('type');
                    $action->output['fileUrl'] = $action->baseUrl . Yii::$app->params['upload'][$type]['basePath'] .
                        '/' . $action->getFilename();
                    $action->output['filePath'] = UploadHelper::getFilePathToSave($action->getSavePath(), $type);
                    FileUploaded::log($action->output['filePath'], $type);
                },
            ],
        ];
    }

//    public function actionGetFtpFileFromPath()
//    {
//        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        if (Yii::$app->request->post()) {
//            $path = Yii::$app->request->post('path');
//            $type = Yii::$app->request->post('type');
//            $user_name = Yii::$app->user->identity->username;
//            if ($user_name && $path && $type) {
//                if (array_key_exists($type, Yii::$app->params['upload'])) {
//                    $extention = Yii::$app->params['upload'][$type]['extensions'];
//                } else {
//                    return [
//                        'responseCode' => 403,
//                        'message' => 'Wrong parameter',
//                        'data' => ''
//                    ];
//
//                }
//                $data = UploadHelper::php_file_tree($user_name, $path, false, $extention);
//                return [
//                    'responseCode' => 200,
//                    'message' => 'OK',
//                    'data' => $data
//                ];
//            } else {
//                return [
//                    'responseCode' => 403,
//                    'message' => 'Wrong parameter',
//                    'data' => ''
//                ];
//
//            }
//        } else {
//            return [
//                'responseCode' => 405,
//                'message' => 'Method not allow',
//                'data' => ''
//            ];
//        }
//    }

    public function actionGetFtpFileFromPath()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->post()) {
            $path = Yii::$app->request->post('path');
            $type = Yii::$app->request->post('type');
            $user_name = Yii::$app->user->identity->username;
            if ($user_name && $path && $type) {
                if (array_key_exists($type, Yii::$app->params['upload'])) {
                    $extention = Yii::$app->params['upload'][$type]['extensions'];
                } else {
                    return [
                        'responseCode' => 403,
                        'message' => 'Wrong parameter',
                        'data' => ''
                    ];

                }
                $data = UploadHelper::php_file_tree($user_name, $path, false, $extention);
                return [
                    'responseCode' => 200,
                    'message' => 'OK',
                    'data' => $data
                ];
            } else {
                return [
                    'responseCode' => 403,
                    'message' => 'Wrong parameter',
                    'data' => ''
                ];

            }
        } else {
            return [
                'responseCode' => 405,
                'message' => 'Method not allow',
                'data' => ''
            ];
        }
    }

}
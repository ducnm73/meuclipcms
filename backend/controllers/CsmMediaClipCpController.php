<?php

namespace backend\controllers;

use awesome\backend\grid\AwsFormatter;
use backend\components\common\Utility;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaClipCpSearch;
use backend\controllers\AppController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaClipCpController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaClipCpController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaClipCpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        ini_set('memory_limit', '2048M');
        $queryParams = Yii::$app->session->get(self::className() . 'queryParams');

        if (!$queryParams) {
            $this->redirect('index');
        }
        $searchModel = new CsmMediaClipCpSearch();
        $dataProvider = $searchModel->search($queryParams);
        $max = 10000;
        if ($dataProvider->totalCount > $max) {
            Yii::$app->session->setFlash('error', Yii::t('backend', 'Không thể thực hiện do số lượng dữ liệu vượt quá {max}. Vui lòng lọc thêm dữ liệu', ['max' => $max]));
            return $this->redirect('index');
        }

        $fields = [
            [
                'name' => 'media_id',
                'headerText' => 'Video ID',
            ],
            [
                'name' => 'name',
                'headerText' => 'Tên video',
                'callback' => function ($model) {
                    return $model->media->name;
                }
            ],
            [
                'name' => 'duration',
                'headerText' => 'Thời lượng',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asMediaDuration($model->media->duration);
                }
            ],
            [
                'name' => 'resolution',
                'headerText' => 'Độ phân giải',
                'callback' => function ($model) {
                    return $model->media->resolution;
                }
            ],
            [
                'name' => 'status',
                'headerText' => 'Trạng thái file',
                'callback' => function ($model) {
                    return Yii::$app->params['media-status'][$model->media->status];
                }
            ],
            [
                'name' => 'created_at',
                'headerText' => 'Ngày tạo',
                'callback' => function ($model) {
                    return $model->media->created_at;
                }
            ],
            [
                'name' => 'updated_at',
                'headerText' => 'Ngày update',
                'callback' => function ($model) {
                    return $model->media->updated_at;
                }
            ],
            [
                'name' => 'attributes',
                'headerText' => 'Thuộc tính',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asJsonNameType($model->media->attributes);
                }
            ],
            [
                'name' => 'published_list',
                'headerText' => 'Upload cho dịch vụ',
                'callback' => function ($model) {
                    return $model->client->name ;

                }
            ],
            [
                'name' => 'service_status',
                'headerText' => 'Trạng thái dịch vụ',
                'callback' => function ($model) {
                    return Yii::$app->params['media-status'][$model->status];

                }
            ],

        ];

        Utility::exportExcel($queryParams, CsmMediaClipCpSearch::className(), 'danh sách video', $fields, 'danh sach video');
    }
}

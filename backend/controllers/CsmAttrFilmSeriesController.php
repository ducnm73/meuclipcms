<?php

namespace backend\controllers;

use common\helpers\S3Service;
use Yii;
use backend\models\CsmAttribute;
use backend\models\CsmAttrFilmSeriesSearch;
use backend\controllers\AppController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmAttrFilmSeriesController implements the CRUD actions for CsmAttribute model.
 */
class CsmAttrFilmSeriesController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmAttribute models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmAttrFilmSeriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmAttribute model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CsmAttribute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmAttribute();
        $model->scenario = CsmAttribute::SCENARIO_FILM_ATTRIBUTE ;
        $model->is_active = INACTIVE;

        $model->type = TYPE_ATTRIBUTE_FILM_SERIES;

        if ($model->load(Yii::$app->request->post())) {
//            if ($model->image_path) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if (file_exists($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//            }
//            if ($model->second_image) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->second_image;
//                if (file_exists($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->second_image = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->second_image = null;
//                }
//            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmAttribute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = CsmAttribute::SCENARIO_FILM_ATTRIBUTE ;

        if ($model->load(Yii::$app->request->post())) {
//            if ($model->isAttributeChanged('image_path')) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if ($model->image_path && file_exists($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_SLASHES);
//                        if ($model->getOldAttribute('image_path')) {
//                            $imagePath = json_decode($model->getOldAttribute('image_path'), true);
//                            if ($imagePath['path']) {
//                                $imagePath = substr($imagePath['path'], 1);
//                                S3Service::deleteObject(Yii::$app->params['s3']['static.bucket'], $imagePath);
//                            }
//                        }
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//            }
//            if ($model->isAttributeChanged('second_image')) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->second_image;
//                if ($model->second_image && file_exists($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->second_image = json_encode($upload['image_path'], JSON_UNESCAPED_SLASHES);
//                        if ($model->getOldAttribute('second_image')) {
//                            $imagePath = json_decode($model->getOldAttribute('second_image'), true);
//                            if ($imagePath['path']) {
//                                $imagePath = substr($imagePath['path'], 1);
//                                S3Service::deleteObject(Yii::$app->params['s3']['static.bucket'], $imagePath);
//                            }
//                        }
//                    }
//                } else {
//                    $model->second_image = null;
//                }
//            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmAttribute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CsmAttribute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmAttribute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmAttribute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use backend\components\behaviors\VietnameseSlugBehavior;
use backend\models\CsmAttribute;
use backend\models\CsmMediaClip;
use backend\models\CsmMediaCustomClipApproveSearch;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use backend\models\User;
use backend\models\CsmCp;
use backend\models\CsmMediaActionLog;
use backend\models\MediaRejectForm;
use backend\components\common\Utility;
use common\helpers\S3Service;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaClipApproveSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \awesome\backend\grid\AwsFormatter;

/**
 * CsmMediaClipApproveController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaCustomClipApproveController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaCustomClipApproveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CsmMedia model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $lang = Yii::$app->request->queryParams['lang'];
        $multipleLanguageFields = ['name', 'short_desc', 'description', 'seo_title', 'seo_description', 'slug'];
        if (!is_null($lang) && $lang != Yii::$app->params['mainLanguage']) {
            foreach ($multipleLanguageFields as $ele) {
                $model->$ele = $model->multiple_language[$lang][$ele];
            }
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /* @var CsmMedia $model */
        $model = $this->findModel($id);
//        $old_image_path = $model->image_path;
//        $old_poster_path = $model->poster_path;
        if ($model->load(Yii::$app->request->post())) {
//            if ($model->image_path != $old_image_path) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->image_path;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->image_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->image_path = null;
//                }
//
//            }
//            if ($model->poster_path != $old_poster_path) {
//                $filePath = Yii::$app->params['upload']['basePath'] . $model->poster_path;
//                if (is_file($filePath)) {
//                    $upload = S3Service::uploadImage($filePath);
//                    if ($upload['errorCode'] == S3Service::SUCCESS) {
//                        $model->poster_path = json_encode($upload['image_path'], JSON_UNESCAPED_UNICODE);
//                    }
//                } else {
//                    $model->poster_path = null;
//                }
//            }
            $attributes = array();
            // update category attributes
            if (count($model->category_list)) {
                $csmAttributes = CsmAttribute::getCategoryByIdsActive($model->category_list);
                foreach ($csmAttributes as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            $attrs = json_decode($model->attributes, true);
            foreach ($attrs as $value) {
                if ($value['type'] != TYPE_ATTRIBUTE_CATEGORY) {
                    $attributes[] = $value;
                }
            }
            // end - clients
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            if ($model->save()) {
                // update status
                $watched_duration = $model->watched_duration;
                if ($model->media_client_status == STATUS_MEDIA_APPROVED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_APPROVE, '', User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_APPROVED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                } else if ($model->media_client_status == STATUS_MEDIA_REJECTED) {
                    CsmMediaActionLog::saveActionLog($model->id, MEDIA_ACTION_DECLINE, $model->reject_reason, User::getUserId(), $watched_duration);
                    CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_REJECTED,
                        STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                }
//                return $this->redirect('index');
                return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        CsmMedia::updateStatusAdmin($id, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia([$id]);
        CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_DELETE, '', User::getUserId(), null);
        CsmMediaPublish::updateStatus($id, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());

        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete item successful"));
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }


    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return array|\yii\db\ActiveRecord
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = CsmMediaClip::find()->where([
            CsmMedia::tableName() . '.id' => $id,
            CsmMedia::tableName() . '.type' => TYPE_MEDIA_CLIP,
            CsmMedia::tableName() . '.status' => [STATUS_MEDIA_PUBLISHED]
        ])->innerJoin(CsmMediaPublish::tableName(), CsmMediaPublish::tableName() . '.media_id = ' .
            CsmMedia::tableName() . '.id')
            ->andWhere(CsmMedia::tableName() . '.is_crawler = 0 OR ' . CsmMedia::tableName() . '.is_crawler IS NULL')
            ->andWhere([
                CsmMediaPublish::tableName() . '.client_id' => User::getClientList(),
                CsmMediaPublish::tableName() . '.status' => [STATUS_MEDIA_APPROVED_PROPOSAL]
            ])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        if ($status == 'approved-accepted') {
            $ids = Yii::$app->getRequest()->post('ids');
            foreach ($ids as $id) {
                CsmMediaActionLog::saveActionLog($id, MEDIA_ACTION_APPROVE, '', User::getUserId(), null);
            }

            CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_APPROVED,
                STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to approved successful"));
        } else {
            //todo remove
            if ($status == 'approved-rejected') {
                $ids = Yii::$app->getRequest()->post('ids');
                /*CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_REJECTED,
                    STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
                Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to rejected successful"));*/
                $rejectModel = new MediaRejectForm(['media_ids' => $ids]);
                return $this->render('/csm-media-clip-approve/_bulk_reject', [
                    'model' => $rejectModel,
                ]);
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Invalid status"));
            }
        }
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

    public function actionDeleteMultiple()
    {
        $ids = Yii::$app->getRequest()->post('ids');

        CsmMediaPublish::updateStatus($ids, STATUS_MEDIA_DELETED,
            STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
        foreach ($ids as $media_id) {
            $log = new CsmMediaActionLog(['media_id' => $media_id, 'action' => MEDIA_ACTION_DELETE, 'note' => '', 'created_by' => User::getUserId()]);
            $rows[] = $log->attributes;
        }
        $logModel = new CsmMediaActionLog();
        Yii::$app->db->createCommand()->batchInsert(CsmMediaActionLog::tableName(), $logModel->attributes(), $rows)->execute();

//        CsmMedia::updateStatusAdmin($ids, STATUS_MEDIA_DELETED, STATUS_MEDIA_PUBLISHED);
//        CsmScheduleDeleted::deleteMedia($ids);
        Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
//        return $this->redirect('index');
        return $this->redirect(array_merge(['index'],Yii::$app->session->get(self::className() . 'queryParams')));
    }

    public function actionExport()
    {
        ini_set('memory_limit', '2048M');
        $queryParams = Yii::$app->session->get(self::className() . 'queryParams');

        if (!$queryParams) {
            $this->redirect('index');
        }
        $searchModel = new CsmMediaCustomClipApproveSearch();
        $dataProvider = $searchModel->search($queryParams);
        $max = 10000;
        if ($dataProvider->totalCount > $max) {
            Yii::$app->session->setFlash('error', Yii::t('backend', 'Không thể thực hiện do số lượng dữ liệu vượt quá {max}. Vui lòng lọc thêm dữ liệu', ['max' => $max]));
            return $this->redirect('index');
        }

        $fields = [
            [
                'name' => 'id',
                'headerText' => 'Video ID',
            ],
            [
                'name' => 'name',
                'headerText' => 'Tên video',
            ],
            [
                'name' => 'duration',
                'headerText' => 'Thời lượng',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asMediaDuration($model->duration);
                }
            ],
            [
                'name' => 'resolution',
                'headerText' => 'Độ phân giải'
            ],
            [
                'name' => 'status',
                'headerText' => 'Trạng thái file',
                'callback' => function ($model) {
                    return Yii::$app->params['media-status'][$model->status];
                }
            ],
            [
                'name' => 'created_at',
                'headerText' => 'Ngày tạo',
                'callback' => function ($model) {
                    return $model->created_at;
                }
            ],
            [
                'name' => 'updated_at',
                'headerText' => 'Ngày update',
                'callback' => function ($model) {
                    return $model->updated_at;
                }
            ],
            [
                'name' => 'attributes',
                'headerText' => 'Thuộc tính',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asJsonNameType($model->attributes);
                }
            ],
//            [
//                'name' => 'category_list',
//                'headerText' => 'Thể loại',
//                'callback' => function ($model) {
//                    if ($value = $model->attributes) {
//                        $decode = json_decode($value, true);
//
//                        if (count($decode)) {
//                            $res = [];
//
//                            foreach ($decode as $obj) {
//                                if ($obj['type'] == TYPE_ATTRIBUTE_CATEGORY)
//                                    $res[] = $obj['name'];
//                            }
//
//                            return implode(', ', $res);
//                        } else if ($decode) {
//                            return $decode['name'];
//                        }
//                    }
//
//                    return '';
//                }
//            ],
            [
                'name' => 'cp_id',
                'headerText' => 'Mã CP',
                'callback' => function ($model) {
                    $cp = CsmCp::findOne($model->cp_id);

                    if ($cp) {
                        return $cp->name;
                    }

                    return '';
                }
            ],
            [
                'name' => 'published_list',
                'headerText' => 'Upload cho các dịch vụ',
                'callback' => function ($model) {
                    $format = new AwsFormatter();
                    return $format->asJsonName($model->published_list);
                }
            ]

        ];

        Utility::exportExcel($queryParams, CsmMediaCustomClipApproveSearch::className(), 'danh sách video chờ duyệt', $fields, 'danh sach video cho duyet');
    }

    public function actionBulkRejectMedia()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $arr_id = Yii::$app->request->post('items_id');
        $reason = Yii::$app->request->post('reason');
        if ($arr_id && sizeof($arr_id) > 0) {
            CsmMediaPublish::updateStatus($arr_id, STATUS_MEDIA_REJECTED, STATUS_MEDIA_APPROVED_PROPOSAL, User::getClientList());
            foreach ($arr_id as $media_id) {
                $log = new CsmMediaActionLog(['media_id' => $media_id, 'action' => MEDIA_ACTION_DECLINE, 'note' => $reason, 'created_by' => User::getUserId()]);
                $rows[] = $log->attributes;
            }
            $logModel = new CsmMediaActionLog();
            Yii::$app->db->createCommand()->batchInsert(CsmMediaActionLog::tableName(), $logModel->attributes(), $rows)->execute();
            return array(
                "responseCode" => 200,
                "message" => Yii::t('backend','Từ chối duyệt thành công')
            );
        } else {
            return array(
                "responseCode" => 201,
                "message" => Yii::t('backend','Thất bại')
            );
        }
    }
}

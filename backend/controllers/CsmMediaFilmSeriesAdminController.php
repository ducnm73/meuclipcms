<?php

namespace backend\controllers;

use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaFilmSeriesAdminSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaFilmSeriesAdminController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaFilmSeriesAdminController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaFilmSeriesAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

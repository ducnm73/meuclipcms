<?php

namespace backend\controllers;

use backend\models\ApiClient;
use backend\models\CsmAttribute;
use backend\models\CsmConvertTasks;
use backend\models\CsmMediaAttribute;
use backend\models\CsmMediaFilmSeries;
use backend\models\CsmMediaHistory;
use backend\models\CsmMediaPublish;
use backend\models\CsmScheduleDeleted;
use backend\models\upload\FileUploaded;
use backend\models\User;
use common\helpers\MediaHelper;
use common\helpers\UploadHelper;
use Yii;
use backend\models\CsmMedia;
use backend\models\CsmMediaFilmSeriesCreateSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CsmMediaFilmSeriesCreateController implements the CRUD actions for CsmMedia model.
 */
class CsmMediaFilmSeriesFtpCreateController extends AppController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CsmMedia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CsmMediaFilmSeriesCreateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set(self::className() . 'queryParams', Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CsmMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CsmMediaFilmSeries();
        $model->scenario = CsmMedia::SCENARIO_FILM_SERIES;
        $model->type = TYPE_MEDIA_FILM_SERIES;
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $values = Yii::$app->request->post();
        $model->validate_media_type = [TYPE_MEDIA_FILM_SERIES];
        // process value of film_series_list change type int to int[]
        if ($values[$model->formName()]['film_series_list'] && !is_array($values[$model->formName()]['film_series_list'])) {
            $values[$model->formName()]['film_series_list'] = [$values[$model->formName()]['film_series_list']];
        }
        if ($model->load($values)) {
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;
            if ($user->cp_id) {
                $model->cp_id = $user->cp_id;
                $model->cp_info = json_encode($user->generateCpJsonObj());
            }

            if ($model->original_path) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }

            // fix add duration max_quantity resolution before convert
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Wrong duration or width or height of video"));
                $model->original_path = '';
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if ($model->image_path) {
                $model->image_path = UploadHelper::getFtpBasePath($model->image_path);
            }

            if ($model->poster_path) {
                $model->poster_path = UploadHelper::getFtpBasePath($model->poster_path);
            }
            $model->duration = $duration;
            $model->max_quantity = min($width, $height);
            $model->resolution = $width . 'x' . $height;

            // category attributes
            $attributes = array();
            $categories = null;
            $clients = array();
            if (count($model->category_list_film)) {
                $categories = CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - category attributes
            // film series attributes
            if (count($model->film_series_list)) {
                $attributes = CsmMedia::processAttr($attributes, $model->film_series_list, $model->episode_name,
                    trim($model->episode_no));
            }
            // end - film series attributes
            // actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - actor attributes
            // director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = trim($model->meta_album);
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = trim($model->meta_year);
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = trim($model->meta_track);
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = trim($model->meta_comment);
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = trim($model->meta_copyright);
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = trim($model->meta_author);
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = trim($model->meta_country);
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = trim($model->meta_language);
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = trim($model->meta_subtitle_language);
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = trim($model->meta_content_filter);
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = trim($model->meta_imdb_rating);
            }
            if ($model->name) {
                $meta[META_TITLE] = trim($model->name);
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = trim($model->description);
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            // end - update metadata

            if (count($attributes)) {
                $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            }
            if (count($clients)) {
                $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            }

            if ($model->audio_path) {
                $audio_path = [];
                $jsonDecode = json_decode($model->audio_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $audio['language_code'] = $item['language_code'];
                        $audio['original_path'] = $item['original_path'];
                        $audio['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $audio_path[] = $audio;
                    }
                    $model->audio_path = json_encode($audio_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->audio_path = null;
                }

            }
            if ($model->subtitle_path) {
                $subtitle_path = [];
                $jsonDecode = json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $subtitle['language_code'] = $item['language_code'];
                        $subtitle['original_path'] = $item['original_path'];
                        $subtitle['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $subtitle_path[] = $subtitle;
                    }
                    $model->subtitle_path = json_encode($subtitle_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->subtitle_path = null;
                }

            }

            if ($model->save()) {
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                //fix get id after save
                if ($model->drm_id) {
                    $model->need_encryption = 1;
                    $model->resource_id = 'CSM_' . $model->id;
                    $model->save();
                } else {
                    $model->need_encryption = 0;
                    $model->resource_id = null;
                    $model->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('backend', "Create item successful"));
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Create item failed"));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Clone to a new CsmMedia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionClone($id)
    {
        $old = CsmMediaFilmSeries::findOne($id);
        $model = $old->cloneForReUpload();
        $model->scenario = CsmMedia::SCENARIO_FILM_SERIES;
        $model->type = TYPE_MEDIA_FILM_SERIES;
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        $values = Yii::$app->request->post();
        $model->validate_media_type = [TYPE_MEDIA_FILM_SERIES];
        // process value of film_series_list change type int to int[]
        if ($values[$model->formName()]['film_series_list'] && !is_array($values[$model->formName()]['film_series_list'])) {
            $values[$model->formName()]['film_series_list'] = [$values[$model->formName()]['film_series_list']];
        }
        if ($model->load($values)) {
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;
            if ($user->cp_id) {
                $model->cp_id = $user->cp_id;
                $model->cp_info = json_encode($user->generateCpJsonObj());
            }

            if ($model->original_path) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }


            // fix add duration max_quantity resolution before convert
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Wrong duration or width or height of video"));
                $model->original_path = '';
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if ($model->image_path) {
                $model->image_path = UploadHelper::getFtpBasePath($model->image_path);
            }

            if ($model->poster_path) {
                $model->poster_path = UploadHelper::getFtpBasePath($model->poster_path);
            }
            $model->duration = $duration;
            $model->max_quantity = min($width, $height);
            $model->resolution = $width . 'x' . $height;
            // category attributes
            $attributes = array();
            $categories = null;
            $clients = array();
            if (count($model->category_list_film)) {
                $categories = CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - category attributes
            // film series attributes
            if (count($model->film_series_list)) {
                $attributes = CsmMedia::processAttr($attributes, $model->film_series_list, $model->episode_name,
                    trim($model->episode_no));
            }
            // end - film series attributes
            // actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - actor attributes
            // director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // end - director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = trim($model->meta_album);
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = trim($model->meta_year);
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = trim($model->meta_track);
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = trim($model->meta_comment);
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = trim($model->meta_copyright);
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = trim($model->meta_author);
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = trim($model->meta_country);
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = trim($model->meta_language);
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = trim($model->meta_subtitle_language);
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = trim($model->meta_content_filter);
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = trim($model->meta_imdb_rating);
            }
            if ($model->name) {
                $meta[META_TITLE] = trim($model->name);
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = trim($model->description);
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            // end - update metadata

            if (count($attributes)) {
                $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            }
            if (count($clients)) {
                $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            }

            if ($model->audio_path) {
                $audio_path = [];
                $jsonDecode = json_decode($model->audio_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $audio['language_code'] = $item['language_code'];
                        $audio['original_path'] = $item['original_path'];
                        $audio['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $audio_path[] = $audio;
                    }
                    $model->audio_path = json_encode($audio_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->audio_path = null;
                }

            }
            if ($model->subtitle_path) {
                $subtitle_path = [];
                $jsonDecode = json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $subtitle['language_code'] = $item['language_code'];
                        $subtitle['original_path'] = $item['original_path'];
                        $subtitle['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $subtitle_path[] = $subtitle;
                    }
                    $model->subtitle_path = json_encode($subtitle_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->subtitle_path = null;
                }

            }

            if ($model->save()) {
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);

                if ($model->drm_id) {
                    $model->need_encryption = 1;
                    $model->resource_id = 'CSM_' . $model->id;
                    $model->save();
                } else {
                    $model->need_encryption = 0;
                    $model->resource_id = null;
                    $model->save();
                }

                Yii::$app->session->setFlash('success', Yii::t('backend', "Create item successful"));
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('backend', "Create item failed"));
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionReUpload($id)
    {

        $model = CsmMediaFilmSeries::getMediaPublish($id, TYPE_MEDIA_FILM_SERIES);
        $model->scenario = CsmMedia::SCENARIO_FILM_SERIES;
        if (!$model) {
            throw new NotFoundHttpException('NOT FOUND');
        }
        $old_att = $model->getAttributes(array(
            'id', 'name', 'slug', 'short_desc', 'description', 'price_download', 'price_play', 'type',
            'max_quantity', 'published_by', 'created_at', 'updated_at', 'published_at', 'duration', 'resolution', 'attributes',
            'cp_id', 'cp_info', 'original_path', 'image_path', 'poster_path', 'file_type', 'convert_status', 'convert_path',
            'convert_priority', 'convert_start_time', 'convert_end_time', 'convert_data_id', 'convert_images',
            'meta_info', 'censored_info', 'logo_path', 'need_censored', 'seo_title', 'seo_description', 'seo_keywords', 'is_crawler',
            'crawler_id', 'crawler_info', 'created_by', 'updated_by', 'reviewed_by', 'published_list', 'tag', 'need_encryption', 'resource_id',
            'drm_id', 'convert_server', 'media_info', 'copyright_id', 'copyright_info', 'last_sync_at'
        ));

        $model->audio_path = null;
        $model->subtitle_path = null;
        $model->convert_audio = null;

        $values = Yii::$app->request->post();
        // process value of film_series_list change type int to int[]
        if ($values[$model->formName()]['film_series_list'] && !is_array($values[$model->formName()]['film_series_list'])) {
            $values[$model->formName()]['film_series_list'] = [$values[$model->formName()]['film_series_list']];
        }
        if ($model->load($values)) {
            $attributes = array();
            $categories = array();
            $clients = array();
            $model->file_type = FILE_TYPE_MEDIA_LOCAL;

            if ($model->original_path == $old_att['original_path']) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "You must Re upload video"));
                return $this->render('reupload', [
                    'model' => $model,
                ]);
            }

            // fix add duration max_quantity resolution before convert
            if ($model->original_path) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }

            // fix add duration max_quantity resolution before convert
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Wrong duration or width or height of video"));
                $model->original_path = '';
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->duration = $duration;
            $model->max_quantity = min($width, $height);
            $model->resolution = $width . 'x' . $height;

            if (count($model->category_list_film)) {
                $categories = CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update category attributes
            // update film series attributes
            if (count($model->film_series_list)) {
                $attributes = CsmMedia::processUpdateAttrFull($attributes, $model->film_series_list, $model->id,
                    $model->episode_name, trim($model->episode_no));
            }
            // done - update film series attributes
            // update actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update actor attributes
            // update director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = trim($model->meta_album);
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = trim($model->meta_year);
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = trim($model->meta_track);
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = trim($model->meta_comment);
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = trim($model->meta_copyright);
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = trim($model->meta_author);
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = trim($model->meta_country);
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = trim($model->meta_language);
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = trim($model->meta_subtitle_language);
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = trim($model->meta_content_filter);
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = trim($model->meta_imdb_rating);
            }
            if ($model->name) {
                $meta[META_TITLE] = trim($model->name);
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = trim($model->description);
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;
            $model->convert_status = 0;

            if ($model->drm_id) {
                $model->need_encryption = 1;
                $model->resource_id = 'CSM_' . $model->id;
            } else {
                $model->need_encryption = 0;
                $model->resource_id = null;
            }

            if ($model->audio_path) {
                $audio_path = [];
                $jsonDecode = json_decode($model->audio_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $audio['language_code'] = $item['language_code'];
                        $audio['original_path'] = $item['original_path'];
                        $audio['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $audio_path[] = $audio;
                    }
                    $model->audio_path = json_encode($audio_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->audio_path = null;
                }

            }
            if ($model->subtitle_path) {
                $subtitle_path = [];
                $jsonDecode = json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $subtitle['language_code'] = $item['language_code'];
                        $subtitle['original_path'] = $item['original_path'];
                        $subtitle['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $subtitle_path[] = $subtitle;
                    }
                    $model->subtitle_path = json_encode($subtitle_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->subtitle_path = null;
                }

            }

            if ($model->save()) {
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                // chỉ cho update status 3 -> 0
                //$publish_for = array_column(ApiClient::getUserAccessClients(),'id');
                // lấy danh sách các dịch vụ đã publish trước đó
                $publish_for = json_decode($old_att['published_list'], JSON_UNESCAPED_UNICODE);
                $publish_for = array_column($publish_for, 'id');
                CsmMediaPublish::updateStatus($model->id, STATUS_MEDIA_DRAFT,
                    STATUS_MEDIA_APPROVED, $publish_for);
                //delete csm-convert-task
                CsmConvertTasks::deleteItemByMediaId($model->id);
                //add to csm-media-history
                CsmMediaHistory::insertMediaHistory($model->id, json_encode($old_att, JSON_UNESCAPED_UNICODE), User::getUserId(), $publish_for, CsmMediaHistory::$STATUS_NOT_PUBLISH, $model->type);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));

                return $this->redirect(['update', 'id' => $id]);
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Update item failed"));
            }
        }
        return $this->render('reupload', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CsmMedia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = CsmMedia::SCENARIO_FILM_SERIES;
        $old_published_list = json_decode($model->published_list);
        $model->validate_media_type = [TYPE_MEDIA_FILM_SERIES];
        $values = Yii::$app->request->post();
        // process value of film_series_list change type int to int[]
        if ($values[$model->formName()]['film_series_list'] && !is_array($values[$model->formName()]['film_series_list'])) {
            $values[$model->formName()]['film_series_list'] = [$values[$model->formName()]['film_series_list']];
        }
        if ($model->load($values)) {
            $attributes = array();
            $categories = array();
            $clients = array();


            if ($model->original_path && $model->getDirtyAttributes(['original_path'])) {
                $model->original_path = UploadHelper::getFtpBasePath($model->original_path);
            }

            // fix add duration max_quantity resolution before convert
            $width = MediaHelper::getWidth($model->getFullOriginalPath());
            $height = MediaHelper::getHeight($model->getFullOriginalPath());
            $duration = MediaHelper::getDuration($model->getFullOriginalPath());

            if (!($duration > 0 && $width > 0 && $height > 0)) {
                Yii::$app->session->setFlash('error', Yii::t('backend', "Wrong duration or width or height of video"));
                $model->original_path = '';
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if ($model->image_path && $model->getDirtyAttributes(['image_path'])) {
                $model->image_path = UploadHelper::getFtpBasePath($model->image_path);
            }

            if ($model->poster_path && $model->getDirtyAttributes(['poster_path'])) {
                $model->poster_path = UploadHelper::getFtpBasePath($model->poster_path);
            }



            // update category attributes
            if (count($model->category_list_film)) {
                $categories = CsmAttribute::getCategoryFilmByIdsActive($model->category_list_film);
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update category attributes
            // update film series attributes
            if (count($model->film_series_list)) {
                $attributes = CsmMedia::processUpdateAttrFull($attributes, $model->film_series_list, $model->id,
                    $model->episode_name, trim($model->episode_no));
            }
            // done - update film series attributes
            // update actor attributes
            if (count($model->actor_list)) {
                $csmAttributes = CsmAttribute::getActorByIdsActive($model->actor_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update actor attributes
            // update director attributes
            if (count($model->director_list)) {
                $csmAttributes = CsmAttribute::getDirectorByIdsActive($model->director_list);
                foreach ($csmAttributes as $csmAttribute) {
                    $attributes[] = $csmAttribute->generateJsonObj();
                }
            }
            // done - update director attributes
            // clients
            if (count($model->client_list)) {
                $clientList = ApiClient::getByIds($model->client_list);
                foreach ($clientList as $client) {
                    /* @var ApiClient $client */
                    $clients[] = $client->generateJsonObj();
                }
            }
            // end - clients
            // update metadata
            $meta = array();
            if ($model->meta_album) {
                $meta[META_ALBUM] = trim($model->meta_album);
            }
            if ($model->meta_year) {
                $meta[META_YEAR] = trim($model->meta_year);
            }
            if ($model->meta_track) {
                $meta[META_TRACK] = trim($model->meta_track);
            }
            if ($model->meta_comment) {
                $meta[META_COMMENT] = trim($model->meta_comment);
            }
            if ($model->meta_copyright) {
                $meta[META_COPYRIGHT] = trim($model->meta_copyright);
            }
            if ($model->meta_author) {
                $meta[META_AUTHOR] = trim($model->meta_author);
            }
            if ($model->meta_country) {
                $meta[META_COUNTRY] = trim($model->meta_country);
            }
            if ($model->meta_language) {
                $meta[META_LANGUAGE] = trim($model->meta_language);
            }
            if ($model->meta_subtitle_language) {
                $meta[META_SUBTITLE_LANGUAGE] = trim($model->meta_subtitle_language);
            }
            if ($model->meta_content_filter) {
                $meta[META_CONTENT_FILTER] = trim($model->meta_content_filter);
            }
            if ($model->meta_imdb_rating) {
                $meta[META_IMDB_RATING] = trim($model->meta_imdb_rating);
            }
            if ($model->name) {
                $meta[META_TITLE] = trim($model->name);
            }
            if ($model->description) {
                $meta[META_DESCRIPTION] = trim($model->description);
            }
            $genres = array();
            if ($categories) {
                foreach ($categories as $csmAttribute) {
                    /* @var CsmAttribute $csmAttribute */
                    $genres[] = $csmAttribute->name;
                }
                $meta[META_GENRE] = implode(", ", $genres);
            }
            $model->meta_info = json_encode($meta, JSON_UNESCAPED_UNICODE);
            $model->attributes = json_encode($attributes, JSON_UNESCAPED_UNICODE);
            $model->published_list = json_encode($clients, JSON_UNESCAPED_UNICODE);
            $model->status = STATUS_MEDIA_DRAFT;
            $model->media_client_status = STATUS_MEDIA_DRAFT;

            if ($model->drm_id) {
                $model->need_encryption = 1;
                $model->resource_id = 'CSM_' . $model->id;
            } else {
                $model->need_encryption = 0;
                $model->resource_id = null;
            }


            if ($model->audio_path) {
                $audio_path = [];
                $jsonDecode = json_decode($model->audio_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $audio['language_code'] = $item['language_code'];
                        $audio['original_path'] = $item['original_path'];
                        $audio['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $audio_path[] = $audio;
                    }
                    $model->audio_path = json_encode($audio_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->audio_path = null;
                }

            }
            if ($model->subtitle_path) {
                $subtitle_path = [];
                $jsonDecode = json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE);
                if (is_array($jsonDecode) && sizeof($jsonDecode) > 0) {
                    foreach ($jsonDecode as $item) {
                        $subtitle['language_code'] = $item['language_code'];
                        $subtitle['original_path'] = $item['original_path'];
                        $subtitle['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                        $subtitle_path[] = $subtitle;
                    }
                    $model->subtitle_path = json_encode($subtitle_path, JSON_UNESCAPED_UNICODE);
                } else {
                    $model->subtitle_path = null;
                }
            }

            if ($model->save()) {
                $old_client_list = array_map(function ($item) {
                    return is_object($item) ? $item->id : $item['id'];
                }, $old_published_list);
                $diff_client_id = array_diff($old_client_list, $model->client_list);
                if ($diff_client_id) {
                    // xóa các dịch vụ không được phân phối
                    CsmMediaPublish::deleteAll(['media_id' => $id, 'client_id' => $diff_client_id]);
                }
                FileUploaded::deleteByUrlAndType($model->original_path);
                FileUploaded::deleteByUrlAndType($model->image_path);
                FileUploaded::deleteByUrlAndType($model->poster_path);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Update item successful"));
                return $this->redirect(['update', 'id' => $id]);
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Update item failed"));
                return $this->redirect('index');
            }
        }

        if ($model->audio_path) {
            $audio_path = [];
            $jsonDecode = json_decode($model->audio_path, JSON_UNESCAPED_UNICODE);
            if (is_array($jsonDecode)) {
                foreach ($jsonDecode as $item) {
                    $audio['language_code'] = $item['language_code'];
                    $audio['original_path'] = $item['original_path'];
                    $audio['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                    $audio['file_url'] = $model->getFileUrl(FILE_TYPE_MEDIA_LOCAL, $item['original_path']);
                    $audio_path[] = $audio;
                }
                $model->audio_path = json_encode($audio_path, JSON_UNESCAPED_UNICODE);
            }

        }
        if ($model->subtitle_path) {
            $subtitle_path = [];
            $jsonDecode = json_decode($model->subtitle_path, JSON_UNESCAPED_UNICODE);
            if (is_array($jsonDecode)) {
                foreach ($jsonDecode as $item) {
                    $subtitle['language_code'] = $item['language_code'];
                    $subtitle['original_path'] = $item['original_path'];
                    $subtitle['file_type'] = FILE_TYPE_MEDIA_LOCAL;
                    $subtitle['file_url'] = $model->getFileUrl(FILE_TYPE_MEDIA_LOCAL, $item['original_path']);
                    $subtitle_path[] = $subtitle;
                }
                $model->subtitle_path = json_encode($subtitle_path, JSON_UNESCAPED_UNICODE);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CsmMedia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if (CsmMedia::checkAndUpdateStatus([$id], STATUS_MEDIA_DELETED,
                [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
            ) {
                CsmScheduleDeleted::deleteMedia([$id]);
                Yii::$app->session->setFlash('success', Yii::t('backend', "Delete items successful"));
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Delete item failed! Item invalid!"));
            }
        }
        return $this->redirect(array_merge(['index'], Yii::$app->session->get(self::className() . 'queryParams')));
    }

    /**
     * Finds the CsmMedia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CsmMedia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CsmMediaFilmSeries::getCreatedById($id, TYPE_MEDIA_FILM_SERIES)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateStatus($status)
    {
        $user = Yii::$app->user->identity;
        /* @var \backend\models\User $user */
        if ($user->cp_id) {
            if ($status == 'reviewed-proposal') {
                $ids = Yii::$app->getRequest()->post('ids');
                if (CsmMedia::checkAndUpdateStatus($ids, STATUS_MEDIA_TRANSFER_SUCCESS,
                    [STATUS_MEDIA_DRAFT, STATUS_MEDIA_REJECTED], $user->cp_id)
                ) {
                    // Update status pending approved
                    CsmMediaPublish::updateStatusCp($ids, STATUS_MEDIA_APPROVED_PROPOSAL, STATUS_MEDIA_DRAFT);
                    Yii::$app->session->setFlash('success', Yii::t('backend', "Change status to need approved successful"));
                } else {
                    Yii::$app->session->setFlash('warning', Yii::t('backend', "Check status invalid!"));
                }
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('backend', "Status is invalid"));
            }
        }
        return $this->redirect(array_merge(['index'], Yii::$app->session->get(self::className() . 'queryParams')));
    }
}

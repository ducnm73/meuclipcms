<?php

namespace awesome\backend\select2Ajax;

use kartik\select2\Select2;
use yii\web\JsExpression;

class Select2Ajax extends Select2
{
    public $url;

    public $dataType = 'json';

    public $delay = 250;

    public $cache = true;

    public $minimumInputLength = 1;

    function registerAssets() {
        Select2AjaxAsset::register($this->getView());
        parent::registerAssets();
    }

    function renderWidget() {
        $this->pluginOptions['minimumInputLength'] = $this->minimumInputLength;
        $this->pluginOptions['ajax'] = [
            'url' => $this->url,
            'dataType' => $this->dataType,
            'delay' => $this->delay,
            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
            'processResults' => new JsExpression('processSelect2AjaxResults'),
            'cache' => $this->cache
        ];
        $this->pluginOptions['escapeMarkup'] = new JsExpression('function (markup) { return markup; }');
        $this->pluginOptions['templateResult'] = new JsExpression('formatSelect2Ajax');
        $this->pluginOptions['templateSelection'] = new JsExpression('formatSelect2AjaxSelection');
        parent::renderWidget();
    }
}
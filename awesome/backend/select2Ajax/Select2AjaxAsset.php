<?php


namespace awesome\backend\select2Ajax;


use awesome\backend\assets\AssetBundle;
use yii\web\View;

class Select2AjaxAsset extends AssetBundle
{
    public $jsOptions = ['position' => View::POS_HEAD];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/select2Ajax']);
        parent::init();
    }
}
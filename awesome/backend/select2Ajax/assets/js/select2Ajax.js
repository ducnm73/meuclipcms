var formatSelect2Ajax = function (item) {
    if (item.loading) {
        return item.text;
    }
    var markup =
        '<div class="row">' +
        '<div class="col-sm-8">';
    if (item.image_path) {
        markup += '<img src="' + item.image_path + '" class="img-rounded" style="width:30px" />';
    }
    if (item.name) {
        markup += '<b style="margin-left:5px">' + item.name + '</b>';
    }
    markup += '</div>' +
        '</div>';
    if (item.description) {
        markup += '<p>' + item.description + '</p>';
    }
    return '<div style="overflow:hidden;">' + markup + '</div>';
};

var formatSelect2AjaxSelection = function (item) {
    return item.name || item.text;
};

var processSelect2AjaxResults = function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
};
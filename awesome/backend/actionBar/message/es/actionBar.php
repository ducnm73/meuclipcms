<?php

return [
    'Bulk Actions' => 'Acciones',
    'Please select one or more items from the list.' => 'Seleccione uno o más elementos de la lista.',
    'General' => 'General',
    'Delete' => 'Borrar',
    'Are you sure you want to delete these items?' => '¿Está seguro de que desea eliminar estos elementos?',
    'Create New' => 'Crear nuevo',
    'Update Status' => 'Estado de actualización',
    'Approved' => 'Aprobado',
    'Reviewed Proposal' => 'Propuesta revisada',
];
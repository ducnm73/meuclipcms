<?php

return [
    'Bulk Actions' => '',
    'Please select one or more items from the list.' => '',
    'General' => '',
    'Delete' => '',
    'Are you sure you want to delete these items?' => '',
    'Create New' => '',
    'Update Status' => '',
    'Approved' => '',
    'Reviewed Proposal' => '',
];
<?php

return [
    'Bulk Actions' => 'Ações em massa',
    'Please select one or more items from the list.' => 'Selecione um ou mais itens da lista.',
    'General' => 'Em geral',
    'Delete' => 'Excluir',
    'Are you sure you want to delete these items?' => 'Tem certeza que deseja deletar estes itens?',
    'Create New' => 'Crie um novo',
    'Update Status' => 'Atualizar o status',
    'Approved' => 'Aprovado',
    'Reviewed Proposal' => 'Proposta Revisada',
];
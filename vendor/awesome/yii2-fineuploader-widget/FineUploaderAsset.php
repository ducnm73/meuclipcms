<?php

namespace awesome\fineuploader;

use yii\web\AssetBundle;

class FineUploaderAsset extends AssetBundle {

    public $sourcePath = '@vendor/awesome/yii2-fineuploader-widget/assets';
    public $basePath = '@webroot/assets';
    public $css = [
        'fine-uploader-new.css',
        'fine-uploader-updated.css',

    ];
    public $js = ['fine-uploader.js'];
    public $depends = [
        'yii\bootstrap\BootstrapAsset'
    ];

}

<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 11/19/2018
 * Time: 10:51 AM
 */

namespace awesome\fineuploader\controllers;

use yii\console\Controller;

/**
 * This is the command line version of File Upload Widget.
 * Class CombineChunkFileController
 * @package awesome\fineuploader\controllers
 */
class CombineChunkFileController extends Controller
{
    /**
     * Main action
     * @param $uuid
     * @param $redisConfig
     * @param $targetFolder
     * @param $partIndex
     * @param $totalParts
     * @param $fileSize
     */
    public function actionIndex($uuid, $redisConfig, $targetFolder, $partIndex, $totalParts, $fileSize)
    {
        $redis = \Yii::$app->$redisConfig;
        /* @var \yii\redis\Connection $redis */
        \Yii::trace("old: " . $targetFolder . DIRECTORY_SEPARATOR . $partIndex);
        \Yii::trace("new: " . $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
        \Yii::info("partIndex: " . $partIndex);
        $result = $this->checkAndMove($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize);
        \Yii::info("result: " . $result);
        if ($result) {
            $redis->set($uuid, 1 + $partIndex);
        } else {
            $redis->set($uuid, -1);
            unlink($targetFolder . DIRECTORY_SEPARATOR . $partIndex);
        }
    }

    private function checkAndMove($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current = 0)
    {
        /* @var \yii\redis\Connection $redis */
        $index = $redis->get($uuid);
        if ($index == -1) {
            return false;
        }
        if (filesize($targetFolder . DIRECTORY_SEPARATOR . $partIndex) != $fileSize) {
            \Yii::error("filesize error: current (" . filesize($targetFolder . DIRECTORY_SEPARATOR . $partIndex)
                . ') != upload (' . $fileSize . ')');
            sleep(5);
            $current++;
            if ($current <= 100) {
                return $this->checkAndMove($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current);
            } else {
                \Yii::error("result: Failed retry > 100");
                return false;
            }
        }
        if ($partIndex == 0) {
            $result = rename($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
        } else {
            \Yii::info("index: " . $index);
            if ($index == $partIndex) {
                $result = $this->moveFileIntoFile($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                    $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
            } else {
                sleep(5);
                $current++;
                if ($current <= 100) {
                    return $this->checkAndMove($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current);
                } else {
                    \Yii::error("result: Failed retry > 100");
                    return false;
                }
            }
        }
        return $result;
    }

    private function moveFileIntoFile($sourcePath, $targetPath)
    {
        try {
            $source = fopen($sourcePath, 'rb');
            $target = fopen($targetPath, 'ab');
            stream_copy_to_stream($source, $target);
            fclose($source);
            fclose($target);
            unlink($sourcePath);
            return true;
        } catch (\Exception $ex) {
            \Yii::error("moveFileIntoFile - message: " . $ex->getMessage());
            \Yii::error("moveFileIntoFile - trace: " . $ex->getTraceAsString());
            return true;
        }
    }
}
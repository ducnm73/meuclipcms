<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 11/19/2018
 * Time: 11:14 AM
 */

namespace awesome\fineuploader;

use Yii;
use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'awesome\fineuploader\controllers';

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $app->controllerMap[$this->id] = [
                'class' => 'awesome\fineuploader\controllers\CombineChunkFileController',
                'module' => $this,
            ];
        }
    }
}
<?php

namespace awesome\fineuploader;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;

/**
 * FineUpload Widget
 * @author HoangL <lehoang.k49cc@gmail.com>
 */
class FineUploader extends InputWidget
{

    /**
     * upload file to URL
     * @var string
     * @example
     * http://xxxxx/upload.php
     * ['article/upload']
     * ['upload']
     */
    public $url;

    /**
     * @var bool
     */
    public $csrf = true;

    /**
     * Enable merge file
     * @var bool
     */
    public $preMergeFile = false;

    /**
     * ID of script template
     * @var string
     */
    public $template;
    public $manualTrigger;

    /**
     *
     * @var bool
     */
    public $isBase = false;

    /**
     * renderTag
     * @var bool
     */
    public $renderTag = true;

    /**
     * fineupload js options
     * @var array
     * @example
     *    {
     *        element: document.getElementById("fineuploader-container"),
     *        request: {
     *            endpoint: "/vendor/fineuploader/php-traditional-server/endpoint.php"
     *        },
     *        deleteFile: {
     *            enabled: true,
     *            endpoint: "/vendor/fineuploader/php-traditional-server/endpoint.php"
     *        },
     *        chunking: {
     *            enabled: true,
     *            concurrent: {
     *                enabled: true
     *            },
     *            success: {
     *                endpoint: "/vendor/fineuploader/php-traditional-server/endpoint.php?done"
     *            }
     *        },
     *        resume: {
     *            enabled: true
     *        },
     *        retry: {
     *            enableAuto: true,
     *            showButton: true
     *        }
     *    }
     * @see https://docs.fineuploader.com/branch/master/api/options.html#form
     */
    public $jsOptions = [];

    /**
     * Maximum allowable concurrent requests
     * @var int
     */
    public $maxConnections = 1;

    /**
     * @var int
     * public $maxConnections = 3;
     */
    public $registerJsPos = View::POS_LOAD;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        //init var
        if (empty($this->url)) {
            throw new InvalidConfigException('Url must be set');
        }
        if (empty($this->id)) {
            $this->id = $this->hasModel() ? Html::getInputId($this->model, $this->attribute) : $this->getId();
        }
        $this->options['id'] = $this->id;
//        $this->jsOptions['element'] = 'document.getElementById("' . $this->id . '")';

        if (empty($this->name)) {
            $this->name = $this->hasModel() ? Html::getInputName($this->model, $this->attribute) : $this->id;
        }

        //register Assets
        $assets = FineUploaderAsset::register($this->view);

        $this->initOptions($assets);
        $this->initCsrfOption();

        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run()
    {

        $uploadBtnText = Yii::t('backend', 'Choose file');
        $processDropText = Yii::t('backend', 'Processing dropped files...');
        $dropFileHereText = Yii::t('backend', 'Drop files here');
        $cancelText = Yii::t('backend', 'Cancel');
        $retryText = Yii::t('backend', 'Retry');
        $deleteText = Yii::t('backend', 'Delete');
        $closeText = Yii::t('backend', 'Close');
        $yesText = Yii::t('backend', 'Yes');
        $noText = Yii::t('backend', 'No');
        $okText = Yii::t('backend', 'Ok');

        if (!$this->template && $this->isBase) {
            $scriptTemplate = <<<EOF
    <script type="text/template" id="$this->id-qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-upload-retry-selector qq-upload-retry">Retry</button>
                </li>
            </ul>
        </div>
    </script>
EOF;
            echo $scriptTemplate;
        } elseif ($this->manualTrigger) {
            $scriptTemplate = <<<EOF
<script type="text/template" id="$this->id-qq-template">

    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
        <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
            <span class="qq-upload-drop-area-text-selector"></span>
        </div>
        <div class="buttons">
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Select files</div>
            </div>
            <button type="button" id="trigger-upload-$this->id" class="btn btn-primary">
                <i class="glyphicon glyphicon-upload"></i> Upload
            </button>
        </div>
        <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
        <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>
    </div>


</script>

    <style>
        #trigger-upload-$this->id {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
            font-weight: normal;
            text-transform: none;
        }

        #$this->id .qq-upload-button {
            margin-right: 15px;
        }

        #$this->id .buttons {
            width: 36%;
        }
    </style>
EOF;
            echo $scriptTemplate;
        } elseif (!$this->template) {
            $scriptTemplate = <<<EOF
 <script type="text/template" id="$this->id-qq-template">
        <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="qq-upload-button-selector qq-upload-button">
                <div>Upload a file</div>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
             <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
            <li>
                <div class="qq-progress-bar-container-selector">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                </div>
                <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                <span class="qq-upload-file-selector qq-upload-file"></span>
                <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                <span class="qq-upload-size-selector qq-upload-size"></span>
                <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
            </li>
        </ul>
        </div>
    </script>
EOF;
            echo $scriptTemplate;
        }

        if ($this->renderTag === true) {
            echo $this->renderTag();
        }

        $this->registerScripts();
    }


//    public function run()
//    {
//
//        $uploadBtnText = Yii::t('backend', 'Choose file');
//        $processDropText = Yii::t('backend', 'Processing dropped files...');
//        $dropFileHereText = Yii::t('backend', 'Drop files here');
//        $cancelText = Yii::t('backend', 'Cancel');
//        $retryText = Yii::t('backend', 'Retry');
//        $deleteText = Yii::t('backend', 'Delete');
//        $closeText = Yii::t('backend', 'Close');
//        $yesText = Yii::t('backend', 'Yes');
//        $noText = Yii::t('backend', 'No');
//        $okText = Yii::t('backend', 'Ok');
//        echo $this->render('@backend/views/partials/fine-uploader-template',
//            [
//                'id' => $this->id,
//                'uploadBtnText' =>$uploadBtnText,
//                'processDropText' => $processDropText,
//                'dropFileHereText' => $dropFileHereText,
//                'cancelText' => $cancelText,
//                'retryText' => $retryText,
//                'deleteText' => $deleteText,
//                'closeText' => $closeText,
//                'yesText' => $yesText,
//                'noText' => $noText,
//                'okText' => $okText
//            ]);
//        $this->registerScripts();
//
//        if ($this->renderTag === true) {
//            echo $this->renderTag();
//        }
//    }
    /**
     * init Uploadify options
     * @param [] $assets
     * @return void
     */
    protected function initOptions($assets)
    {
        $baseUrl = $assets->baseUrl;
        $this->jsOptions['request']['endpoint'] = $this->url;
        // 1 thời điểm chỉ có 1 file được upload lên
        $this->jsOptions['multiple'] = false;
        //todo fix CORS
//        $this->jsOptions['cors']['expected'] = true ;
        // bỏ concurrent chungking
        $this->jsOptions['chunking']['concurrent']['enabled'] = false ;
    }

    /**
     * @return void
     */
    protected function initCsrfOption()
    {
        if (false === $this->csrf) {
            return;
        }
        $request = Yii::$app->request;
        $request->enableCsrfValidation = true;
        $csrfParam = $request->csrfParam;
        $csrfValue = $request->getCsrfToken();
        $session = Yii::$app->session;
        $session->open();

        //write csrfValue to session
        if ($request->enableCookieValidation) {
            $cookieCsrfValue = Yii::$app->getRequest()->getCookies()->getValue($csrfParam);
            if (null === $cookieCsrfValue) {
                $cookieCsrfValue = Yii::$app->getResponse()->getCookies()->getValue($csrfParam);
            }
            $session->set($csrfParam, $cookieCsrfValue);
        }

        $sessionIdName = $session->getName();
        $sessionIdValue = $session->getId();
        $params = [
            $sessionIdName => $sessionIdValue,
            $csrfParam => $csrfValue,
        ];
        if ($this->preMergeFile) {
            $params['pre-merge-file'] = 1;
        }
        $this->jsOptions = ArrayHelper::merge($this->jsOptions, [
            'request' => [
                'params' => $params
            ]
        ]);
    }

    /**
     * render file input tag
     * @return string
     */
    protected function renderTag()
    {
        return Html::fileInput($this->name, null, $this->options);
    }

    /**
     * register script
     */

    protected function registerScripts()
    {
        $this->jsOptions['element'] = new JsExpression("document.getElementById('$this->id')");
        if (!$this->template) {
            $this->jsOptions['template'] = "$this->id-qq-template";
        }
        $this->jsOptions['maxConnections'] = $this->maxConnections;

        $jsonOptions = Json::encode($this->jsOptions);
        if ($this->manualTrigger) {
            $script = <<<EOF
var uploader_$this->id = new qq.FineUploader(
        $jsonOptions
);

qq(document.getElementById("trigger-upload-$this->id")).attach("click", function() {
            if(validateMultiLanguage('$this->id'))
                uploader_$this->id.uploadStoredFiles();
        });        
EOF;
        } else {
            $script = <<<EOF
var uploader_$this->id = new qq.FineUploader(
        $jsonOptions
);
EOF;

        }

        $this->view->registerJs($script, $this->registerJsPos);

    }

}

<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 9/5/2018
 * Time: 2:59 PM
 */

namespace awesome\fineuploader;


class UploadUtils
{
    public static function setExecuteTime($time) {
//        Yii::$app->session->timeout = $time;
        set_time_limit($time);
        ini_set('max_execution_time', $time);
        ini_set('max_input_time', $time);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hoangl
 * Date: 8/22/2018
 * Time: 8:53 AM
 */

namespace awesome\fineuploader;


class UploadHandler
{
    /**
     * Field sent to server to detect enable pre merge file
     * @var bool
     */
    public $preMergeFile = null;

    /**
     * Defined redis connection configuration
     */
    public $redisConfig = null;

    /**
     *
     * Defined on common/config/bootstrap.php
     * exam: Yii::setAlias('yii-exec', dirname(dirname(__DIR__)) . '/yii');
     * @var string
     */
    public $yiiExecPath = 'yii-exec';

    public $moduleName = null;

    public $allowedExtensions = array();
    public $sizeLimit = null;
    public $inputName = 'qqfile';
    public $chunksFolder = 'chunks';
    public $mergeFile = 'm';
    public $chunksCleanupProbability = 0.001; // Once in 1000 requests on avg
    public $chunksExpireIn = 604800; // One week
    protected $uploadName;

    /**
     * Get the original filename
     */
    public function getName()
    {
        if (isset($_REQUEST['qqfilename'])) {
            return $_REQUEST['qqfilename'];
        }
        if (isset($_FILES[$this->inputName])) {
            return $_FILES[$this->inputName]['name'];
        }
    }

    /**
     * Get the name of the uploaded file
     */
    public function getUploadName()
    {
        return $this->uploadName;
    }

    public function combineChunks($uploadDirectory, $name = null)
    {
        $start_time_handle = $this->getCurrentTime();
        $uuid = $_POST['qquuid'];
        if ($name === null) {
            $name = $this->getName();
        }
        $this->uploadName = $this->getName();
        $targetFolder = $this->chunksFolder . DIRECTORY_SEPARATOR . $uuid;
        $totalParts = isset($_REQUEST['qqtotalparts']) ? (int)$_REQUEST['qqtotalparts'] : 1;
        $targetPath = join(DIRECTORY_SEPARATOR, array($uploadDirectory, $name));
        if (!file_exists($targetPath)) {
            mkdir(dirname($targetPath), 0777, true);
        }
        if (isset($_REQUEST[$this->preMergeFile]) && $this->redisConfig && $this->moduleName) {
            $redisConfig1 = $this->redisConfig;
            $redis = \Yii::$app->$redisConfig1;
            // redis - qquuid - chunk/qquuid - total - E:/KHONOIDUNG/cms/backend/web/uploads/media2/videos/random_path
            if (!$this->checkAndMove($redis, $uuid, $targetFolder, $totalParts, $targetPath)) {
                // combine error
                $end_time_handle = $this->getCurrentTime();
                \Yii::info('3001|ERROR|combineChunks|checkAndMove false|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                return array("success" => false, "error" => '3001|upload fail', "uuid" => $uuid, "preventRetry" => true);
            }
        } else {
            $target = fopen($targetPath, 'wb');
            for ($i = 0; $i < $totalParts; $i++) {
                $chunk = fopen($targetFolder . DIRECTORY_SEPARATOR . $i, "rb");
                stream_copy_to_stream($chunk, $target);
                fclose($chunk);
            }
            // Success
            fclose($target);
            for ($i = 0; $i < $totalParts; $i++) {
                unlink($targetFolder . DIRECTORY_SEPARATOR . $i);
            }
        }
        rmdir($targetFolder);
        if (!is_null($this->sizeLimit) && filesize($targetPath) > $this->sizeLimit) {
            unlink($targetPath);
            http_response_code(413);
            $end_time_handle = $this->getCurrentTime();
            \Yii::info('3002|ERROR|combineChunks|file size bigger limit|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
            return array("success" => false, "error" => '3002|upload fail', "uuid" => $uuid, "preventRetry" => true);
        }

        $end_time_handle = $this->getCurrentTime();
        \Yii::info('2000|SUCCESS|combineChunks|ok|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
        return array("success" => true, "uuid" => $uuid, 'filePath' => $name);
    }

    /**
     * Process the upload.
     * @param string $uploadDirectory Target directory.
     * @param string $name Overwrites the name of the file.
     */

    public function logChunkInfo()
    {
        try {
            if (isset($_REQUEST['qquuid'])) {
                \Yii::info('qquuid = ' . $_REQUEST['qquuid'], 'debug');
            }
            if (isset($_REQUEST['qqpartindex'])) {
                \Yii::info('qqpartindex = ' . $_REQUEST['qqpartindex'], 'debug');
            }
            if (isset($_REQUEST['qqpartbyteoffset'])) {
                \Yii::info('qqpartbyteoffset = ' . $_REQUEST['qqpartbyteoffset'], 'debug');
            }
            if (isset($_REQUEST['qqtotalfilesize'])) {
                \Yii::info('qqtotalfilesize = ' . $_REQUEST['qqtotalfilesize'], 'debug');
            }
            if (isset($_REQUEST['qqtotalparts'])) {
                \Yii::info('qqtotalparts = ' . $_REQUEST['qqtotalparts'], 'debug');
            }
            if (isset($_REQUEST['qqfilename'])) {
                \Yii::info('qqfilename = ' . $_REQUEST['qqfilename'], 'debug');
            }
            if (isset($_REQUEST['qqchunksize'])) {
                \Yii::info('qqchunksize = ' . $_REQUEST['qqchunksize'], 'debug');
            }
        } catch (\Exception $e) {
            \Yii::info('logChunkInfo ERROR', 'debug');
        };
    }

    public function getFileInfo()
    {
        $info = '';
        if (isset($_REQUEST['qquuid'])) {
            $info = $info . 'qquuid=' . $_REQUEST['qquuid'] . '|';
        }
        if (isset($_REQUEST['qqpartindex'])) {
            $info = $info . 'qqpartindex=' . $_REQUEST['qqpartindex'] . '|';
        }
        if (isset($_REQUEST['qqpartbyteoffset'])) {
            $info = $info . 'qqpartbyteoffset=' . $_REQUEST['qqpartbyteoffset'] . '|';
        }
        if (isset($_REQUEST['qqtotalfilesize'])) {
            $info = $info . 'qqtotalfilesize=' . $_REQUEST['qqtotalfilesize'] . '|';
        }
        if (isset($_REQUEST['qqtotalparts'])) {
            $info = $info . 'qqtotalparts=' . $_REQUEST['qqtotalparts'] . '|';
        }
        if (isset($_REQUEST['qqfilename'])) {
            $info = $info . 'qqfilename=' . $_REQUEST['qqfilename'] . '|';
        }
        if (isset($_REQUEST['qqchunksize'])) {
            $info = $info . 'qqchunksize=' . $_REQUEST['qqchunksize'] . '|';
        }
        return $info;
    }

    public function getCurrentTime()
    {
        return date('Y/m/d H:i:s');
    }

    public function handleUpload($uploadDirectory, $name = null)
    {
        // $uploadDirectory = E:/KHONOIDUNG/cms/backend/web/uploads/media2/videos
        $start_time_handle = $this->getCurrentTime();
        // xóa các thư mục tạm trong thư mục chunk mà quá 1 tuần
        if (is_writable($this->chunksFolder) &&
            1 == mt_rand(1, 1 / $this->chunksCleanupProbability)) {
            // Run garbage collection
            \Yii::info('CLEAN UP CHUNK', 'debug');
            $this->cleanupChunks();
        }
        // Check that the max upload size specified in class configuration does not
        // exceed size allowed by server config
//        if ($this->toBytes(ini_get('post_max_size')) < $this->sizeLimit ||
//            $this->toBytes(ini_get('upload_max_filesize')) < $this->sizeLimit) {
//            $neededRequestSize = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
//            return array('error' => "Server error. Increase post_max_size and upload_max_filesize to " . $neededRequestSize);
//        }
//        if ($this->isInaccessible($uploadDirectory)) {
//            return array('error' => "Server error. Uploads directory '$uploadDirectory' isn't writable");
//        }
        $type = $_SERVER['CONTENT_TYPE'];
        if (isset($_SERVER['HTTP_CONTENT_TYPE'])) {
            $type = $_SERVER['HTTP_CONTENT_TYPE'];
        }
        if (!isset($type)) {
            return array('error' => "No files were uploaded.");
        }
//        else {
////            if (strpos(strtolower($type), 'multipart/') !== 0) {
////                return array('error' => "Server error. Not a multipart request. Please set forceMultipart to default value (true)." . $type);
////            }
////        }
        // Get size and name
        // $file = qqfile
        $file = $_FILES[$this->inputName];
        $size = $file['size'];
        if (isset($_REQUEST['qqtotalfilesize'])) {
            $size = $_REQUEST['qqtotalfilesize'];
        }
        if ($name === null) {
            $name = $this->getName();
        }
        // check file error
        if ($file['error']) {
            \Yii::info('3003|ERROR|handleUpload|file error: ' . $file['error'], 'debug');
            return array('error' => 'Upload Error, message: ' . $file['error']);
        }

        // Validate name
        if ($name === null || $name === '') {
            return array('error' => 'File name empty.');
        }
        // Validate file size
        if ($size == 0) {
            return array('error' => 'File is empty.');
        }
        if (!is_null($this->sizeLimit) && $size > $this->sizeLimit) {
            \Yii::info('3004|ERROR|handleUpload|File too large|' . $this->getFileInfo(), 'debug');
            return array('error' => 'File is too large.', 'preventRetry' => true);
        }
        // Validate file extension
        $pathinfo = pathinfo($name);
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
        if ($this->allowedExtensions && !in_array(strtolower($ext),
                array_map("strtolower", $this->allowedExtensions))) {
            \Yii::info('3005|ERROR|handleUpload|file invalid extension|' . $this->getFileInfo(), 'debug');
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of ' . $these . '.');
        }
        // Save a chunk

        $totalParts = isset($_REQUEST['qqtotalparts']) ? (int)$_REQUEST['qqtotalparts'] : 1;
        $uuid = $_REQUEST['qquuid'];
        if ($totalParts > 1) {
            # chunked upload
            $partIndex = (int)$_REQUEST['qqpartindex'];
            $targetFolder = $this->chunksFolder . DIRECTORY_SEPARATOR . $uuid;

            if (!file_exists($targetFolder)) {
                mkdir($targetFolder, 0777, true);
            }
            // từng chunk part lưu tạm ở đây
            $target = $targetFolder . DIRECTORY_SEPARATOR . $partIndex;
            // kích thước file trong thư mục php tmp
            $fileSize = filesize($file['tmp_name']);

            // move file từ thư mục tmp php sang path file tạm, $target = chunk/qquuid/index
            $success = move_uploaded_file($file['tmp_name'], $target);
            if (!$success) {
                $end_time_handle = $this->getCurrentTime();
                \Yii::info('3006|ERROR|handleUpload|move_uploaded_file from php tmp false|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                return array('error' => '3006|Move chunk part file fail! Please try again', "uuid" => $uuid);
            }
            if ($success && isset($_REQUEST[$this->preMergeFile]) && $this->redisConfig && $this->moduleName) {
                $redisConfig1 = $this->redisConfig;
                $redis = \Yii::$app->$redisConfig1;

                //cuongtt remove loop and add clearstatcache
                //redis - qquuid - chunk path lưu tạm ở thư mục chunk, path chưa có gồm index - chỉ số chunk part - qqtotalparts - kích thước file trong thư mục tmp php
                if ($this->MovePart($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize)) {
//                    $result = 1 + $partIndex ;
//                    \Yii::info('XXXXX redis set next part = ' . $result, 'debug');
                    $result = $redis->set($uuid, 1 + $partIndex);
                    if($result != '1'){
                        \Yii::info('3015|ERROR|REDIS|Cannot set to redis|' . $result . '|' . $this->getFileInfo(), 'debug');
                    }
                    $end_time_handle = $this->getCurrentTime();
                    \Yii::info('2001|SUCCESS|handleUpload|checkAndMovePart ok|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                    return array("success" => true, "uuid" => $uuid);
                } else {
                    $end_time_handle = $this->getCurrentTime();
                    \Yii::info('3007|ERROR|handleUpload|checkAndMovePart false|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                    //$redis->set($uuid, -1);
                    //unlink($targetFolder . DIRECTORY_SEPARATOR . $partIndex);
                    return array('error' => '3007|Upload load failed! Please try again', "uuid" => $uuid);
                }


//                $file = '@' . $this->yiiExecPath;
//                $command = '"' . \Yii::getAlias($file) . '" ' . $this->moduleName . ' ' .
//                    $uuid . ' ' . $this->redisConfig . ' "' . $targetFolder . '" ' .
//                    $partIndex . ' ' . $totalParts . ' ' . $fileSize;
//                \Yii::info("ConsoleRunner: " . $command);
//                if ($this->isWindows() === true) {
//                    pclose(popen('start /B cmd /C "php ' . $command . ' >NUL 2>NUL"', 'r'));
//                } else {
//                    pclose(popen(PHP_BINDIR . '/php ' . $command . ' > /dev/null &', 'r'));
//                }
            }
//            return array("success" => true, "uuid" => $uuid);
        } else {
            # non-chunked upload
            $target = join(DIRECTORY_SEPARATOR, array($uploadDirectory, $name));
            if ($target) {
                $this->uploadName = basename($target);
                if (!is_dir(dirname($target))) {
                    mkdir(dirname($target), 0777, true);
                }
                if (move_uploaded_file($file['tmp_name'], $target)) {
                    $end_time_handle = $this->getCurrentTime();
                    \Yii::info('2002|SUCCESS|handleUpload|move_uploaded_file ok|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                    return array('success' => true, 'uuid' => $uuid, 'filePath' => $name);
                } else {
                    $end_time_handle = $this->getCurrentTime();
                    \Yii::info('3008|ERROR|handleUpload|move_uploaded_file false|' . $this->getFileInfo() . $start_time_handle . '|' . $end_time_handle, 'debug');
                }
            }
            return array(
                'error' => 'Could not save uploaded file. "' . $target . '" ' .
                    'The upload was cancelled, or server error encountered'
            );
        }
    }

    // todo remove this stupid func
    private function checkAndMovePart($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current = 0)
    {
        /* @var \yii\redis\Connection $redis */

        $index = $redis->get($uuid);
        if ($index == -1) {
            \Yii::info('3009|ERROR|checkAndMovePart|get qquuid = -1|' . $this->getFileInfo(), 'debug');
            return false;
        }

        // kiểm tra đã move xong chưa ???
        // targetFolder = chunk/qquuid

        if (filesize($targetFolder . DIRECTORY_SEPARATOR . $partIndex) != $fileSize) {
            \Yii::error("filesize error: current (" . filesize($targetFolder . DIRECTORY_SEPARATOR . $partIndex)
                . ') != upload (' . $fileSize . ')');
            \Yii::info('3010|ERROR|checkAndMovePart|check file size error|' . $this->getFileInfo(), 'debug');
            sleep(5);
            $current++;
            if ($current <= 100) {
                return $this->checkAndMovePart($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current);
            } else {
                \Yii::info('3011|ERROR|checkAndMovePart|Failed retry 100|' . $this->getFileInfo(), 'debug');
                \Yii::error("result: Failed retry > 100");
                return false;
            }
        }
        // nếu đã move xong, merge chunk/qquuid/index -> chunk/qquuid/total.
        \Yii::info('partIndex = ' .$partIndex . '| index get from redis =' . $index, 'debug') ;
        if ($partIndex == 0) {
            $result = rename($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
        } else {
            if ($index == $partIndex) {
                $result = $this->moveFileIntoFile($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                    $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
            } else {
                sleep(5);
                $current++;
                if ($current <= 100) {
                    return $this->checkAndMovePart($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current);
                } else {
                    \Yii::info('3012|ERROR|checkAndMovePart|Failed retry 100|' . $this->getFileInfo(), 'debug');
                    \Yii::error("result: Failed retry > 100");
                    return false;
                }
            }
        }
        return $result;
    }


    private function MovePart($redis, $uuid, $targetFolder, $partIndex, $totalParts, $fileSize, $current = 0)
    {
        /* @var \yii\redis\Connection $redis */
        $index = $redis->get($uuid);
        if ($index == -1) {
            \Yii::info('3009|ERROR|MovePart|get qquuid = -1|' . $this->getFileInfo(), 'debug');
            return false;
        }

//        clearstatcache();
//        if (filesize($targetFolder . DIRECTORY_SEPARATOR . $partIndex) != $fileSize) {
//            \Yii::info('3010|ERROR|MovePart|check file size error|' . $this->getFileInfo(), 'debug');
//            return false ;
//        }

        // nếu đã move xong, merge chunk/qquuid/index -> chunk/qquuid/total.
        if ($partIndex == 0) {
            $result = rename($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
        } else {
//            \Yii::info('XXXXX current part = ' . $partIndex . '|index get from redis=' . $index, 'debug');
            if ($index == $partIndex) {
                $result = $this->moveFileIntoFile($targetFolder . DIRECTORY_SEPARATOR . $partIndex,
                    $targetFolder . DIRECTORY_SEPARATOR . $totalParts);
            } else {
                \Yii::info('3012|ERROR|REDIS next chunk part invalid' . $this->getFileInfo(), 'debug');
                return false;
            }
        }
        return $result;
    }

    private function moveFileIntoFile($sourcePath, $targetPath)
    {
        try {
            $source = fopen($sourcePath, 'rb');
            $target = fopen($targetPath, 'ab');
            stream_copy_to_stream($source, $target);
            fclose($source);
            fclose($target);
            unlink($sourcePath);
            return true;
        } catch (\Exception $ex) {
            \Yii::error("moveFileIntoFile - message: " . $ex->getMessage());
            \Yii::error("moveFileIntoFile - trace: " . $ex->getTraceAsString());
            \Yii::info('3013|ERROR|moveFileIntoFile|catch exception|' . $this->getFileInfo(), 'debug');
            return true;
        }
    }


    // redis - qquuid - chunk/qquuid - total - E:/KHONOIDUNG/cms/backend/web/uploads/media2/videos/random_path
    private function checkAndMove($redis, $uuid, $targetFolder, $totalParts, $targetPath, $retry = 0)
    {
        /* @var \yii\redis\Connection $redis */
        $index = $redis->get($uuid);
        if ($index == $totalParts) {
            rename($targetFolder . DIRECTORY_SEPARATOR . $totalParts, $targetPath);
            $redis->del($uuid);
            unlink($targetFolder);
            return true;
        } else if ($retry >= 100) {
            $redis->del($uuid);
            \Yii::info('3014|ERROR|checkAndMove|Failed retry 100|' . $this->getFileInfo(), 'debug');
            return false;
        } else {
            sleep(5);
            $this->checkAndMove($redis, $uuid, $targetFolder, $totalParts, $targetPath, $retry + 1);
        }
    }

    /**
     * Process a delete.
     * @param string $uploadDirectory Target directory.
     * @params string $name Overwrites the name of the file.
     *
     */
    public function handleDelete($uploadDirectory, $name = null)
    {
        if ($this->isInaccessible($uploadDirectory)) {
            return array('error' => "Server error. Uploads directory isn't writable" . ((!$this->isWindows()) ? " or executable." : "."));
        }
        $targetFolder = $uploadDirectory;
        $uuid = false;
        $method = $_SERVER["REQUEST_METHOD"];
        if ($method == "DELETE") {
            $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $tokens = explode('/', $url);
            $uuid = $tokens[sizeof($tokens) - 1];
        } else {
            if ($method == "POST") {
                $uuid = $_REQUEST['qquuid'];
            } else {
                return array(
                    "success" => false,
                    "error" => "Invalid request method! " . $method
                );
            }
        }
        $target = join(DIRECTORY_SEPARATOR, array($targetFolder, $uuid));
        if (is_dir($target)) {
            $this->removeDir($target);
            return array("success" => true, "uuid" => $uuid);
        } else {
            return array(
                "success" => false,
                "error" => "File not found! Unable to delete." . $url,
                "path" => $uuid
            );
        }
    }

    /**
     * Returns a path to use with this upload. Check that the name does not exist,
     * and appends a suffix otherwise.
     * @param string $uploadDirectory Target directory
     * @param string $filename The name of the file to use.
     */
    protected function getUniqueTargetPath($uploadDirectory, $filename)
    {
        // Allow only one process at the time to get a unique file name, otherwise
        // if multiple people would upload a file with the same name at the same time
        // only the latest would be saved.
        if (function_exists('sem_acquire')) {
            $lock = sem_get(ftok(__FILE__, 'u'));
            sem_acquire($lock);
        }
        $pathinfo = pathinfo($filename);
        $base = $pathinfo['filename'];
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
        $ext = $ext == '' ? $ext : '.' . $ext;
        $unique = $base;
        $suffix = 0;
        // Get unique file name for the file, by appending random suffix.
        while (file_exists($uploadDirectory . DIRECTORY_SEPARATOR . $unique . $ext)) {
            $suffix += rand(1, 999);
            $unique = $base . '-' . $suffix;
        }
        $result = $uploadDirectory . DIRECTORY_SEPARATOR . $unique . $ext;
        // Create an empty target file
        if (!touch($result)) {
            // Failed
            $result = false;
        }
        if (function_exists('sem_acquire')) {
            sem_release($lock);
        }
        return $result;
    }

    /**
     * Deletes all file parts in the chunks folder for files uploaded
     * more than chunksExpireIn seconds ago
     */
    protected function cleanupChunks()
    {
        foreach (scandir($this->chunksFolder) as $item) {
            if ($item == "." || $item == "..") {
                continue;
            }
            $path = $this->chunksFolder . DIRECTORY_SEPARATOR . $item;
            if (!is_dir($path)) {
                continue;
            }
            if (time() - filemtime($path) > $this->chunksExpireIn) {
                $this->removeDir($path);
            }
        }
    }

    /**
     * Removes a directory and all files contained inside
     * @param string $dir
     */
    protected function removeDir($dir)
    {
        foreach (scandir($dir) as $item) {
            if ($item == "." || $item == "..") {
                continue;
            }
            if (is_dir($item)) {
                $this->removeDir($item);
            } else {
                unlink(join(DIRECTORY_SEPARATOR, array($dir, $item)));
            }
        }
        rmdir($dir);
    }

    /**
     * Converts a given size with units to bytes.
     * @param string $str
     */
    protected function toBytes($str)
    {
        $str = trim($str);
        $last = strtolower($str[strlen($str) - 1]);
        if (is_numeric($last)) {
            $val = (int)$str;
        } else {
            $val = (int)substr($str, 0, -1);
        }
        switch ($last) {
            case 'g':
            case 'G':
                $val *= 1024;
            case 'm':
            case 'M':
                $val *= 1024;
            case 'k':
            case 'K':
                $val *= 1024;
        }
        return $val;
    }

    /**
     * Determines whether a directory can be accessed.
     *
     * is_executable() is not reliable on Windows prior PHP 5.0.0
     *  (http://www.php.net/manual/en/function.is-executable.php)
     * The following tests if the current OS is Windows and if so, merely
     * checks if the folder is writable;
     * otherwise, it checks additionally for executable status (like before).
     *
     * @param string $directory The target directory to test access
     */
    protected function isInaccessible($directory)
    {
        $isWin = $this->isWindows();
        $folderInaccessible = ($isWin) ? !is_writable($directory) : (!is_writable($directory) && !is_executable($directory));
        return $folderInaccessible;
    }

    /**
     * Determines is the OS is Windows or not
     *
     * @return boolean
     */
    protected function isWindows()
    {
        $isWin = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');
        return $isWin;
    }
}
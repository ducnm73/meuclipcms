<?php

namespace awesome\fineuploader;

use Closure;
use Yii;
use yii\base\Action;
use yii\helpers\VarDumper;
use yii\validators\FileValidator;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\base\Exception;

/**
 * @author HoangL <lehoang.k49cc@gmail.com>
 */
class UploadAction extends Action
{

    /**
     * SavePath
     * @var string
     */
    public $basePath = '@webroot/upload';

    /**
     * WebUrl
     * @var string
     */
    public $baseUrl = '@web/upload';

    /**
     * @var bool
     */
    public $enableCsrf = true;

    /**
     * Time for excute upload: 30 minutes * 60 = 1800
     * @var int
     */
    public $timeout = 3600;

    /**
     * @var array | Closure
     * @example
     * // [$object, 'methodName']
     * // function(){}
     */
    public $format;

    /**
     * file validator options
     * @var []
     * @see http://stuff.cebe.cc/yii2docs/yii-validators-filevalidator.html
     * @example
     * [
     * 'maxSize' => 1000,
     * 'extensions' => ['jpg', 'png']
     * ]
     */
    public $validateOptions;

    /**
     * @var string
     */
    public $postFieldName = 'qqfile';

    /**
     * If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
     * @var string
     */
    public $chunksFolder = "chunks";

    /**
     * Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
     * For example: endpoint.php?done
     * @var string
     */
    public $chunksGetParams = "done";

    /**
     * Defined redis connection configuration
     */
    public $redisConfig = null;

    /**
     * Save Relative File Name
     * image/yyyymmdd/xxx.jpg
     * @var string
     */
    public $filename;

    /**
     * @var bool
     */
    public $overwriteIfExist = false;

    /**
     * @var int
     */
    public $fileChmod = 0644;

    /**
     * @var int
     */
    public $dirChmod = 0755;

    /**
     * throw yii\base\Exception will break
     * @var Closure
     * beforeSave($UploadAction)
     */
    public $beforeSave;

    /**
     * throw yii\base\Exception will break
     * @var Closure
     * afterSave($filename, $fullFilename, $UploadAction)
     */
    public $afterSave;

    /**
     * OutputBuffer
     * @var []
     */
    public $output = ['error' => false];

    /**
     * Field sent to server to detect enable pre merge file
     * @var string
     */
    private $preMergeFile = 'pre-merge-file';

    public $moduleName = 'module-name';

    public function init()
    {
        $this->initCsrf();

        if (empty($this->basePath)) {
            throw new Exception('basePath not exist');
        }
        // basePath = E:/KHONOIDUNG/cms/backend/web/uploads/media2/videos
        $this->basePath = Yii::getAlias($this->basePath);
        if (empty($this->baseUrl)) {
            throw new Exception('baseUrl not exist');
        }
        $this->baseUrl = Yii::getAlias($this->baseUrl);
        // baseUrl = http://192.168.146.113:9020/uploads

        if (false === is_callable($this->format) && false === is_array($this->format)) {
            throw new Exception('format is invalid');
        }

        return parent::init();
    }

    /**
     * Get the original filename
     */
    public function getName()
    {
        if (isset($_REQUEST['qqfilename'])) {
            return $_REQUEST['qqfilename'];
        }
        if (isset($_FILES[$this->postFieldName])) {
            return $_FILES[$this->postFieldName]['name'];
        }
        return null;
    }

    public function getExtension()
    {
        $ext = $this->getName() ? strtolower(pathinfo($this->getName(), PATHINFO_EXTENSION)) : "";
        return $ext;
    }

    public function run()
    {
        try {
            UploadUtils::setExecuteTime($this->timeout);

            $uploader = new UploadHandler();
            // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
            // all files types allowed by default
            if (is_array($this->validateOptions) && isset($this->validateOptions['extensions'])) {
                $uploader->allowedExtensions = $this->validateOptions['extensions'];
            } else {
                $uploader->allowedExtensions = array();
            }
            // Specify max file size in bytes.
            if (is_array($this->validateOptions) && isset($this->validateOptions['maxSize'])) {
                $uploader->sizeLimit = $this->validateOptions['maxSize'];
            } else {
                $uploader->sizeLimit = null;
            }
            //qqfile
            $uploader->inputName = $this->postFieldName;
            //pre-merge-file
            $uploader->preMergeFile = $this->preMergeFile;
            //uploadChunks
            $uploader->redisConfig = $this->redisConfig;
            //moduleName = fineuploader
            $uploader->moduleName = $this->moduleName;

            // chunksFolder = E:/KHONOIDUNG/cms/backend/web/uploads/media2/videos/chunks
            $uploader->chunksFolder = $this->basePath . '/' . $this->chunksFolder;

            if (null !== $this->beforeSave) {
                call_user_func($this->beforeSave, $this);
            }

            // Process finish chunk upload. For example: endpoint.php?done
            $file_name_t = $this->getFilename();
            if (isset($_GET[$this->chunksGetParams])) {
                $result = $uploader->combineChunks($this->basePath, $file_name_t);
            } else {
                // Handles upload requests
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()

                $result = $uploader->handleUpload($this->basePath, $file_name_t);
            }
            if (isset($result['success']) && $result['success']) {
                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $uploader->getUploadName();
            }
            $this->output = $result;
            if ($this->afterSave !== null) {
                call_user_func($this->afterSave, $this);
            }
        } catch (Exception $e) {
            $this->output = array('error' => $e->getMessage());
        }
        Yii::$app->response->format = 'json';
        return $this->output;
    }


    /**
     * @return string
     * @throws Exception
     */
    protected function getSaveFileName()
    {
        return call_user_func($this->format, $this);
    }

    protected function initCsrf()
    {
        $session = Yii::$app->getSession();
        $request = Yii::$app->getRequest();
        if (false === $this->enableCsrf) {
            return;
        }
        $request->enableCsrfValidation = true;
        $request->enableCsrfCookie = false; //verify with session
        $sessionName = $session->getName();
        $postSessionId = $request->post($sessionName);
        if ($postSessionId != $session->getId()) {
            $session->destroy();
            $session->setId($postSessionId);
            $session->open();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getFilename()
    {
        if (null === $this->filename) {
            $this->filename = $this->getSaveFileName();
        }
        return $this->filename;
    }

    /**
     * @return string
     */
    public function getSavePath()
    {
        return rtrim($this->basePath, '\\/') . '/' . $this->filename;
    }

    /**
     * @return string
     */
    public function getWebUrl()
    {
        return rtrim($this->baseUrl, '\\/') . '/' . $this->filename;
    }
}
